onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lab8_check_tb/DUT/CPU/clk
add wave -noupdate /lab8_check_tb/LEDR
add wave -noupdate /lab8_check_tb/DUT/CPU/program_counter/out
add wave -noupdate /lab8_check_tb/DUT/CPU/struct_reg/out
add wave -noupdate /lab8_check_tb/DUT/CPU/state_machine/next
add wave -noupdate /lab8_check_tb/DUT/CPU/state_machine/reset
add wave -noupdate -divider REGFILE
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R0
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R1
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R2
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R3
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R4
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R5
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R6
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R7
add wave -noupdate -divider REGISTERS
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/rA/out
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/rB/out
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/rC/out
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/status/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 270
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {11216336 ps} {11217146 ps}
