module regfile(data_in, writenum, write, readnum, clk, data_out);
  input [15:0] data_in;
  input [2:0] writenum, readnum;
  input write, clk;
  output [15:0] data_out ;
  wire [15:0] R0, R1, R2, R3, R4, R5, R6, R7;
  wire [7:0] writenum_OHval, writenum_OH ;

  decoder #(3, 8) dec_write (writenum, writenum_OHval) ; //Instantiate the decoder to convert binary to one hot values.

  assign writenum_OH = write ? writenum_OHval : 8'b0 ; //MUX to decide whether to assign a write value to any of the registers.

  //Instantiate the eight registers to store 16 bit information.
  register regR0 (data_in, writenum_OH[0], clk, R0) ;
  register regR1 (data_in, writenum_OH[1], clk, R1) ;
  register regR2 (data_in, writenum_OH[2], clk, R2) ;
  register regR3 (data_in, writenum_OH[3], clk, R3) ;
  register regR4 (data_in, writenum_OH[4], clk, R4) ;
  register regR5 (data_in, writenum_OH[5], clk, R5) ;
  register regR6 (data_in, writenum_OH[6], clk, R6) ;
  register regR7 (data_in, writenum_OH[7], clk, R7) ;

  Muxb8 #(16) mux_out(R7, R6, R5, R4, R3, R2, R1, R0, readnum, data_out) ; //Use a 8 channel MUX to choose the value to read.

endmodule

module decoder (a,b) ; //Decoder module as provided in the slide notes.
  parameter n=2 ;
  parameter m=4 ;

  input [n-1:0] a ;
  output [m-1:0] b ;

  wire [m-1:0] b = 1 << a ;
endmodule

module register (data_in, write, clk, out) ; //Modified register that only saves the file if write is true.
  parameter n = 16;
  input [n-1:0] data_in ;
  input write , clk;
  output [n-1:0] out ;
  reg [n-1:0] out  = 'b0; //{n,{1'b0}} ;

  always @(posedge clk) begin
    if(write == 1'b1) out <= data_in ;
  end
endmodule

module Mux8 (a7, a6, a5, a4, a3, a2, a1, a0, s, b) ; //8 Channel MUX provided by the slide notes.
  parameter k = 1;
  input [k-1:0] a0, a1, a2, a3, a4, a5, a6, a7 ;
  input [7:0] s ;
  output [k-1:0] b;
  reg [k-1:0] b;

  always @(*) begin //Choose between the different one hot values.
    case(s)
      8'b00000001: b = a0 ;
      8'b00000010: b = a1 ;
      8'b00000100: b = a2 ;
      8'b00001000: b = a3 ;
      8'b00010000: b = a4 ;
      8'b00100000: b = a5 ;
      8'b01000000: b = a6 ;
      8'b10000000: b = a7 ;
      default: b = {k{1'bx}} ;
    endcase
  end
endmodule

module Muxb8 (a7, a6, a5, a4, a3, a2, a1, a0, sb, b) ; //This 8 channel MUX works with a binary input instead of onehot.
  parameter k = 1;
  input [k-1:0] a0, a1, a2, a3, a4, a5, a6, a7 ;
  input [2:0] sb ;
  output [k-1:0] b;
  wire [7:0] s ;

  decoder #(3, 8) d(sb, s) ; //Convert the binary numbers to one hot so they can be used by a regular multiplexer.
  Mux8 #(k) m (a7, a6, a5, a4, a3, a2, a1, a0, s, b) ; //Istantiate the regular MUX to decide which value.
endmodule