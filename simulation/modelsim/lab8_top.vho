-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 15.0.0 Build 145 04/22/2015 SJ Web Edition"

-- DATE "11/04/2018 20:04:08"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	lab8_top IS
    PORT (
	KEY : IN std_logic_vector(3 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0);
	HEX4 : OUT std_logic_vector(6 DOWNTO 0);
	HEX5 : OUT std_logic_vector(6 DOWNTO 0);
	CLOCK_50 : IN std_logic
	);
END lab8_top;

-- Design Ports Information
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_AD10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_AE12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_AE26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_AE27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_AE28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_AG27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_AF28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_AG28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_AH28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_AJ29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_AH29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_AH30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_AG30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_AF29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_AF30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_AD27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_AE29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_AD29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_AD30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_AC29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_AC30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_AD26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_AC27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_AD25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_AC25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_AB25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[0]	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[1]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[2]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[3]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[4]	=>  Location: PIN_W24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[5]	=>  Location: PIN_V23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[6]	=>  Location: PIN_W25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[0]	=>  Location: PIN_V25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[1]	=>  Location: PIN_AA28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[2]	=>  Location: PIN_Y27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[3]	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[4]	=>  Location: PIN_AB26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[5]	=>  Location: PIN_AA26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[6]	=>  Location: PIN_AA25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AD11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AF10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AF9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AC12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_AC9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AE11,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF lab8_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX4 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX5 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(39 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(39 DOWNTO 0);
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \CPU|struct_reg|out[14]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor26~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor17~combout\ : std_logic;
SIGNAL \CPU|struct_reg|out[13]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor13~0_combout\ : std_logic;
SIGNAL \CPU|struct_reg|out[11]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor12~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor12~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor5~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor13~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor5~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor16~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~3_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor16~0_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|Equal0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor20~0_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[4]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~21_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor26~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr3~2_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor29~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor28~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor28~2_combout\ : std_logic;
SIGNAL \CPU|FSM|psel[2]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~4_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~29_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~5_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr29~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor6~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor6~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~14_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr3~3_combout\ : std_logic;
SIGNAL \CPU|FSM|state_next_reset[3]~3_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor15~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~17_combout\ : std_logic;
SIGNAL \CPU|FSM|loads~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor19~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor19~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~16_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr3~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor42~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor42~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor43~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor43~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor47~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor44~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor31~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~13_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor47~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~3_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr9~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor6~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~15_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr11~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr16~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor21~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor20~combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|c[1]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|bsel~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr20~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr9~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~11_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~10_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~12_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr39~3_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \CPU|DP|Ain[4]~4_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[4]~1_combout\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \CPU|DP|rB|out[2]~DUPLICATE_q\ : std_logic;
SIGNAL \Equal5~1_combout\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \CPU|FSM|always0~26_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr26~combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[1]~15_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr11~combout\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \CPU|DP|U3|b[0]~16_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[0]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[0]~feeder_combout\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \CPU|DP|U3|b[5]~11_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|out~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr31~combout\ : std_logic;
SIGNAL \CPU|FSM|nsel[2]~0_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|out[2]~5_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|out~0_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|out[0]~3_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|out~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[5]~7_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[6]~6_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[4]~5_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector10~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[0]~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[5]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[5]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[3]~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[2]~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector10~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector10~2_combout\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \CPU|DP|U3|b~0_combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[6]~10_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector9~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[6]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector9~5_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|shift[1]~0_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~2_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr20~combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[13]~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[13]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[13]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[13]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector2~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector2~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[12]~5_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[14]~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[15]~18_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux0~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[4]~12_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[4]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector11~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector11~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector11~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[5]~12_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux0~1_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a15\ : std_logic;
SIGNAL \CPU|DP|U3|b[15]~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR2|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR6|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector0~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[14]~3_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux1~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a14\ : std_logic;
SIGNAL \CPU|DP|U3|b[14]~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector1~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR0|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector1~2_combout\ : std_logic;
SIGNAL \CPU|DP|rB|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[13]~4_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux2~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux2~1_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a3\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \Selector12~0_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~0_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~1_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[11]~6_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux4~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a11\ : std_logic;
SIGNAL \CPU|DP|U3|b[11]~5_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[11]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR6|out[11]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector4~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector4~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector4~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[10]~7_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux5~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux5~1_combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[10]~6_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR6|out[10]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector5~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[10]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[10]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector5~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector5~2_combout\ : std_logic;
SIGNAL \CPU|DP|rB|out[10]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[9]~8_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux6~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a9\ : std_logic;
SIGNAL \CPU|DP|U3|b[9]~7_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector6~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[8]~9_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux7~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux7~1_combout\ : std_logic;
SIGNAL \CPU|mem_addr[8]~8_combout\ : std_logic;
SIGNAL \LED_load~4_combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[8]~8_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[8]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR0|out[8]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector7~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[7]~10_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux8~0_combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[7]~9_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[7]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR0|out[7]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR5|out[7]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector8~2_combout\ : std_logic;
SIGNAL \CPU|DP|rB|out[7]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[6]~11_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux9~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a7\ : std_logic;
SIGNAL \Selector8~0_combout\ : std_logic;
SIGNAL \CPU|Add0~30\ : std_logic;
SIGNAL \CPU|Add0~34\ : std_logic;
SIGNAL \CPU|Add0~13_sumout\ : std_logic;
SIGNAL \CPU|FSM|psel[2]~1_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~3_combout\ : std_logic;
SIGNAL \CPU|Add0~14\ : std_logic;
SIGNAL \CPU|Add0~18\ : std_logic;
SIGNAL \CPU|Add0~6\ : std_logic;
SIGNAL \CPU|Add0~2\ : std_logic;
SIGNAL \CPU|Add0~9_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~2_combout\ : std_logic;
SIGNAL \CPU|mem_addr[7]~7_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a6\ : std_logic;
SIGNAL \Selector9~0_combout\ : std_logic;
SIGNAL \CPU|Add0~1_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~0_combout\ : std_logic;
SIGNAL \CPU|program_counter|out[6]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|mem_addr[6]~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \CPU|struct_reg|out[8]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|out[9]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor33~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ovf~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ovf~1_combout\ : std_logic;
SIGNAL \CPU|DP|status|out[2]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|status|out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor40~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor33~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor34~combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor37~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~6_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~7_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~22_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~25_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[1]~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR3|out[0]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector15~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector15~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector15~2_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[1]~25_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[1]~26_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[1]~16_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[1]~5_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[1]~3_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[0]~27_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[0]~28_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[0]~17_combout\ : std_logic;
SIGNAL \CPU|DP|U3|b[3]~13_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR7|out[3]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector12~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[3]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector12~2_combout\ : std_logic;
SIGNAL \CPU|DP|rB|out[4]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~21_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~22_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~14_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux10~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux10~1_combout\ : std_logic;
SIGNAL \CPU|mem_addr[5]~6_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a4\ : std_logic;
SIGNAL \Selector11~0_combout\ : std_logic;
SIGNAL \CPU|DP|rB|out[5]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[4]~19_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[4]~20_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[4]~13_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux11~0_combout\ : std_logic;
SIGNAL \CPU|mem_addr[4]~5_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a5\ : std_logic;
SIGNAL \Selector10~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|writenum_OH[7]~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector14~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[1]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector14~2_combout\ : std_logic;
SIGNAL \CPU|DP|rB|out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[2]~23_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[2]~24_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[2]~15_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux12~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux12~1_combout\ : std_logic;
SIGNAL \CPU|mem_addr[3]~4_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a2\ : std_logic;
SIGNAL \Selector13~0_combout\ : std_logic;
SIGNAL \CPU|program_counter|out[2]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|U3|b[2]~14_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector13~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[2]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector13~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux13~0_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~8_combout\ : std_logic;
SIGNAL \CPU|Add0~33_sumout\ : std_logic;
SIGNAL \CPU|mem_addr[2]~3_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \Selector15~0_combout\ : std_logic;
SIGNAL \CPU|Add0~26\ : std_logic;
SIGNAL \CPU|Add0~29_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~7_combout\ : std_logic;
SIGNAL \CPU|program_counter|out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|mem_addr[1]~2_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a10\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \CPU|struct_reg|out[10]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor39~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~23_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~27_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr37~2_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr37~3_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr37~combout\ : std_logic;
SIGNAL \CPU|Add0~25_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~6_combout\ : std_logic;
SIGNAL \CPU|mem_addr[0]~1_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a1\ : std_logic;
SIGNAL \Selector14~0_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|out[1]~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a12\ : std_logic;
SIGNAL \CPU|DP|U3|b[12]~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR1|out[12]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector3~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector3~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|Selector3~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux3~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|Equal0~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|Equal0~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|Equal0~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|Equal0~3_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor37~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor39~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~8_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~9_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr18~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|state_next_reset[1]~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor27~combout\ : std_logic;
SIGNAL \write~combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a13\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor48~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor48~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr3~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr39~0_combout\ : std_logic;
SIGNAL \CPU|FSM|state_next_reset[2]~2_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~28_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~1_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~24_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr9~3_combout\ : std_logic;
SIGNAL \CPU|Add0~10\ : std_logic;
SIGNAL \CPU|Add0~21_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~5_combout\ : std_logic;
SIGNAL \CPU|program_counter|out[8]~DUPLICATE_q\ : std_logic;
SIGNAL \LED_load~2_combout\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|c[1]~3_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux14~0_combout\ : std_logic;
SIGNAL \LED_load~3_combout\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor27~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor33~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor41~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~4_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~5_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~6_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr41~2_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor3~combout\ : std_logic;
SIGNAL \CPU|FSM|state_next_reset[0]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor46~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~20_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr1~2_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr1~1_combout\ : std_logic;
SIGNAL \CPU|FSM|always0~19_combout\ : std_logic;
SIGNAL \CPU|FSM|state_next_reset[4]~4_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor2~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr46~0_combout\ : std_logic;
SIGNAL \Equal5~0_combout\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor38~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr37~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr37~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr35~0_combout\ : std_logic;
SIGNAL \CPU|Add0~17_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~4_combout\ : std_logic;
SIGNAL \CPU|program_counter|out[4]~DUPLICATE_q\ : std_logic;
SIGNAL \LED_load~1_combout\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \CPU|struct_reg|out[15]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|WideNor28~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideNor28~combout\ : std_logic;
SIGNAL \CPU|FSM|always0~18_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr29~0_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr39~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr39~2_combout\ : std_logic;
SIGNAL \CPU|Add0~5_sumout\ : std_logic;
SIGNAL \CPU|nextPCmux|out~1_combout\ : std_logic;
SIGNAL \CPU|program_counter|out[5]~DUPLICATE_q\ : std_logic;
SIGNAL \LED_load~0_combout\ : std_logic;
SIGNAL \Selector4~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|Mux15~0_combout\ : std_logic;
SIGNAL \LED_load~combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr9~2_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr7~1_combout\ : std_logic;
SIGNAL \CPU|FSM|WideOr1~3_combout\ : std_logic;
SIGNAL \CPU|FSM|Equal0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|Equal0~1_combout\ : std_logic;
SIGNAL \H0|WideOr6~0_combout\ : std_logic;
SIGNAL \H0|WideOr5~0_combout\ : std_logic;
SIGNAL \H0|WideOr4~0_combout\ : std_logic;
SIGNAL \H0|WideOr3~0_combout\ : std_logic;
SIGNAL \H0|WideOr2~0_combout\ : std_logic;
SIGNAL \H0|WideOr1~0_combout\ : std_logic;
SIGNAL \H0|WideOr0~0_combout\ : std_logic;
SIGNAL \H1|WideOr6~0_combout\ : std_logic;
SIGNAL \H1|WideOr5~0_combout\ : std_logic;
SIGNAL \H1|WideOr4~0_combout\ : std_logic;
SIGNAL \H1|WideOr3~0_combout\ : std_logic;
SIGNAL \H1|WideOr2~0_combout\ : std_logic;
SIGNAL \H1|WideOr1~0_combout\ : std_logic;
SIGNAL \H1|WideOr0~0_combout\ : std_logic;
SIGNAL \H2|WideOr6~0_combout\ : std_logic;
SIGNAL \H2|WideOr5~0_combout\ : std_logic;
SIGNAL \H2|WideOr4~0_combout\ : std_logic;
SIGNAL \H2|WideOr3~0_combout\ : std_logic;
SIGNAL \H2|WideOr2~0_combout\ : std_logic;
SIGNAL \H2|WideOr1~0_combout\ : std_logic;
SIGNAL \H2|WideOr0~0_combout\ : std_logic;
SIGNAL \H3|WideOr6~0_combout\ : std_logic;
SIGNAL \H3|WideOr5~0_combout\ : std_logic;
SIGNAL \H3|WideOr4~0_combout\ : std_logic;
SIGNAL \H3|WideOr3~0_combout\ : std_logic;
SIGNAL \H3|WideOr2~0_combout\ : std_logic;
SIGNAL \H3|WideOr1~0_combout\ : std_logic;
SIGNAL \H3|WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|DP|status|out[0]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|status|out\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|program_counter|out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|Data_Address|out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|g\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \CPU|FSM|vsel\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \LED_register|out\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR0|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|FSM|STATE|out\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \CPU|FSM|m_cmd\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \CPU|DP|rC|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|rB|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|c\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|rA|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|p\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \CPU|DP|U2|ALU_AddSub|as|p\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|nextPCmux|out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR7|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR1|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR3|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR2|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|struct_decoder|nsel_mux|out\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR4|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR6|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR5|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|U3|b\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|rB|ALT_INV_out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|rB|ALT_INV_out[2]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|rB|ALT_INV_out[4]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|rB|ALT_INV_out[5]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|rB|ALT_INV_out[10]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|program_counter|ALT_INV_out[2]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|program_counter|ALT_INV_out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|program_counter|ALT_INV_out[8]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|program_counter|ALT_INV_out[4]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|program_counter|ALT_INV_out[5]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|program_counter|ALT_INV_out[6]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|status|ALT_INV_out[0]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|status|ALT_INV_out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[9]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[11]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|status|ALT_INV_out[2]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_SW[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[1]~input_o\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[0]~28_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[0]~27_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[1]~5_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[4]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[1]~26_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[1]~25_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[2]~24_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[2]~23_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~22_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~21_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[4]~20_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[4]~19_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~29_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~6_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~5_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~4_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~28_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|U3|ALT_INV_b[0]~16_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[1]~15_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[2]~14_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[3]~13_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[4]~12_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[5]~11_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[6]~10_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[7]~9_combout\ : std_logic;
SIGNAL \ALT_INV_Equal5~1_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[8]~8_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[9]~7_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[10]~6_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[11]~5_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[12]~4_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[13]~3_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[14]~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr11~combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b[15]~1_combout\ : std_logic;
SIGNAL \CPU|DP|U3|ALT_INV_b~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr26~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr37~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~27_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr37~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr37~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector15~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR5|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR6|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR4|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector15~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|regR2|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR3|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR1|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR0|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regR7|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector14~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector14~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector13~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector13~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector12~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector12~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector11~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector11~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector10~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector10~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector7~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector7~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector6~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector6~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector5~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector5~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector4~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector4~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector3~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector3~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector2~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector2~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector0~1_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector0~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out[0]~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr31~combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out~2_combout\ : std_logic;
SIGNAL \CPU|struct_reg|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out~1_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|nsel_mux|ALT_INV_out~0_combout\ : std_logic;
SIGNAL \ALT_INV_LED_load~4_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[8]~8_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[4]~5_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[3]~4_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[2]~3_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[1]~2_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[0]~1_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~8_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~7_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~6_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~5_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~4_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~3_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~2_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~1_combout\ : std_logic;
SIGNAL \CPU|nextPCmux|ALT_INV_out~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr39~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr39~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~26_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~25_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux10~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux10~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux12~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux12~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux13~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux14~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~2_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux15~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux11~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\ : std_logic_vector(14 DOWNTO 2);
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux8~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux7~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux7~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux9~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux4~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux2~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\ : std_logic_vector(13 DOWNTO 2);
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux5~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux5~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux3~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ALT_INV_ovf~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux0~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|main|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|as|ALT_INV_p\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|DP|rA|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALT_INV_Bin[15]~18_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[0]~17_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[1]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[1]~16_combout\ : std_logic;
SIGNAL \CPU|DP|rB|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\ : std_logic_vector(12 DOWNTO 2);
SIGNAL \CPU|DP|ALT_INV_Bin[2]~15_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~14_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[4]~13_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[5]~12_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[6]~11_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[7]~10_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[8]~9_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[9]~8_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[10]~7_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[11]~6_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[12]~5_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ALT_INV_comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[13]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[14]~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[4]~1_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ALT_INV_comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[14]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~2_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|ALT_INV_shift[1]~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal5~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr46~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr9~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~24_combout\ : std_logic;
SIGNAL \ALT_INV_LED_load~3_combout\ : std_logic;
SIGNAL \CPU|Data_Address|ALT_INV_out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|program_counter|ALT_INV_out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \ALT_INV_LED_load~2_combout\ : std_logic;
SIGNAL \ALT_INV_LED_load~1_combout\ : std_logic;
SIGNAL \ALT_INV_LED_load~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~23_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~22_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor41~combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[6]~0_combout\ : std_logic;
SIGNAL \H3|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|DP|rC|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \H2|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \H1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \H0|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr3~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr29~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr39~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr3~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr1~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr1~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr7~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_psel[2]~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~21_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor2~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr3~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_m_cmd\ : std_logic_vector(1 DOWNTO 1);
SIGNAL \CPU|FSM|ALT_INV_loads~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr35~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr37~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr37~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor37~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor38~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor39~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor40~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~20_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~19_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr1~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr18~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr29~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~18_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~17_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr9~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr9~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_vsel\ : std_logic_vector(3 DOWNTO 3);
SIGNAL \CPU|FSM|ALT_INV_WideNor28~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~16_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor6~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr9~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr20~0_combout\ : std_logic;
SIGNAL \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor21~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor20~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor19~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_bsel~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr16~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr11~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~15_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~14_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr41~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~13_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~12_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor19~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr3~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~11_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor48~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor48~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~10_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor46~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor43~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~9_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~8_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideOr1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor34~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~7_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~6_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor29~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor27~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor31~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~5_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~4_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_psel[2]~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[4]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor20~0_combout\ : std_logic;
SIGNAL \CPU|struct_decoder|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor17~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~3_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor16~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor12~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor12~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~2_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor6~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor13~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor13~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor15~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor16~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor26~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor26~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor28~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor28~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor28~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor33~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor37~0_combout\ : std_logic;
SIGNAL \CPU|DP|status|ALT_INV_out\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|FSM|ALT_INV_WideNor39~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor33~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor33~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor42~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor42~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor44~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor47~combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor47~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor43~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_always0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor6~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor5~1_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor5~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor27~0_combout\ : std_logic;
SIGNAL \CPU|FSM|ALT_INV_WideNor3~combout\ : std_logic;
SIGNAL \CPU|FSM|STATE|ALT_INV_out\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \CPU|FSM|ALT_INV_nsel[2]~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~33_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~29_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \CPU|ALT_INV_Add0~1_sumout\ : std_logic;

BEGIN

ww_KEY <= KEY;
ww_SW <= SW;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
ww_CLOCK_50 <= CLOCK_50;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \CPU|DP|rC|out\(15) & \CPU|DP|rC|out\(14) & 
\CPU|DP|rC|out\(13) & \CPU|DP|rC|out\(12) & \CPU|DP|rC|out\(11) & \CPU|DP|rC|out\(10) & \CPU|DP|rC|out\(9) & \CPU|DP|rC|out\(8) & \CPU|DP|rC|out\(7) & \CPU|DP|rC|out\(6) & \CPU|DP|rC|out\(5) & \CPU|DP|rC|out\(4) & \CPU|DP|rC|out\(3) & 
\CPU|DP|rC|out\(2) & \CPU|DP|rC|out\(1) & \CPU|DP|rC|out\(0));

\MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\CPU|mem_addr[7]~7_combout\ & \CPU|mem_addr[6]~0_combout\ & \CPU|mem_addr[5]~6_combout\ & \CPU|mem_addr[4]~5_combout\ & \CPU|mem_addr[3]~4_combout\ & \CPU|mem_addr[2]~3_combout\ & 
\CPU|mem_addr[1]~2_combout\ & \CPU|mem_addr[0]~1_combout\);

\MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\CPU|mem_addr[7]~7_combout\ & \CPU|mem_addr[6]~0_combout\ & \CPU|mem_addr[5]~6_combout\ & \CPU|mem_addr[4]~5_combout\ & \CPU|mem_addr[3]~4_combout\ & \CPU|mem_addr[2]~3_combout\ & 
\CPU|mem_addr[1]~2_combout\ & \CPU|mem_addr[0]~1_combout\);

\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);
\MEM|mem_rtl_0|auto_generated|ram_block1a1\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(1);
\MEM|mem_rtl_0|auto_generated|ram_block1a2\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(2);
\MEM|mem_rtl_0|auto_generated|ram_block1a3\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(3);
\MEM|mem_rtl_0|auto_generated|ram_block1a4\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(4);
\MEM|mem_rtl_0|auto_generated|ram_block1a5\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(5);
\MEM|mem_rtl_0|auto_generated|ram_block1a6\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(6);
\MEM|mem_rtl_0|auto_generated|ram_block1a7\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(7);
\MEM|mem_rtl_0|auto_generated|ram_block1a8\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(8);
\MEM|mem_rtl_0|auto_generated|ram_block1a9\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(9);
\MEM|mem_rtl_0|auto_generated|ram_block1a10\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(10);
\MEM|mem_rtl_0|auto_generated|ram_block1a11\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(11);
\MEM|mem_rtl_0|auto_generated|ram_block1a12\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(12);
\MEM|mem_rtl_0|auto_generated|ram_block1a13\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(13);
\MEM|mem_rtl_0|auto_generated|ram_block1a14\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(14);
\MEM|mem_rtl_0|auto_generated|ram_block1a15\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(15);
\CPU|DP|rB|ALT_INV_out[1]~DUPLICATE_q\ <= NOT \CPU|DP|rB|out[1]~DUPLICATE_q\;
\CPU|DP|rB|ALT_INV_out[2]~DUPLICATE_q\ <= NOT \CPU|DP|rB|out[2]~DUPLICATE_q\;
\CPU|DP|rB|ALT_INV_out[4]~DUPLICATE_q\ <= NOT \CPU|DP|rB|out[4]~DUPLICATE_q\;
\CPU|DP|rB|ALT_INV_out[5]~DUPLICATE_q\ <= NOT \CPU|DP|rB|out[5]~DUPLICATE_q\;
\CPU|DP|rB|ALT_INV_out[10]~DUPLICATE_q\ <= NOT \CPU|DP|rB|out[10]~DUPLICATE_q\;
\CPU|program_counter|ALT_INV_out[2]~DUPLICATE_q\ <= NOT \CPU|program_counter|out[2]~DUPLICATE_q\;
\CPU|program_counter|ALT_INV_out[1]~DUPLICATE_q\ <= NOT \CPU|program_counter|out[1]~DUPLICATE_q\;
\CPU|program_counter|ALT_INV_out[8]~DUPLICATE_q\ <= NOT \CPU|program_counter|out[8]~DUPLICATE_q\;
\CPU|program_counter|ALT_INV_out[4]~DUPLICATE_q\ <= NOT \CPU|program_counter|out[4]~DUPLICATE_q\;
\CPU|program_counter|ALT_INV_out[5]~DUPLICATE_q\ <= NOT \CPU|program_counter|out[5]~DUPLICATE_q\;
\CPU|program_counter|ALT_INV_out[6]~DUPLICATE_q\ <= NOT \CPU|program_counter|out[6]~DUPLICATE_q\;
\CPU|DP|status|ALT_INV_out[0]~DUPLICATE_q\ <= NOT \CPU|DP|status|out[0]~DUPLICATE_q\;
\CPU|DP|status|ALT_INV_out[1]~DUPLICATE_q\ <= NOT \CPU|DP|status|out[1]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[9]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[9]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[8]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[10]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[11]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[11]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[15]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[13]~DUPLICATE_q\;
\CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\ <= NOT \CPU|struct_reg|out[14]~DUPLICATE_q\;
\CPU|DP|status|ALT_INV_out[2]~DUPLICATE_q\ <= NOT \CPU|DP|status|out[2]~DUPLICATE_q\;
\ALT_INV_SW[6]~input_o\ <= NOT \SW[6]~input_o\;
\ALT_INV_SW[5]~input_o\ <= NOT \SW[5]~input_o\;
\ALT_INV_SW[7]~input_o\ <= NOT \SW[7]~input_o\;
\ALT_INV_SW[0]~input_o\ <= NOT \SW[0]~input_o\;
\ALT_INV_SW[1]~input_o\ <= NOT \SW[1]~input_o\;
\ALT_INV_SW[2]~input_o\ <= NOT \SW[2]~input_o\;
\ALT_INV_SW[3]~input_o\ <= NOT \SW[3]~input_o\;
\ALT_INV_SW[4]~input_o\ <= NOT \SW[4]~input_o\;
\ALT_INV_KEY[1]~input_o\ <= NOT \KEY[1]~input_o\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~3_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ai|c[1]~3_combout\;
\CPU|DP|ALT_INV_Bin[0]~28_combout\ <= NOT \CPU|DP|Bin[0]~28_combout\;
\CPU|DP|ALT_INV_Bin[0]~27_combout\ <= NOT \CPU|DP|Bin[0]~27_combout\;
\CPU|DP|ALT_INV_Ain[1]~5_combout\ <= NOT \CPU|DP|Ain[1]~5_combout\;
\CPU|DP|ALT_INV_Ain[4]~4_combout\ <= NOT \CPU|DP|Ain[4]~4_combout\;
\CPU|DP|ALT_INV_Bin[1]~26_combout\ <= NOT \CPU|DP|Bin[1]~26_combout\;
\CPU|DP|ALT_INV_Bin[1]~25_combout\ <= NOT \CPU|DP|Bin[1]~25_combout\;
\CPU|DP|ALT_INV_Bin[2]~24_combout\ <= NOT \CPU|DP|Bin[2]~24_combout\;
\CPU|DP|ALT_INV_Bin[2]~23_combout\ <= NOT \CPU|DP|Bin[2]~23_combout\;
\CPU|DP|ALT_INV_Bin[3]~22_combout\ <= NOT \CPU|DP|Bin[3]~22_combout\;
\CPU|DP|ALT_INV_Bin[3]~21_combout\ <= NOT \CPU|DP|Bin[3]~21_combout\;
\CPU|DP|ALT_INV_Bin[4]~20_combout\ <= NOT \CPU|DP|Bin[4]~20_combout\;
\CPU|DP|ALT_INV_Bin[4]~19_combout\ <= NOT \CPU|DP|Bin[4]~19_combout\;
\CPU|FSM|ALT_INV_always0~29_combout\ <= NOT \CPU|FSM|always0~29_combout\;
\CPU|FSM|ALT_INV_WideOr41~6_combout\ <= NOT \CPU|FSM|WideOr41~6_combout\;
\CPU|FSM|ALT_INV_WideOr41~5_combout\ <= NOT \CPU|FSM|WideOr41~5_combout\;
\CPU|FSM|ALT_INV_WideOr41~4_combout\ <= NOT \CPU|FSM|WideOr41~4_combout\;
\CPU|FSM|ALT_INV_WideOr41~3_combout\ <= NOT \CPU|FSM|WideOr41~3_combout\;
\CPU|FSM|ALT_INV_always0~28_combout\ <= NOT \CPU|FSM|always0~28_combout\;
\CPU|DP|U3|ALT_INV_b\(0) <= NOT \CPU|DP|U3|b\(0);
\CPU|DP|U3|ALT_INV_b[0]~16_combout\ <= NOT \CPU|DP|U3|b[0]~16_combout\;
\CPU|DP|U3|ALT_INV_b\(1) <= NOT \CPU|DP|U3|b\(1);
\CPU|DP|U3|ALT_INV_b[1]~15_combout\ <= NOT \CPU|DP|U3|b[1]~15_combout\;
\CPU|DP|U3|ALT_INV_b\(2) <= NOT \CPU|DP|U3|b\(2);
\CPU|DP|U3|ALT_INV_b[2]~14_combout\ <= NOT \CPU|DP|U3|b[2]~14_combout\;
\CPU|DP|U3|ALT_INV_b\(3) <= NOT \CPU|DP|U3|b\(3);
\CPU|DP|U3|ALT_INV_b[3]~13_combout\ <= NOT \CPU|DP|U3|b[3]~13_combout\;
\CPU|DP|U3|ALT_INV_b\(4) <= NOT \CPU|DP|U3|b\(4);
\CPU|DP|U3|ALT_INV_b[4]~12_combout\ <= NOT \CPU|DP|U3|b[4]~12_combout\;
\CPU|DP|U3|ALT_INV_b\(5) <= NOT \CPU|DP|U3|b\(5);
\CPU|DP|U3|ALT_INV_b[5]~11_combout\ <= NOT \CPU|DP|U3|b[5]~11_combout\;
\CPU|DP|U3|ALT_INV_b\(6) <= NOT \CPU|DP|U3|b\(6);
\CPU|DP|U3|ALT_INV_b[6]~10_combout\ <= NOT \CPU|DP|U3|b[6]~10_combout\;
\CPU|DP|U3|ALT_INV_b\(7) <= NOT \CPU|DP|U3|b\(7);
\CPU|DP|U3|ALT_INV_b[7]~9_combout\ <= NOT \CPU|DP|U3|b[7]~9_combout\;
\ALT_INV_Equal5~1_combout\ <= NOT \Equal5~1_combout\;
\CPU|DP|U3|ALT_INV_b\(8) <= NOT \CPU|DP|U3|b\(8);
\CPU|DP|U3|ALT_INV_b[8]~8_combout\ <= NOT \CPU|DP|U3|b[8]~8_combout\;
\CPU|DP|U3|ALT_INV_b[9]~7_combout\ <= NOT \CPU|DP|U3|b[9]~7_combout\;
\CPU|DP|U3|ALT_INV_b\(10) <= NOT \CPU|DP|U3|b\(10);
\CPU|DP|U3|ALT_INV_b[10]~6_combout\ <= NOT \CPU|DP|U3|b[10]~6_combout\;
\CPU|DP|U3|ALT_INV_b\(11) <= NOT \CPU|DP|U3|b\(11);
\CPU|DP|U3|ALT_INV_b[11]~5_combout\ <= NOT \CPU|DP|U3|b[11]~5_combout\;
\CPU|DP|U3|ALT_INV_b\(12) <= NOT \CPU|DP|U3|b\(12);
\CPU|DP|U3|ALT_INV_b[12]~4_combout\ <= NOT \CPU|DP|U3|b[12]~4_combout\;
\CPU|DP|U3|ALT_INV_b\(13) <= NOT \CPU|DP|U3|b\(13);
\CPU|DP|U3|ALT_INV_b[13]~3_combout\ <= NOT \CPU|DP|U3|b[13]~3_combout\;
\CPU|DP|U3|ALT_INV_b\(14) <= NOT \CPU|DP|U3|b\(14);
\CPU|DP|U3|ALT_INV_b[14]~2_combout\ <= NOT \CPU|DP|U3|b[14]~2_combout\;
\CPU|FSM|ALT_INV_WideOr11~combout\ <= NOT \CPU|FSM|WideOr11~combout\;
\CPU|DP|U3|ALT_INV_b\(15) <= NOT \CPU|DP|U3|b\(15);
\CPU|DP|U3|ALT_INV_b[15]~1_combout\ <= NOT \CPU|DP|U3|b[15]~1_combout\;
\CPU|DP|U3|ALT_INV_b~0_combout\ <= NOT \CPU|DP|U3|b~0_combout\;
\CPU|FSM|ALT_INV_WideOr26~combout\ <= NOT \CPU|FSM|WideOr26~combout\;
\CPU|FSM|ALT_INV_WideOr37~combout\ <= NOT \CPU|FSM|WideOr37~combout\;
\CPU|FSM|ALT_INV_always0~27_combout\ <= NOT \CPU|FSM|always0~27_combout\;
\CPU|FSM|ALT_INV_WideOr37~3_combout\ <= NOT \CPU|FSM|WideOr37~3_combout\;
\CPU|FSM|ALT_INV_WideOr37~2_combout\ <= NOT \CPU|FSM|WideOr37~2_combout\;
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector15~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector15~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR5|out\(0);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR6|out\(0);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR4|out\(0);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector15~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector15~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR2|out\(0);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR3|out\(0);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR1|out\(0);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR0|out\(0);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|regR7|out\(0);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector14~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector14~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR5|out\(1);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR6|out\(1);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR4|out\(1);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector14~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR2|out\(1);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR3|out\(1);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR1|out\(1);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR0|out\(1);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|regR7|out\(1);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector13~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector13~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR5|out\(2);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR6|out\(2);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR4|out\(2);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector13~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR2|out\(2);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR3|out\(2);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR1|out\(2);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR0|out\(2);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|regR7|out\(2);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector12~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector12~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR5|out\(3);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR6|out\(3);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR4|out\(3);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector12~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR2|out\(3);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR3|out\(3);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR1|out\(3);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR0|out\(3);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|regR7|out\(3);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector11~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector11~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR5|out\(4);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR6|out\(4);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR4|out\(4);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector11~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector11~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR2|out\(4);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR3|out\(4);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR1|out\(4);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR0|out\(4);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|regR7|out\(4);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector10~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector10~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR5|out\(5);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR6|out\(5);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR4|out\(5);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector10~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector10~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR2|out\(5);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR3|out\(5);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR1|out\(5);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR0|out\(5);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|regR7|out\(5);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~4_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector9~4_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR5|out\(6);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR6|out\(6);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR4|out\(6);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~3_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR2|out\(6);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR3|out\(6);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR1|out\(6);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR0|out\(6);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|regR7|out\(6);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~2_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector8~2_combout\;
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR5|out\(7);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR6|out\(7);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR4|out\(7);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR2|out\(7);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR3|out\(7);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR1|out\(7);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR0|out\(7);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|regR7|out\(7);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector7~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR5|out\(8);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR6|out\(8);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR4|out\(8);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector7~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR2|out\(8);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR3|out\(8);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR1|out\(8);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR0|out\(8);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|regR7|out\(8);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector6~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR5|out\(9);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR6|out\(9);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR4|out\(9);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector6~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR2|out\(9);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR3|out\(9);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR1|out\(9);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR0|out\(9);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|regR7|out\(9);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector5~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector5~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR5|out\(10);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR6|out\(10);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR4|out\(10);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector5~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector5~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR2|out\(10);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR3|out\(10);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR1|out\(10);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR0|out\(10);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|regR7|out\(10);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector4~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector4~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR5|out\(11);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR6|out\(11);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR4|out\(11);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector4~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector4~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR2|out\(11);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR3|out\(11);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR1|out\(11);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR0|out\(11);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|regR7|out\(11);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector3~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector3~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR5|out\(12);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR6|out\(12);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR4|out\(12);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector3~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector3~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR2|out\(12);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR3|out\(12);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR1|out\(12);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR0|out\(12);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|regR7|out\(12);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector2~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR5|out\(13);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR6|out\(13);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR4|out\(13);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector2~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector2~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR2|out\(13);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR3|out\(13);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR1|out\(13);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR0|out\(13);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|regR7|out\(13);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~2_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector1~2_combout\;
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector1~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR5|out\(14);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR6|out\(14);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR4|out\(14);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR2|out\(14);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR3|out\(14);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR1|out\(14);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR0|out\(14);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|regR7|out\(14);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector0~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\;
\CPU|DP|REGFILE|regR5|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR5|out\(15);
\CPU|DP|REGFILE|regR6|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR6|out\(15);
\CPU|DP|REGFILE|regR4|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR4|out\(15);
\CPU|struct_decoder|nsel_mux|ALT_INV_out\(2) <= NOT \CPU|struct_decoder|nsel_mux|out\(2);
\CPU|struct_decoder|nsel_mux|ALT_INV_out\(1) <= NOT \CPU|struct_decoder|nsel_mux|out\(1);
\CPU|struct_decoder|nsel_mux|ALT_INV_out\(0) <= NOT \CPU|struct_decoder|nsel_mux|out\(0);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector0~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\;
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\;
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\;
\CPU|DP|REGFILE|regR2|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR2|out\(15);
\CPU|DP|REGFILE|regR3|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR3|out\(15);
\CPU|DP|REGFILE|regR1|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR1|out\(15);
\CPU|DP|REGFILE|regR0|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR0|out\(15);
\CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\;
\CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\ <= NOT \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\;
\CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\ <= NOT \CPU|struct_decoder|nsel_mux|out[2]~5_combout\;
\CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\ <= NOT \CPU|struct_decoder|nsel_mux|out[1]~4_combout\;
\CPU|struct_decoder|nsel_mux|ALT_INV_out[0]~3_combout\ <= NOT \CPU|struct_decoder|nsel_mux|out[0]~3_combout\;
\CPU|FSM|ALT_INV_WideOr31~combout\ <= NOT \CPU|FSM|WideOr31~combout\;
\CPU|struct_decoder|nsel_mux|ALT_INV_out~2_combout\ <= NOT \CPU|struct_decoder|nsel_mux|out~2_combout\;
\CPU|struct_reg|ALT_INV_out\(6) <= NOT \CPU|struct_reg|out\(6);
\CPU|struct_decoder|nsel_mux|ALT_INV_out~1_combout\ <= NOT \CPU|struct_decoder|nsel_mux|out~1_combout\;
\CPU|struct_reg|ALT_INV_out\(5) <= NOT \CPU|struct_reg|out\(5);
\CPU|struct_decoder|nsel_mux|ALT_INV_out~0_combout\ <= NOT \CPU|struct_decoder|nsel_mux|out~0_combout\;
\CPU|struct_reg|ALT_INV_out\(7) <= NOT \CPU|struct_reg|out\(7);
\CPU|DP|REGFILE|regR7|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|regR7|out\(15);
\ALT_INV_LED_load~4_combout\ <= NOT \LED_load~4_combout\;
\CPU|ALT_INV_mem_addr[8]~8_combout\ <= NOT \CPU|mem_addr[8]~8_combout\;
\CPU|ALT_INV_mem_addr[4]~5_combout\ <= NOT \CPU|mem_addr[4]~5_combout\;
\CPU|ALT_INV_mem_addr[3]~4_combout\ <= NOT \CPU|mem_addr[3]~4_combout\;
\CPU|ALT_INV_mem_addr[2]~3_combout\ <= NOT \CPU|mem_addr[2]~3_combout\;
\CPU|ALT_INV_mem_addr[1]~2_combout\ <= NOT \CPU|mem_addr[1]~2_combout\;
\CPU|ALT_INV_mem_addr[0]~1_combout\ <= NOT \CPU|mem_addr[0]~1_combout\;
\CPU|nextPCmux|ALT_INV_out~8_combout\ <= NOT \CPU|nextPCmux|out~8_combout\;
\CPU|nextPCmux|ALT_INV_out~7_combout\ <= NOT \CPU|nextPCmux|out~7_combout\;
\CPU|nextPCmux|ALT_INV_out~6_combout\ <= NOT \CPU|nextPCmux|out~6_combout\;
\CPU|nextPCmux|ALT_INV_out~5_combout\ <= NOT \CPU|nextPCmux|out~5_combout\;
\CPU|nextPCmux|ALT_INV_out~4_combout\ <= NOT \CPU|nextPCmux|out~4_combout\;
\CPU|nextPCmux|ALT_INV_out~3_combout\ <= NOT \CPU|nextPCmux|out~3_combout\;
\CPU|nextPCmux|ALT_INV_out~2_combout\ <= NOT \CPU|nextPCmux|out~2_combout\;
\CPU|nextPCmux|ALT_INV_out~1_combout\ <= NOT \CPU|nextPCmux|out~1_combout\;
\CPU|nextPCmux|ALT_INV_out~0_combout\ <= NOT \CPU|nextPCmux|out~0_combout\;
\CPU|FSM|ALT_INV_WideOr39~2_combout\ <= NOT \CPU|FSM|WideOr39~2_combout\;
\CPU|FSM|ALT_INV_WideOr39~1_combout\ <= NOT \CPU|FSM|WideOr39~1_combout\;
\CPU|FSM|ALT_INV_always0~26_combout\ <= NOT \CPU|FSM|always0~26_combout\;
\CPU|FSM|ALT_INV_always0~25_combout\ <= NOT \CPU|FSM|always0~25_combout\;
\CPU|DP|U2|ALT_INV_Equal0~2_combout\ <= NOT \CPU|DP|U2|Equal0~2_combout\;
\CPU|DP|U2|ALT_INV_Equal0~1_combout\ <= NOT \CPU|DP|U2|Equal0~1_combout\;
\CPU|DP|U2|ALT_INV_Equal0~0_combout\ <= NOT \CPU|DP|U2|Equal0~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux10~1_combout\ <= NOT \CPU|DP|U2|main|Mux10~1_combout\;
\CPU|DP|U2|main|ALT_INV_Mux10~0_combout\ <= NOT \CPU|DP|U2|main|Mux10~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux12~1_combout\ <= NOT \CPU|DP|U2|main|Mux12~1_combout\;
\CPU|DP|U2|main|ALT_INV_Mux12~0_combout\ <= NOT \CPU|DP|U2|main|Mux12~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux13~0_combout\ <= NOT \CPU|DP|U2|main|Mux13~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux14~0_combout\ <= NOT \CPU|DP|U2|main|Mux14~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~2_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\;
\CPU|DP|U2|main|ALT_INV_Mux15~0_combout\ <= NOT \CPU|DP|U2|main|Mux15~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux11~0_combout\ <= NOT \CPU|DP|U2|main|Mux11~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(4) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(4);
\CPU|DP|U2|main|ALT_INV_Mux8~0_combout\ <= NOT \CPU|DP|U2|main|Mux8~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux7~1_combout\ <= NOT \CPU|DP|U2|main|Mux7~1_combout\;
\CPU|DP|U2|main|ALT_INV_Mux7~0_combout\ <= NOT \CPU|DP|U2|main|Mux7~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux9~0_combout\ <= NOT \CPU|DP|U2|main|Mux9~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(6) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(6);
\CPU|DP|U2|main|ALT_INV_Mux6~0_combout\ <= NOT \CPU|DP|U2|main|Mux6~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(9) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(9);
\CPU|DP|U2|main|ALT_INV_Mux4~0_combout\ <= NOT \CPU|DP|U2|main|Mux4~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(11) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(11);
\CPU|DP|U2|main|ALT_INV_Mux2~1_combout\ <= NOT \CPU|DP|U2|main|Mux2~1_combout\;
\CPU|DP|U2|main|ALT_INV_Mux2~0_combout\ <= NOT \CPU|DP|U2|main|Mux2~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(13) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(13);
\CPU|DP|U2|main|ALT_INV_Mux5~1_combout\ <= NOT \CPU|DP|U2|main|Mux5~1_combout\;
\CPU|DP|U2|main|ALT_INV_Mux5~0_combout\ <= NOT \CPU|DP|U2|main|Mux5~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux3~0_combout\ <= NOT \CPU|DP|U2|main|Mux3~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux1~0_combout\ <= NOT \CPU|DP|U2|main|Mux1~0_combout\;
\CPU|DP|U2|ALU_AddSub|ALT_INV_ovf~0_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ovf~0_combout\;
\CPU|DP|U2|main|ALT_INV_Mux0~1_combout\ <= NOT \CPU|DP|U2|main|Mux0~1_combout\;
\CPU|DP|U2|main|ALT_INV_Mux0~0_combout\ <= NOT \CPU|DP|U2|main|Mux0~0_combout\;
\CPU|DP|U2|ALU_AddSub|as|ALT_INV_p\(0) <= NOT \CPU|DP|U2|ALU_AddSub|as|p\(0);
\CPU|DP|rA|ALT_INV_out\(15) <= NOT \CPU|DP|rA|out\(15);
\CPU|DP|ALT_INV_Bin[15]~18_combout\ <= NOT \CPU|DP|Bin[15]~18_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(14) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(14);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(12) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(12);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~1_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(7) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(7);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~0_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(2) <= NOT \CPU|DP|U2|ALU_AddSub|ai|c\(2);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~1_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\;
\CPU|DP|rA|ALT_INV_out\(0) <= NOT \CPU|DP|rA|out\(0);
\CPU|DP|ALT_INV_Bin[0]~17_combout\ <= NOT \CPU|DP|Bin[0]~17_combout\;
\CPU|struct_reg|ALT_INV_out\(0) <= NOT \CPU|struct_reg|out\(0);
\CPU|DP|ALT_INV_Ain[1]~3_combout\ <= NOT \CPU|DP|Ain[1]~3_combout\;
\CPU|DP|rA|ALT_INV_out\(1) <= NOT \CPU|DP|rA|out\(1);
\CPU|DP|ALT_INV_Bin[1]~16_combout\ <= NOT \CPU|DP|Bin[1]~16_combout\;
\CPU|DP|rB|ALT_INV_out\(0) <= NOT \CPU|DP|rB|out\(0);
\CPU|struct_reg|ALT_INV_out\(1) <= NOT \CPU|struct_reg|out\(1);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(2) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(2);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(2) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(2);
\CPU|DP|rA|ALT_INV_out\(2) <= NOT \CPU|DP|rA|out\(2);
\CPU|DP|ALT_INV_Bin[2]~15_combout\ <= NOT \CPU|DP|Bin[2]~15_combout\;
\CPU|DP|rB|ALT_INV_out\(1) <= NOT \CPU|DP|rB|out\(1);
\CPU|struct_reg|ALT_INV_out\(2) <= NOT \CPU|struct_reg|out\(2);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(3) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(3);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(3) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(3);
\CPU|DP|rA|ALT_INV_out\(3) <= NOT \CPU|DP|rA|out\(3);
\CPU|DP|ALT_INV_Bin[3]~14_combout\ <= NOT \CPU|DP|Bin[3]~14_combout\;
\CPU|DP|rB|ALT_INV_out\(2) <= NOT \CPU|DP|rB|out\(2);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(4) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(4);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(4) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(4);
\CPU|DP|rA|ALT_INV_out\(4) <= NOT \CPU|DP|rA|out\(4);
\CPU|DP|ALT_INV_Bin[4]~13_combout\ <= NOT \CPU|DP|Bin[4]~13_combout\;
\CPU|DP|rB|ALT_INV_out\(3) <= NOT \CPU|DP|rB|out\(3);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(5) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(5);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(5) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(5);
\CPU|DP|rA|ALT_INV_out\(5) <= NOT \CPU|DP|rA|out\(5);
\CPU|DP|ALT_INV_Bin[5]~12_combout\ <= NOT \CPU|DP|Bin[5]~12_combout\;
\CPU|DP|rB|ALT_INV_out\(4) <= NOT \CPU|DP|rB|out\(4);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(6) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(6);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(6) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(6);
\CPU|DP|rA|ALT_INV_out\(6) <= NOT \CPU|DP|rA|out\(6);
\CPU|DP|ALT_INV_Bin[6]~11_combout\ <= NOT \CPU|DP|Bin[6]~11_combout\;
\CPU|DP|rB|ALT_INV_out\(5) <= NOT \CPU|DP|rB|out\(5);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(7) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(7);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(7) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(7);
\CPU|DP|rA|ALT_INV_out\(7) <= NOT \CPU|DP|rA|out\(7);
\CPU|DP|ALT_INV_Bin[7]~10_combout\ <= NOT \CPU|DP|Bin[7]~10_combout\;
\CPU|DP|rB|ALT_INV_out\(6) <= NOT \CPU|DP|rB|out\(6);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(8) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(8);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(8) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(8);
\CPU|DP|rA|ALT_INV_out\(8) <= NOT \CPU|DP|rA|out\(8);
\CPU|DP|ALT_INV_Bin[8]~9_combout\ <= NOT \CPU|DP|Bin[8]~9_combout\;
\CPU|DP|rB|ALT_INV_out\(7) <= NOT \CPU|DP|rB|out\(7);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(9) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(9);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(9) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(9);
\CPU|DP|rA|ALT_INV_out\(9) <= NOT \CPU|DP|rA|out\(9);
\CPU|DP|ALT_INV_Bin[9]~8_combout\ <= NOT \CPU|DP|Bin[9]~8_combout\;
\CPU|DP|rB|ALT_INV_out\(8) <= NOT \CPU|DP|rB|out\(8);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(10) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(10);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(10) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(10);
\CPU|DP|rA|ALT_INV_out\(10) <= NOT \CPU|DP|rA|out\(10);
\CPU|DP|ALT_INV_Bin[10]~7_combout\ <= NOT \CPU|DP|Bin[10]~7_combout\;
\CPU|DP|rB|ALT_INV_out\(9) <= NOT \CPU|DP|rB|out\(9);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(11) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(11);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(11) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(11);
\CPU|DP|rA|ALT_INV_out\(11) <= NOT \CPU|DP|rA|out\(11);
\CPU|DP|ALT_INV_Bin[11]~6_combout\ <= NOT \CPU|DP|Bin[11]~6_combout\;
\CPU|DP|rB|ALT_INV_out\(10) <= NOT \CPU|DP|rB|out\(10);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(12) <= NOT \CPU|DP|U2|ALU_AddSub|ai|p\(12);
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(12) <= NOT \CPU|DP|U2|ALU_AddSub|ai|g\(12);
\CPU|DP|rA|ALT_INV_out\(12) <= NOT \CPU|DP|rA|out\(12);
\CPU|DP|ALT_INV_Bin[12]~5_combout\ <= NOT \CPU|DP|Bin[12]~5_combout\;
\CPU|DP|rB|ALT_INV_out\(11) <= NOT \CPU|DP|rB|out\(11);
\CPU|DP|rA|ALT_INV_out\(13) <= NOT \CPU|DP|rA|out\(13);
\CPU|DP|U2|ALU_AddSub|ALT_INV_comb~1_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|comb~1_combout\;
\CPU|DP|ALT_INV_Bin[13]~4_combout\ <= NOT \CPU|DP|Bin[13]~4_combout\;
\CPU|DP|rB|ALT_INV_out\(12) <= NOT \CPU|DP|rB|out\(12);
\CPU|DP|ALT_INV_Ain[14]~2_combout\ <= NOT \CPU|DP|Ain[14]~2_combout\;
\CPU|DP|ALT_INV_Ain[4]~1_combout\ <= NOT \CPU|DP|Ain[4]~1_combout\;
\CPU|DP|rA|ALT_INV_out\(14) <= NOT \CPU|DP|rA|out\(14);
\CPU|DP|U2|ALU_AddSub|ALT_INV_comb~0_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|comb~0_combout\;
\CPU|DP|ALT_INV_Bin[14]~3_combout\ <= NOT \CPU|DP|Bin[14]~3_combout\;
\CPU|DP|ALT_INV_Bin[3]~2_combout\ <= NOT \CPU|DP|Bin[3]~2_combout\;
\CPU|struct_decoder|ALT_INV_shift[1]~0_combout\ <= NOT \CPU|struct_decoder|shift[1]~0_combout\;
\CPU|DP|ALT_INV_Bin[3]~1_combout\ <= NOT \CPU|DP|Bin[3]~1_combout\;
\CPU|DP|ALT_INV_Bin[3]~0_combout\ <= NOT \CPU|DP|Bin[3]~0_combout\;
\CPU|struct_reg|ALT_INV_out\(3) <= NOT \CPU|struct_reg|out\(3);
\CPU|DP|rB|ALT_INV_out\(13) <= NOT \CPU|DP|rB|out\(13);
\CPU|DP|rB|ALT_INV_out\(14) <= NOT \CPU|DP|rB|out\(14);
\CPU|DP|rB|ALT_INV_out\(15) <= NOT \CPU|DP|rB|out\(15);
\CPU|struct_reg|ALT_INV_out\(4) <= NOT \CPU|struct_reg|out\(4);
\ALT_INV_Equal5~0_combout\ <= NOT \Equal5~0_combout\;
\CPU|FSM|ALT_INV_WideOr46~0_combout\ <= NOT \CPU|FSM|WideOr46~0_combout\;
\CPU|FSM|ALT_INV_WideOr9~3_combout\ <= NOT \CPU|FSM|WideOr9~3_combout\;
\CPU|FSM|ALT_INV_always0~24_combout\ <= NOT \CPU|FSM|always0~24_combout\;
\ALT_INV_LED_load~3_combout\ <= NOT \LED_load~3_combout\;
\CPU|Data_Address|ALT_INV_out\(2) <= NOT \CPU|Data_Address|out\(2);
\CPU|program_counter|ALT_INV_out\(2) <= NOT \CPU|program_counter|out\(2);
\CPU|Data_Address|ALT_INV_out\(1) <= NOT \CPU|Data_Address|out\(1);
\CPU|program_counter|ALT_INV_out\(1) <= NOT \CPU|program_counter|out\(1);
\ALT_INV_LED_load~2_combout\ <= NOT \LED_load~2_combout\;
\CPU|Data_Address|ALT_INV_out\(0) <= NOT \CPU|Data_Address|out\(0);
\CPU|program_counter|ALT_INV_out\(0) <= NOT \CPU|program_counter|out\(0);
\CPU|Data_Address|ALT_INV_out\(8) <= NOT \CPU|Data_Address|out\(8);
\CPU|program_counter|ALT_INV_out\(8) <= NOT \CPU|program_counter|out\(8);
\ALT_INV_LED_load~1_combout\ <= NOT \LED_load~1_combout\;
\CPU|Data_Address|ALT_INV_out\(4) <= NOT \CPU|Data_Address|out\(4);
\CPU|program_counter|ALT_INV_out\(4) <= NOT \CPU|program_counter|out\(4);
\CPU|Data_Address|ALT_INV_out\(3) <= NOT \CPU|Data_Address|out\(3);
\CPU|program_counter|ALT_INV_out\(3) <= NOT \CPU|program_counter|out\(3);
\ALT_INV_LED_load~0_combout\ <= NOT \LED_load~0_combout\;
\CPU|Data_Address|ALT_INV_out\(7) <= NOT \CPU|Data_Address|out\(7);
\CPU|program_counter|ALT_INV_out\(7) <= NOT \CPU|program_counter|out\(7);
\CPU|Data_Address|ALT_INV_out\(5) <= NOT \CPU|Data_Address|out\(5);
\CPU|program_counter|ALT_INV_out\(5) <= NOT \CPU|program_counter|out\(5);
\CPU|FSM|ALT_INV_WideOr41~2_combout\ <= NOT \CPU|FSM|WideOr41~2_combout\;
\CPU|FSM|ALT_INV_always0~23_combout\ <= NOT \CPU|FSM|always0~23_combout\;
\CPU|FSM|ALT_INV_always0~22_combout\ <= NOT \CPU|FSM|always0~22_combout\;
\CPU|FSM|ALT_INV_WideNor41~combout\ <= NOT \CPU|FSM|WideNor41~combout\;
\CPU|ALT_INV_mem_addr[6]~0_combout\ <= NOT \CPU|mem_addr[6]~0_combout\;
\CPU|Data_Address|ALT_INV_out\(6) <= NOT \CPU|Data_Address|out\(6);
\CPU|program_counter|ALT_INV_out\(6) <= NOT \CPU|program_counter|out\(6);
\H3|ALT_INV_WideOr0~0_combout\ <= NOT \H3|WideOr0~0_combout\;
\CPU|DP|rC|ALT_INV_out\(15) <= NOT \CPU|DP|rC|out\(15);
\CPU|DP|rC|ALT_INV_out\(14) <= NOT \CPU|DP|rC|out\(14);
\CPU|DP|rC|ALT_INV_out\(13) <= NOT \CPU|DP|rC|out\(13);
\CPU|DP|rC|ALT_INV_out\(12) <= NOT \CPU|DP|rC|out\(12);
\H2|ALT_INV_WideOr0~0_combout\ <= NOT \H2|WideOr0~0_combout\;
\CPU|DP|rC|ALT_INV_out\(11) <= NOT \CPU|DP|rC|out\(11);
\CPU|DP|rC|ALT_INV_out\(10) <= NOT \CPU|DP|rC|out\(10);
\CPU|DP|rC|ALT_INV_out\(9) <= NOT \CPU|DP|rC|out\(9);
\CPU|DP|rC|ALT_INV_out\(8) <= NOT \CPU|DP|rC|out\(8);
\H1|ALT_INV_WideOr0~0_combout\ <= NOT \H1|WideOr0~0_combout\;
\CPU|DP|rC|ALT_INV_out\(7) <= NOT \CPU|DP|rC|out\(7);
\CPU|DP|rC|ALT_INV_out\(6) <= NOT \CPU|DP|rC|out\(6);
\CPU|DP|rC|ALT_INV_out\(5) <= NOT \CPU|DP|rC|out\(5);
\CPU|DP|rC|ALT_INV_out\(4) <= NOT \CPU|DP|rC|out\(4);
\H0|ALT_INV_WideOr0~0_combout\ <= NOT \H0|WideOr0~0_combout\;
\CPU|DP|rC|ALT_INV_out\(3) <= NOT \CPU|DP|rC|out\(3);
\CPU|DP|rC|ALT_INV_out\(2) <= NOT \CPU|DP|rC|out\(2);
\CPU|DP|rC|ALT_INV_out\(1) <= NOT \CPU|DP|rC|out\(1);
\CPU|DP|rC|ALT_INV_out\(0) <= NOT \CPU|DP|rC|out\(0);
\CPU|FSM|ALT_INV_Equal0~0_combout\ <= NOT \CPU|FSM|Equal0~0_combout\;
\CPU|FSM|ALT_INV_WideOr3~3_combout\ <= NOT \CPU|FSM|WideOr3~3_combout\;
\CPU|FSM|ALT_INV_WideOr29~1_combout\ <= NOT \CPU|FSM|WideOr29~1_combout\;
\CPU|FSM|ALT_INV_WideOr39~0_combout\ <= NOT \CPU|FSM|WideOr39~0_combout\;
\CPU|FSM|ALT_INV_WideOr3~2_combout\ <= NOT \CPU|FSM|WideOr3~2_combout\;
\CPU|FSM|ALT_INV_WideOr1~3_combout\ <= NOT \CPU|FSM|WideOr1~3_combout\;
\CPU|FSM|ALT_INV_WideOr1~2_combout\ <= NOT \CPU|FSM|WideOr1~2_combout\;
\CPU|FSM|ALT_INV_WideOr7~1_combout\ <= NOT \CPU|FSM|WideOr7~1_combout\;
\CPU|FSM|ALT_INV_WideOr7~0_combout\ <= NOT \CPU|FSM|WideOr7~0_combout\;
\CPU|FSM|ALT_INV_psel[2]~1_combout\ <= NOT \CPU|FSM|psel[2]~1_combout\;
\CPU|FSM|ALT_INV_always0~21_combout\ <= NOT \CPU|FSM|always0~21_combout\;
\CPU|FSM|ALT_INV_WideNor2~combout\ <= NOT \CPU|FSM|WideNor2~combout\;
\CPU|FSM|ALT_INV_WideOr3~1_combout\ <= NOT \CPU|FSM|WideOr3~1_combout\;
\CPU|FSM|ALT_INV_m_cmd\(1) <= NOT \CPU|FSM|m_cmd\(1);
\CPU|FSM|ALT_INV_loads~combout\ <= NOT \CPU|FSM|loads~combout\;
\CPU|FSM|ALT_INV_WideOr35~0_combout\ <= NOT \CPU|FSM|WideOr35~0_combout\;
\CPU|FSM|ALT_INV_WideOr37~1_combout\ <= NOT \CPU|FSM|WideOr37~1_combout\;
\CPU|FSM|ALT_INV_WideOr37~0_combout\ <= NOT \CPU|FSM|WideOr37~0_combout\;
\CPU|FSM|ALT_INV_WideNor37~combout\ <= NOT \CPU|FSM|WideNor37~combout\;
\CPU|FSM|ALT_INV_WideNor38~combout\ <= NOT \CPU|FSM|WideNor38~combout\;
\CPU|FSM|ALT_INV_WideNor39~combout\ <= NOT \CPU|FSM|WideNor39~combout\;
\CPU|FSM|ALT_INV_WideNor40~combout\ <= NOT \CPU|FSM|WideNor40~combout\;
\CPU|FSM|ALT_INV_always0~20_combout\ <= NOT \CPU|FSM|always0~20_combout\;
\CPU|FSM|ALT_INV_always0~19_combout\ <= NOT \CPU|FSM|always0~19_combout\;
\CPU|FSM|ALT_INV_WideOr1~1_combout\ <= NOT \CPU|FSM|WideOr1~1_combout\;
\CPU|FSM|ALT_INV_WideOr18~0_combout\ <= NOT \CPU|FSM|WideOr18~0_combout\;
\CPU|FSM|ALT_INV_WideOr29~0_combout\ <= NOT \CPU|FSM|WideOr29~0_combout\;
\CPU|FSM|ALT_INV_always0~18_combout\ <= NOT \CPU|FSM|always0~18_combout\;
\CPU|FSM|ALT_INV_always0~17_combout\ <= NOT \CPU|FSM|always0~17_combout\;
\CPU|FSM|ALT_INV_WideOr9~2_combout\ <= NOT \CPU|FSM|WideOr9~2_combout\;
\CPU|FSM|ALT_INV_WideOr9~1_combout\ <= NOT \CPU|FSM|WideOr9~1_combout\;
\CPU|FSM|ALT_INV_vsel\(3) <= NOT \CPU|FSM|vsel\(3);
\CPU|FSM|ALT_INV_WideNor28~2_combout\ <= NOT \CPU|FSM|WideNor28~2_combout\;
\CPU|FSM|ALT_INV_always0~16_combout\ <= NOT \CPU|FSM|always0~16_combout\;
\CPU|FSM|ALT_INV_WideNor6~combout\ <= NOT \CPU|FSM|WideNor6~combout\;
\CPU|FSM|ALT_INV_WideOr9~0_combout\ <= NOT \CPU|FSM|WideOr9~0_combout\;
\CPU|FSM|ALT_INV_WideOr20~0_combout\ <= NOT \CPU|FSM|WideOr20~0_combout\;
\CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~0_combout\ <= NOT \CPU|DP|U2|ALU_AddSub|ai|c[1]~0_combout\;
\CPU|FSM|ALT_INV_WideNor21~combout\ <= NOT \CPU|FSM|WideNor21~combout\;
\CPU|FSM|ALT_INV_WideNor20~combout\ <= NOT \CPU|FSM|WideNor20~combout\;
\CPU|FSM|ALT_INV_WideNor19~combout\ <= NOT \CPU|FSM|WideNor19~combout\;
\CPU|FSM|ALT_INV_bsel~combout\ <= NOT \CPU|FSM|bsel~combout\;
\CPU|FSM|ALT_INV_WideOr16~0_combout\ <= NOT \CPU|FSM|WideOr16~0_combout\;
\CPU|FSM|ALT_INV_WideOr11~0_combout\ <= NOT \CPU|FSM|WideOr11~0_combout\;
\CPU|FSM|ALT_INV_always0~15_combout\ <= NOT \CPU|FSM|always0~15_combout\;
\CPU|FSM|ALT_INV_always0~14_combout\ <= NOT \CPU|FSM|always0~14_combout\;
\CPU|FSM|ALT_INV_WideOr41~1_combout\ <= NOT \CPU|FSM|WideOr41~1_combout\;
\CPU|FSM|ALT_INV_WideOr41~0_combout\ <= NOT \CPU|FSM|WideOr41~0_combout\;
\CPU|FSM|ALT_INV_WideNor1~0_combout\ <= NOT \CPU|FSM|WideNor1~0_combout\;
\CPU|FSM|ALT_INV_always0~13_combout\ <= NOT \CPU|FSM|always0~13_combout\;
\CPU|FSM|ALT_INV_always0~12_combout\ <= NOT \CPU|FSM|always0~12_combout\;
\CPU|FSM|ALT_INV_WideNor19~0_combout\ <= NOT \CPU|FSM|WideNor19~0_combout\;
\CPU|FSM|ALT_INV_WideOr3~0_combout\ <= NOT \CPU|FSM|WideOr3~0_combout\;
\CPU|FSM|ALT_INV_always0~11_combout\ <= NOT \CPU|FSM|always0~11_combout\;
\CPU|FSM|ALT_INV_WideNor48~combout\ <= NOT \CPU|FSM|WideNor48~combout\;
\CPU|FSM|ALT_INV_WideNor48~0_combout\ <= NOT \CPU|FSM|WideNor48~0_combout\;
\CPU|FSM|ALT_INV_always0~10_combout\ <= NOT \CPU|FSM|always0~10_combout\;
\CPU|FSM|ALT_INV_WideNor46~combout\ <= NOT \CPU|FSM|WideNor46~combout\;
\CPU|FSM|ALT_INV_WideNor43~combout\ <= NOT \CPU|FSM|WideNor43~combout\;
\CPU|FSM|ALT_INV_always0~9_combout\ <= NOT \CPU|FSM|always0~9_combout\;
\CPU|FSM|ALT_INV_always0~8_combout\ <= NOT \CPU|FSM|always0~8_combout\;
\CPU|FSM|ALT_INV_WideOr1~0_combout\ <= NOT \CPU|FSM|WideOr1~0_combout\;
\CPU|FSM|ALT_INV_WideNor34~combout\ <= NOT \CPU|FSM|WideNor34~combout\;
\CPU|FSM|ALT_INV_always0~7_combout\ <= NOT \CPU|FSM|always0~7_combout\;
\CPU|FSM|ALT_INV_always0~6_combout\ <= NOT \CPU|FSM|always0~6_combout\;
\CPU|FSM|ALT_INV_WideNor29~combout\ <= NOT \CPU|FSM|WideNor29~combout\;
\CPU|FSM|ALT_INV_WideNor27~combout\ <= NOT \CPU|FSM|WideNor27~combout\;
\CPU|FSM|ALT_INV_WideNor31~0_combout\ <= NOT \CPU|FSM|WideNor31~0_combout\;
\CPU|FSM|ALT_INV_always0~5_combout\ <= NOT \CPU|FSM|always0~5_combout\;
\CPU|FSM|ALT_INV_always0~4_combout\ <= NOT \CPU|FSM|always0~4_combout\;
\CPU|FSM|ALT_INV_psel[2]~0_combout\ <= NOT \CPU|FSM|psel[2]~0_combout\;
\CPU|DP|ALT_INV_Ain[4]~0_combout\ <= NOT \CPU|DP|Ain[4]~0_combout\;
\CPU|FSM|ALT_INV_WideNor20~0_combout\ <= NOT \CPU|FSM|WideNor20~0_combout\;
\CPU|struct_decoder|ALT_INV_Equal0~0_combout\ <= NOT \CPU|struct_decoder|Equal0~0_combout\;
\CPU|FSM|ALT_INV_WideNor17~combout\ <= NOT \CPU|FSM|WideNor17~combout\;
\CPU|FSM|ALT_INV_always0~3_combout\ <= NOT \CPU|FSM|always0~3_combout\;
\CPU|FSM|ALT_INV_WideNor16~combout\ <= NOT \CPU|FSM|WideNor16~combout\;
\CPU|FSM|ALT_INV_WideNor12~combout\ <= NOT \CPU|FSM|WideNor12~combout\;
\CPU|FSM|ALT_INV_WideNor12~0_combout\ <= NOT \CPU|FSM|WideNor12~0_combout\;
\CPU|FSM|ALT_INV_always0~2_combout\ <= NOT \CPU|FSM|always0~2_combout\;
\CPU|FSM|ALT_INV_WideNor6~1_combout\ <= NOT \CPU|FSM|WideNor6~1_combout\;
\CPU|FSM|ALT_INV_WideNor13~combout\ <= NOT \CPU|FSM|WideNor13~combout\;
\CPU|FSM|ALT_INV_WideNor13~0_combout\ <= NOT \CPU|FSM|WideNor13~0_combout\;
\CPU|FSM|ALT_INV_WideNor15~0_combout\ <= NOT \CPU|FSM|WideNor15~0_combout\;
\CPU|FSM|ALT_INV_WideNor16~0_combout\ <= NOT \CPU|FSM|WideNor16~0_combout\;
\CPU|FSM|ALT_INV_WideNor26~1_combout\ <= NOT \CPU|FSM|WideNor26~1_combout\;
\CPU|FSM|ALT_INV_WideNor26~0_combout\ <= NOT \CPU|FSM|WideNor26~0_combout\;
\CPU|FSM|ALT_INV_WideNor28~combout\ <= NOT \CPU|FSM|WideNor28~combout\;
\CPU|FSM|ALT_INV_WideNor28~1_combout\ <= NOT \CPU|FSM|WideNor28~1_combout\;
\CPU|FSM|ALT_INV_WideNor28~0_combout\ <= NOT \CPU|FSM|WideNor28~0_combout\;
\CPU|FSM|ALT_INV_WideNor33~combout\ <= NOT \CPU|FSM|WideNor33~combout\;
\CPU|FSM|ALT_INV_WideNor37~0_combout\ <= NOT \CPU|FSM|WideNor37~0_combout\;
\CPU|DP|status|ALT_INV_out\(0) <= NOT \CPU|DP|status|out\(0);
\CPU|DP|status|ALT_INV_out\(1) <= NOT \CPU|DP|status|out\(1);
\CPU|FSM|ALT_INV_WideNor39~0_combout\ <= NOT \CPU|FSM|WideNor39~0_combout\;
\CPU|FSM|ALT_INV_WideNor33~1_combout\ <= NOT \CPU|FSM|WideNor33~1_combout\;
\CPU|struct_reg|ALT_INV_out\(9) <= NOT \CPU|struct_reg|out\(9);
\CPU|struct_reg|ALT_INV_out\(8) <= NOT \CPU|struct_reg|out\(8);
\CPU|FSM|ALT_INV_WideNor33~0_combout\ <= NOT \CPU|FSM|WideNor33~0_combout\;
\CPU|struct_reg|ALT_INV_out\(10) <= NOT \CPU|struct_reg|out\(10);
\CPU|FSM|ALT_INV_WideNor42~combout\ <= NOT \CPU|FSM|WideNor42~combout\;
\CPU|FSM|ALT_INV_WideNor42~0_combout\ <= NOT \CPU|FSM|WideNor42~0_combout\;
\CPU|FSM|ALT_INV_WideNor44~0_combout\ <= NOT \CPU|FSM|WideNor44~0_combout\;
\CPU|FSM|ALT_INV_WideNor47~combout\ <= NOT \CPU|FSM|WideNor47~combout\;
\CPU|FSM|ALT_INV_WideNor47~0_combout\ <= NOT \CPU|FSM|WideNor47~0_combout\;
\CPU|FSM|ALT_INV_WideNor43~0_combout\ <= NOT \CPU|FSM|WideNor43~0_combout\;
\CPU|FSM|ALT_INV_always0~1_combout\ <= NOT \CPU|FSM|always0~1_combout\;
\CPU|FSM|ALT_INV_always0~0_combout\ <= NOT \CPU|FSM|always0~0_combout\;
\CPU|FSM|ALT_INV_WideNor6~0_combout\ <= NOT \CPU|FSM|WideNor6~0_combout\;
\CPU|FSM|ALT_INV_WideNor5~1_combout\ <= NOT \CPU|FSM|WideNor5~1_combout\;
\CPU|FSM|ALT_INV_WideNor7~0_combout\ <= NOT \CPU|FSM|WideNor7~0_combout\;
\CPU|FSM|ALT_INV_WideNor5~0_combout\ <= NOT \CPU|FSM|WideNor5~0_combout\;
\CPU|FSM|ALT_INV_WideNor27~0_combout\ <= NOT \CPU|FSM|WideNor27~0_combout\;
\CPU|struct_reg|ALT_INV_out\(12) <= NOT \CPU|struct_reg|out\(12);
\CPU|struct_reg|ALT_INV_out\(11) <= NOT \CPU|struct_reg|out\(11);
\CPU|struct_reg|ALT_INV_out\(15) <= NOT \CPU|struct_reg|out\(15);
\CPU|struct_reg|ALT_INV_out\(13) <= NOT \CPU|struct_reg|out\(13);
\CPU|struct_reg|ALT_INV_out\(14) <= NOT \CPU|struct_reg|out\(14);
\CPU|FSM|ALT_INV_WideNor3~combout\ <= NOT \CPU|FSM|WideNor3~combout\;
\CPU|FSM|STATE|ALT_INV_out\(4) <= NOT \CPU|FSM|STATE|out\(4);
\CPU|FSM|STATE|ALT_INV_out\(3) <= NOT \CPU|FSM|STATE|out\(3);
\CPU|FSM|STATE|ALT_INV_out\(2) <= NOT \CPU|FSM|STATE|out\(2);
\CPU|FSM|STATE|ALT_INV_out\(1) <= NOT \CPU|FSM|STATE|out\(1);
\CPU|FSM|STATE|ALT_INV_out\(0) <= NOT \CPU|FSM|STATE|out\(0);
\CPU|FSM|ALT_INV_nsel[2]~0_combout\ <= NOT \CPU|FSM|nsel[2]~0_combout\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a1\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a2\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a3\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a4\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a5\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a6\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a7\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a8\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a9\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a10\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a11\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a12\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a13\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a14\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a15\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\;
\CPU|ALT_INV_Add0~33_sumout\ <= NOT \CPU|Add0~33_sumout\;
\CPU|ALT_INV_Add0~29_sumout\ <= NOT \CPU|Add0~29_sumout\;
\CPU|ALT_INV_Add0~25_sumout\ <= NOT \CPU|Add0~25_sumout\;
\CPU|ALT_INV_Add0~21_sumout\ <= NOT \CPU|Add0~21_sumout\;
\CPU|ALT_INV_Add0~17_sumout\ <= NOT \CPU|Add0~17_sumout\;
\CPU|ALT_INV_Add0~13_sumout\ <= NOT \CPU|Add0~13_sumout\;
\CPU|ALT_INV_Add0~9_sumout\ <= NOT \CPU|Add0~9_sumout\;
\CPU|ALT_INV_Add0~5_sumout\ <= NOT \CPU|Add0~5_sumout\;
\CPU|ALT_INV_Add0~1_sumout\ <= NOT \CPU|Add0~1_sumout\;
\CPU|DP|status|ALT_INV_out\(2) <= NOT \CPU|DP|status|out\(2);

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(0),
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(1),
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(2),
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(3),
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(4),
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(5),
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(6),
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED_register|out\(7),
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \CPU|FSM|Equal0~1_combout\,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOOBUF_X89_Y8_N39
\HEX0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(0));

-- Location: IOOBUF_X89_Y11_N79
\HEX0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(1));

-- Location: IOOBUF_X89_Y11_N96
\HEX0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(2));

-- Location: IOOBUF_X89_Y4_N79
\HEX0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(3));

-- Location: IOOBUF_X89_Y13_N56
\HEX0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(4));

-- Location: IOOBUF_X89_Y13_N39
\HEX0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(5));

-- Location: IOOBUF_X89_Y4_N96
\HEX0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(6));

-- Location: IOOBUF_X89_Y6_N39
\HEX1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(0));

-- Location: IOOBUF_X89_Y6_N56
\HEX1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(1));

-- Location: IOOBUF_X89_Y16_N39
\HEX1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(2));

-- Location: IOOBUF_X89_Y16_N56
\HEX1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(3));

-- Location: IOOBUF_X89_Y15_N39
\HEX1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(4));

-- Location: IOOBUF_X89_Y15_N56
\HEX1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(5));

-- Location: IOOBUF_X89_Y8_N56
\HEX1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(6));

-- Location: IOOBUF_X89_Y9_N22
\HEX2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(0));

-- Location: IOOBUF_X89_Y23_N39
\HEX2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(1));

-- Location: IOOBUF_X89_Y23_N56
\HEX2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(2));

-- Location: IOOBUF_X89_Y20_N79
\HEX2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(3));

-- Location: IOOBUF_X89_Y25_N39
\HEX2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(4));

-- Location: IOOBUF_X89_Y20_N96
\HEX2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(5));

-- Location: IOOBUF_X89_Y25_N56
\HEX2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(6));

-- Location: IOOBUF_X89_Y16_N5
\HEX3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(0));

-- Location: IOOBUF_X89_Y16_N22
\HEX3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(1));

-- Location: IOOBUF_X89_Y4_N45
\HEX3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(2));

-- Location: IOOBUF_X89_Y4_N62
\HEX3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(3));

-- Location: IOOBUF_X89_Y21_N39
\HEX3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(4));

-- Location: IOOBUF_X89_Y11_N62
\HEX3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(5));

-- Location: IOOBUF_X89_Y9_N5
\HEX3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(6));

-- Location: IOOBUF_X89_Y11_N45
\HEX4[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(0));

-- Location: IOOBUF_X89_Y13_N5
\HEX4[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(1));

-- Location: IOOBUF_X89_Y13_N22
\HEX4[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(2));

-- Location: IOOBUF_X89_Y8_N22
\HEX4[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(3));

-- Location: IOOBUF_X89_Y15_N22
\HEX4[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(4));

-- Location: IOOBUF_X89_Y15_N5
\HEX4[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(5));

-- Location: IOOBUF_X89_Y20_N45
\HEX4[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(6));

-- Location: IOOBUF_X89_Y20_N62
\HEX5[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \CPU|DP|status|ALT_INV_out[0]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_HEX5(0));

-- Location: IOOBUF_X89_Y21_N56
\HEX5[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX5(1));

-- Location: IOOBUF_X89_Y25_N22
\HEX5[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX5(2));

-- Location: IOOBUF_X89_Y23_N22
\HEX5[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \CPU|DP|status|ALT_INV_out\(2),
	devoe => ww_devoe,
	o => ww_HEX5(3));

-- Location: IOOBUF_X89_Y9_N56
\HEX5[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX5(4));

-- Location: IOOBUF_X89_Y23_N5
\HEX5[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX5(5));

-- Location: IOOBUF_X89_Y9_N39
\HEX5[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \CPU|DP|status|ALT_INV_out\(1),
	devoe => ww_devoe,
	o => ww_HEX5(6));

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: CLKCTRL_G6
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: FF_X83_Y6_N1
\CPU|struct_reg|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector0~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(15));

-- Location: FF_X83_Y6_N5
\CPU|struct_reg|out[14]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector1~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[14]~DUPLICATE_q\);

-- Location: MLABCELL_X82_Y6_N51
\CPU|FSM|WideNor26~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor26~0_combout\ = ( !\CPU|FSM|STATE|out\(4) & ( (!\CPU|FSM|STATE|out\(3) & (\CPU|FSM|STATE|out\(2) & \CPU|FSM|STATE|out\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(3),
	datac => \CPU|FSM|STATE|ALT_INV_out\(2),
	datad => \CPU|FSM|STATE|ALT_INV_out\(1),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(4),
	combout => \CPU|FSM|WideNor26~0_combout\);

-- Location: MLABCELL_X82_Y6_N12
\CPU|FSM|WideNor17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor17~combout\ = ( \CPU|struct_reg|out[14]~DUPLICATE_q\ & ( \CPU|FSM|WideNor27~0_combout\ & ( (!\CPU|FSM|STATE|out\(0) & (\CPU|struct_reg|out\(15) & (\CPU|FSM|WideNor26~0_combout\ & !\CPU|struct_reg|out\(13)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(0),
	datab => \CPU|struct_reg|ALT_INV_out\(15),
	datac => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(13),
	datae => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	dataf => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	combout => \CPU|FSM|WideNor17~combout\);

-- Location: FF_X83_Y6_N32
\CPU|struct_reg|out[13]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector2~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[13]~DUPLICATE_q\);

-- Location: LABCELL_X83_Y6_N39
\CPU|FSM|WideNor13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor13~0_combout\ = ( !\CPU|struct_reg|out[13]~DUPLICATE_q\ & ( (!\CPU|struct_reg|out[15]~DUPLICATE_q\ & \CPU|struct_reg|out[14]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor13~0_combout\);

-- Location: FF_X83_Y6_N55
\CPU|struct_reg|out[11]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector4~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[11]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y6_N9
\CPU|FSM|WideNor12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor12~0_combout\ = ( \CPU|struct_reg|out[11]~DUPLICATE_q\ & ( (!\CPU|FSM|STATE|out\(1) & (\CPU|FSM|STATE|out\(2) & \CPU|struct_reg|out\(12))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|STATE|ALT_INV_out\(2),
	datad => \CPU|struct_reg|ALT_INV_out\(12),
	dataf => \CPU|struct_reg|ALT_INV_out[11]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor12~0_combout\);

-- Location: LABCELL_X81_Y6_N51
\CPU|FSM|WideNor12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor12~combout\ = ( \CPU|FSM|WideNor13~0_combout\ & ( \CPU|FSM|WideNor12~0_combout\ & ( (!\CPU|FSM|STATE|out\(4) & (!\CPU|FSM|STATE|out\(0) & !\CPU|FSM|STATE|out\(3))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(4),
	datac => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	datae => \CPU|FSM|ALT_INV_WideNor13~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor12~0_combout\,
	combout => \CPU|FSM|WideNor12~combout\);

-- Location: MLABCELL_X82_Y6_N30
\CPU|FSM|WideNor5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor5~0_combout\ = ( !\CPU|FSM|STATE|out\(4) & ( (!\CPU|FSM|STATE|out\(1) & (\CPU|FSM|STATE|out\(2) & (!\CPU|FSM|STATE|out\(0) & !\CPU|FSM|STATE|out\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(1),
	datab => \CPU|FSM|STATE|ALT_INV_out\(2),
	datac => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(4),
	combout => \CPU|FSM|WideNor5~0_combout\);

-- Location: LABCELL_X83_Y6_N42
\CPU|FSM|WideNor13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor13~combout\ = ( \CPU|FSM|WideNor5~0_combout\ & ( (\CPU|FSM|WideNor27~0_combout\ & \CPU|FSM|WideNor13~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor13~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	combout => \CPU|FSM|WideNor13~combout\);

-- Location: LABCELL_X83_Y6_N21
\CPU|FSM|WideNor5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor5~1_combout\ = ( \CPU|struct_reg|out[13]~DUPLICATE_q\ & ( (!\CPU|struct_reg|out[14]~DUPLICATE_q\ & \CPU|struct_reg|out[15]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor5~1_combout\);

-- Location: LABCELL_X83_Y6_N15
\CPU|FSM|WideNor16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor16~combout\ = ( !\CPU|FSM|STATE|out\(0) & ( (!\CPU|struct_reg|out\(12) & (\CPU|struct_reg|out[11]~DUPLICATE_q\ & (\CPU|FSM|WideNor26~0_combout\ & \CPU|FSM|WideNor5~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000010000000000000001000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(12),
	datab => \CPU|struct_reg|ALT_INV_out[11]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	dataf => \CPU|FSM|STATE|ALT_INV_out\(0),
	combout => \CPU|FSM|WideNor16~combout\);

-- Location: LABCELL_X83_Y7_N42
\CPU|FSM|always0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~3_combout\ = ( !\CPU|FSM|WideNor16~combout\ & ( (!\CPU|FSM|WideNor12~combout\ & (\CPU|FSM|always0~2_combout\ & (!\CPU|FSM|WideNor15~0_combout\ & !\CPU|FSM|WideNor13~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datab => \CPU|FSM|ALT_INV_always0~2_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor13~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor16~combout\,
	combout => \CPU|FSM|always0~3_combout\);

-- Location: LABCELL_X83_Y7_N3
\CPU|FSM|WideNor16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor16~0_combout\ = ( \CPU|FSM|WideNor5~1_combout\ & ( (!\CPU|FSM|STATE|out\(0) & \CPU|FSM|WideNor26~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	combout => \CPU|FSM|WideNor16~0_combout\);

-- Location: LABCELL_X83_Y6_N9
\CPU|struct_decoder|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|Equal0~0_combout\ = ( !\CPU|struct_reg|out[13]~DUPLICATE_q\ & ( (!\CPU|struct_reg|out[14]~DUPLICATE_q\ & \CPU|struct_reg|out[15]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	combout => \CPU|struct_decoder|Equal0~0_combout\);

-- Location: LABCELL_X83_Y6_N51
\CPU|FSM|WideNor20~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor20~0_combout\ = ( !\CPU|struct_reg|out[13]~DUPLICATE_q\ & ( (\CPU|struct_reg|out[14]~DUPLICATE_q\ & (!\CPU|struct_reg|out[11]~DUPLICATE_q\ & !\CPU|struct_reg|out[15]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datac => \CPU|struct_reg|ALT_INV_out[11]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor20~0_combout\);

-- Location: LABCELL_X83_Y6_N48
\CPU|DP|Ain[4]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[4]~0_combout\ = ( !\CPU|FSM|STATE|out\(0) & ( (\CPU|FSM|WideNor26~0_combout\ & ((\CPU|FSM|WideNor20~0_combout\) # (\CPU|struct_decoder|Equal0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000111111000000000011111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor20~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	dataf => \CPU|FSM|STATE|ALT_INV_out\(0),
	combout => \CPU|DP|Ain[4]~0_combout\);

-- Location: LABCELL_X83_Y7_N15
\CPU|FSM|always0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~21_combout\ = ( !\CPU|DP|Ain[4]~0_combout\ & ( (!\CPU|FSM|WideNor17~combout\ & (\CPU|FSM|always0~3_combout\ & !\CPU|FSM|WideNor16~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datac => \CPU|FSM|ALT_INV_always0~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~0_combout\,
	combout => \CPU|FSM|always0~21_combout\);

-- Location: LABCELL_X81_Y8_N15
\CPU|FSM|WideNor26~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor26~1_combout\ = ( \CPU|FSM|WideNor26~0_combout\ & ( \CPU|FSM|STATE|out\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	dataf => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	combout => \CPU|FSM|WideNor26~1_combout\);

-- Location: LABCELL_X83_Y7_N54
\CPU|FSM|WideOr3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr3~2_combout\ = ( \CPU|struct_decoder|Equal0~0_combout\ & ( \CPU|FSM|WideNor26~1_combout\ ) ) # ( !\CPU|struct_decoder|Equal0~0_combout\ & ( (\CPU|FSM|always0~21_combout\ & (\CPU|FSM|WideNor28~0_combout\ & \CPU|FSM|WideNor26~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_always0~21_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor28~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor26~1_combout\,
	dataf => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	combout => \CPU|FSM|WideOr3~2_combout\);

-- Location: MLABCELL_X82_Y8_N18
\CPU|FSM|WideNor29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor29~combout\ = ( \CPU|struct_decoder|Equal0~0_combout\ & ( \CPU|FSM|STATE|out\(2) & ( (\CPU|FSM|STATE|out\(3) & (!\CPU|FSM|STATE|out\(1) & (!\CPU|FSM|STATE|out\(4) & !\CPU|FSM|STATE|out\(0)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(3),
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	datae => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	dataf => \CPU|FSM|STATE|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor29~combout\);

-- Location: MLABCELL_X82_Y8_N51
\CPU|FSM|WideNor28~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor28~1_combout\ = ( \CPU|FSM|STATE|out\(2) & ( (!\CPU|FSM|STATE|out\(1) & (!\CPU|FSM|STATE|out\(4) & \CPU|FSM|STATE|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor28~1_combout\);

-- Location: LABCELL_X83_Y8_N15
\CPU|FSM|WideNor28~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor28~2_combout\ = ( \CPU|FSM|WideNor28~1_combout\ & ( \CPU|FSM|WideNor28~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|ALT_INV_WideNor28~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor28~1_combout\,
	combout => \CPU|FSM|WideNor28~2_combout\);

-- Location: MLABCELL_X82_Y5_N30
\CPU|FSM|psel[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|psel[2]~0_combout\ = ( \CPU|FSM|WideNor20~0_combout\ & ( \CPU|FSM|WideNor26~0_combout\ & ( \CPU|FSM|STATE|out\(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	datae => \CPU|FSM|ALT_INV_WideNor20~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	combout => \CPU|FSM|psel[2]~0_combout\);

-- Location: LABCELL_X83_Y6_N45
\CPU|FSM|always0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~4_combout\ = ( \CPU|FSM|WideNor28~0_combout\ & ( (\CPU|FSM|WideNor26~0_combout\ & \CPU|FSM|STATE|out\(0)) ) ) # ( !\CPU|FSM|WideNor28~0_combout\ & ( (\CPU|FSM|WideNor26~0_combout\ & (\CPU|struct_decoder|Equal0~0_combout\ & 
-- \CPU|FSM|STATE|out\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datac => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	dataf => \CPU|FSM|ALT_INV_WideNor28~0_combout\,
	combout => \CPU|FSM|always0~4_combout\);

-- Location: MLABCELL_X82_Y6_N57
\CPU|FSM|always0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~29_combout\ = ( !\CPU|FSM|WideNor15~0_combout\ & ( (!\CPU|FSM|WideNor12~combout\ & (!\CPU|FSM|WideNor13~combout\ & (!\CPU|FSM|always0~4_combout\ & !\CPU|FSM|WideNor16~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datac => \CPU|FSM|ALT_INV_always0~4_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor16~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|FSM|always0~29_combout\);

-- Location: MLABCELL_X82_Y6_N6
\CPU|FSM|always0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~5_combout\ = ( \CPU|FSM|always0~2_combout\ & ( \CPU|FSM|always0~29_combout\ & ( (!\CPU|FSM|WideNor16~0_combout\ & (!\CPU|FSM|WideNor17~combout\ & (!\CPU|FSM|psel[2]~0_combout\ & !\CPU|DP|Ain[4]~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datac => \CPU|FSM|ALT_INV_psel[2]~0_combout\,
	datad => \CPU|DP|ALT_INV_Ain[4]~0_combout\,
	datae => \CPU|FSM|ALT_INV_always0~2_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~29_combout\,
	combout => \CPU|FSM|always0~5_combout\);

-- Location: LABCELL_X81_Y6_N12
\CPU|FSM|WideOr29~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr29~1_combout\ = ( \CPU|FSM|WideNor27~combout\ & ( \CPU|FSM|always0~5_combout\ & ( (\CPU|FSM|STATE|out\(0) & \CPU|FSM|WideNor26~0_combout\) ) ) ) # ( !\CPU|FSM|WideNor27~combout\ & ( \CPU|FSM|always0~5_combout\ & ( (\CPU|FSM|STATE|out\(0) & 
-- (((!\CPU|FSM|WideNor29~combout\ & \CPU|FSM|WideNor28~2_combout\)) # (\CPU|FSM|WideNor26~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000011001000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor29~combout\,
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor28~2_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor27~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|WideOr29~1_combout\);

-- Location: LABCELL_X83_Y6_N24
\CPU|FSM|WideNor6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor6~0_combout\ = ( !\CPU|struct_reg|out[13]~DUPLICATE_q\ & ( (!\CPU|struct_reg|out\(11) & (\CPU|struct_reg|out\(12) & \CPU|struct_reg|out[14]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor6~0_combout\);

-- Location: LABCELL_X80_Y6_N24
\CPU|FSM|WideNor6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor6~combout\ = ( \CPU|struct_reg|out\(15) & ( \CPU|FSM|WideNor5~0_combout\ & ( \CPU|FSM|WideNor6~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|ALT_INV_WideNor6~0_combout\,
	datae => \CPU|struct_reg|ALT_INV_out\(15),
	dataf => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	combout => \CPU|FSM|WideNor6~combout\);

-- Location: LABCELL_X81_Y7_N57
\CPU|FSM|always0~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~14_combout\ = ( \CPU|FSM|always0~5_combout\ & ( (!\CPU|FSM|WideNor27~combout\ & (\CPU|FSM|WideNor28~combout\ & !\CPU|FSM|WideNor26~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor27~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor28~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor26~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|always0~14_combout\);

-- Location: LABCELL_X85_Y7_N33
\CPU|FSM|WideOr3~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr3~3_combout\ = ( !\CPU|FSM|always0~14_combout\ & ( (!\CPU|FSM|WideOr3~2_combout\ & (!\CPU|FSM|WideOr29~1_combout\ & !\CPU|FSM|WideNor6~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr3~2_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor6~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~14_combout\,
	combout => \CPU|FSM|WideOr3~3_combout\);

-- Location: MLABCELL_X82_Y6_N45
\CPU|FSM|state_next_reset[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|state_next_reset[3]~3_combout\ = ( \CPU|FSM|WideOr3~3_combout\ & ( (!\KEY[1]~input_o\) # ((!\CPU|FSM|WideOr3~1_combout\) # (!\CPU|FSM|WideOr3~0_combout\)) ) ) # ( !\CPU|FSM|WideOr3~3_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111001111111111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_KEY[1]~input_o\,
	datac => \CPU|FSM|ALT_INV_WideOr3~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr3~3_combout\,
	combout => \CPU|FSM|state_next_reset[3]~3_combout\);

-- Location: FF_X82_Y6_N47
\CPU|FSM|STATE|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|state_next_reset[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|STATE|out\(3));

-- Location: LABCELL_X83_Y5_N3
\CPU|FSM|WideNor15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor15~0_combout\ = ( !\CPU|FSM|STATE|out\(4) & ( !\CPU|FSM|STATE|out\(1) & ( (!\CPU|FSM|STATE|out\(3) & (\CPU|FSM|STATE|out\(0) & \CPU|FSM|STATE|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(3),
	datac => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|STATE|ALT_INV_out\(2),
	datae => \CPU|FSM|STATE|ALT_INV_out\(4),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(1),
	combout => \CPU|FSM|WideNor15~0_combout\);

-- Location: LABCELL_X81_Y6_N27
\CPU|FSM|always0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~17_combout\ = ( \CPU|FSM|WideNor13~combout\ ) # ( !\CPU|FSM|WideNor13~combout\ & ( (!\CPU|FSM|always0~2_combout\) # (\CPU|FSM|WideNor12~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111101010101111111110101010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datad => \CPU|FSM|ALT_INV_always0~2_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor13~combout\,
	combout => \CPU|FSM|always0~17_combout\);

-- Location: MLABCELL_X84_Y5_N12
\CPU|FSM|loads\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|loads~combout\ = ( \CPU|FSM|WideNor16~combout\ & ( !\CPU|FSM|always0~17_combout\ & ( !\CPU|FSM|WideNor15~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor16~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~17_combout\,
	combout => \CPU|FSM|loads~combout\);

-- Location: LABCELL_X83_Y7_N12
\CPU|FSM|WideNor19~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor19~0_combout\ = ( !\CPU|FSM|STATE|out\(0) & ( (!\CPU|struct_reg|out\(14) & (!\CPU|struct_reg|out\(13) & \CPU|struct_reg|out[15]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(14),
	datac => \CPU|struct_reg|ALT_INV_out\(13),
	datad => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	dataf => \CPU|FSM|STATE|ALT_INV_out\(0),
	combout => \CPU|FSM|WideNor19~0_combout\);

-- Location: LABCELL_X83_Y7_N51
\CPU|FSM|WideNor19\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor19~combout\ = ( \CPU|FSM|WideNor19~0_combout\ & ( \CPU|FSM|WideNor26~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor19~0_combout\,
	combout => \CPU|FSM|WideNor19~combout\);

-- Location: LABCELL_X83_Y7_N21
\CPU|FSM|always0~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~16_combout\ = ( \CPU|FSM|WideNor19~combout\ & ( (!\CPU|FSM|WideNor16~0_combout\ & (!\CPU|FSM|WideNor17~combout\ & \CPU|FSM|always0~3_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000000010000000100000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datac => \CPU|FSM|ALT_INV_always0~3_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor19~combout\,
	combout => \CPU|FSM|always0~16_combout\);

-- Location: LABCELL_X85_Y7_N30
\CPU|FSM|WideOr3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr3~1_combout\ = ( !\CPU|FSM|m_cmd\(1) & ( (!\CPU|FSM|loads~combout\ & !\CPU|FSM|always0~16_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_loads~combout\,
	datad => \CPU|FSM|ALT_INV_always0~16_combout\,
	dataf => \CPU|FSM|ALT_INV_m_cmd\(1),
	combout => \CPU|FSM|WideOr3~1_combout\);

-- Location: LABCELL_X83_Y6_N36
\CPU|FSM|WideNor42~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor42~0_combout\ = ( \CPU|struct_reg|out\(13) & ( (!\CPU|struct_reg|out[15]~DUPLICATE_q\ & !\CPU|struct_reg|out[14]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out\(13),
	combout => \CPU|FSM|WideNor42~0_combout\);

-- Location: LABCELL_X83_Y8_N18
\CPU|FSM|WideNor42\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor42~combout\ = ( \CPU|FSM|WideNor42~0_combout\ & ( \CPU|FSM|WideNor5~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor42~0_combout\,
	combout => \CPU|FSM|WideNor42~combout\);

-- Location: MLABCELL_X82_Y5_N12
\CPU|FSM|WideNor43~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor43~0_combout\ = ( !\CPU|struct_reg|out\(15) & ( \CPU|FSM|STATE|out\(4) & ( (!\CPU|FSM|STATE|out\(3) & (\CPU|struct_reg|out[14]~DUPLICATE_q\ & !\CPU|struct_reg|out\(13))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(3),
	datac => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out\(13),
	datae => \CPU|struct_reg|ALT_INV_out\(15),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(4),
	combout => \CPU|FSM|WideNor43~0_combout\);

-- Location: MLABCELL_X82_Y5_N42
\CPU|FSM|WideNor43\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor43~combout\ = ( !\CPU|struct_reg|out\(11) & ( \CPU|FSM|WideNor43~0_combout\ & ( (!\CPU|FSM|STATE|out\(1) & (\CPU|FSM|STATE|out\(0) & (\CPU|FSM|STATE|out\(2) & \CPU|struct_reg|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(1),
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|FSM|STATE|ALT_INV_out\(2),
	datad => \CPU|struct_reg|ALT_INV_out\(12),
	datae => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|FSM|ALT_INV_WideNor43~0_combout\,
	combout => \CPU|FSM|WideNor43~combout\);

-- Location: MLABCELL_X82_Y5_N18
\CPU|FSM|WideNor47~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor47~0_combout\ = ( \CPU|FSM|STATE|out\(1) & ( \CPU|FSM|WideNor43~0_combout\ & ( (!\CPU|FSM|STATE|out\(0) & !\CPU|FSM|STATE|out\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|STATE|ALT_INV_out\(2),
	datae => \CPU|FSM|STATE|ALT_INV_out\(1),
	dataf => \CPU|FSM|ALT_INV_WideNor43~0_combout\,
	combout => \CPU|FSM|WideNor47~0_combout\);

-- Location: MLABCELL_X82_Y5_N3
\CPU|FSM|WideNor44~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor44~0_combout\ = ( !\CPU|struct_reg|out\(11) & ( \CPU|FSM|WideNor47~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|FSM|ALT_INV_WideNor47~0_combout\,
	combout => \CPU|FSM|WideNor44~0_combout\);

-- Location: MLABCELL_X82_Y8_N57
\CPU|FSM|WideNor31~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor31~0_combout\ = ( !\CPU|FSM|STATE|out\(2) & ( (!\CPU|FSM|STATE|out\(4) & \CPU|FSM|STATE|out\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor31~0_combout\);

-- Location: MLABCELL_X82_Y5_N39
\CPU|FSM|WideNor1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor1~0_combout\ = ( !\CPU|FSM|STATE|out\(3) & ( !\CPU|FSM|STATE|out\(2) & ( !\CPU|FSM|STATE|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|STATE|ALT_INV_out\(4),
	datae => \CPU|FSM|STATE|ALT_INV_out\(3),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor1~0_combout\);

-- Location: MLABCELL_X82_Y8_N33
\CPU|FSM|WideOr41~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~0_combout\ = ( !\CPU|FSM|STATE|out\(1) & ( ((\CPU|FSM|WideNor1~0_combout\ & !\CPU|FSM|STATE|out\(0))) # (\CPU|FSM|WideNor31~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100110011001111110011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor31~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor1~0_combout\,
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(1),
	combout => \CPU|FSM|WideOr41~0_combout\);

-- Location: MLABCELL_X82_Y8_N3
\CPU|FSM|always0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~13_combout\ = ( !\CPU|FSM|STATE|out\(2) & ( (!\CPU|FSM|STATE|out\(0) & (\CPU|FSM|STATE|out\(1) & (!\CPU|FSM|STATE|out\(4) & \CPU|FSM|STATE|out\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(0),
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(2),
	combout => \CPU|FSM|always0~13_combout\);

-- Location: LABCELL_X80_Y6_N51
\CPU|FSM|WideNor47\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor47~combout\ = ( \CPU|FSM|WideNor47~0_combout\ & ( (\CPU|struct_reg|out\(12) & \CPU|struct_reg|out\(11)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000101010100000000000000000000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|FSM|ALT_INV_WideNor47~0_combout\,
	combout => \CPU|FSM|WideNor47~combout\);

-- Location: LABCELL_X81_Y8_N27
\CPU|FSM|WideOr41~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~3_combout\ = ( !\CPU|FSM|WideNor46~combout\ & ( ((\CPU|FSM|WideNor47~combout\) # (\CPU|FSM|always0~13_combout\)) # (\CPU|FSM|WideNor48~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111111111111010111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor48~combout\,
	datac => \CPU|FSM|ALT_INV_always0~13_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor47~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor46~combout\,
	combout => \CPU|FSM|WideOr41~3_combout\);

-- Location: LABCELL_X81_Y8_N30
\CPU|FSM|WideOr41~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~1_combout\ = ( \CPU|FSM|WideOr41~3_combout\ & ( \CPU|FSM|always0~9_combout\ & ( (!\CPU|FSM|WideOr41~0_combout\ & (\CPU|FSM|WideNor43~combout\ & !\CPU|FSM|WideNor42~combout\)) ) ) ) # ( !\CPU|FSM|WideOr41~3_combout\ & ( 
-- \CPU|FSM|always0~9_combout\ & ( (!\CPU|FSM|WideOr41~0_combout\ & (!\CPU|FSM|WideNor42~combout\ & ((!\CPU|FSM|WideNor44~0_combout\) # (\CPU|FSM|WideNor43~combout\)))) ) ) ) # ( \CPU|FSM|WideOr41~3_combout\ & ( !\CPU|FSM|always0~9_combout\ & ( 
-- !\CPU|FSM|WideOr41~0_combout\ ) ) ) # ( !\CPU|FSM|WideOr41~3_combout\ & ( !\CPU|FSM|always0~9_combout\ & ( !\CPU|FSM|WideOr41~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010100000001000000010000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr41~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor44~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr41~3_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~9_combout\,
	combout => \CPU|FSM|WideOr41~1_combout\);

-- Location: LABCELL_X81_Y8_N36
\CPU|FSM|vsel[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|vsel\(3) = ( \CPU|FSM|WideNor28~2_combout\ & ( \CPU|FSM|always0~5_combout\ & ( (!\CPU|FSM|WideNor26~0_combout\ & (\CPU|FSM|STATE|out\(0) & (!\CPU|FSM|WideNor27~combout\ & !\CPU|FSM|WideNor29~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|FSM|ALT_INV_WideNor27~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor29~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor28~2_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|vsel\(3));

-- Location: LABCELL_X85_Y7_N9
\CPU|FSM|WideOr9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr9~1_combout\ = ( !\CPU|FSM|WideNor6~combout\ & ( (!\CPU|FSM|vsel\(3) & !\CPU|FSM|always0~16_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_vsel\(3),
	datad => \CPU|FSM|ALT_INV_always0~16_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor6~combout\,
	combout => \CPU|FSM|WideOr9~1_combout\);

-- Location: MLABCELL_X82_Y6_N54
\CPU|FSM|WideNor6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor6~1_combout\ = ( \CPU|FSM|WideNor5~0_combout\ & ( \CPU|FSM|WideNor6~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|ALT_INV_WideNor6~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	combout => \CPU|FSM|WideNor6~1_combout\);

-- Location: MLABCELL_X82_Y6_N33
\CPU|FSM|WideNor7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor7~0_combout\ = ( \CPU|FSM|WideNor5~0_combout\ & ( \CPU|FSM|WideNor27~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	combout => \CPU|FSM|WideNor7~0_combout\);

-- Location: LABCELL_X80_Y6_N21
\CPU|FSM|always0~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~15_combout\ = ( \CPU|FSM|WideNor7~0_combout\ & ( !\CPU|FSM|always0~0_combout\ & ( (!\CPU|struct_reg|out\(13) & (!\CPU|struct_reg|out\(15))) # (\CPU|struct_reg|out\(13) & ((!\CPU|struct_reg|out\(14)))) ) ) ) # ( 
-- !\CPU|FSM|WideNor7~0_combout\ & ( !\CPU|FSM|always0~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111101011001010110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(15),
	datab => \CPU|struct_reg|ALT_INV_out\(14),
	datac => \CPU|struct_reg|ALT_INV_out\(13),
	datae => \CPU|FSM|ALT_INV_WideNor7~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~0_combout\,
	combout => \CPU|FSM|always0~15_combout\);

-- Location: LABCELL_X80_Y6_N0
\CPU|FSM|WideOr11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr11~0_combout\ = ( \CPU|FSM|always0~15_combout\ & ( ((!\CPU|struct_reg|out\(15) & \CPU|FSM|WideNor6~1_combout\)) # (\CPU|FSM|WideNor12~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011111010111100000000000000000000111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(15),
	datac => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor6~1_combout\,
	datae => \CPU|FSM|ALT_INV_always0~15_combout\,
	combout => \CPU|FSM|WideOr11~0_combout\);

-- Location: LABCELL_X80_Y6_N42
\CPU|FSM|WideOr16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr16~0_combout\ = ( \CPU|struct_reg|out\(13) & ( \CPU|FSM|always0~0_combout\ & ( (\CPU|FSM|WideNor5~0_combout\ & (!\CPU|struct_reg|out\(14) & \CPU|struct_reg|out\(15))) ) ) ) # ( \CPU|struct_reg|out\(13) & ( !\CPU|FSM|always0~0_combout\ & ( 
-- (\CPU|FSM|WideNor5~0_combout\ & ((!\CPU|struct_reg|out\(14) & (\CPU|struct_reg|out\(15))) # (\CPU|struct_reg|out\(14) & (!\CPU|struct_reg|out\(15) & \CPU|FSM|WideNor27~0_combout\)))) ) ) ) # ( !\CPU|struct_reg|out\(13) & ( !\CPU|FSM|always0~0_combout\ & ( 
-- (\CPU|FSM|WideNor5~0_combout\ & (!\CPU|struct_reg|out\(14) & (\CPU|struct_reg|out\(15) & \CPU|FSM|WideNor27~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000100000001000001010000000000000000000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	datab => \CPU|struct_reg|ALT_INV_out\(14),
	datac => \CPU|struct_reg|ALT_INV_out\(15),
	datad => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	datae => \CPU|struct_reg|ALT_INV_out\(13),
	dataf => \CPU|FSM|ALT_INV_always0~0_combout\,
	combout => \CPU|FSM|WideOr16~0_combout\);

-- Location: LABCELL_X83_Y7_N36
\CPU|FSM|WideNor21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor21~combout\ = ( \CPU|FSM|WideNor20~0_combout\ & ( (\CPU|FSM|WideNor26~0_combout\ & (!\CPU|FSM|STATE|out\(0) & \CPU|struct_reg|out\(12))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000100000001000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	dataf => \CPU|FSM|ALT_INV_WideNor20~0_combout\,
	combout => \CPU|FSM|WideNor21~combout\);

-- Location: LABCELL_X83_Y7_N39
\CPU|FSM|WideNor20\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor20~combout\ = ( \CPU|FSM|WideNor20~0_combout\ & ( (\CPU|FSM|WideNor26~0_combout\ & (!\CPU|FSM|STATE|out\(0) & !\CPU|struct_reg|out\(12))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001000000010000000100000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor26~0_combout\,
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	dataf => \CPU|FSM|ALT_INV_WideNor20~0_combout\,
	combout => \CPU|FSM|WideNor20~combout\);

-- Location: LABCELL_X83_Y7_N24
\CPU|DP|U2|ALU_AddSub|ai|c[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c[1]~0_combout\ = ( \CPU|FSM|WideNor16~0_combout\ & ( \CPU|FSM|always0~3_combout\ & ( \CPU|FSM|WideNor17~combout\ ) ) ) # ( !\CPU|FSM|WideNor16~0_combout\ & ( \CPU|FSM|always0~3_combout\ & ( ((!\CPU|FSM|WideNor19~combout\ & 
-- ((\CPU|FSM|WideNor20~combout\) # (\CPU|FSM|WideNor21~combout\)))) # (\CPU|FSM|WideNor17~combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000111011101110110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor19~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor21~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor20~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~3_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c[1]~0_combout\);

-- Location: LABCELL_X83_Y7_N45
\CPU|FSM|bsel\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|bsel~combout\ = ( !\CPU|FSM|WideNor5~1_combout\ & ( (!\CPU|FSM|WideNor12~combout\ & (\CPU|FSM|always0~2_combout\ & (!\CPU|FSM|WideNor13~combout\ & \CPU|FSM|WideNor15~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datab => \CPU|FSM|ALT_INV_always0~2_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	combout => \CPU|FSM|bsel~combout\);

-- Location: LABCELL_X83_Y7_N18
\CPU|FSM|WideOr20~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr20~0_combout\ = ( \CPU|FSM|always0~3_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|c[1]~0_combout\ & (!\CPU|FSM|bsel~combout\ & ((!\CPU|FSM|WideNor16~0_combout\) # (\CPU|FSM|WideNor17~combout\)))) ) ) # ( !\CPU|FSM|always0~3_combout\ & ( 
-- (!\CPU|DP|U2|ALU_AddSub|ai|c[1]~0_combout\ & !\CPU|FSM|bsel~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000010110000000000001011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~0_combout\,
	datad => \CPU|FSM|ALT_INV_bsel~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~3_combout\,
	combout => \CPU|FSM|WideOr20~0_combout\);

-- Location: LABCELL_X85_Y7_N3
\CPU|FSM|WideOr9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr9~0_combout\ = ( \CPU|FSM|WideOr20~0_combout\ & ( !\CPU|FSM|always0~14_combout\ & ( (!\CPU|FSM|WideOr11~0_combout\ & !\CPU|FSM|WideOr16~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110000001100000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr16~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr20~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~14_combout\,
	combout => \CPU|FSM|WideOr9~0_combout\);

-- Location: LABCELL_X83_Y7_N0
\CPU|FSM|always0~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~11_combout\ = ( !\CPU|FSM|STATE|out\(4) & ( (\CPU|FSM|STATE|out\(3) & (\CPU|FSM|STATE|out\(1) & \CPU|FSM|STATE|out\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(3),
	datac => \CPU|FSM|STATE|ALT_INV_out\(1),
	datad => \CPU|FSM|STATE|ALT_INV_out\(2),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(4),
	combout => \CPU|FSM|always0~11_combout\);

-- Location: LABCELL_X81_Y8_N12
\CPU|FSM|always0~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~10_combout\ = ( \CPU|FSM|always0~9_combout\ & ( (!\CPU|FSM|WideNor46~combout\ & (!\CPU|FSM|WideNor43~combout\ & (!\CPU|FSM|WideNor42~combout\ & !\CPU|FSM|WideNor44~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor46~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor44~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~9_combout\,
	combout => \CPU|FSM|always0~10_combout\);

-- Location: LABCELL_X81_Y8_N42
\CPU|FSM|always0~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~12_combout\ = ( \CPU|FSM|always0~10_combout\ & ( (!\CPU|FSM|WideNor48~combout\ & (\CPU|FSM|always0~11_combout\ & (\CPU|FSM|WideNor19~0_combout\ & !\CPU|FSM|WideNor47~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000010000000000000001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor48~combout\,
	datab => \CPU|FSM|ALT_INV_always0~11_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor19~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor47~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~10_combout\,
	combout => \CPU|FSM|always0~12_combout\);

-- Location: LABCELL_X85_Y7_N48
\CPU|FSM|WideOr39~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr39~3_combout\ = ( !\CPU|FSM|always0~12_combout\ & ( \CPU|FSM|WideOr39~2_combout\ & ( (\CPU|FSM|WideOr9~1_combout\ & (\CPU|FSM|WideOr41~1_combout\ & (\CPU|FSM|WideOr9~0_combout\ & \CPU|FSM|WideOr3~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr9~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr9~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	datae => \CPU|FSM|ALT_INV_always0~12_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	combout => \CPU|FSM|WideOr39~3_combout\);

-- Location: FF_X87_Y8_N11
\CPU|program_counter|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(1),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(1));

-- Location: IOIBUF_X12_Y0_N18
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: MLABCELL_X84_Y6_N24
\CPU|DP|Ain[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[4]~4_combout\ = ( !\CPU|FSM|WideNor15~0_combout\ & ( (!\CPU|FSM|WideNor16~combout\ & !\CPU|FSM|WideNor12~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110000000000110011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor16~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor12~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|DP|Ain[4]~4_combout\);

-- Location: MLABCELL_X84_Y6_N42
\CPU|DP|Ain[4]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[4]~1_combout\ = ( \CPU|FSM|WideNor16~0_combout\ & ( \CPU|DP|Ain[4]~4_combout\ & ( (\CPU|FSM|WideNor17~combout\ & (\CPU|FSM|always0~2_combout\ & !\CPU|FSM|WideNor13~combout\)) ) ) ) # ( !\CPU|FSM|WideNor16~0_combout\ & ( 
-- \CPU|DP|Ain[4]~4_combout\ & ( (\CPU|FSM|always0~2_combout\ & (!\CPU|FSM|WideNor13~combout\ & ((\CPU|DP|Ain[4]~0_combout\) # (\CPU|FSM|WideNor17~combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000111000000000000010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datab => \CPU|DP|ALT_INV_Ain[4]~0_combout\,
	datac => \CPU|FSM|ALT_INV_always0~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~4_combout\,
	combout => \CPU|DP|Ain[4]~1_combout\);

-- Location: IOIBUF_X8_Y0_N35
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: FF_X85_Y6_N26
\CPU|DP|rB|out[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector13~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out[2]~DUPLICATE_q\);

-- Location: LABCELL_X80_Y8_N54
\Equal5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal5~1_combout\ = ( \Equal5~0_combout\ & ( \LED_load~1_combout\ & ( (\LED_load~0_combout\ & (\LED_load~2_combout\ & \LED_load~3_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~2_combout\,
	datad => \ALT_INV_LED_load~3_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \ALT_INV_LED_load~1_combout\,
	combout => \Equal5~1_combout\);

-- Location: IOIBUF_X16_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: LABCELL_X83_Y7_N9
\CPU|FSM|always0~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~26_combout\ = ( \CPU|FSM|always0~5_combout\ & ( \CPU|FSM|WideNor26~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor26~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|always0~26_combout\);

-- Location: LABCELL_X83_Y7_N6
\CPU|FSM|WideOr26\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr26~combout\ = ( !\CPU|FSM|always0~26_combout\ & ( (!\CPU|FSM|always0~3_combout\) # ((!\CPU|FSM|WideNor17~combout\ & !\CPU|FSM|WideNor16~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011001100111111001100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_always0~3_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~26_combout\,
	combout => \CPU|FSM|WideOr26~combout\);

-- Location: LABCELL_X79_Y8_N48
\CPU|DP|U3|b[1]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[1]~15_combout\ = ( \CPU|program_counter|out[1]~DUPLICATE_q\ & ( \CPU|FSM|WideNor6~combout\ & ( (!\CPU|struct_reg|out\(1) & (!\CPU|FSM|WideOr11~0_combout\ & ((!\CPU|DP|rC|out\(1)) # (\CPU|FSM|WideOr26~combout\)))) ) ) ) # ( 
-- !\CPU|program_counter|out[1]~DUPLICATE_q\ & ( \CPU|FSM|WideNor6~combout\ & ( (!\CPU|struct_reg|out\(1) & ((!\CPU|DP|rC|out\(1)) # (\CPU|FSM|WideOr26~combout\))) ) ) ) # ( \CPU|program_counter|out[1]~DUPLICATE_q\ & ( !\CPU|FSM|WideNor6~combout\ & ( 
-- (!\CPU|FSM|WideOr11~0_combout\ & ((!\CPU|DP|rC|out\(1)) # (\CPU|FSM|WideOr26~combout\))) ) ) ) # ( !\CPU|program_counter|out[1]~DUPLICATE_q\ & ( !\CPU|FSM|WideNor6~combout\ & ( (!\CPU|DP|rC|out\(1)) # (\CPU|FSM|WideOr26~combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101011111111101000001111000010001000110011001000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(1),
	datab => \CPU|struct_reg|ALT_INV_out\(1),
	datac => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr26~combout\,
	datae => \CPU|program_counter|ALT_INV_out[1]~DUPLICATE_q\,
	dataf => \CPU|FSM|ALT_INV_WideNor6~combout\,
	combout => \CPU|DP|U3|b[1]~15_combout\);

-- Location: LABCELL_X79_Y8_N24
\CPU|DP|U3|b[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(1) = ( \CPU|DP|U3|b[1]~15_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\Equal5~1_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a1\))) # (\Equal5~1_combout\ & (\SW[1]~input_o\)))) ) ) # ( !\CPU|DP|U3|b[1]~15_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000001010001010000000101000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_Equal5~1_combout\,
	datac => \ALT_INV_SW[1]~input_o\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\,
	dataf => \CPU|DP|U3|ALT_INV_b[1]~15_combout\,
	combout => \CPU|DP|U3|b\(1));

-- Location: MLABCELL_X78_Y6_N36
\CPU|FSM|WideOr11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr11~combout\ = ( !\CPU|FSM|WideOr29~1_combout\ & ( !\CPU|FSM|WideNor6~combout\ & ( !\CPU|FSM|WideOr11~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor6~combout\,
	combout => \CPU|FSM|WideOr11~combout\);

-- Location: IOIBUF_X16_Y0_N18
\SW[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: IOIBUF_X2_Y0_N41
\SW[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: FF_X87_Y8_N22
\CPU|program_counter|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(5),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(5));

-- Location: MLABCELL_X78_Y8_N54
\CPU|DP|U3|b[0]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[0]~16_combout\ = ( \CPU|FSM|WideOr11~0_combout\ & ( \CPU|FSM|WideOr26~combout\ & ( (!\CPU|program_counter|out\(0) & ((!\CPU|struct_reg|out\(0)) # (!\CPU|FSM|WideNor6~combout\))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( 
-- \CPU|FSM|WideOr26~combout\ & ( (!\CPU|struct_reg|out\(0)) # (!\CPU|FSM|WideNor6~combout\) ) ) ) # ( \CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(0) & (!\CPU|program_counter|out\(0) & ((!\CPU|struct_reg|out\(0)) # 
-- (!\CPU|FSM|WideNor6~combout\)))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(0) & ((!\CPU|struct_reg|out\(0)) # (!\CPU|FSM|WideNor6~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110000011100000111000000000000011101110111011101110111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(0),
	datab => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datac => \CPU|DP|rC|ALT_INV_out\(0),
	datad => \CPU|program_counter|ALT_INV_out\(0),
	datae => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[0]~16_combout\);

-- Location: MLABCELL_X78_Y8_N0
\CPU|DP|U3|b[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(0) = ( \CPU|DP|U3|b[0]~16_combout\ & ( \Equal5~1_combout\ & ( (\SW[0]~input_o\ & \CPU|FSM|vsel\(3)) ) ) ) # ( !\CPU|DP|U3|b[0]~16_combout\ & ( \Equal5~1_combout\ ) ) # ( \CPU|DP|U3|b[0]~16_combout\ & ( !\Equal5~1_combout\ & ( 
-- (\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & \CPU|FSM|vsel\(3)) ) ) ) # ( !\CPU|DP|U3|b[0]~16_combout\ & ( !\Equal5~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000001010000010111111111111111110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	datab => \ALT_INV_SW[0]~input_o\,
	datac => \CPU|FSM|ALT_INV_vsel\(3),
	datae => \CPU|DP|U3|ALT_INV_b[0]~16_combout\,
	dataf => \ALT_INV_Equal5~1_combout\,
	combout => \CPU|DP|U3|b\(0));

-- Location: LABCELL_X75_Y8_N9
\CPU|DP|REGFILE|regR7|out[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[0]~feeder_combout\ = ( \CPU|DP|U3|b\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regR7|out[0]~feeder_combout\);

-- Location: FF_X75_Y8_N10
\CPU|DP|REGFILE|regR7|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[0]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(0));

-- Location: LABCELL_X77_Y8_N6
\CPU|DP|REGFILE|regR1|out[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[0]~feeder_combout\ = ( \CPU|DP|U3|b\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regR1|out[0]~feeder_combout\);

-- Location: IOIBUF_X4_Y0_N35
\SW[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: MLABCELL_X78_Y8_N24
\CPU|DP|U3|b[5]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[5]~11_combout\ = ( \CPU|FSM|WideOr11~0_combout\ & ( \CPU|FSM|WideOr26~combout\ & ( (!\CPU|program_counter|out\(5) & ((!\CPU|struct_reg|out\(5)) # (!\CPU|FSM|WideNor6~combout\))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( 
-- \CPU|FSM|WideOr26~combout\ & ( (!\CPU|struct_reg|out\(5)) # (!\CPU|FSM|WideNor6~combout\) ) ) ) # ( \CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|program_counter|out\(5) & (!\CPU|DP|rC|out\(5) & ((!\CPU|struct_reg|out\(5)) # 
-- (!\CPU|FSM|WideNor6~combout\)))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(5) & ((!\CPU|struct_reg|out\(5)) # (!\CPU|FSM|WideNor6~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000010100000110000001000000011111111101010101100110010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(5),
	datab => \CPU|program_counter|ALT_INV_out\(5),
	datac => \CPU|DP|rC|ALT_INV_out\(5),
	datad => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datae => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[5]~11_combout\);

-- Location: LABCELL_X79_Y8_N27
\CPU|DP|U3|b[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(5) = ( \CPU|DP|U3|b[5]~11_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\Equal5~1_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a5\))) # (\Equal5~1_combout\ & (\SW[5]~input_o\)))) ) ) # ( !\CPU|DP|U3|b[5]~11_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000001010001010000000101000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_Equal5~1_combout\,
	datac => \ALT_INV_SW[5]~input_o\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\,
	dataf => \CPU|DP|U3|ALT_INV_b[5]~11_combout\,
	combout => \CPU|DP|U3|b\(5));

-- Location: FF_X79_Y8_N4
\CPU|DP|REGFILE|regR7|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(5));

-- Location: LABCELL_X80_Y9_N21
\CPU|struct_decoder|nsel_mux|out~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out~1_combout\ = ( \CPU|FSM|WideOr29~0_combout\ & ( \CPU|struct_reg|out\(5) & ( ((\CPU|FSM|WideNor43~combout\ & (!\CPU|FSM|WideNor42~combout\ & \CPU|FSM|always0~9_combout\))) # (\CPU|FSM|WideOr29~1_combout\) ) ) ) # ( 
-- !\CPU|FSM|WideOr29~0_combout\ & ( \CPU|struct_reg|out\(5) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110000010011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datac => \CPU|FSM|ALT_INV_always0~9_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	dataf => \CPU|struct_reg|ALT_INV_out\(5),
	combout => \CPU|struct_decoder|nsel_mux|out~1_combout\);

-- Location: LABCELL_X80_Y6_N33
\CPU|FSM|WideOr31\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr31~combout\ = ( !\CPU|FSM|WideOr11~0_combout\ & ( (!\CPU|FSM|WideOr16~0_combout\ & !\CPU|FSM|WideNor6~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000000000000000000011110000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideOr16~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datae => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	combout => \CPU|FSM|WideOr31~combout\);

-- Location: LABCELL_X80_Y9_N54
\CPU|FSM|nsel[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|nsel[2]~0_combout\ = ( !\CPU|struct_reg|out\(13) & ( (!\CPU|struct_reg|out\(14)) # ((!\CPU|struct_reg|out[15]~DUPLICATE_q\) # ((!\CPU|FSM|WideNor7~0_combout\) # ((\CPU|FSM|always0~0_combout\)))) ) ) # ( \CPU|struct_reg|out\(13) & ( 
-- ((!\CPU|struct_reg|out[15]~DUPLICATE_q\) # ((!\CPU|FSM|WideNor15~0_combout\) # ((\CPU|FSM|always0~17_combout\)))) # (\CPU|struct_reg|out\(14)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "1111111011111111111111011111110111111110111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(14),
	datab => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	datad => \CPU|FSM|ALT_INV_always0~0_combout\,
	datae => \CPU|struct_reg|ALT_INV_out\(13),
	dataf => \CPU|FSM|ALT_INV_always0~17_combout\,
	datag => \CPU|FSM|ALT_INV_WideNor7~0_combout\,
	combout => \CPU|FSM|nsel[2]~0_combout\);

-- Location: LABCELL_X81_Y6_N33
\CPU|struct_decoder|nsel_mux|out[2]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out[2]~5_combout\ = ( \CPU|FSM|nsel[2]~0_combout\ & ( (\CPU|struct_reg|out[10]~DUPLICATE_q\ & !\CPU|FSM|WideOr31~combout\) ) ) # ( !\CPU|FSM|nsel[2]~0_combout\ & ( ((\CPU|struct_reg|out[10]~DUPLICATE_q\ & 
-- !\CPU|FSM|WideOr31~combout\)) # (\CPU|struct_reg|out\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101010101010111110101010100001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(2),
	datac => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datad => \CPU|FSM|ALT_INV_WideOr31~combout\,
	dataf => \CPU|FSM|ALT_INV_nsel[2]~0_combout\,
	combout => \CPU|struct_decoder|nsel_mux|out[2]~5_combout\);

-- Location: LABCELL_X80_Y9_N45
\CPU|struct_decoder|nsel_mux|out~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out~0_combout\ = ( \CPU|FSM|WideOr29~0_combout\ & ( \CPU|FSM|always0~9_combout\ & ( (\CPU|struct_reg|out\(7) & (((\CPU|FSM|WideNor43~combout\ & !\CPU|FSM|WideNor42~combout\)) # (\CPU|FSM|WideOr29~1_combout\))) ) ) ) # ( 
-- !\CPU|FSM|WideOr29~0_combout\ & ( \CPU|FSM|always0~9_combout\ & ( \CPU|struct_reg|out\(7) ) ) ) # ( \CPU|FSM|WideOr29~0_combout\ & ( !\CPU|FSM|always0~9_combout\ & ( (\CPU|struct_reg|out\(7) & \CPU|FSM|WideOr29~1_combout\) ) ) ) # ( 
-- !\CPU|FSM|WideOr29~0_combout\ & ( !\CPU|FSM|always0~9_combout\ & ( \CPU|struct_reg|out\(7) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000000000011001100110011001100110001000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datab => \CPU|struct_reg|ALT_INV_out\(7),
	datac => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datad => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~9_combout\,
	combout => \CPU|struct_decoder|nsel_mux|out~0_combout\);

-- Location: FF_X81_Y7_N34
\CPU|struct_reg|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector7~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(8));

-- Location: LABCELL_X81_Y6_N57
\CPU|struct_decoder|nsel_mux|out[0]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out[0]~3_combout\ = ( \CPU|FSM|nsel[2]~0_combout\ & ( (\CPU|struct_reg|out\(8) & !\CPU|FSM|WideOr31~combout\) ) ) # ( !\CPU|FSM|nsel[2]~0_combout\ & ( ((\CPU|struct_reg|out\(8) & !\CPU|FSM|WideOr31~combout\)) # 
-- (\CPU|struct_reg|out\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111100001111010111110000111101010101000000000101010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(8),
	datac => \CPU|struct_reg|ALT_INV_out\(0),
	datad => \CPU|FSM|ALT_INV_WideOr31~combout\,
	dataf => \CPU|FSM|ALT_INV_nsel[2]~0_combout\,
	combout => \CPU|struct_decoder|nsel_mux|out[0]~3_combout\);

-- Location: LABCELL_X80_Y9_N18
\CPU|struct_decoder|nsel_mux|out~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out~2_combout\ = ( \CPU|FSM|WideOr29~0_combout\ & ( \CPU|struct_reg|out\(6) & ( ((\CPU|FSM|WideNor43~combout\ & (!\CPU|FSM|WideNor42~combout\ & \CPU|FSM|always0~9_combout\))) # (\CPU|FSM|WideOr29~1_combout\) ) ) ) # ( 
-- !\CPU|FSM|WideOr29~0_combout\ & ( \CPU|struct_reg|out\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110000111101001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datac => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datad => \CPU|FSM|ALT_INV_always0~9_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	dataf => \CPU|struct_reg|ALT_INV_out\(6),
	combout => \CPU|struct_decoder|nsel_mux|out~2_combout\);

-- Location: LABCELL_X80_Y9_N12
\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ = ( \CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ( \CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (\CPU|struct_decoder|nsel_mux|out~0_combout\) # (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\) ) ) ) # 
-- ( !\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ( \CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (\CPU|struct_decoder|nsel_mux|out~1_combout\ & ((\CPU|struct_decoder|nsel_mux|out~0_combout\) # (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\))) ) ) ) 
-- # ( \CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & ((\CPU|struct_decoder|nsel_mux|out~0_combout\) # (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\))) ) 
-- ) ) # ( !\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (\CPU|struct_decoder|nsel_mux|out~1_combout\ & (\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & ((\CPU|struct_decoder|nsel_mux|out~0_combout\) # 
-- (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100010001000000110011001100000101010101010000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out~1_combout\,
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\,
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out~0_combout\,
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out[0]~3_combout\,
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\);

-- Location: LABCELL_X81_Y6_N39
\CPU|struct_decoder|nsel_mux|out[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out\(1) = ( \CPU|struct_reg|out\(6) & ( !\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & ( (\CPU|FSM|WideOr29~0_combout\ & (!\CPU|FSM|WideOr29~1_combout\ & ((!\CPU|FSM|WideNor43~combout\) # (!\CPU|FSM|always0~25_combout\)))) 
-- ) ) ) # ( !\CPU|struct_reg|out\(6) & ( !\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111001100000010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datab => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datad => \CPU|FSM|ALT_INV_always0~25_combout\,
	datae => \CPU|struct_reg|ALT_INV_out\(6),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\,
	combout => \CPU|struct_decoder|nsel_mux|out\(1));

-- Location: LABCELL_X80_Y7_N57
\CPU|DP|REGFILE|writenum_OH[5]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[5]~7_combout\ = (!\CPU|FSM|WideOr11~combout\ & (!\CPU|struct_decoder|nsel_mux|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & \CPU|struct_decoder|nsel_mux|out\(1))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000001000000000000000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\);

-- Location: FF_X79_Y8_N28
\CPU|DP|REGFILE|regR5|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(5),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(5));

-- Location: LABCELL_X81_Y6_N6
\CPU|DP|REGFILE|writenum_OH[6]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[6]~6_combout\ = ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|FSM|WideOr11~combout\ & (\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\);

-- Location: FF_X80_Y8_N35
\CPU|DP|REGFILE|regR6|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(5));

-- Location: LABCELL_X80_Y7_N18
\CPU|DP|REGFILE|writenum_OH[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[4]~5_combout\ = ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|FSM|WideOr11~combout\ & (\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|struct_decoder|nsel_mux|out\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100010000000000010001000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	combout => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\);

-- Location: FF_X81_Y9_N14
\CPU|DP|REGFILE|regR4|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(5));

-- Location: LABCELL_X81_Y9_N12
\CPU|DP|REGFILE|mux_out|m|Selector10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector10~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(5) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|DP|REGFILE|regR5|out\(5)) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(5) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR5|out\(5) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(5) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(5) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(5) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(5) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000100010000000000111011100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(5),
	datac => \CPU|DP|REGFILE|regR6|ALT_INV_out\(5),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(5),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector10~1_combout\);

-- Location: LABCELL_X80_Y7_N21
\CPU|DP|REGFILE|writenum_OH[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[0]~1_combout\ = ( \CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|FSM|WideOr11~combout\ & (\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|struct_decoder|nsel_mux|out\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	combout => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\);

-- Location: FF_X79_Y8_N13
\CPU|DP|REGFILE|regR0|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(5));

-- Location: LABCELL_X73_Y8_N24
\CPU|DP|REGFILE|regR1|out[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[5]~feeder_combout\ = ( \CPU|DP|U3|b\(5) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(5),
	combout => \CPU|DP|REGFILE|regR1|out[5]~feeder_combout\);

-- Location: FF_X73_Y8_N25
\CPU|DP|REGFILE|regR1|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[5]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(5));

-- Location: LABCELL_X80_Y9_N48
\CPU|DP|REGFILE|mux_out|m|Selector9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ = ( !\CPU|struct_decoder|nsel_mux|out~1_combout\ & ( \CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (!\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & (!\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ & 
-- !\CPU|struct_decoder|nsel_mux|out~0_combout\)) ) ) ) # ( \CPU|struct_decoder|nsel_mux|out~1_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (!\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & (!\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ & 
-- !\CPU|struct_decoder|nsel_mux|out~0_combout\)) ) ) ) # ( !\CPU|struct_decoder|nsel_mux|out~1_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (!\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ & (!\CPU|struct_decoder|nsel_mux|out~0_combout\ & 
-- (!\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ $ (!\CPU|struct_decoder|nsel_mux|out[0]~3_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110000000000000101000000000000011000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\,
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out[0]~3_combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\,
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out~0_combout\,
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out~1_combout\,
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\);

-- Location: MLABCELL_X78_Y9_N21
\CPU|DP|REGFILE|regR3|out[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[5]~feeder_combout\ = ( \CPU|DP|U3|b\(5) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(5),
	combout => \CPU|DP|REGFILE|regR3|out[5]~feeder_combout\);

-- Location: LABCELL_X81_Y6_N54
\CPU|DP|REGFILE|writenum_OH[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[3]~3_combout\ = ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|FSM|WideOr11~combout\ & (!\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|struct_decoder|nsel_mux|out\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\);

-- Location: FF_X78_Y9_N22
\CPU|DP|REGFILE|regR3|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[5]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(5));

-- Location: LABCELL_X81_Y6_N30
\CPU|DP|REGFILE|writenum_OH[2]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[2]~4_combout\ = ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|FSM|WideOr11~combout\ & (\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|struct_decoder|nsel_mux|out\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\);

-- Location: FF_X82_Y7_N32
\CPU|DP|REGFILE|regR2|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(5));

-- Location: LABCELL_X80_Y9_N24
\CPU|DP|REGFILE|mux_out|m|Selector9~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ = ( !\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ & ( \CPU|struct_decoder|nsel_mux|out~2_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~0_combout\ ) ) ) # ( !\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ & 
-- ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & !\CPU|struct_decoder|nsel_mux|out~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000000000000000000011111111000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\,
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out~0_combout\,
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\,
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\);

-- Location: MLABCELL_X82_Y7_N30
\CPU|DP|REGFILE|mux_out|m|Selector10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector10~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(5) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|regR3|out\(5)) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(5) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & \CPU|DP|REGFILE|regR3|out\(5)) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(5) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(5))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ((\CPU|DP|REGFILE|regR1|out\(5)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(5) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(5))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & 
-- ((\CPU|DP|REGFILE|regR1|out\(5)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101001101010011010100110101001100000000111100000000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR0|ALT_INV_out\(5),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(5),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	datad => \CPU|DP|REGFILE|regR3|ALT_INV_out\(5),
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(5),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector10~0_combout\);

-- Location: MLABCELL_X82_Y7_N0
\CPU|DP|REGFILE|mux_out|m|Selector10~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector10~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector10~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # 
-- (\CPU|DP|REGFILE|mux_out|m|Selector10~1_combout\)))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(5))) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector10~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & 
-- ((\CPU|DP|REGFILE|mux_out|m|Selector10~1_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111110101000001011111010111000101111101011100010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR7|ALT_INV_out\(5),
	datab => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datac => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector10~1_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector10~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector10~2_combout\);

-- Location: FF_X82_Y7_N50
\CPU|DP|rB|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector10~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(5));

-- Location: IOIBUF_X4_Y0_N1
\SW[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: MLABCELL_X78_Y7_N15
\CPU|DP|U3|b~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b~0_combout\ = ( \CPU|FSM|WideNor6~combout\ & ( \CPU|struct_reg|out\(7) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out\(7),
	datae => \CPU|FSM|ALT_INV_WideNor6~combout\,
	combout => \CPU|DP|U3|b~0_combout\);

-- Location: LABCELL_X79_Y8_N54
\CPU|DP|U3|b[6]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[6]~10_combout\ = ( \CPU|struct_reg|out\(6) & ( \CPU|FSM|WideOr26~combout\ & ( (!\CPU|FSM|WideNor6~combout\ & ((!\CPU|FSM|WideOr11~0_combout\) # (!\CPU|program_counter|out[6]~DUPLICATE_q\))) ) ) ) # ( !\CPU|struct_reg|out\(6) & ( 
-- \CPU|FSM|WideOr26~combout\ & ( (!\CPU|FSM|WideOr11~0_combout\) # (!\CPU|program_counter|out[6]~DUPLICATE_q\) ) ) ) # ( \CPU|struct_reg|out\(6) & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(6) & (!\CPU|FSM|WideNor6~combout\ & 
-- ((!\CPU|FSM|WideOr11~0_combout\) # (!\CPU|program_counter|out[6]~DUPLICATE_q\)))) ) ) ) # ( !\CPU|struct_reg|out\(6) & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(6) & ((!\CPU|FSM|WideOr11~0_combout\) # 
-- (!\CPU|program_counter|out[6]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110010001000110000001000000011111111101010101111000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	datab => \CPU|DP|rC|ALT_INV_out\(6),
	datac => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datad => \CPU|program_counter|ALT_INV_out[6]~DUPLICATE_q\,
	datae => \CPU|struct_reg|ALT_INV_out\(6),
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[6]~10_combout\);

-- Location: LABCELL_X79_Y8_N21
\CPU|DP|U3|b[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(6) = ( \CPU|DP|U3|b[6]~10_combout\ & ( \Equal5~1_combout\ & ( (\CPU|FSM|vsel\(3) & \SW[6]~input_o\) ) ) ) # ( !\CPU|DP|U3|b[6]~10_combout\ & ( \Equal5~1_combout\ ) ) # ( \CPU|DP|U3|b[6]~10_combout\ & ( !\Equal5~1_combout\ & ( 
-- (\MEM|mem_rtl_0|auto_generated|ram_block1a6\ & \CPU|FSM|vsel\(3)) ) ) ) # ( !\CPU|DP|U3|b[6]~10_combout\ & ( !\Equal5~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000100010001000111111111111111110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\,
	datab => \CPU|FSM|ALT_INV_vsel\(3),
	datad => \ALT_INV_SW[6]~input_o\,
	datae => \CPU|DP|U3|ALT_INV_b[6]~10_combout\,
	dataf => \ALT_INV_Equal5~1_combout\,
	combout => \CPU|DP|U3|b\(6));

-- Location: FF_X78_Y7_N52
\CPU|DP|REGFILE|regR7|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(6));

-- Location: FF_X79_Y8_N22
\CPU|DP|REGFILE|regR5|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(6),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(6));

-- Location: FF_X80_Y8_N40
\CPU|DP|REGFILE|regR6|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(6));

-- Location: FF_X81_Y9_N32
\CPU|DP|REGFILE|regR4|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(6));

-- Location: LABCELL_X81_Y9_N30
\CPU|DP|REGFILE|mux_out|m|Selector9~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector9~4_combout\ = ( \CPU|DP|REGFILE|regR4|out\(6) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|DP|REGFILE|regR5|out\(6)) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(6) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR5|out\(6) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(6) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(6) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(6) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(6) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000100010000000000111011100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(6),
	datac => \CPU|DP|REGFILE|regR6|ALT_INV_out\(6),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(6),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector9~4_combout\);

-- Location: FF_X80_Y8_N28
\CPU|DP|REGFILE|regR1|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(6));

-- Location: FF_X79_Y8_N58
\CPU|DP|REGFILE|regR0|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(6));

-- Location: MLABCELL_X78_Y9_N27
\CPU|DP|REGFILE|regR3|out[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[6]~feeder_combout\ = ( \CPU|DP|U3|b\(6) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(6),
	combout => \CPU|DP|REGFILE|regR3|out[6]~feeder_combout\);

-- Location: FF_X78_Y9_N28
\CPU|DP|REGFILE|regR3|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[6]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(6));

-- Location: FF_X81_Y9_N20
\CPU|DP|REGFILE|regR2|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(6));

-- Location: LABCELL_X81_Y9_N18
\CPU|DP|REGFILE|mux_out|m|Selector9~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\ = ( \CPU|DP|REGFILE|regR2|out\(6) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(6)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(6) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(6) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(6) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR0|out\(6))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR3|out\(6)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(6) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR0|out\(6))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- ((\CPU|DP|REGFILE|regR3|out\(6)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100001111001100110000111101010101000000000101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR1|ALT_INV_out\(6),
	datab => \CPU|DP|REGFILE|regR0|ALT_INV_out\(6),
	datac => \CPU|DP|REGFILE|regR3|ALT_INV_out\(6),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(6),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\);

-- Location: LABCELL_X81_Y9_N0
\CPU|DP|REGFILE|mux_out|m|Selector9~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector9~5_combout\ = ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\ & ( \CPU|DP|REGFILE|regR7|out\(6) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # (\CPU|DP|REGFILE|mux_out|m|Selector9~4_combout\) ) ) ) # ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\ & ( \CPU|DP|REGFILE|regR7|out\(6) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~3_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector9~4_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111010101010101010111110000111111110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR7|ALT_INV_out\(6),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~4_combout\,
	datae => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~3_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector9~5_combout\);

-- Location: FF_X80_Y9_N1
\CPU|DP|rB|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector9~5_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(6));

-- Location: FF_X83_Y9_N25
\CPU|DP|rA|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector7~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(8));

-- Location: FF_X82_Y7_N56
\CPU|DP|rA|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector5~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(10));

-- Location: FF_X84_Y7_N2
\CPU|DP|rA|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector4~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(11));

-- Location: FF_X82_Y7_N41
\CPU|DP|rB|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector5~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(10));

-- Location: LABCELL_X85_Y6_N54
\CPU|struct_decoder|shift[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|shift[1]~0_combout\ = ( \CPU|struct_reg|out\(13) & ( \CPU|struct_reg|out\(4) ) ) # ( !\CPU|struct_reg|out\(13) & ( (\CPU|struct_reg|out\(4) & ((!\CPU|struct_reg|out\(15)) # (\CPU|struct_reg|out\(14)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000011000011110000001100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_reg|ALT_INV_out\(14),
	datac => \CPU|struct_reg|ALT_INV_out\(4),
	datad => \CPU|struct_reg|ALT_INV_out\(15),
	dataf => \CPU|struct_reg|ALT_INV_out\(13),
	combout => \CPU|struct_decoder|shift[1]~0_combout\);

-- Location: MLABCELL_X82_Y7_N6
\CPU|DP|Bin[3]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~2_combout\ = ( \CPU|FSM|WideNor12~combout\ & ( !\CPU|struct_decoder|shift[1]~0_combout\ ) ) # ( !\CPU|FSM|WideNor12~combout\ & ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( (((!\CPU|FSM|WideNor15~0_combout\) # 
-- (!\CPU|FSM|always0~2_combout\)) # (\CPU|FSM|WideNor13~combout\)) # (\CPU|FSM|WideNor5~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	datad => \CPU|FSM|ALT_INV_always0~2_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor12~combout\,
	dataf => \CPU|struct_decoder|ALT_INV_shift[1]~0_combout\,
	combout => \CPU|DP|Bin[3]~2_combout\);

-- Location: FF_X84_Y7_N22
\CPU|DP|rB|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector3~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(12));

-- Location: LABCELL_X85_Y9_N3
\CPU|FSM|WideOr20\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr20~combout\ = (!\CPU|FSM|WideOr20~0_combout\) # (\CPU|FSM|always0~16_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100001111111111110000111111111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_always0~16_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr20~0_combout\,
	combout => \CPU|FSM|WideOr20~combout\);

-- Location: FF_X84_Y8_N20
\CPU|DP|rC|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux3~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(12));

-- Location: LABCELL_X79_Y7_N30
\CPU|DP|U3|b[13]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[13]~3_combout\ = ( \CPU|FSM|WideOr26~combout\ & ( !\CPU|DP|U3|b~0_combout\ ) ) # ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|U3|b~0_combout\ & !\CPU|DP|rC|out\(13)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110011001100110011000000110000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datac => \CPU|DP|rC|ALT_INV_out\(13),
	datae => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[13]~3_combout\);

-- Location: LABCELL_X80_Y7_N51
\CPU|DP|U3|b[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(13) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( \CPU|DP|U3|b[13]~3_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\Equal5~0_combout\) # (!\LED_load~4_combout\)))) ) ) ) # ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( !\CPU|DP|U3|b[13]~3_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( !\CPU|DP|U3|b[13]~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_Equal5~0_combout\,
	datad => \ALT_INV_LED_load~4_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\,
	dataf => \CPU|DP|U3|ALT_INV_b[13]~3_combout\,
	combout => \CPU|DP|U3|b\(13));

-- Location: LABCELL_X79_Y7_N42
\CPU|DP|REGFILE|regR7|out[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[13]~feeder_combout\ = ( \CPU|DP|U3|b\(13) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(13),
	combout => \CPU|DP|REGFILE|regR7|out[13]~feeder_combout\);

-- Location: FF_X79_Y7_N43
\CPU|DP|REGFILE|regR7|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[13]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(13));

-- Location: FF_X82_Y7_N28
\CPU|DP|REGFILE|regR2|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(13));

-- Location: MLABCELL_X78_Y7_N24
\CPU|DP|REGFILE|regR1|out[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[13]~feeder_combout\ = ( \CPU|DP|U3|b\(13) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(13),
	combout => \CPU|DP|REGFILE|regR1|out[13]~feeder_combout\);

-- Location: FF_X78_Y7_N25
\CPU|DP|REGFILE|regR1|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[13]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(13));

-- Location: LABCELL_X77_Y7_N54
\CPU|DP|REGFILE|regR3|out[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[13]~feeder_combout\ = ( \CPU|DP|U3|b\(13) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(13),
	combout => \CPU|DP|REGFILE|regR3|out[13]~feeder_combout\);

-- Location: FF_X77_Y7_N55
\CPU|DP|REGFILE|regR3|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[13]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(13));

-- Location: FF_X83_Y7_N34
\CPU|DP|REGFILE|regR0|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(13));

-- Location: MLABCELL_X84_Y7_N30
\CPU|DP|REGFILE|mux_out|m|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector2~0_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( \CPU|DP|REGFILE|regR2|out\(13) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( \CPU|DP|REGFILE|regR1|out\(13) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( \CPU|DP|REGFILE|regR3|out\(13) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( \CPU|DP|REGFILE|regR0|out\(13) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000011110000111100110011001100110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR2|ALT_INV_out\(13),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(13),
	datac => \CPU|DP|REGFILE|regR3|ALT_INV_out\(13),
	datad => \CPU|DP|REGFILE|regR0|ALT_INV_out\(13),
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector2~0_combout\);

-- Location: FF_X83_Y7_N11
\CPU|DP|REGFILE|regR6|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(13));

-- Location: FF_X80_Y7_N52
\CPU|DP|REGFILE|regR5|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(13),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(13));

-- Location: FF_X85_Y7_N44
\CPU|DP|REGFILE|regR4|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(13));

-- Location: LABCELL_X85_Y7_N42
\CPU|DP|REGFILE|mux_out|m|Selector2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(13) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|DP|REGFILE|regR5|out\(13)) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(13) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & (!\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|DP|REGFILE|regR5|out\(13))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(13) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & (\CPU|DP|REGFILE|regR6|out\(13) & \CPU|struct_decoder|nsel_mux|out\(0))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(13) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (!\CPU|struct_decoder|nsel_mux|out\(2) & (\CPU|DP|REGFILE|regR6|out\(13) & \CPU|struct_decoder|nsel_mux|out\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001000000000101000000000101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datab => \CPU|DP|REGFILE|regR6|ALT_INV_out\(13),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|DP|REGFILE|regR5|ALT_INV_out\(13),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(13),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\);

-- Location: MLABCELL_X84_Y7_N36
\CPU|DP|REGFILE|mux_out|m|Selector2~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector2~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\ & ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(13) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(13) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\ & ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector2~1_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\ & \CPU|DP|REGFILE|mux_out|m|Selector2~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000111111111111111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|regR7|ALT_INV_out\(13),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector2~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector2~1_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector2~2_combout\);

-- Location: FF_X84_Y7_N35
\CPU|DP|rB|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector2~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(13));

-- Location: MLABCELL_X84_Y7_N18
\CPU|DP|Bin[12]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[12]~5_combout\ = ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|rB|out\(11) ) ) ) # ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|rB|out\(13) ) ) ) # ( \CPU|DP|Bin[3]~2_combout\ & ( 
-- !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|rB|out\(12) ) ) ) # ( !\CPU|DP|Bin[3]~2_combout\ & ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000011110000111101010101010101010000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(13),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|DP|rB|ALT_INV_out\(12),
	datad => \CPU|DP|rB|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	combout => \CPU|DP|Bin[12]~5_combout\);

-- Location: MLABCELL_X84_Y7_N27
\CPU|DP|U2|ALU_AddSub|ai|p[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(12) = ( \CPU|DP|Bin[12]~5_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(12)))) ) ) # ( !\CPU|DP|Bin[12]~5_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(12)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011110011000011001111001111110011000011001111001100001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|DP|rA|ALT_INV_out\(12),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[12]~5_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(12));

-- Location: MLABCELL_X84_Y7_N24
\CPU|DP|U2|ALU_AddSub|ai|g[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(12) = ( \CPU|DP|Bin[12]~5_combout\ & ( (!\CPU|DP|Ain[4]~1_combout\ & (!\CPU|struct_reg|out\(11) & \CPU|DP|rA|out\(12))) ) ) # ( !\CPU|DP|Bin[12]~5_combout\ & ( (!\CPU|DP|Ain[4]~1_combout\ & (\CPU|struct_reg|out\(11) & 
-- \CPU|DP|rA|out\(12))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|rA|ALT_INV_out\(12),
	dataf => \CPU|DP|ALT_INV_Bin[12]~5_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(12));

-- Location: FF_X79_Y6_N40
\CPU|DP|rA|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector1~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(14));

-- Location: MLABCELL_X84_Y9_N48
\CPU|DP|Ain[14]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[14]~2_combout\ = ( \CPU|DP|rA|out\(14) & ( !\CPU|DP|Ain[4]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|rA|ALT_INV_out\(14),
	combout => \CPU|DP|Ain[14]~2_combout\);

-- Location: LABCELL_X80_Y9_N36
\CPU|DP|Bin[15]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[15]~18_combout\ = ( \CPU|DP|rB|out\(15) & ( \CPU|FSM|bsel~combout\ & ( \CPU|struct_reg|out\(4) ) ) ) # ( !\CPU|DP|rB|out\(15) & ( \CPU|FSM|bsel~combout\ & ( \CPU|struct_reg|out\(4) ) ) ) # ( \CPU|DP|rB|out\(15) & ( !\CPU|FSM|bsel~combout\ & ( 
-- ((!\CPU|struct_reg|out\(4) & ((!\CPU|struct_reg|out\(3)) # (\CPU|DP|rB|out\(14)))) # (\CPU|struct_reg|out\(4) & ((\CPU|struct_reg|out\(3))))) # (\CPU|struct_decoder|Equal0~0_combout\) ) ) ) # ( !\CPU|DP|rB|out\(15) & ( !\CPU|FSM|bsel~combout\ & ( 
-- (\CPU|DP|rB|out\(14) & (!\CPU|struct_reg|out\(4) & (\CPU|struct_reg|out\(3) & !\CPU|struct_decoder|Equal0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000000110001111111111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(14),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|struct_reg|ALT_INV_out\(3),
	datad => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	datae => \CPU|DP|rB|ALT_INV_out\(15),
	dataf => \CPU|FSM|ALT_INV_bsel~combout\,
	combout => \CPU|DP|Bin[15]~18_combout\);

-- Location: FF_X79_Y9_N14
\CPU|DP|rA|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector0~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(15));

-- Location: LABCELL_X83_Y9_N21
\CPU|DP|U2|main|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux0~0_combout\ = ( \CPU|DP|Ain[4]~1_combout\ & ( (!\CPU|DP|Bin[15]~18_combout\ & \CPU|struct_reg|out\(11)) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( (!\CPU|DP|Bin[15]~18_combout\ & ((\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[15]~18_combout\ 
-- & (\CPU|DP|rA|out\(15) & !\CPU|struct_reg|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001100000000111100110000000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Bin[15]~18_combout\,
	datac => \CPU|DP|rA|ALT_INV_out\(15),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|main|Mux0~0_combout\);

-- Location: MLABCELL_X84_Y9_N45
\CPU|DP|U2|ALU_AddSub|comb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|comb~0_combout\ = ( \CPU|DP|Bin[14]~3_combout\ & ( !\CPU|struct_reg|out\(11) ) ) # ( !\CPU|DP|Bin[14]~3_combout\ & ( \CPU|struct_reg|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[14]~3_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|comb~0_combout\);

-- Location: LABCELL_X83_Y8_N54
\CPU|DP|U2|ALU_AddSub|as|p[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|as|p\(0) = ( \CPU|DP|Bin[15]~18_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(15)))) ) ) # ( !\CPU|DP|Bin[15]~18_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(15)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011110011000011001111001111110011000011001111001100001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|DP|rA|ALT_INV_out\(15),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[15]~18_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|as|p\(0));

-- Location: FF_X84_Y7_N38
\CPU|DP|rA|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector2~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(13));

-- Location: MLABCELL_X84_Y7_N9
\CPU|DP|U2|ALU_AddSub|comb~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|comb~1_combout\ = ( \CPU|DP|Bin[13]~4_combout\ & ( !\CPU|struct_reg|out\(11) ) ) # ( !\CPU|DP|Bin[13]~4_combout\ & ( \CPU|struct_reg|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[13]~4_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|comb~1_combout\);

-- Location: MLABCELL_X84_Y8_N48
\CPU|DP|U2|ALU_AddSub|ai|p[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(11) = ( \CPU|DP|Ain[4]~1_combout\ & ( \CPU|DP|Bin[11]~6_combout\ & ( !\CPU|struct_reg|out\(11) ) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( \CPU|DP|Bin[11]~6_combout\ & ( !\CPU|struct_reg|out\(11) $ (\CPU|DP|rA|out\(11)) ) ) ) # ( 
-- \CPU|DP|Ain[4]~1_combout\ & ( !\CPU|DP|Bin[11]~6_combout\ & ( \CPU|struct_reg|out\(11) ) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( !\CPU|DP|Bin[11]~6_combout\ & ( !\CPU|struct_reg|out\(11) $ (!\CPU|DP|rA|out\(11)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110000111100001100110011001111000011110000111100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[11]~6_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(11));

-- Location: MLABCELL_X84_Y8_N45
\CPU|DP|U2|ALU_AddSub|ai|g[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(11) = ( \CPU|DP|Bin[11]~6_combout\ & ( (\CPU|DP|rA|out\(11) & (!\CPU|struct_reg|out\(11) & !\CPU|DP|Ain[4]~1_combout\)) ) ) # ( !\CPU|DP|Bin[11]~6_combout\ & ( (\CPU|DP|rA|out\(11) & (\CPU|struct_reg|out\(11) & 
-- !\CPU|DP|Ain[4]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(11),
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[11]~6_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(11));

-- Location: FF_X81_Y9_N50
\CPU|DP|rA|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector6~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(9));

-- Location: LABCELL_X85_Y8_N45
\CPU|DP|U2|ALU_AddSub|ai|g[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(9) = ( \CPU|DP|Bin[9]~8_combout\ & ( (\CPU|DP|rA|out\(9) & (!\CPU|struct_reg|out\(11) & !\CPU|DP|Ain[4]~1_combout\)) ) ) # ( !\CPU|DP|Bin[9]~8_combout\ & ( (\CPU|DP|rA|out\(9) & (\CPU|struct_reg|out\(11) & 
-- !\CPU|DP|Ain[4]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(9),
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[9]~8_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(9));

-- Location: MLABCELL_X84_Y7_N6
\CPU|DP|U2|ALU_AddSub|ai|g[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(10) = ( !\CPU|DP|Ain[4]~1_combout\ & ( (\CPU|DP|rA|out\(10) & (!\CPU|DP|Bin[10]~7_combout\ $ (!\CPU|struct_reg|out\(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010101010000000001010101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(10),
	datac => \CPU|DP|ALT_INV_Bin[10]~7_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(10));

-- Location: LABCELL_X85_Y8_N21
\CPU|DP|U2|ALU_AddSub|ai|p[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(9) = ( \CPU|DP|Bin[9]~8_combout\ & ( !\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(9) & !\CPU|DP|Ain[4]~1_combout\))) ) ) # ( !\CPU|DP|Bin[9]~8_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(9)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110011000110011011001100011001110011001110011001001100111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(9),
	datab => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[9]~8_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(9));

-- Location: FF_X81_Y9_N47
\CPU|DP|rA|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector8~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(7));

-- Location: LABCELL_X85_Y9_N36
\CPU|DP|U2|ALU_AddSub|ai|g[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(7) = ( \CPU|DP|Bin[7]~10_combout\ & ( (!\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(7) & !\CPU|DP|Ain[4]~1_combout\)) ) ) # ( !\CPU|DP|Bin[7]~10_combout\ & ( (\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(7) & 
-- !\CPU|DP|Ain[4]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(7),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[7]~10_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(7));

-- Location: LABCELL_X85_Y9_N0
\CPU|DP|U2|ALU_AddSub|ai|g[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(8) = ( \CPU|DP|Bin[8]~9_combout\ & ( (\CPU|DP|rA|out\(8) & (!\CPU|struct_reg|out\(11) & !\CPU|DP|Ain[4]~1_combout\)) ) ) # ( !\CPU|DP|Bin[8]~9_combout\ & ( (\CPU|DP|rA|out\(8) & (\CPU|struct_reg|out\(11) & 
-- !\CPU|DP|Ain[4]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000000000110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rA|ALT_INV_out\(8),
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[8]~9_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(8));

-- Location: LABCELL_X85_Y9_N57
\CPU|DP|U2|ALU_AddSub|ai|p[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(7) = ( \CPU|DP|Bin[7]~10_combout\ & ( !\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(7) & !\CPU|DP|Ain[4]~1_combout\))) ) ) # ( !\CPU|DP|Bin[7]~10_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(7)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001010101010110100101010110100101101010101010010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(7),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[7]~10_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(7));

-- Location: LABCELL_X79_Y8_N36
\CPU|DP|U3|b[4]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[4]~12_combout\ = ( \CPU|FSM|WideOr11~0_combout\ & ( \CPU|FSM|WideOr26~combout\ & ( (!\CPU|program_counter|out[4]~DUPLICATE_q\ & ((!\CPU|FSM|WideNor6~combout\) # (!\CPU|struct_reg|out\(4)))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( 
-- \CPU|FSM|WideOr26~combout\ & ( (!\CPU|FSM|WideNor6~combout\) # (!\CPU|struct_reg|out\(4)) ) ) ) # ( \CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|program_counter|out[4]~DUPLICATE_q\ & (!\CPU|DP|rC|out\(4) & 
-- ((!\CPU|FSM|WideNor6~combout\) # (!\CPU|struct_reg|out\(4))))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(4) & ((!\CPU|FSM|WideNor6~combout\) # (!\CPU|struct_reg|out\(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011000000100010001000000011111111111100001010101010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out[4]~DUPLICATE_q\,
	datab => \CPU|DP|rC|ALT_INV_out\(4),
	datac => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(4),
	datae => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[4]~12_combout\);

-- Location: LABCELL_X79_Y8_N45
\CPU|DP|U3|b[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(4) = ( \CPU|DP|U3|b[4]~12_combout\ & ( \Equal5~1_combout\ & ( (\SW[4]~input_o\ & \CPU|FSM|vsel\(3)) ) ) ) # ( !\CPU|DP|U3|b[4]~12_combout\ & ( \Equal5~1_combout\ ) ) # ( \CPU|DP|U3|b[4]~12_combout\ & ( !\Equal5~1_combout\ & ( 
-- (\CPU|FSM|vsel\(3) & \MEM|mem_rtl_0|auto_generated|ram_block1a4\) ) ) ) # ( !\CPU|DP|U3|b[4]~12_combout\ & ( !\Equal5~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000111111111111111111110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_SW[4]~input_o\,
	datac => \CPU|FSM|ALT_INV_vsel\(3),
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\,
	datae => \CPU|DP|U3|ALT_INV_b[4]~12_combout\,
	dataf => \ALT_INV_Equal5~1_combout\,
	combout => \CPU|DP|U3|b\(4));

-- Location: LABCELL_X75_Y8_N24
\CPU|DP|REGFILE|regR7|out[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[4]~feeder_combout\ = ( \CPU|DP|U3|b\(4) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(4),
	combout => \CPU|DP|REGFILE|regR7|out[4]~feeder_combout\);

-- Location: FF_X75_Y8_N25
\CPU|DP|REGFILE|regR7|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[4]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(4));

-- Location: FF_X79_Y8_N46
\CPU|DP|REGFILE|regR5|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(4),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(4));

-- Location: FF_X80_Y8_N23
\CPU|DP|REGFILE|regR6|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(4));

-- Location: FF_X81_Y9_N8
\CPU|DP|REGFILE|regR4|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(4));

-- Location: LABCELL_X81_Y9_N6
\CPU|DP|REGFILE|mux_out|m|Selector11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector11~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(4) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|struct_decoder|nsel_mux|out\(0)) # (\CPU|DP|REGFILE|regR5|out\(4)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(4) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR5|out\(4) & (!\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(4) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR6|out\(4) & (\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(4) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|DP|REGFILE|regR6|out\(4) & (\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000000001010000000000000101111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR5|ALT_INV_out\(4),
	datab => \CPU|DP|REGFILE|regR6|ALT_INV_out\(4),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(4),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector11~1_combout\);

-- Location: FF_X79_Y8_N40
\CPU|DP|REGFILE|regR0|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(4));

-- Location: FF_X80_Y8_N58
\CPU|DP|REGFILE|regR1|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(4));

-- Location: FF_X82_Y8_N10
\CPU|DP|REGFILE|regR3|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(4));

-- Location: FF_X82_Y7_N26
\CPU|DP|REGFILE|regR2|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(4));

-- Location: MLABCELL_X82_Y7_N24
\CPU|DP|REGFILE|mux_out|m|Selector11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector11~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(4) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|regR3|out\(4)) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(4) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & \CPU|DP|REGFILE|regR3|out\(4)) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(4) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(4))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ((\CPU|DP|REGFILE|regR1|out\(4)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(4) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(4))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & 
-- ((\CPU|DP|REGFILE|regR1|out\(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101001101010011010100110101001100000000111100000000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR0|ALT_INV_out\(4),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(4),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	datad => \CPU|DP|REGFILE|regR3|ALT_INV_out\(4),
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(4),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector11~0_combout\);

-- Location: MLABCELL_X82_Y7_N57
\CPU|DP|REGFILE|mux_out|m|Selector11~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector11~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector11~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # 
-- ((\CPU|DP|REGFILE|mux_out|m|Selector11~1_combout\)))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (((\CPU|DP|REGFILE|regR7|out\(4))))) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector11~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ 
-- & ((\CPU|DP|REGFILE|mux_out|m|Selector11~1_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(4))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111000001011010111110001101101011111000110110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datab => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datac => \CPU|DP|REGFILE|regR7|ALT_INV_out\(4),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector11~1_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector11~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector11~2_combout\);

-- Location: FF_X82_Y7_N59
\CPU|DP|rA|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector11~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(4));

-- Location: LABCELL_X85_Y8_N0
\CPU|DP|U2|ALU_AddSub|ai|g[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(4) = ( \CPU|DP|Bin[4]~13_combout\ & ( (!\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(4) & !\CPU|DP|Ain[4]~1_combout\)) ) ) # ( !\CPU|DP|Bin[4]~13_combout\ & ( (\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(4) & 
-- !\CPU|DP|Ain[4]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(4),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[4]~13_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(4));

-- Location: FF_X81_Y9_N2
\CPU|DP|rA|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector9~5_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(6));

-- Location: LABCELL_X85_Y9_N33
\CPU|DP|U2|ALU_AddSub|ai|g[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(6) = ( !\CPU|DP|Ain[4]~1_combout\ & ( \CPU|DP|Bin[6]~11_combout\ & ( (\CPU|DP|rA|out\(6) & !\CPU|struct_reg|out\(11)) ) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( !\CPU|DP|Bin[6]~11_combout\ & ( (\CPU|DP|rA|out\(6) & 
-- \CPU|struct_reg|out\(11)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000000000001010101000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(6),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[6]~11_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(6));

-- Location: LABCELL_X85_Y8_N18
\CPU|DP|U2|ALU_AddSub|ai|p[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(6) = ( \CPU|DP|Bin[6]~11_combout\ & ( !\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(6) & !\CPU|DP|Ain[4]~1_combout\))) ) ) # ( !\CPU|DP|Bin[6]~11_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(6)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110000110011001111000011001111000011110011001100001111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(6),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[6]~11_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(6));

-- Location: FF_X82_Y7_N2
\CPU|DP|rA|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector10~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(5));

-- Location: FF_X82_Y7_N44
\CPU|DP|rB|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector11~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(4));

-- Location: MLABCELL_X82_Y7_N45
\CPU|DP|Bin[5]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[5]~12_combout\ = ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(4) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(5) ) ) ) # ( \CPU|DP|Bin[3]~1_combout\ & ( 
-- !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(6) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011010101010101010100000000111111110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(6),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|DP|rB|ALT_INV_out\(4),
	datad => \CPU|DP|rB|ALT_INV_out\(5),
	datae => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	combout => \CPU|DP|Bin[5]~12_combout\);

-- Location: LABCELL_X85_Y8_N57
\CPU|DP|U2|ALU_AddSub|ai|g[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(5) = ( \CPU|DP|Bin[5]~12_combout\ & ( (!\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(5) & !\CPU|DP|Ain[4]~1_combout\)) ) ) # ( !\CPU|DP|Bin[5]~12_combout\ & ( (\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(5) & 
-- !\CPU|DP|Ain[4]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(5),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[5]~12_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(5));

-- Location: MLABCELL_X84_Y6_N15
\CPU|DP|U2|ALU_AddSub|ai|p[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(5) = ( \CPU|DP|Bin[5]~12_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(5)))) ) ) # ( !\CPU|DP|Bin[5]~12_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(5)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110100101010101011010010110101010010110101010101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datad => \CPU|DP|rA|ALT_INV_out\(5),
	dataf => \CPU|DP|ALT_INV_Bin[5]~12_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(5));

-- Location: LABCELL_X85_Y8_N48
\CPU|DP|U2|ALU_AddSub|ai|c[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(7) = ( \CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( \CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(6) & !\CPU|DP|U2|ALU_AddSub|ai|p\(6)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( 
-- \CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(6) & ((!\CPU|DP|U2|ALU_AddSub|ai|p\(6)) # (!\CPU|DP|U2|ALU_AddSub|ai|g\(5)))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( !\CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ & ( 
-- (!\CPU|DP|U2|ALU_AddSub|ai|g\(6) & ((!\CPU|DP|U2|ALU_AddSub|ai|p\(6)) # ((!\CPU|DP|U2|ALU_AddSub|ai|g\(4) & !\CPU|DP|U2|ALU_AddSub|ai|g\(5))))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( !\CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ & ( 
-- (!\CPU|DP|U2|ALU_AddSub|ai|g\(6) & ((!\CPU|DP|U2|ALU_AddSub|ai|p\(6)) # (!\CPU|DP|U2|ALU_AddSub|ai|g\(5)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011000000110010001100000011001100110000001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(4),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(6),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(6),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(5),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(5),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~0_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(7));

-- Location: LABCELL_X85_Y8_N24
\CPU|DP|U2|ALU_AddSub|ai|comb~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|p\(8) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (\CPU|DP|U2|ALU_AddSub|ai|p\(9) & ((\CPU|DP|U2|ALU_AddSub|ai|g\(8)) # (\CPU|DP|U2|ALU_AddSub|ai|g\(7)))) ) ) ) # ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|p\(8) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (\CPU|DP|U2|ALU_AddSub|ai|p\(9) & \CPU|DP|U2|ALU_AddSub|ai|g\(8)) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|p\(8) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (\CPU|DP|U2|ALU_AddSub|ai|p\(9) & 
-- (((\CPU|DP|U2|ALU_AddSub|ai|p\(7)) # (\CPU|DP|U2|ALU_AddSub|ai|g\(8))) # (\CPU|DP|U2|ALU_AddSub|ai|g\(7)))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(8) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (\CPU|DP|U2|ALU_AddSub|ai|p\(9) & \CPU|DP|U2|ALU_AddSub|ai|g\(8)) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000101010101010100000101000001010001010100010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(9),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(7),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(8),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(7),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(8),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(7),
	combout => \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\);

-- Location: MLABCELL_X84_Y8_N57
\CPU|DP|U2|ALU_AddSub|ai|c[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(12) = ( \CPU|DP|U2|ALU_AddSub|ai|g\(10) & ( \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|p\(11) & !\CPU|DP|U2|ALU_AddSub|ai|g\(11)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(10) & ( 
-- \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(11) & ((!\CPU|DP|U2|ALU_AddSub|ai|p\(11)) # (!\CPU|DP|U2|ALU_AddSub|ai|p\(10)))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|g\(10) & ( !\CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( 
-- (!\CPU|DP|U2|ALU_AddSub|ai|p\(11) & !\CPU|DP|U2|ALU_AddSub|ai|g\(11)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(10) & ( !\CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(11) & ((!\CPU|DP|U2|ALU_AddSub|ai|p\(11)) # 
-- ((!\CPU|DP|U2|ALU_AddSub|ai|p\(10)) # (!\CPU|DP|U2|ALU_AddSub|ai|g\(9))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011100000101000001010000011100000111000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(11),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(10),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(11),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(9),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(10),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(12));

-- Location: MLABCELL_X84_Y8_N12
\CPU|DP|U2|ALU_AddSub|ai|c[14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(14) = ( \CPU|DP|U2|ALU_AddSub|ai|p\(12) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|DP|U2|ALU_AddSub|comb~1_combout\ & (\CPU|DP|rA|out\(13) & (\CPU|DP|U2|ALU_AddSub|ai|g\(12) & !\CPU|DP|Ain[4]~1_combout\))) # 
-- (\CPU|DP|U2|ALU_AddSub|comb~1_combout\ & (((\CPU|DP|rA|out\(13) & !\CPU|DP|Ain[4]~1_combout\)) # (\CPU|DP|U2|ALU_AddSub|ai|g\(12)))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(12) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|DP|U2|ALU_AddSub|comb~1_combout\ 
-- & (\CPU|DP|rA|out\(13) & (\CPU|DP|U2|ALU_AddSub|ai|g\(12) & !\CPU|DP|Ain[4]~1_combout\))) # (\CPU|DP|U2|ALU_AddSub|comb~1_combout\ & (((\CPU|DP|rA|out\(13) & !\CPU|DP|Ain[4]~1_combout\)) # (\CPU|DP|U2|ALU_AddSub|ai|g\(12)))) ) ) ) # ( 
-- \CPU|DP|U2|ALU_AddSub|ai|p\(12) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( ((\CPU|DP|rA|out\(13) & !\CPU|DP|Ain[4]~1_combout\)) # (\CPU|DP|U2|ALU_AddSub|comb~1_combout\) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(12) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( 
-- (!\CPU|DP|U2|ALU_AddSub|comb~1_combout\ & (\CPU|DP|rA|out\(13) & (\CPU|DP|U2|ALU_AddSub|ai|g\(12) & !\CPU|DP|Ain[4]~1_combout\))) # (\CPU|DP|U2|ALU_AddSub|comb~1_combout\ & (((\CPU|DP|rA|out\(13) & !\CPU|DP|Ain[4]~1_combout\)) # 
-- (\CPU|DP|U2|ALU_AddSub|ai|g\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001011100000011011101110011001100010111000000110001011100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(13),
	datab => \CPU|DP|U2|ALU_AddSub|ALT_INV_comb~1_combout\,
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(12),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(12),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(12),
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(14));

-- Location: MLABCELL_X84_Y9_N0
\CPU|DP|U2|main|Mux0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux0~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|as|p\(0) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( (!\CPU|struct_reg|out\(12) & (((!\CPU|DP|Ain[14]~2_combout\ & !\CPU|DP|U2|ALU_AddSub|comb~0_combout\)))) # (\CPU|struct_reg|out\(12) & 
-- (\CPU|DP|U2|main|Mux0~0_combout\)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|as|p\(0) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( (!\CPU|struct_reg|out\(12) & (((\CPU|DP|U2|ALU_AddSub|comb~0_combout\) # (\CPU|DP|Ain[14]~2_combout\)))) # (\CPU|struct_reg|out\(12) & 
-- (\CPU|DP|U2|main|Mux0~0_combout\)) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|as|p\(0) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( (!\CPU|struct_reg|out\(12) & (((!\CPU|DP|Ain[14]~2_combout\) # (!\CPU|DP|U2|ALU_AddSub|comb~0_combout\)))) # (\CPU|struct_reg|out\(12) & 
-- (\CPU|DP|U2|main|Mux0~0_combout\)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|as|p\(0) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( (!\CPU|struct_reg|out\(12) & (((\CPU|DP|Ain[14]~2_combout\ & \CPU|DP|U2|ALU_AddSub|comb~0_combout\)))) # (\CPU|struct_reg|out\(12) & 
-- (\CPU|DP|U2|main|Mux0~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100011101110111011101000100011101110111011101000100010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|main|ALT_INV_Mux0~0_combout\,
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Ain[14]~2_combout\,
	datad => \CPU|DP|U2|ALU_AddSub|ALT_INV_comb~0_combout\,
	datae => \CPU|DP|U2|ALU_AddSub|as|ALT_INV_p\(0),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(14),
	combout => \CPU|DP|U2|main|Mux0~1_combout\);

-- Location: FF_X84_Y9_N2
\CPU|DP|rC|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux0~1_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(15));

-- Location: M10K_X76_Y7_N0
\MEM|mem_rtl_0|auto_generated|ram_block1a0\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	mem_init4 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init3 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init2 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init0 => "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000CCCC00000000FF00000040E0000000668000000066BF000000C004000000A485000000D501000000A485000000B8A5000000A2A3000000A08100000086BF0000008680000000E00000000084000000005F02000000D314000000D209000000D105000000D0010000008460000000D300000000D41900000066C0000000D618",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	init_file => "db/lab8_top.ram0_RAM_adc09678.hdl.mif",
	init_file_layout => "port_a",
	logical_ram_name => "RAM:MEM|altsyncram:mem_rtl_0|altsyncram_ser1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 8,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 40,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 255,
	port_a_logical_ram_depth => 256,
	port_a_logical_ram_width => 16,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 8,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 40,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 255,
	port_b_logical_ram_depth => 256,
	port_b_logical_ram_width => 16,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \write~combout\,
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	portadatain => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LABCELL_X75_Y7_N51
\CPU|DP|U3|b[15]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[15]~1_combout\ = ( !\CPU|DP|U3|b~0_combout\ & ( \CPU|FSM|WideOr26~combout\ ) ) # ( !\CPU|DP|U3|b~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( !\CPU|DP|rC|out\(15) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|rC|ALT_INV_out\(15),
	datae => \CPU|DP|U3|ALT_INV_b~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[15]~1_combout\);

-- Location: LABCELL_X80_Y7_N36
\CPU|DP|U3|b[15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(15) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( \CPU|DP|U3|b[15]~1_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\LED_load~4_combout\) # (!\Equal5~0_combout\)))) ) ) ) # ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( !\CPU|DP|U3|b[15]~1_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( !\CPU|DP|U3|b[15]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~4_combout\,
	datad => \ALT_INV_Equal5~0_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\,
	dataf => \CPU|DP|U3|ALT_INV_b[15]~1_combout\,
	combout => \CPU|DP|U3|b\(15));

-- Location: LABCELL_X79_Y7_N54
\CPU|DP|REGFILE|regR7|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[15]~feeder_combout\ = ( \CPU|DP|U3|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(15),
	combout => \CPU|DP|REGFILE|regR7|out[15]~feeder_combout\);

-- Location: FF_X79_Y7_N55
\CPU|DP|REGFILE|regR7|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[15]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(15));

-- Location: FF_X80_Y9_N35
\CPU|DP|REGFILE|regR0|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(15));

-- Location: LABCELL_X77_Y7_N0
\CPU|DP|REGFILE|regR3|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[15]~feeder_combout\ = ( \CPU|DP|U3|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(15),
	combout => \CPU|DP|REGFILE|regR3|out[15]~feeder_combout\);

-- Location: FF_X77_Y7_N1
\CPU|DP|REGFILE|regR3|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[15]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(15));

-- Location: LABCELL_X77_Y7_N6
\CPU|DP|REGFILE|regR1|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[15]~feeder_combout\ = ( \CPU|DP|U3|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(15),
	combout => \CPU|DP|REGFILE|regR1|out[15]~feeder_combout\);

-- Location: FF_X77_Y7_N7
\CPU|DP|REGFILE|regR1|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[15]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(15));

-- Location: LABCELL_X79_Y9_N54
\CPU|DP|REGFILE|regR2|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR2|out[15]~feeder_combout\ = ( \CPU|DP|U3|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(15),
	combout => \CPU|DP|REGFILE|regR2|out[15]~feeder_combout\);

-- Location: FF_X79_Y9_N56
\CPU|DP|REGFILE|regR2|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR2|out[15]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(15));

-- Location: LABCELL_X79_Y9_N3
\CPU|DP|REGFILE|mux_out|m|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(15) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) # (\CPU|DP|REGFILE|regR3|out\(15)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(15) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|regR3|out\(15) & !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(15) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(15))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ((\CPU|DP|REGFILE|regR1|out\(15)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(15) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(15))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & 
-- ((\CPU|DP|REGFILE|regR1|out\(15)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001011111010100000101111100110000001100000011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR0|ALT_INV_out\(15),
	datab => \CPU|DP|REGFILE|regR3|ALT_INV_out\(15),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	datad => \CPU|DP|REGFILE|regR1|ALT_INV_out\(15),
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(15),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\);

-- Location: MLABCELL_X78_Y7_N30
\CPU|DP|REGFILE|regR6|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR6|out[15]~feeder_combout\ = ( \CPU|DP|U3|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(15),
	combout => \CPU|DP|REGFILE|regR6|out[15]~feeder_combout\);

-- Location: FF_X78_Y7_N31
\CPU|DP|REGFILE|regR6|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR6|out[15]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(15));

-- Location: FF_X80_Y7_N56
\CPU|DP|REGFILE|regR4|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(15));

-- Location: FF_X80_Y7_N38
\CPU|DP|REGFILE|regR5|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(15),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(15));

-- Location: MLABCELL_X78_Y8_N36
\CPU|DP|REGFILE|mux_out|m|Selector0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\ = ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & ((\CPU|DP|REGFILE|regR5|out\(15)))) # (\CPU|struct_decoder|nsel_mux|out\(0) & 
-- (\CPU|DP|REGFILE|regR4|out\(15))) ) ) ) # ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR6|out\(15) & \CPU|struct_decoder|nsel_mux|out\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000000000000001111001100110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR6|ALT_INV_out\(15),
	datab => \CPU|DP|REGFILE|regR4|ALT_INV_out\(15),
	datac => \CPU|DP|REGFILE|regR5|ALT_INV_out\(15),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\);

-- Location: LABCELL_X79_Y9_N12
\CPU|DP|REGFILE|mux_out|m|Selector0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector0~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(15)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(15)) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(15))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector0~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector0~1_combout\ & ( (\CPU|DP|REGFILE|regR7|out\(15) & \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101111100000101010111111111010101011111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR7|ALT_INV_out\(15),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector0~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector0~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector0~2_combout\);

-- Location: FF_X80_Y9_N38
\CPU|DP|rB|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector0~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(15));

-- Location: MLABCELL_X84_Y8_N24
\CPU|DP|Bin[14]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[14]~3_combout\ = ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(13) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(14) ) ) ) # ( \CPU|DP|Bin[3]~1_combout\ & ( 
-- !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(15) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000011110000111101010101010101010000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(14),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|DP|rB|ALT_INV_out\(15),
	datad => \CPU|DP|rB|ALT_INV_out\(13),
	datae => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	combout => \CPU|DP|Bin[14]~3_combout\);

-- Location: MLABCELL_X84_Y9_N57
\CPU|DP|U2|main|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux1~0_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( (!\CPU|struct_reg|out\(11) & ((!\CPU|DP|Ain[14]~2_combout\ & (!\CPU|struct_reg|out\(12) & !\CPU|DP|Bin[14]~3_combout\)) # (\CPU|DP|Ain[14]~2_combout\ & 
-- ((\CPU|DP|Bin[14]~3_combout\))))) # (\CPU|struct_reg|out\(11) & (!\CPU|DP|Bin[14]~3_combout\ $ (((!\CPU|struct_reg|out\(12) & !\CPU|DP|Ain[14]~2_combout\))))) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( (!\CPU|struct_reg|out\(12) & 
-- (!\CPU|struct_reg|out\(11) $ (!\CPU|DP|Ain[14]~2_combout\ $ (\CPU|DP|Bin[14]~3_combout\)))) # (\CPU|struct_reg|out\(12) & ((!\CPU|struct_reg|out\(11) & (\CPU|DP|Ain[14]~2_combout\ & \CPU|DP|Bin[14]~3_combout\)) # (\CPU|struct_reg|out\(11) & 
-- ((!\CPU|DP|Bin[14]~3_combout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101100110000110010110011000011010010101010010101001010101001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Ain[14]~2_combout\,
	datad => \CPU|DP|ALT_INV_Bin[14]~3_combout\,
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(14),
	combout => \CPU|DP|U2|main|Mux1~0_combout\);

-- Location: FF_X84_Y9_N59
\CPU|DP|rC|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux1~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(14));

-- Location: LABCELL_X77_Y7_N30
\CPU|DP|U3|b[14]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[14]~2_combout\ = ( !\CPU|DP|U3|b~0_combout\ & ( (!\CPU|DP|rC|out\(14)) # (\CPU|FSM|WideOr26~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100110011000000000000000011111111001100110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr26~combout\,
	datad => \CPU|DP|rC|ALT_INV_out\(14),
	datae => \CPU|DP|U3|ALT_INV_b~0_combout\,
	combout => \CPU|DP|U3|b[14]~2_combout\);

-- Location: LABCELL_X80_Y7_N12
\CPU|DP|U3|b[14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(14) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( \CPU|DP|U3|b[14]~2_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\LED_load~4_combout\) # (!\Equal5~0_combout\)))) ) ) ) # ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( !\CPU|DP|U3|b[14]~2_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( !\CPU|DP|U3|b[14]~2_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~4_combout\,
	datad => \ALT_INV_Equal5~0_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\,
	dataf => \CPU|DP|U3|ALT_INV_b[14]~2_combout\,
	combout => \CPU|DP|U3|b\(14));

-- Location: LABCELL_X75_Y7_N33
\CPU|DP|REGFILE|regR7|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[14]~feeder_combout\ = ( \CPU|DP|U3|b\(14) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(14),
	combout => \CPU|DP|REGFILE|regR7|out[14]~feeder_combout\);

-- Location: FF_X75_Y7_N34
\CPU|DP|REGFILE|regR7|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[14]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(14));

-- Location: FF_X81_Y6_N19
\CPU|DP|REGFILE|regR6|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(14));

-- Location: FF_X80_Y7_N13
\CPU|DP|REGFILE|regR5|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(14),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(14));

-- Location: FF_X80_Y7_N44
\CPU|DP|REGFILE|regR4|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(14));

-- Location: LABCELL_X80_Y6_N15
\CPU|DP|REGFILE|mux_out|m|Selector1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector1~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(14) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(1) & (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(14)))) # 
-- (\CPU|struct_decoder|nsel_mux|out\(1) & (((\CPU|DP|REGFILE|regR5|out\(14))) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(14) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(1) & 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(14)))) # (\CPU|struct_decoder|nsel_mux|out\(1) & (!\CPU|struct_decoder|nsel_mux|out\(0) & ((\CPU|DP|REGFILE|regR5|out\(14))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001001000110000100110101011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datac => \CPU|DP|REGFILE|regR6|ALT_INV_out\(14),
	datad => \CPU|DP|REGFILE|regR5|ALT_INV_out\(14),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(14),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector1~1_combout\);

-- Location: FF_X79_Y7_N29
\CPU|DP|REGFILE|regR3|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(14));

-- Location: FF_X79_Y7_N14
\CPU|DP|REGFILE|regR1|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(14));

-- Location: LABCELL_X79_Y6_N18
\CPU|DP|REGFILE|regR0|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR0|out[14]~feeder_combout\ = \CPU|DP|U3|b\(14)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|U3|ALT_INV_b\(14),
	combout => \CPU|DP|REGFILE|regR0|out[14]~feeder_combout\);

-- Location: FF_X79_Y6_N20
\CPU|DP|REGFILE|regR0|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR0|out[14]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(14));

-- Location: FF_X79_Y6_N50
\CPU|DP|REGFILE|regR2|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(14));

-- Location: LABCELL_X79_Y6_N48
\CPU|DP|REGFILE|mux_out|m|Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(14) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(14)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(14) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(14) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(14) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(14)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR3|out\(14))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(14) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(14)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- (\CPU|DP|REGFILE|regR3|out\(14))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101000011110101010100110011000000000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR3|ALT_INV_out\(14),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(14),
	datac => \CPU|DP|REGFILE|regR0|ALT_INV_out\(14),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(14),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\);

-- Location: LABCELL_X79_Y6_N39
\CPU|DP|REGFILE|mux_out|m|Selector1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector1~2_combout\ = ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(14) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # (\CPU|DP|REGFILE|mux_out|m|Selector1~1_combout\) ) ) ) # ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(14) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector1~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector1~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111010101010101010111110000111111110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR7|ALT_INV_out\(14),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~1_combout\,
	datae => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector1~2_combout\);

-- Location: MLABCELL_X78_Y6_N0
\CPU|DP|rB|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|rB|out[14]~feeder_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector1~2_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector1~2_combout\,
	combout => \CPU|DP|rB|out[14]~feeder_combout\);

-- Location: FF_X78_Y6_N1
\CPU|DP|rB|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|rB|out[14]~feeder_combout\,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(14));

-- Location: MLABCELL_X84_Y7_N51
\CPU|DP|Bin[13]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[13]~4_combout\ = ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|rB|out\(12) ) ) ) # ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|rB|out\(14) ) ) ) # ( \CPU|DP|Bin[3]~2_combout\ & ( 
-- !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|rB|out\(13) ) ) ) # ( !\CPU|DP|Bin[3]~2_combout\ & ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000001111111100110011001100110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(12),
	datab => \CPU|DP|rB|ALT_INV_out\(14),
	datac => \CPU|struct_reg|ALT_INV_out\(4),
	datad => \CPU|DP|rB|ALT_INV_out\(13),
	datae => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	combout => \CPU|DP|Bin[13]~4_combout\);

-- Location: MLABCELL_X84_Y8_N39
\CPU|DP|U2|ALU_AddSub|ai|p[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(13) = ( \CPU|DP|rA|out\(13) & ( !\CPU|DP|Ain[4]~1_combout\ $ (!\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[13]~4_combout\)) ) ) # ( !\CPU|DP|rA|out\(13) & ( !\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[13]~4_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110000000011111111000011000011001111001100001100111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[13]~4_combout\,
	dataf => \CPU|DP|rA|ALT_INV_out\(13),
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(13));

-- Location: MLABCELL_X84_Y8_N36
\CPU|DP|U2|main|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux2~0_combout\ = ( \CPU|DP|rA|out\(13) & ( (!\CPU|DP|Bin[13]~4_combout\ & ((\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[13]~4_combout\ & (!\CPU|DP|Ain[4]~1_combout\ & !\CPU|struct_reg|out\(11))) ) ) # ( !\CPU|DP|rA|out\(13) & ( 
-- (!\CPU|DP|Bin[13]~4_combout\ & \CPU|struct_reg|out\(11)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000001100111100000000110011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|DP|ALT_INV_Bin[13]~4_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|rA|ALT_INV_out\(13),
	combout => \CPU|DP|U2|main|Mux2~0_combout\);

-- Location: MLABCELL_X84_Y8_N0
\CPU|DP|U2|main|Mux2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux2~1_combout\ = ( \CPU|DP|U2|main|Mux2~0_combout\ & ( \CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(12) $ (!\CPU|DP|U2|ALU_AddSub|ai|p\(13))) # (\CPU|struct_reg|out\(12)) ) ) ) # ( !\CPU|DP|U2|main|Mux2~0_combout\ & ( 
-- \CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|g\(12) $ (!\CPU|DP|U2|ALU_AddSub|ai|p\(13)))) ) ) ) # ( \CPU|DP|U2|main|Mux2~0_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|DP|U2|ALU_AddSub|ai|p\(13) 
-- $ (((!\CPU|DP|U2|ALU_AddSub|ai|p\(12) & !\CPU|DP|U2|ALU_AddSub|ai|g\(12))))) # (\CPU|struct_reg|out\(12)) ) ) ) # ( !\CPU|DP|U2|main|Mux2~0_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(13) $ 
-- (((!\CPU|DP|U2|ALU_AddSub|ai|p\(12) & !\CPU|DP|U2|ALU_AddSub|ai|g\(12)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100110010000000011111111011001100001100110000000011111111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(12),
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(12),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(13),
	datae => \CPU|DP|U2|main|ALT_INV_Mux2~0_combout\,
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(12),
	combout => \CPU|DP|U2|main|Mux2~1_combout\);

-- Location: FF_X84_Y8_N2
\CPU|DP|rC|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux2~1_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(13));

-- Location: IOIBUF_X4_Y0_N52
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: LABCELL_X85_Y6_N42
\Selector12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector12~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a3\)) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a3\)) # (\Equal5~0_combout\ & 
-- ((\SW[3]~input_o\))))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a3\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010100010101110101010001010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\,
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_Equal5~0_combout\,
	datad => \ALT_INV_SW[3]~input_o\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector12~0_combout\);

-- Location: FF_X85_Y6_N44
\CPU|struct_reg|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector12~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(3));

-- Location: LABCELL_X85_Y6_N39
\CPU|DP|Bin[3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~0_combout\ = ( \CPU|struct_reg|out\(13) & ( (!\CPU|struct_reg|out\(4) & !\CPU|struct_reg|out\(3)) ) ) # ( !\CPU|struct_reg|out\(13) & ( (!\CPU|struct_reg|out\(4) & ((!\CPU|struct_reg|out\(3)) # ((!\CPU|struct_reg|out\(14) & 
-- \CPU|struct_reg|out\(15))))) # (\CPU|struct_reg|out\(4) & (((!\CPU|struct_reg|out\(14) & \CPU|struct_reg|out\(15))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100011111000100010001111100010001000100010001000100010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(4),
	datab => \CPU|struct_reg|ALT_INV_out\(3),
	datac => \CPU|struct_reg|ALT_INV_out\(14),
	datad => \CPU|struct_reg|ALT_INV_out\(15),
	dataf => \CPU|struct_reg|ALT_INV_out\(13),
	combout => \CPU|DP|Bin[3]~0_combout\);

-- Location: MLABCELL_X82_Y7_N9
\CPU|DP|Bin[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~1_combout\ = ( \CPU|FSM|WideNor12~combout\ & ( !\CPU|DP|Bin[3]~0_combout\ ) ) # ( !\CPU|FSM|WideNor12~combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( (((!\CPU|FSM|always0~2_combout\) # (!\CPU|FSM|WideNor15~0_combout\)) # 
-- (\CPU|FSM|WideNor13~combout\)) # (\CPU|FSM|WideNor5~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datac => \CPU|FSM|ALT_INV_always0~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor12~combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~0_combout\,
	combout => \CPU|DP|Bin[3]~1_combout\);

-- Location: MLABCELL_X82_Y7_N18
\CPU|DP|Bin[11]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[11]~6_combout\ = ( \CPU|DP|rB|out\(12) & ( \CPU|DP|Bin[3]~1_combout\ & ( (!\CPU|DP|Bin[3]~2_combout\) # (\CPU|DP|rB|out\(10)) ) ) ) # ( !\CPU|DP|rB|out\(12) & ( \CPU|DP|Bin[3]~1_combout\ & ( (\CPU|DP|rB|out\(10) & \CPU|DP|Bin[3]~2_combout\) ) 
-- ) ) # ( \CPU|DP|rB|out\(12) & ( !\CPU|DP|Bin[3]~1_combout\ & ( (!\CPU|DP|Bin[3]~2_combout\ & (\CPU|struct_reg|out\(4))) # (\CPU|DP|Bin[3]~2_combout\ & ((\CPU|DP|rB|out\(11)))) ) ) ) # ( !\CPU|DP|rB|out\(12) & ( !\CPU|DP|Bin[3]~1_combout\ & ( 
-- (!\CPU|DP|Bin[3]~2_combout\ & (\CPU|struct_reg|out\(4))) # (\CPU|DP|Bin[3]~2_combout\ & ((\CPU|DP|rB|out\(11)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100001111010101010000111100000000001100111111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(4),
	datab => \CPU|DP|rB|ALT_INV_out\(10),
	datac => \CPU|DP|rB|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	datae => \CPU|DP|rB|ALT_INV_out\(12),
	dataf => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	combout => \CPU|DP|Bin[11]~6_combout\);

-- Location: LABCELL_X85_Y8_N3
\CPU|DP|U2|ALU_AddSub|ai|c[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(11) = ( \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|p\(10) & !\CPU|DP|U2|ALU_AddSub|ai|g\(10)) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(10) & 
-- ((!\CPU|DP|U2|ALU_AddSub|ai|g\(9)) # (!\CPU|DP|U2|ALU_AddSub|ai|p\(10)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110000000000111111000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(9),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(10),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(10),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(11));

-- Location: MLABCELL_X84_Y9_N30
\CPU|DP|U2|main|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux4~0_combout\ = ( \CPU|struct_reg|out\(11) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(11) & ( !\CPU|DP|Bin[11]~6_combout\ $ (((\CPU|DP|rA|out\(11) & (!\CPU|struct_reg|out\(12) & !\CPU|DP|Ain[4]~1_combout\)))) ) ) ) # ( !\CPU|struct_reg|out\(11) & ( 
-- \CPU|DP|U2|ALU_AddSub|ai|c\(11) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|Bin[11]~6_combout\ $ (((!\CPU|DP|rA|out\(11)) # (\CPU|DP|Ain[4]~1_combout\))))) # (\CPU|struct_reg|out\(12) & (\CPU|DP|rA|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & 
-- \CPU|DP|Bin[11]~6_combout\))) ) ) ) # ( \CPU|struct_reg|out\(11) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(11) & ( !\CPU|DP|Bin[11]~6_combout\ $ (((!\CPU|struct_reg|out\(12) & ((!\CPU|DP|rA|out\(11)) # (\CPU|DP|Ain[4]~1_combout\))))) ) ) ) # ( 
-- !\CPU|struct_reg|out\(11) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(11) & ( (!\CPU|DP|Bin[11]~6_combout\ & (!\CPU|struct_reg|out\(12) & ((!\CPU|DP|rA|out\(11)) # (\CPU|DP|Ain[4]~1_combout\)))) # (\CPU|DP|Bin[11]~6_combout\ & (\CPU|DP|rA|out\(11) & 
-- ((!\CPU|DP|Ain[4]~1_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110001010000011100111000110001000000100111001011111101000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(11),
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datad => \CPU|DP|ALT_INV_Bin[11]~6_combout\,
	datae => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(11),
	combout => \CPU|DP|U2|main|Mux4~0_combout\);

-- Location: FF_X84_Y9_N32
\CPU|DP|rC|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux4~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(11));

-- Location: LABCELL_X79_Y7_N18
\CPU|DP|U3|b[11]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[11]~5_combout\ = ( \CPU|FSM|WideOr26~combout\ & ( !\CPU|DP|U3|b~0_combout\ ) ) # ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|U3|b~0_combout\ & !\CPU|DP|rC|out\(11)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110011001100110011000000110000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datac => \CPU|DP|rC|ALT_INV_out\(11),
	datae => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[11]~5_combout\);

-- Location: LABCELL_X80_Y7_N39
\CPU|DP|U3|b[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(11) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( \CPU|DP|U3|b[11]~5_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\Equal5~0_combout\) # (!\LED_load~4_combout\)))) ) ) ) # ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( !\CPU|DP|U3|b[11]~5_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( !\CPU|DP|U3|b[11]~5_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_Equal5~0_combout\,
	datad => \ALT_INV_LED_load~4_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\,
	dataf => \CPU|DP|U3|ALT_INV_b[11]~5_combout\,
	combout => \CPU|DP|U3|b\(11));

-- Location: LABCELL_X79_Y7_N57
\CPU|DP|REGFILE|regR7|out[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[11]~feeder_combout\ = ( \CPU|DP|U3|b\(11) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(11),
	combout => \CPU|DP|REGFILE|regR7|out[11]~feeder_combout\);

-- Location: FF_X79_Y7_N59
\CPU|DP|REGFILE|regR7|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[11]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(11));

-- Location: FF_X80_Y7_N40
\CPU|DP|REGFILE|regR5|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(11),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(11));

-- Location: FF_X80_Y7_N58
\CPU|DP|REGFILE|regR4|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(11));

-- Location: MLABCELL_X78_Y7_N3
\CPU|DP|REGFILE|regR6|out[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR6|out[11]~feeder_combout\ = ( \CPU|DP|U3|b\(11) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(11),
	combout => \CPU|DP|REGFILE|regR6|out[11]~feeder_combout\);

-- Location: FF_X78_Y7_N4
\CPU|DP|REGFILE|regR6|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR6|out[11]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(11));

-- Location: MLABCELL_X84_Y7_N54
\CPU|DP|REGFILE|mux_out|m|Selector4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector4~1_combout\ = ( \CPU|struct_decoder|nsel_mux|out\(1) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR5|out\(11))) # (\CPU|struct_decoder|nsel_mux|out\(0) & 
-- ((\CPU|DP|REGFILE|regR4|out\(11)))) ) ) ) # ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|DP|REGFILE|regR6|out\(11)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101001001110010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(11),
	datac => \CPU|DP|REGFILE|regR4|ALT_INV_out\(11),
	datad => \CPU|DP|REGFILE|regR6|ALT_INV_out\(11),
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector4~1_combout\);

-- Location: FF_X79_Y7_N22
\CPU|DP|REGFILE|regR1|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(11));

-- Location: FF_X79_Y7_N38
\CPU|DP|REGFILE|regR3|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(11));

-- Location: FF_X83_Y7_N28
\CPU|DP|REGFILE|regR0|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(11));

-- Location: FF_X84_Y7_N14
\CPU|DP|REGFILE|regR2|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(11));

-- Location: MLABCELL_X84_Y7_N12
\CPU|DP|REGFILE|mux_out|m|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector4~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(11) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) # (\CPU|DP|REGFILE|regR3|out\(11)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(11) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|regR3|out\(11) & !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(11) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ((\CPU|DP|REGFILE|regR0|out\(11)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR1|out\(11))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(11) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ((\CPU|DP|REGFILE|regR0|out\(11)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & 
-- (\CPU|DP|REGFILE|regR1|out\(11))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111110101000001011111010100110000001100000011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR1|ALT_INV_out\(11),
	datab => \CPU|DP|REGFILE|regR3|ALT_INV_out\(11),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	datad => \CPU|DP|REGFILE|regR0|ALT_INV_out\(11),
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(11),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector4~0_combout\);

-- Location: MLABCELL_X84_Y7_N0
\CPU|DP|REGFILE|mux_out|m|Selector4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector4~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector4~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # 
-- ((\CPU|DP|REGFILE|mux_out|m|Selector4~1_combout\)))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (((\CPU|DP|REGFILE|regR7|out\(11))))) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector4~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & 
-- ((\CPU|DP|REGFILE|mux_out|m|Selector4~1_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111110001011110011111000101111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datab => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datac => \CPU|DP|REGFILE|regR7|ALT_INV_out\(11),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector4~1_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector4~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector4~2_combout\);

-- Location: FF_X84_Y7_N58
\CPU|DP|rB|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector4~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(11));

-- Location: MLABCELL_X82_Y7_N36
\CPU|DP|Bin[10]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[10]~7_combout\ = ( \CPU|DP|rB|out\(10) & ( \CPU|DP|Bin[3]~1_combout\ & ( (!\CPU|DP|Bin[3]~2_combout\ & ((\CPU|DP|rB|out\(11)))) # (\CPU|DP|Bin[3]~2_combout\ & (\CPU|DP|rB|out\(9))) ) ) ) # ( !\CPU|DP|rB|out\(10) & ( \CPU|DP|Bin[3]~1_combout\ & 
-- ( (!\CPU|DP|Bin[3]~2_combout\ & ((\CPU|DP|rB|out\(11)))) # (\CPU|DP|Bin[3]~2_combout\ & (\CPU|DP|rB|out\(9))) ) ) ) # ( \CPU|DP|rB|out\(10) & ( !\CPU|DP|Bin[3]~1_combout\ & ( (\CPU|DP|Bin[3]~2_combout\) # (\CPU|struct_reg|out\(4)) ) ) ) # ( 
-- !\CPU|DP|rB|out\(10) & ( !\CPU|DP|Bin[3]~1_combout\ & ( (\CPU|struct_reg|out\(4) & !\CPU|DP|Bin[3]~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000010101011111111100001111001100110000111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(4),
	datab => \CPU|DP|rB|ALT_INV_out\(9),
	datac => \CPU|DP|rB|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	datae => \CPU|DP|rB|ALT_INV_out\(10),
	dataf => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	combout => \CPU|DP|Bin[10]~7_combout\);

-- Location: MLABCELL_X84_Y8_N9
\CPU|DP|U2|ALU_AddSub|ai|p[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(10) = ( \CPU|DP|Ain[4]~1_combout\ & ( !\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[10]~7_combout\) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( !\CPU|DP|rA|out\(10) $ (!\CPU|struct_reg|out\(11) $ (\CPU|DP|Bin[10]~7_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101010100101010110101010010100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(10),
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[10]~7_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(10));

-- Location: MLABCELL_X84_Y8_N6
\CPU|DP|U2|main|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux5~0_combout\ = ( \CPU|DP|Ain[4]~1_combout\ & ( (\CPU|struct_reg|out\(11) & !\CPU|DP|Bin[10]~7_combout\) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( (!\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(10) & \CPU|DP|Bin[10]~7_combout\)) # 
-- (\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[10]~7_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001101000100001100110100010000110011000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(10),
	datab => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[10]~7_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|main|Mux5~0_combout\);

-- Location: LABCELL_X85_Y8_N15
\CPU|DP|U2|main|Mux5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux5~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|g\(9) & ( \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(10))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux5~0_combout\))) ) ) ) # 
-- ( !\CPU|DP|U2|ALU_AddSub|ai|g\(9) & ( \CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(10))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux5~0_combout\))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|g\(9) 
-- & ( !\CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(10))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux5~0_combout\))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(9) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|comb~1_combout\ & ( (!\CPU|struct_reg|out\(12) & (\CPU|DP|U2|ALU_AddSub|ai|p\(10))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux5~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001001110111100010001101110110001000110111011000100011011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(12),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(10),
	datad => \CPU|DP|U2|main|ALT_INV_Mux5~0_combout\,
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(9),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~1_combout\,
	combout => \CPU|DP|U2|main|Mux5~1_combout\);

-- Location: FF_X85_Y8_N40
\CPU|DP|rC|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U2|main|Mux5~1_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(10));

-- Location: LABCELL_X79_Y7_N15
\CPU|DP|U3|b[10]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[10]~6_combout\ = ( \CPU|FSM|WideOr26~combout\ & ( !\CPU|DP|U3|b~0_combout\ ) ) # ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(10) & !\CPU|DP|U3|b~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000111100001111000010100000101000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(10),
	datac => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[10]~6_combout\);

-- Location: LABCELL_X80_Y7_N15
\CPU|DP|U3|b[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(10) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( \CPU|DP|U3|b[10]~6_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\Equal5~0_combout\) # (!\LED_load~4_combout\)))) ) ) ) # ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( !\CPU|DP|U3|b[10]~6_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( !\CPU|DP|U3|b[10]~6_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_Equal5~0_combout\,
	datad => \ALT_INV_LED_load~4_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\,
	dataf => \CPU|DP|U3|ALT_INV_b[10]~6_combout\,
	combout => \CPU|DP|U3|b\(10));

-- Location: FF_X80_Y7_N26
\CPU|DP|REGFILE|regR4|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(10));

-- Location: FF_X80_Y7_N16
\CPU|DP|REGFILE|regR5|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(10),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(10));

-- Location: MLABCELL_X78_Y7_N45
\CPU|DP|REGFILE|regR6|out[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR6|out[10]~feeder_combout\ = ( \CPU|DP|U3|b\(10) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(10),
	combout => \CPU|DP|REGFILE|regR6|out[10]~feeder_combout\);

-- Location: FF_X78_Y7_N47
\CPU|DP|REGFILE|regR6|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR6|out[10]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(10));

-- Location: LABCELL_X81_Y7_N39
\CPU|DP|REGFILE|mux_out|m|Selector5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector5~1_combout\ = ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & ((\CPU|DP|REGFILE|regR5|out\(10)))) # (\CPU|struct_decoder|nsel_mux|out\(0) & 
-- (\CPU|DP|REGFILE|regR4|out\(10))) ) ) ) # ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|DP|REGFILE|regR6|out\(10)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000000000110101001101010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR4|ALT_INV_out\(10),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(10),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|DP|REGFILE|regR6|ALT_INV_out\(10),
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector5~1_combout\);

-- Location: FF_X79_Y7_N47
\CPU|DP|REGFILE|regR7|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(10));

-- Location: LABCELL_X77_Y7_N21
\CPU|DP|REGFILE|regR3|out[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[10]~feeder_combout\ = ( \CPU|DP|U3|b\(10) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(10),
	combout => \CPU|DP|REGFILE|regR3|out[10]~feeder_combout\);

-- Location: FF_X77_Y7_N22
\CPU|DP|REGFILE|regR3|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[10]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(10));

-- Location: FF_X77_Y7_N34
\CPU|DP|REGFILE|regR0|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(10));

-- Location: LABCELL_X77_Y7_N51
\CPU|DP|REGFILE|regR1|out[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[10]~feeder_combout\ = ( \CPU|DP|U3|b\(10) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(10),
	combout => \CPU|DP|REGFILE|regR1|out[10]~feeder_combout\);

-- Location: FF_X77_Y7_N52
\CPU|DP|REGFILE|regR1|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[10]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(10));

-- Location: FF_X82_Y7_N14
\CPU|DP|REGFILE|regR2|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(10));

-- Location: MLABCELL_X82_Y7_N12
\CPU|DP|REGFILE|mux_out|m|Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector5~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(10) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) # (\CPU|DP|REGFILE|regR3|out\(10)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(10) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (\CPU|DP|REGFILE|regR3|out\(10) & !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(10) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(10))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ((\CPU|DP|REGFILE|regR1|out\(10)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(10) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & (\CPU|DP|REGFILE|regR0|out\(10))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & 
-- ((\CPU|DP|REGFILE|regR1|out\(10)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000111111001100000011111101010000010100000101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR3|ALT_INV_out\(10),
	datab => \CPU|DP|REGFILE|regR0|ALT_INV_out\(10),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	datad => \CPU|DP|REGFILE|regR1|ALT_INV_out\(10),
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(10),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector5~0_combout\);

-- Location: MLABCELL_X82_Y7_N54
\CPU|DP|REGFILE|mux_out|m|Selector5~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector5~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector5~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # 
-- ((\CPU|DP|REGFILE|mux_out|m|Selector5~1_combout\)))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (((\CPU|DP|REGFILE|regR7|out\(10))))) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector5~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & 
-- (\CPU|DP|REGFILE|mux_out|m|Selector5~1_combout\)) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((\CPU|DP|REGFILE|regR7|out\(10)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001011111000010100101111110001010110111111000101011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datab => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector5~1_combout\,
	datad => \CPU|DP|REGFILE|regR7|ALT_INV_out\(10),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector5~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector5~2_combout\);

-- Location: FF_X82_Y7_N40
\CPU|DP|rB|out[10]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector5~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out[10]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y8_N33
\CPU|DP|Bin[9]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[9]~8_combout\ = ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(8) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(9) ) ) ) # ( \CPU|DP|Bin[3]~1_combout\ & ( 
-- !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out[10]~DUPLICATE_q\ ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101000000001111111100110011001100110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(4),
	datab => \CPU|DP|rB|ALT_INV_out\(9),
	datac => \CPU|DP|rB|ALT_INV_out\(8),
	datad => \CPU|DP|rB|ALT_INV_out[10]~DUPLICATE_q\,
	datae => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	combout => \CPU|DP|Bin[9]~8_combout\);

-- Location: MLABCELL_X84_Y8_N30
\CPU|DP|U2|ALU_AddSub|ai|c[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(9) = ( \CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(8) & !\CPU|DP|U2|ALU_AddSub|ai|p\(8)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|g\(8) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(8) & !\CPU|DP|U2|ALU_AddSub|ai|p\(8)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(8) & ((!\CPU|DP|U2|ALU_AddSub|ai|p\(7)) # (!\CPU|DP|U2|ALU_AddSub|ai|p\(8)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011000000110011000000000011001100110011001100110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(8),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(7),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(8),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(7),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(7),
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(9));

-- Location: LABCELL_X83_Y9_N51
\CPU|DP|U2|main|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux6~0_combout\ = ( \CPU|DP|rA|out\(9) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(9) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[9]~8_combout\ $ (!\CPU|DP|Ain[4]~1_combout\)))) # (\CPU|struct_reg|out\(12) & 
-- ((!\CPU|struct_reg|out\(11) & (\CPU|DP|Bin[9]~8_combout\ & !\CPU|DP|Ain[4]~1_combout\)) # (\CPU|struct_reg|out\(11) & (!\CPU|DP|Bin[9]~8_combout\)))) ) ) ) # ( !\CPU|DP|rA|out\(9) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(9) & ( (!\CPU|struct_reg|out\(11) & 
-- (!\CPU|struct_reg|out\(12) & \CPU|DP|Bin[9]~8_combout\)) # (\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[9]~8_combout\))) ) ) ) # ( \CPU|DP|rA|out\(9) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(9) & ( (!\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[9]~8_combout\ & 
-- (!\CPU|struct_reg|out\(12) & \CPU|DP|Ain[4]~1_combout\)) # (\CPU|DP|Bin[9]~8_combout\ & ((!\CPU|DP|Ain[4]~1_combout\))))) # (\CPU|struct_reg|out\(11) & (!\CPU|DP|Bin[9]~8_combout\ $ (((!\CPU|struct_reg|out\(12) & \CPU|DP|Ain[4]~1_combout\))))) ) ) ) # ( 
-- !\CPU|DP|rA|out\(9) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(9) & ( (!\CPU|struct_reg|out\(11) & (!\CPU|struct_reg|out\(12) & !\CPU|DP|Bin[9]~8_combout\)) # (\CPU|struct_reg|out\(11) & (!\CPU|struct_reg|out\(12) $ (!\CPU|DP|Bin[9]~8_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001010010010100010110101001010001011000010110001001011001011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Bin[9]~8_combout\,
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datae => \CPU|DP|rA|ALT_INV_out\(9),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(9),
	combout => \CPU|DP|U2|main|Mux6~0_combout\);

-- Location: FF_X83_Y9_N53
\CPU|DP|rC|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux6~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(9));

-- Location: MLABCELL_X78_Y7_N36
\CPU|DP|U3|b[9]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[9]~7_combout\ = ( \CPU|DP|rC|out\(9) & ( (\CPU|FSM|WideOr26~combout\ & !\CPU|DP|U3|b~0_combout\) ) ) # ( !\CPU|DP|rC|out\(9) & ( !\CPU|DP|U3|b~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000001100110000000011111111000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr26~combout\,
	datad => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datae => \CPU|DP|rC|ALT_INV_out\(9),
	combout => \CPU|DP|U3|b[9]~7_combout\);

-- Location: LABCELL_X80_Y7_N9
\CPU|DP|U3|b[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(9) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ & ( \CPU|DP|U3|b[9]~7_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\Equal5~0_combout\) # (!\LED_load~4_combout\)))) ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ & 
-- ( !\CPU|DP|U3|b[9]~7_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a9\ & ( !\CPU|DP|U3|b[9]~7_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_Equal5~0_combout\,
	datad => \ALT_INV_LED_load~4_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\,
	dataf => \CPU|DP|U3|ALT_INV_b[9]~7_combout\,
	combout => \CPU|DP|U3|b\(9));

-- Location: FF_X79_Y7_N35
\CPU|DP|REGFILE|regR7|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(9));

-- Location: FF_X81_Y8_N52
\CPU|DP|REGFILE|regR0|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(9));

-- Location: FF_X78_Y7_N28
\CPU|DP|REGFILE|regR1|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(9));

-- Location: FF_X79_Y9_N19
\CPU|DP|REGFILE|regR3|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(9));

-- Location: FF_X81_Y9_N56
\CPU|DP|REGFILE|regR2|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(9));

-- Location: LABCELL_X81_Y9_N54
\CPU|DP|REGFILE|mux_out|m|Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(9) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(9)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(9) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(9) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(9) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR0|out\(9))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR3|out\(9)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(9) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR0|out\(9))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- ((\CPU|DP|REGFILE|regR3|out\(9)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100001111010101010000111100110011000000000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR0|ALT_INV_out\(9),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(9),
	datac => \CPU|DP|REGFILE|regR3|ALT_INV_out\(9),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(9),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\);

-- Location: FF_X80_Y7_N11
\CPU|DP|REGFILE|regR5|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(9),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(9));

-- Location: FF_X78_Y7_N35
\CPU|DP|REGFILE|regR6|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(9));

-- Location: FF_X81_Y9_N26
\CPU|DP|REGFILE|regR4|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(9));

-- Location: LABCELL_X81_Y9_N24
\CPU|DP|REGFILE|mux_out|m|Selector6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(9) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|DP|REGFILE|regR5|out\(9)) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(9) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR5|out\(9) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(9) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(9) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(9) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(9) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000100010000000000111011100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(9),
	datac => \CPU|DP|REGFILE|regR6|ALT_INV_out\(9),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(9),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\);

-- Location: LABCELL_X81_Y9_N48
\CPU|DP|REGFILE|mux_out|m|Selector6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector6~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(9)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(9)) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(9))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector6~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector6~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & \CPU|DP|REGFILE|regR7|out\(9)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001101100011011000110111011101110111011101110111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datab => \CPU|DP|REGFILE|regR7|ALT_INV_out\(9),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector6~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector6~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector6~2_combout\);

-- Location: FF_X83_Y9_N13
\CPU|DP|rB|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector6~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(9));

-- Location: LABCELL_X85_Y9_N51
\CPU|DP|Bin[8]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[8]~9_combout\ = ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(7) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(8) ) ) ) # ( \CPU|DP|Bin[3]~1_combout\ & ( 
-- !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(9) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000011110000111100000000111111110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(7),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|DP|rB|ALT_INV_out\(9),
	datad => \CPU|DP|rB|ALT_INV_out\(8),
	datae => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	combout => \CPU|DP|Bin[8]~9_combout\);

-- Location: LABCELL_X85_Y9_N39
\CPU|DP|U2|ALU_AddSub|ai|p[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(8) = ( \CPU|DP|Bin[8]~9_combout\ & ( !\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(8) & !\CPU|DP|Ain[4]~1_combout\))) ) ) # ( !\CPU|DP|Bin[8]~9_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(8)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001010101010110100101010110100101101010101010010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(8),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[8]~9_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(8));

-- Location: LABCELL_X85_Y9_N54
\CPU|DP|U2|main|Mux7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux7~0_combout\ = ( \CPU|DP|Bin[8]~9_combout\ & ( (!\CPU|struct_reg|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(8))) ) ) # ( !\CPU|DP|Bin[8]~9_combout\ & ( \CPU|struct_reg|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100000000101000000000000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datad => \CPU|DP|rA|ALT_INV_out\(8),
	dataf => \CPU|DP|ALT_INV_Bin[8]~9_combout\,
	combout => \CPU|DP|U2|main|Mux7~0_combout\);

-- Location: LABCELL_X85_Y9_N24
\CPU|DP|U2|main|Mux7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux7~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(8))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux7~0_combout\))) ) ) ) # ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|struct_reg|out\(12) & (\CPU|DP|U2|ALU_AddSub|ai|p\(8))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux7~0_combout\))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(8))) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux7~0_combout\))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(7) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( 
-- (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(8) $ (((!\CPU|DP|U2|ALU_AddSub|ai|p\(7)))))) # (\CPU|struct_reg|out\(12) & (((\CPU|DP|U2|main|Mux7~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010011110001101100011011000110100100111001001111000110110001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(12),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(8),
	datac => \CPU|DP|U2|main|ALT_INV_Mux7~0_combout\,
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(7),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(7),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(7),
	combout => \CPU|DP|U2|main|Mux7~1_combout\);

-- Location: FF_X85_Y9_N26
\CPU|DP|rC|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux7~1_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(8));

-- Location: FF_X81_Y7_N37
\CPU|Data_Address|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(8),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(8));

-- Location: LABCELL_X79_Y7_N0
\CPU|mem_addr[8]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[8]~8_combout\ = ( \CPU|FSM|WideOr41~1_combout\ & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out[8]~DUPLICATE_q\ ) ) ) # ( !\CPU|FSM|WideOr41~1_combout\ & ( \CPU|FSM|always0~12_combout\ & ( 
-- \CPU|program_counter|out[8]~DUPLICATE_q\ ) ) ) # ( \CPU|FSM|WideOr41~1_combout\ & ( !\CPU|FSM|always0~12_combout\ & ( (!\CPU|FSM|WideNor2~combout\ & (\CPU|Data_Address|out\(8))) # (\CPU|FSM|WideNor2~combout\ & ((\CPU|program_counter|out[8]~DUPLICATE_q\))) 
-- ) ) ) # ( !\CPU|FSM|WideOr41~1_combout\ & ( !\CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out[8]~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100000011111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|Data_Address|ALT_INV_out\(8),
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|program_counter|ALT_INV_out[8]~DUPLICATE_q\,
	datae => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[8]~8_combout\);

-- Location: LABCELL_X80_Y7_N24
\LED_load~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED_load~4_combout\ = ( !\CPU|mem_addr[3]~4_combout\ & ( \CPU|mem_addr[8]~8_combout\ & ( (!\CPU|mem_addr[2]~3_combout\ & (!\CPU|mem_addr[1]~2_combout\ & (!\CPU|mem_addr[4]~5_combout\ & !\CPU|mem_addr[0]~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	datab => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datac => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	datad => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datae => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	dataf => \CPU|ALT_INV_mem_addr[8]~8_combout\,
	combout => \LED_load~4_combout\);

-- Location: LABCELL_X79_Y7_N9
\CPU|DP|U3|b[8]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[8]~8_combout\ = ( \CPU|FSM|WideOr26~combout\ & ( !\CPU|DP|U3|b~0_combout\ ) ) # ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(8) & !\CPU|DP|U3|b~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000111100001111000010100000101000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(8),
	datac => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[8]~8_combout\);

-- Location: LABCELL_X80_Y7_N48
\CPU|DP|U3|b[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(8) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a8\ & ( \CPU|DP|U3|b[8]~8_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\LED_load~4_combout\) # (!\Equal5~0_combout\)))) ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a8\ & 
-- ( !\CPU|DP|U3|b[8]~8_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a8\ & ( !\CPU|DP|U3|b[8]~8_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~4_combout\,
	datad => \ALT_INV_Equal5~0_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\,
	dataf => \CPU|DP|U3|ALT_INV_b[8]~8_combout\,
	combout => \CPU|DP|U3|b\(8));

-- Location: LABCELL_X79_Y7_N51
\CPU|DP|REGFILE|regR7|out[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[8]~feeder_combout\ = ( \CPU|DP|U3|b\(8) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(8),
	combout => \CPU|DP|REGFILE|regR7|out[8]~feeder_combout\);

-- Location: FF_X79_Y7_N52
\CPU|DP|REGFILE|regR7|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[8]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(8));

-- Location: FF_X79_Y7_N4
\CPU|DP|REGFILE|regR1|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(8));

-- Location: LABCELL_X77_Y7_N39
\CPU|DP|REGFILE|regR0|out[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR0|out[8]~feeder_combout\ = ( \CPU|DP|U3|b\(8) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(8),
	combout => \CPU|DP|REGFILE|regR0|out[8]~feeder_combout\);

-- Location: FF_X77_Y7_N40
\CPU|DP|REGFILE|regR0|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR0|out[8]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(8));

-- Location: FF_X81_Y9_N22
\CPU|DP|REGFILE|regR2|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(8));

-- Location: FF_X81_Y6_N46
\CPU|DP|REGFILE|regR3|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(8));

-- Location: LABCELL_X80_Y9_N6
\CPU|DP|REGFILE|mux_out|m|Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( \CPU|DP|REGFILE|regR2|out\(8) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( \CPU|DP|REGFILE|regR3|out\(8) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( \CPU|DP|REGFILE|regR1|out\(8) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ( \CPU|DP|REGFILE|regR0|out\(8) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011010101010101010100000000111111110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR1|ALT_INV_out\(8),
	datab => \CPU|DP|REGFILE|regR0|ALT_INV_out\(8),
	datac => \CPU|DP|REGFILE|regR2|ALT_INV_out\(8),
	datad => \CPU|DP|REGFILE|regR3|ALT_INV_out\(8),
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\);

-- Location: FF_X80_Y7_N50
\CPU|DP|REGFILE|regR5|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(8),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(8));

-- Location: FF_X81_Y6_N49
\CPU|DP|REGFILE|regR6|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(8));

-- Location: FF_X81_Y6_N2
\CPU|DP|REGFILE|regR4|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(8));

-- Location: LABCELL_X81_Y6_N0
\CPU|DP|REGFILE|mux_out|m|Selector7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(8) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|DP|REGFILE|regR5|out\(8)) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(8) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR5|out\(8) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(8) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & \CPU|DP|REGFILE|regR6|out\(8))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(8) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & \CPU|DP|REGFILE|regR6|out\(8))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000100000001000000111000001110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(8),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datad => \CPU|DP|REGFILE|regR6|ALT_INV_out\(8),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(8),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\);

-- Location: LABCELL_X83_Y9_N45
\CPU|DP|REGFILE|mux_out|m|Selector7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector7~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(8)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(8)) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (\CPU|DP|REGFILE|regR7|out\(8))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector7~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector7~1_combout\ & ( (\CPU|DP|REGFILE|regR7|out\(8) & \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101111101010000010111110101111101011111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR7|ALT_INV_out\(8),
	datac => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector7~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector7~2_combout\);

-- Location: FF_X83_Y9_N56
\CPU|DP|rB|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector7~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(8));

-- Location: LABCELL_X85_Y9_N9
\CPU|DP|Bin[7]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[7]~10_combout\ = ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(6) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(7) ) ) ) # ( \CPU|DP|Bin[3]~1_combout\ & ( 
-- !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(8) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000000001111111101010101010101010000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(7),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|DP|rB|ALT_INV_out\(6),
	datad => \CPU|DP|rB|ALT_INV_out\(8),
	datae => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	combout => \CPU|DP|Bin[7]~10_combout\);

-- Location: LABCELL_X85_Y8_N36
\CPU|DP|U2|main|Mux8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux8~0_combout\ = ( \CPU|DP|Ain[4]~1_combout\ & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|Bin[7]~10_combout\ & ((\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[7]~10_combout\ & (!\CPU|struct_reg|out\(12) & !\CPU|struct_reg|out\(11))) ) ) 
-- ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( \CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|rA|out\(7) & ((!\CPU|DP|Bin[7]~10_combout\ & ((\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[7]~10_combout\ & (!\CPU|struct_reg|out\(12) & !\CPU|struct_reg|out\(11))))) # 
-- (\CPU|DP|rA|out\(7) & (!\CPU|DP|Bin[7]~10_combout\ $ (!\CPU|struct_reg|out\(12) $ (!\CPU|struct_reg|out\(11))))) ) ) ) # ( \CPU|DP|Ain[4]~1_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|Bin[7]~10_combout\ & (!\CPU|struct_reg|out\(12) $ 
-- (\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[7]~10_combout\ & (!\CPU|struct_reg|out\(12) & \CPU|struct_reg|out\(11))) ) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(7) & ( (!\CPU|DP|Bin[7]~10_combout\ & (!\CPU|struct_reg|out\(11) $ 
-- (((\CPU|struct_reg|out\(12)) # (\CPU|DP|rA|out\(7)))))) # (\CPU|DP|Bin[7]~10_combout\ & ((!\CPU|DP|rA|out\(7) & (!\CPU|struct_reg|out\(12) & \CPU|struct_reg|out\(11))) # (\CPU|DP|rA|out\(7) & ((!\CPU|struct_reg|out\(11)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001000101101010101000000101101001100001100110100101000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Bin[7]~10_combout\,
	datab => \CPU|DP|rA|ALT_INV_out\(7),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(7),
	combout => \CPU|DP|U2|main|Mux8~0_combout\);

-- Location: FF_X85_Y8_N38
\CPU|DP|rC|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux8~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(7));

-- Location: LABCELL_X79_Y8_N15
\CPU|DP|U3|b[7]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[7]~9_combout\ = ( \CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|U3|b~0_combout\ & ((!\CPU|FSM|WideOr11~0_combout\) # (!\CPU|program_counter|out\(7)))) ) ) # ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|U3|b~0_combout\ & (!\CPU|DP|rC|out\(7) & 
-- ((!\CPU|FSM|WideOr11~0_combout\) # (!\CPU|program_counter|out\(7))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100100000000000110010000000000011001000110010001100100011001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	datab => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datac => \CPU|program_counter|ALT_INV_out\(7),
	datad => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[7]~9_combout\);

-- Location: LABCELL_X79_Y8_N30
\CPU|DP|U3|b[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(7) = ( \CPU|DP|U3|b[7]~9_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\Equal5~1_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a7\))) # (\Equal5~1_combout\ & (\SW[7]~input_o\)))) ) ) # ( !\CPU|DP|U3|b[7]~9_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000001010001010000000101000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_Equal5~1_combout\,
	datac => \ALT_INV_SW[7]~input_o\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\,
	dataf => \CPU|DP|U3|ALT_INV_b[7]~9_combout\,
	combout => \CPU|DP|U3|b\(7));

-- Location: FF_X79_Y7_N7
\CPU|DP|REGFILE|regR7|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(7));

-- Location: FF_X81_Y8_N22
\CPU|DP|REGFILE|regR1|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(7));

-- Location: LABCELL_X77_Y8_N24
\CPU|DP|REGFILE|regR3|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[7]~feeder_combout\ = ( \CPU|DP|U3|b\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(7),
	combout => \CPU|DP|REGFILE|regR3|out[7]~feeder_combout\);

-- Location: FF_X77_Y8_N25
\CPU|DP|REGFILE|regR3|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[7]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(7));

-- Location: MLABCELL_X78_Y8_N21
\CPU|DP|REGFILE|regR0|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR0|out[7]~feeder_combout\ = ( \CPU|DP|U3|b\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(7),
	combout => \CPU|DP|REGFILE|regR0|out[7]~feeder_combout\);

-- Location: FF_X78_Y8_N22
\CPU|DP|REGFILE|regR0|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR0|out[7]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(7));

-- Location: FF_X81_Y9_N38
\CPU|DP|REGFILE|regR2|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(7));

-- Location: LABCELL_X81_Y9_N36
\CPU|DP|REGFILE|mux_out|m|Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(7) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(7)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(7) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(7) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(7) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(7)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR3|out\(7))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(7) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(7)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- (\CPU|DP|REGFILE|regR3|out\(7))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100110011000011110011001101010101000000000101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR1|ALT_INV_out\(7),
	datab => \CPU|DP|REGFILE|regR3|ALT_INV_out\(7),
	datac => \CPU|DP|REGFILE|regR0|ALT_INV_out\(7),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(7),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\);

-- Location: FF_X78_Y7_N40
\CPU|DP|REGFILE|regR6|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(7));

-- Location: MLABCELL_X78_Y8_N42
\CPU|DP|REGFILE|regR5|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR5|out[7]~feeder_combout\ = ( \CPU|DP|U3|b\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(7),
	combout => \CPU|DP|REGFILE|regR5|out[7]~feeder_combout\);

-- Location: FF_X78_Y8_N44
\CPU|DP|REGFILE|regR5|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR5|out[7]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(7));

-- Location: FF_X78_Y8_N32
\CPU|DP|REGFILE|regR4|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(7));

-- Location: MLABCELL_X78_Y8_N30
\CPU|DP|REGFILE|mux_out|m|Selector8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(7) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(1) & (\CPU|DP|REGFILE|regR6|out\(7) & ((\CPU|struct_decoder|nsel_mux|out\(0))))) # 
-- (\CPU|struct_decoder|nsel_mux|out\(1) & (((\CPU|struct_decoder|nsel_mux|out\(0)) # (\CPU|DP|REGFILE|regR5|out\(7))))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(7) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(1) & 
-- (\CPU|DP|REGFILE|regR6|out\(7) & ((\CPU|struct_decoder|nsel_mux|out\(0))))) # (\CPU|struct_decoder|nsel_mux|out\(1) & (((\CPU|DP|REGFILE|regR5|out\(7) & !\CPU|struct_decoder|nsel_mux|out\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001101010000000000110101111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR6|ALT_INV_out\(7),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(7),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(7),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\);

-- Location: LABCELL_X81_Y9_N45
\CPU|DP|REGFILE|mux_out|m|Selector8~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector8~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(7)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(7)) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\)) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ((\CPU|DP|REGFILE|regR7|out\(7)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector8~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector8~1_combout\ & ( (\CPU|DP|REGFILE|regR7|out\(7) & \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111101010100000111111111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datac => \CPU|DP|REGFILE|regR7|ALT_INV_out\(7),
	datad => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector8~2_combout\);

-- Location: MLABCELL_X82_Y9_N0
\CPU|DP|rB|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|rB|out[7]~feeder_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector8~2_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector8~2_combout\,
	combout => \CPU|DP|rB|out[7]~feeder_combout\);

-- Location: FF_X82_Y9_N1
\CPU|DP|rB|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|rB|out[7]~feeder_combout\,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(7));

-- Location: MLABCELL_X82_Y7_N51
\CPU|DP|Bin[6]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[6]~11_combout\ = ( \CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(5) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( \CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(6) ) ) ) # ( \CPU|DP|Bin[3]~1_combout\ & ( 
-- !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|DP|rB|out\(7) ) ) ) # ( !\CPU|DP|Bin[3]~1_combout\ & ( !\CPU|DP|Bin[3]~2_combout\ & ( \CPU|struct_reg|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000011110000111100000000111111110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(5),
	datab => \CPU|struct_reg|ALT_INV_out\(4),
	datac => \CPU|DP|rB|ALT_INV_out\(7),
	datad => \CPU|DP|rB|ALT_INV_out\(6),
	datae => \CPU|DP|ALT_INV_Bin[3]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~2_combout\,
	combout => \CPU|DP|Bin[6]~11_combout\);

-- Location: LABCELL_X85_Y8_N42
\CPU|DP|U2|ALU_AddSub|ai|c[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(6) = ( \CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( (!\CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ & (!\CPU|DP|U2|ALU_AddSub|ai|g\(4) & !\CPU|DP|U2|ALU_AddSub|ai|g\(5))) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|g\(5) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~0_combout\,
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(4),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(5),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(5),
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(6));

-- Location: LABCELL_X85_Y9_N12
\CPU|DP|U2|main|Mux9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux9~0_combout\ = ( \CPU|DP|rA|out\(6) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(6) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|Ain[4]~1_combout\ $ (!\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[6]~11_combout\)))) # (\CPU|struct_reg|out\(12) & 
-- ((!\CPU|struct_reg|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|Bin[6]~11_combout\)) # (\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[6]~11_combout\))))) ) ) ) # ( !\CPU|DP|rA|out\(6) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(6) & ( (!\CPU|struct_reg|out\(11) & 
-- (!\CPU|struct_reg|out\(12) & \CPU|DP|Bin[6]~11_combout\)) # (\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[6]~11_combout\))) ) ) ) # ( \CPU|DP|rA|out\(6) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(6) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|DP|Ain[4]~1_combout\ $ 
-- (!\CPU|struct_reg|out\(11) $ (\CPU|DP|Bin[6]~11_combout\)))) # (\CPU|struct_reg|out\(12) & ((!\CPU|struct_reg|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|Bin[6]~11_combout\)) # (\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[6]~11_combout\))))) ) ) ) # ( 
-- !\CPU|DP|rA|out\(6) & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(6) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|struct_reg|out\(11) $ (\CPU|DP|Bin[6]~11_combout\))) # (\CPU|struct_reg|out\(12) & (\CPU|struct_reg|out\(11) & !\CPU|DP|Bin[6]~11_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010010100001010001011011100001000001111101000001000011101101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(12),
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[6]~11_combout\,
	datae => \CPU|DP|rA|ALT_INV_out\(6),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(6),
	combout => \CPU|DP|U2|main|Mux9~0_combout\);

-- Location: FF_X85_Y9_N14
\CPU|DP|rC|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux9~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(6));

-- Location: MLABCELL_X82_Y8_N12
\Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (((\MEM|mem_rtl_0|auto_generated|ram_block1a7\)))) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a7\)) # (\Equal5~0_combout\ & 
-- ((\SW[7]~input_o\))))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a7\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001110000111110000111000011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_Equal5~0_combout\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\,
	datad => \ALT_INV_SW[7]~input_o\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector8~0_combout\);

-- Location: FF_X82_Y8_N14
\CPU|struct_reg|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector8~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(7));

-- Location: FF_X87_Y8_N2
\CPU|program_counter|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(6),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(6));

-- Location: FF_X87_Y8_N8
\CPU|program_counter|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(4),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(4));

-- Location: MLABCELL_X87_Y8_N33
\CPU|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~29_sumout\ = SUM(( (\CPU|struct_reg|out\(1) & \CPU|FSM|WideOr37~combout\) ) + ( \CPU|program_counter|out\(1) ) + ( \CPU|Add0~26\ ))
-- \CPU|Add0~30\ = CARRY(( (\CPU|struct_reg|out\(1) & \CPU|FSM|WideOr37~combout\) ) + ( \CPU|program_counter|out\(1) ) + ( \CPU|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(1),
	datac => \CPU|FSM|ALT_INV_WideOr37~combout\,
	dataf => \CPU|program_counter|ALT_INV_out\(1),
	cin => \CPU|Add0~26\,
	sumout => \CPU|Add0~29_sumout\,
	cout => \CPU|Add0~30\);

-- Location: MLABCELL_X87_Y8_N36
\CPU|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~33_sumout\ = SUM(( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(2)) ) + ( \CPU|program_counter|out\(2) ) + ( \CPU|Add0~30\ ))
-- \CPU|Add0~34\ = CARRY(( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(2)) ) + ( \CPU|program_counter|out\(2) ) + ( \CPU|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(2),
	dataf => \CPU|program_counter|ALT_INV_out\(2),
	cin => \CPU|Add0~30\,
	sumout => \CPU|Add0~33_sumout\,
	cout => \CPU|Add0~34\);

-- Location: MLABCELL_X87_Y8_N39
\CPU|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~13_sumout\ = SUM(( \CPU|program_counter|out\(3) ) + ( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(3)) ) + ( \CPU|Add0~34\ ))
-- \CPU|Add0~14\ = CARRY(( \CPU|program_counter|out\(3) ) + ( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(3)) ) + ( \CPU|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111001111110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(3),
	datad => \CPU|program_counter|ALT_INV_out\(3),
	cin => \CPU|Add0~34\,
	sumout => \CPU|Add0~13_sumout\,
	cout => \CPU|Add0~14\);

-- Location: MLABCELL_X82_Y5_N57
\CPU|FSM|psel[2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|psel[2]~1_combout\ = ( \CPU|FSM|always0~21_combout\ & ( !\CPU|FSM|always0~4_combout\ & ( \CPU|FSM|psel[2]~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_psel[2]~0_combout\,
	datae => \CPU|FSM|ALT_INV_always0~21_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~4_combout\,
	combout => \CPU|FSM|psel[2]~1_combout\);

-- Location: LABCELL_X88_Y8_N21
\CPU|nextPCmux|out~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~3_combout\ = ( \CPU|DP|rC|out\(3) & ( \CPU|FSM|psel[2]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	dataf => \CPU|DP|rC|ALT_INV_out\(3),
	combout => \CPU|nextPCmux|out~3_combout\);

-- Location: MLABCELL_X87_Y8_N3
\CPU|nextPCmux|out[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(3) = ( \CPU|Add0~13_sumout\ & ( \CPU|nextPCmux|out~3_combout\ ) ) # ( !\CPU|Add0~13_sumout\ & ( \CPU|nextPCmux|out~3_combout\ ) ) # ( \CPU|Add0~13_sumout\ & ( !\CPU|nextPCmux|out~3_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr41~2_combout\) # (!\CPU|FSM|WideOr9~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datae => \CPU|ALT_INV_Add0~13_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~3_combout\,
	combout => \CPU|nextPCmux|out\(3));

-- Location: FF_X87_Y8_N5
\CPU|program_counter|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(3),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(3));

-- Location: MLABCELL_X87_Y8_N42
\CPU|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~17_sumout\ = SUM(( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(4)) ) + ( \CPU|program_counter|out\(4) ) + ( \CPU|Add0~14\ ))
-- \CPU|Add0~18\ = CARRY(( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(4)) ) + ( \CPU|program_counter|out\(4) ) + ( \CPU|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(4),
	dataf => \CPU|program_counter|ALT_INV_out\(4),
	cin => \CPU|Add0~14\,
	sumout => \CPU|Add0~17_sumout\,
	cout => \CPU|Add0~18\);

-- Location: MLABCELL_X87_Y8_N45
\CPU|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~5_sumout\ = SUM(( \CPU|program_counter|out[5]~DUPLICATE_q\ ) + ( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(5)) ) + ( \CPU|Add0~18\ ))
-- \CPU|Add0~6\ = CARRY(( \CPU|program_counter|out[5]~DUPLICATE_q\ ) + ( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(5)) ) + ( \CPU|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datad => \CPU|program_counter|ALT_INV_out[5]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out\(5),
	cin => \CPU|Add0~18\,
	sumout => \CPU|Add0~5_sumout\,
	cout => \CPU|Add0~6\);

-- Location: MLABCELL_X87_Y8_N48
\CPU|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~1_sumout\ = SUM(( \CPU|program_counter|out\(6) ) + ( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(6)) ) + ( \CPU|Add0~6\ ))
-- \CPU|Add0~2\ = CARRY(( \CPU|program_counter|out\(6) ) + ( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(6)) ) + ( \CPU|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111001111110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(6),
	datad => \CPU|program_counter|ALT_INV_out\(6),
	cin => \CPU|Add0~6\,
	sumout => \CPU|Add0~1_sumout\,
	cout => \CPU|Add0~2\);

-- Location: MLABCELL_X87_Y8_N51
\CPU|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~9_sumout\ = SUM(( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(7)) ) + ( \CPU|program_counter|out\(7) ) + ( \CPU|Add0~2\ ))
-- \CPU|Add0~10\ = CARRY(( (\CPU|FSM|WideOr37~combout\ & \CPU|struct_reg|out\(7)) ) + ( \CPU|program_counter|out\(7) ) + ( \CPU|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(7),
	dataf => \CPU|program_counter|ALT_INV_out\(7),
	cin => \CPU|Add0~2\,
	sumout => \CPU|Add0~9_sumout\,
	cout => \CPU|Add0~10\);

-- Location: LABCELL_X88_Y8_N27
\CPU|nextPCmux|out~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~2_combout\ = (\CPU|FSM|psel[2]~1_combout\ & \CPU|DP|rC|out\(7))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	datac => \CPU|DP|rC|ALT_INV_out\(7),
	combout => \CPU|nextPCmux|out~2_combout\);

-- Location: MLABCELL_X87_Y8_N12
\CPU|nextPCmux|out[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(7) = ( \CPU|Add0~9_sumout\ & ( \CPU|nextPCmux|out~2_combout\ ) ) # ( !\CPU|Add0~9_sumout\ & ( \CPU|nextPCmux|out~2_combout\ ) ) # ( \CPU|Add0~9_sumout\ & ( !\CPU|nextPCmux|out~2_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr9~3_combout\) # (!\CPU|FSM|WideOr41~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datae => \CPU|ALT_INV_Add0~9_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~2_combout\,
	combout => \CPU|nextPCmux|out\(7));

-- Location: FF_X87_Y8_N14
\CPU|program_counter|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(7),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(7));

-- Location: FF_X75_Y7_N41
\CPU|Data_Address|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(7),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(7));

-- Location: LABCELL_X75_Y7_N39
\CPU|mem_addr[7]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[7]~7_combout\ = ( \CPU|Data_Address|out\(7) & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(7) ) ) ) # ( !\CPU|Data_Address|out\(7) & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(7) ) ) ) # ( 
-- \CPU|Data_Address|out\(7) & ( !\CPU|FSM|always0~12_combout\ & ( ((!\CPU|FSM|WideNor2~combout\ & \CPU|FSM|WideOr41~1_combout\)) # (\CPU|program_counter|out\(7)) ) ) ) # ( !\CPU|Data_Address|out\(7) & ( !\CPU|FSM|always0~12_combout\ & ( 
-- (\CPU|program_counter|out\(7) & ((!\CPU|FSM|WideOr41~1_combout\) # (\CPU|FSM|WideNor2~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000101010101011111010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out\(7),
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datae => \CPU|Data_Address|ALT_INV_out\(7),
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[7]~7_combout\);

-- Location: MLABCELL_X82_Y8_N54
\Selector9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (((\MEM|mem_rtl_0|auto_generated|ram_block1a6\)))) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a6\))) # (\Equal5~0_combout\ & 
-- (\SW[6]~input_o\)))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a6\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001101010011001100110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[6]~input_o\,
	datab => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\,
	datac => \ALT_INV_LED_load~0_combout\,
	datad => \ALT_INV_Equal5~0_combout\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector9~0_combout\);

-- Location: FF_X82_Y8_N56
\CPU|struct_reg|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector9~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(6));

-- Location: LABCELL_X88_Y8_N12
\CPU|nextPCmux|out~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~0_combout\ = ( \CPU|FSM|psel[2]~1_combout\ & ( \CPU|DP|rC|out\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|rC|ALT_INV_out\(6),
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|nextPCmux|out~0_combout\);

-- Location: MLABCELL_X87_Y8_N0
\CPU|nextPCmux|out[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(6) = ( \CPU|Add0~1_sumout\ & ( \CPU|nextPCmux|out~0_combout\ ) ) # ( !\CPU|Add0~1_sumout\ & ( \CPU|nextPCmux|out~0_combout\ ) ) # ( \CPU|Add0~1_sumout\ & ( !\CPU|nextPCmux|out~0_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr9~3_combout\) # (!\CPU|FSM|WideOr41~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datae => \CPU|ALT_INV_Add0~1_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~0_combout\,
	combout => \CPU|nextPCmux|out\(6));

-- Location: FF_X87_Y8_N1
\CPU|program_counter|out[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(6),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out[6]~DUPLICATE_q\);

-- Location: FF_X77_Y8_N40
\CPU|Data_Address|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(6),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(6));

-- Location: LABCELL_X77_Y8_N39
\CPU|mem_addr[6]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[6]~0_combout\ = ( \CPU|Data_Address|out\(6) & ( ((!\CPU|FSM|always0~12_combout\ & (!\CPU|FSM|WideNor2~combout\ & \CPU|FSM|WideOr41~1_combout\))) # (\CPU|program_counter|out[6]~DUPLICATE_q\) ) ) # ( !\CPU|Data_Address|out\(6) & ( 
-- (\CPU|program_counter|out[6]~DUPLICATE_q\ & (((!\CPU|FSM|WideOr41~1_combout\) # (\CPU|FSM|WideNor2~combout\)) # (\CPU|FSM|always0~12_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100010101010101011101010101010101000101010101010111010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out[6]~DUPLICATE_q\,
	datab => \CPU|FSM|ALT_INV_always0~12_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datae => \CPU|Data_Address|ALT_INV_out\(6),
	combout => \CPU|mem_addr[6]~0_combout\);

-- Location: LABCELL_X81_Y7_N33
\Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = ( \Equal5~0_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a8\ & ( (!\LED_load~0_combout\) # ((!\LED_load~2_combout\) # ((!\LED_load~3_combout\) # (!\LED_load~1_combout\))) ) ) ) # ( !\Equal5~0_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a8\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_LED_load~2_combout\,
	datac => \ALT_INV_LED_load~3_combout\,
	datad => \ALT_INV_LED_load~1_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\,
	combout => \Selector7~0_combout\);

-- Location: FF_X81_Y7_N35
\CPU|struct_reg|out[8]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector7~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[8]~DUPLICATE_q\);

-- Location: FF_X81_Y7_N20
\CPU|struct_reg|out[9]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector6~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[9]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y7_N27
\CPU|FSM|WideNor33~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor33~1_combout\ = ( !\CPU|struct_reg|out[9]~DUPLICATE_q\ & ( !\CPU|struct_reg|out[8]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[9]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor33~1_combout\);

-- Location: MLABCELL_X84_Y6_N51
\CPU|DP|U2|ALU_AddSub|ovf~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ovf~0_combout\ = ( \CPU|DP|Bin[15]~18_combout\ & ( (!\CPU|struct_reg|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(15))) ) ) # ( !\CPU|DP|Bin[15]~18_combout\ & ( (\CPU|struct_reg|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & 
-- \CPU|DP|rA|out\(15))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000101000000000000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datad => \CPU|DP|rA|ALT_INV_out\(15),
	dataf => \CPU|DP|ALT_INV_Bin[15]~18_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ovf~0_combout\);

-- Location: LABCELL_X83_Y8_N45
\CPU|DP|U2|ALU_AddSub|ovf~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ovf~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( \CPU|DP|Ain[14]~2_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ovf~0_combout\ & !\CPU|DP|U2|ALU_AddSub|as|p\(0)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( \CPU|DP|Ain[14]~2_combout\ 
-- & ( (!\CPU|DP|U2|ALU_AddSub|ovf~0_combout\ & (\CPU|DP|U2|ALU_AddSub|comb~0_combout\ & !\CPU|DP|U2|ALU_AddSub|as|p\(0))) # (\CPU|DP|U2|ALU_AddSub|ovf~0_combout\ & (!\CPU|DP|U2|ALU_AddSub|comb~0_combout\)) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( 
-- !\CPU|DP|Ain[14]~2_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ovf~0_combout\ & (\CPU|DP|U2|ALU_AddSub|comb~0_combout\ & !\CPU|DP|U2|ALU_AddSub|as|p\(0))) # (\CPU|DP|U2|ALU_AddSub|ovf~0_combout\ & (!\CPU|DP|U2|ALU_AddSub|comb~0_combout\)) ) ) ) # ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(14) & ( !\CPU|DP|Ain[14]~2_combout\ & ( \CPU|DP|U2|ALU_AddSub|ovf~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001111000011000000111100001100001100110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U2|ALU_AddSub|ALT_INV_ovf~0_combout\,
	datac => \CPU|DP|U2|ALU_AddSub|ALT_INV_comb~0_combout\,
	datad => \CPU|DP|U2|ALU_AddSub|as|ALT_INV_p\(0),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(14),
	dataf => \CPU|DP|ALT_INV_Ain[14]~2_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ovf~1_combout\);

-- Location: FF_X83_Y8_N47
\CPU|DP|status|out[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|ALU_AddSub|ovf~1_combout\,
	sclr => \CPU|struct_reg|out\(12),
	ena => \CPU|FSM|loads~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|out[2]~DUPLICATE_q\);

-- Location: FF_X83_Y8_N2
\CPU|DP|status|out[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U2|main|Mux0~1_combout\,
	sload => VCC,
	ena => \CPU|FSM|loads~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|out[1]~DUPLICATE_q\);

-- Location: LABCELL_X83_Y8_N27
\CPU|FSM|WideNor40\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor40~combout\ = ( \CPU|FSM|WideNor33~0_combout\ & ( (\CPU|FSM|WideNor33~1_combout\ & (\CPU|struct_reg|out[10]~DUPLICATE_q\ & (!\CPU|DP|status|out[2]~DUPLICATE_q\ & \CPU|DP|status|out[1]~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor33~1_combout\,
	datab => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datac => \CPU|DP|status|ALT_INV_out[2]~DUPLICATE_q\,
	datad => \CPU|DP|status|ALT_INV_out[1]~DUPLICATE_q\,
	dataf => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	combout => \CPU|FSM|WideNor40~combout\);

-- Location: LABCELL_X80_Y8_N33
\CPU|FSM|WideNor33\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor33~combout\ = ( \CPU|FSM|WideNor33~0_combout\ & ( (\CPU|FSM|WideNor33~1_combout\ & !\CPU|struct_reg|out[10]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001000100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor33~1_combout\,
	datab => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	dataf => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	combout => \CPU|FSM|WideNor33~combout\);

-- Location: LABCELL_X80_Y8_N30
\CPU|FSM|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr1~0_combout\ = ( \CPU|FSM|WideNor33~0_combout\ & ( (!\CPU|struct_reg|out[10]~DUPLICATE_q\ & (!\CPU|struct_reg|out[8]~DUPLICATE_q\ & \CPU|struct_reg|out\(9))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datac => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out\(9),
	dataf => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	combout => \CPU|FSM|WideOr1~0_combout\);

-- Location: LABCELL_X80_Y8_N3
\CPU|FSM|WideNor34\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor34~combout\ = ( \CPU|struct_reg|out[8]~DUPLICATE_q\ & ( (\CPU|FSM|WideNor33~0_combout\ & (!\CPU|struct_reg|out[10]~DUPLICATE_q\ & (\CPU|DP|status|out\(0) & !\CPU|struct_reg|out\(9)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000100000000000000010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	datab => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datac => \CPU|DP|status|ALT_INV_out\(0),
	datad => \CPU|struct_reg|ALT_INV_out\(9),
	dataf => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor34~combout\);

-- Location: FF_X83_Y8_N1
\CPU|DP|status|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U2|main|Mux0~1_combout\,
	sload => VCC,
	ena => \CPU|FSM|loads~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|out\(1));

-- Location: LABCELL_X83_Y8_N39
\CPU|FSM|WideNor37\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor37~combout\ = ( !\CPU|DP|status|out[2]~DUPLICATE_q\ & ( \CPU|DP|status|out\(1) & ( (\CPU|struct_reg|out\(9) & (\CPU|struct_reg|out[8]~DUPLICATE_q\ & (!\CPU|struct_reg|out[10]~DUPLICATE_q\ & \CPU|FSM|WideNor33~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(9),
	datab => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	datac => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datad => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	datae => \CPU|DP|status|ALT_INV_out[2]~DUPLICATE_q\,
	dataf => \CPU|DP|status|ALT_INV_out\(1),
	combout => \CPU|FSM|WideNor37~combout\);

-- Location: MLABCELL_X82_Y8_N27
\CPU|FSM|always0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~6_combout\ = ( \CPU|FSM|WideNor28~1_combout\ & ( (!\CPU|FSM|STATE|out\(0) & (!\CPU|FSM|STATE|out\(1) & ((\CPU|FSM|WideNor31~0_combout\)))) # (\CPU|FSM|STATE|out\(0) & (((!\CPU|FSM|STATE|out\(1) & \CPU|FSM|WideNor31~0_combout\)) # 
-- (\CPU|FSM|WideNor28~0_combout\))) ) ) # ( !\CPU|FSM|WideNor28~1_combout\ & ( (!\CPU|FSM|STATE|out\(1) & \CPU|FSM|WideNor31~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000001100110000000101110011010000010111001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(0),
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|ALT_INV_WideNor28~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor31~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor28~1_combout\,
	combout => \CPU|FSM|always0~6_combout\);

-- Location: LABCELL_X81_Y8_N18
\CPU|FSM|always0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~7_combout\ = ( !\CPU|FSM|WideNor28~combout\ & ( \CPU|FSM|always0~5_combout\ & ( (!\CPU|FSM|WideNor27~combout\ & (!\CPU|FSM|WideNor26~1_combout\ & (!\CPU|FSM|always0~6_combout\ & !\CPU|FSM|WideNor29~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor27~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor26~1_combout\,
	datac => \CPU|FSM|ALT_INV_always0~6_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor29~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor28~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|always0~7_combout\);

-- Location: LABCELL_X80_Y8_N6
\CPU|FSM|always0~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~22_combout\ = ( !\CPU|FSM|WideNor38~combout\ & ( \CPU|FSM|always0~7_combout\ & ( (!\CPU|FSM|WideNor33~combout\ & (!\CPU|FSM|WideOr1~0_combout\ & (!\CPU|FSM|WideNor34~combout\ & !\CPU|FSM|WideNor37~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor33~combout\,
	datab => \CPU|FSM|ALT_INV_WideOr1~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor34~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor37~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor38~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~7_combout\,
	combout => \CPU|FSM|always0~22_combout\);

-- Location: LABCELL_X80_Y8_N12
\CPU|FSM|always0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~25_combout\ = ( !\CPU|FSM|WideNor41~combout\ & ( \CPU|FSM|always0~22_combout\ & ( (!\CPU|FSM|WideNor39~combout\ & (!\CPU|FSM|WideNor42~combout\ & !\CPU|FSM|WideNor40~combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000100000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor39~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor40~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor41~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~22_combout\,
	combout => \CPU|FSM|always0~25_combout\);

-- Location: LABCELL_X81_Y6_N36
\CPU|struct_decoder|nsel_mux|out[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out\(2) = ( \CPU|struct_reg|out\(7) & ( !\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ & ( (\CPU|FSM|WideOr29~0_combout\ & (!\CPU|FSM|WideOr29~1_combout\ & ((!\CPU|FSM|WideNor43~combout\) # (!\CPU|FSM|always0~25_combout\)))) 
-- ) ) ) # ( !\CPU|struct_reg|out\(7) & ( !\CPU|struct_decoder|nsel_mux|out[2]~5_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111001100100000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datab => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	datac => \CPU|FSM|ALT_INV_always0~25_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datae => \CPU|struct_reg|ALT_INV_out\(7),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\,
	combout => \CPU|struct_decoder|nsel_mux|out\(2));

-- Location: LABCELL_X81_Y6_N24
\CPU|DP|REGFILE|writenum_OH[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[1]~2_combout\ = ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|FSM|WideOr11~combout\ & (!\CPU|struct_decoder|nsel_mux|out\(0) & \CPU|struct_decoder|nsel_mux|out\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\);

-- Location: FF_X77_Y8_N7
\CPU|DP|REGFILE|regR1|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[0]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(0));

-- Location: LABCELL_X77_Y6_N27
\CPU|DP|REGFILE|regR3|out[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR3|out[0]~feeder_combout\ = ( \CPU|DP|U3|b\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regR3|out[0]~feeder_combout\);

-- Location: FF_X77_Y6_N28
\CPU|DP|REGFILE|regR3|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR3|out[0]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(0));

-- Location: FF_X79_Y6_N19
\CPU|DP|REGFILE|regR0|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(0));

-- Location: FF_X79_Y6_N32
\CPU|DP|REGFILE|regR2|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(0));

-- Location: LABCELL_X79_Y6_N30
\CPU|DP|REGFILE|mux_out|m|Selector15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector15~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(0) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(0)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(0) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(0) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(0) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(0)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR3|out\(0))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(0) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(0)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- (\CPU|DP|REGFILE|regR3|out\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100110011000011110011001101010101000000000101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR1|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR3|ALT_INV_out\(0),
	datac => \CPU|DP|REGFILE|regR0|ALT_INV_out\(0),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(0),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector15~0_combout\);

-- Location: FF_X80_Y6_N19
\CPU|DP|REGFILE|regR6|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(0));

-- Location: FF_X78_Y8_N2
\CPU|DP|REGFILE|regR5|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(0),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(0));

-- Location: FF_X78_Y8_N14
\CPU|DP|REGFILE|regR4|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(0));

-- Location: MLABCELL_X78_Y8_N12
\CPU|DP|REGFILE|mux_out|m|Selector15~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector15~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(0) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|struct_decoder|nsel_mux|out\(0)) # (\CPU|DP|REGFILE|regR5|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(0) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR5|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & !\CPU|struct_decoder|nsel_mux|out\(0))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(0) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR6|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & \CPU|struct_decoder|nsel_mux|out\(0))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(0) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|DP|REGFILE|regR6|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & \CPU|struct_decoder|nsel_mux|out\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000110000000000000011000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR6|ALT_INV_out\(0),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(0),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(0),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector15~1_combout\);

-- Location: LABCELL_X79_Y6_N21
\CPU|DP|REGFILE|mux_out|m|Selector15~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector15~2_combout\ = ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(0) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( ((!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\ & 
-- \CPU|DP|REGFILE|mux_out|m|Selector15~0_combout\)) # (\CPU|DP|REGFILE|mux_out|m|Selector15~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101011111111000010101111111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datab => \CPU|DP|REGFILE|regR7|ALT_INV_out\(0),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector15~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector15~1_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector15~2_combout\);

-- Location: FF_X85_Y6_N2
\CPU|DP|rB|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector15~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(0));

-- Location: FF_X85_Y6_N25
\CPU|DP|rB|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector13~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(2));

-- Location: LABCELL_X85_Y6_N3
\CPU|DP|Bin[1]~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[1]~25_combout\ = ( \CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|struct_reg|out\(1) ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[1]~DUPLICATE_q\ ) ) ) # ( 
-- \CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out\(2) ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out\(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101001100110011001100001111000011110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(0),
	datab => \CPU|DP|rB|ALT_INV_out\(2),
	datac => \CPU|DP|rB|ALT_INV_out[1]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out\(1),
	datae => \CPU|struct_decoder|ALT_INV_shift[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~0_combout\,
	combout => \CPU|DP|Bin[1]~25_combout\);

-- Location: MLABCELL_X84_Y6_N12
\CPU|DP|Bin[1]~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[1]~26_combout\ = ( \CPU|FSM|WideNor15~0_combout\ & ( \CPU|struct_reg|out\(1) ) ) # ( !\CPU|FSM|WideNor15~0_combout\ & ( \CPU|DP|Bin[1]~25_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|ALT_INV_Bin[1]~25_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(1),
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|DP|Bin[1]~26_combout\);

-- Location: MLABCELL_X84_Y6_N54
\CPU|DP|Bin[1]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[1]~16_combout\ = ( \CPU|DP|Bin[1]~25_combout\ & ( \CPU|DP|Bin[1]~26_combout\ ) ) # ( !\CPU|DP|Bin[1]~25_combout\ & ( \CPU|DP|Bin[1]~26_combout\ & ( (!\CPU|FSM|WideNor5~1_combout\ & (!\CPU|FSM|WideNor13~combout\ & (!\CPU|FSM|WideNor12~combout\ 
-- & \CPU|FSM|always0~2_combout\))) ) ) ) # ( \CPU|DP|Bin[1]~25_combout\ & ( !\CPU|DP|Bin[1]~26_combout\ & ( (((!\CPU|FSM|always0~2_combout\) # (\CPU|FSM|WideNor12~combout\)) # (\CPU|FSM|WideNor13~combout\)) # (\CPU|FSM|WideNor5~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110111111100000000100000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datad => \CPU|FSM|ALT_INV_always0~2_combout\,
	datae => \CPU|DP|ALT_INV_Bin[1]~25_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~26_combout\,
	combout => \CPU|DP|Bin[1]~16_combout\);

-- Location: FF_X79_Y6_N38
\CPU|DP|rA|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector15~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(0));

-- Location: FF_X79_Y6_N17
\CPU|DP|rA|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector14~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(1));

-- Location: MLABCELL_X84_Y6_N18
\CPU|DP|Ain[1]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[1]~5_combout\ = ( \CPU|FSM|WideNor16~0_combout\ & ( (\CPU|FSM|WideNor17~combout\ & !\CPU|FSM|WideNor15~0_combout\) ) ) # ( !\CPU|FSM|WideNor16~0_combout\ & ( (!\CPU|FSM|WideNor15~0_combout\ & ((\CPU|FSM|WideNor17~combout\) # 
-- (\CPU|DP|Ain[4]~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100000000001111110000000000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[4]~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	combout => \CPU|DP|Ain[1]~5_combout\);

-- Location: MLABCELL_X84_Y6_N0
\CPU|DP|Ain[1]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[1]~3_combout\ = ( \CPU|FSM|WideNor13~combout\ & ( \CPU|DP|Ain[1]~5_combout\ & ( \CPU|DP|rA|out\(1) ) ) ) # ( !\CPU|FSM|WideNor13~combout\ & ( \CPU|DP|Ain[1]~5_combout\ & ( (\CPU|DP|rA|out\(1) & ((!\CPU|FSM|always0~2_combout\) # 
-- ((\CPU|FSM|WideNor16~combout\) # (\CPU|FSM|WideNor12~combout\)))) ) ) ) # ( \CPU|FSM|WideNor13~combout\ & ( !\CPU|DP|Ain[1]~5_combout\ & ( \CPU|DP|rA|out\(1) ) ) ) # ( !\CPU|FSM|WideNor13~combout\ & ( !\CPU|DP|Ain[1]~5_combout\ & ( \CPU|DP|rA|out\(1) ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100100011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_always0~2_combout\,
	datab => \CPU|DP|rA|ALT_INV_out\(1),
	datac => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor16~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor13~combout\,
	dataf => \CPU|DP|ALT_INV_Ain[1]~5_combout\,
	combout => \CPU|DP|Ain[1]~3_combout\);

-- Location: FF_X85_Y6_N7
\CPU|DP|rB|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector14~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(1));

-- Location: LABCELL_X85_Y6_N36
\CPU|DP|Bin[0]~27\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[0]~27_combout\ = ( \CPU|struct_decoder|Equal0~0_combout\ & ( \CPU|DP|rB|out\(0) ) ) # ( !\CPU|struct_decoder|Equal0~0_combout\ & ( (!\CPU|struct_reg|out\(4) & (!\CPU|struct_reg|out\(3) & (\CPU|DP|rB|out\(0)))) # (\CPU|struct_reg|out\(4) & 
-- (((\CPU|DP|rB|out\(1))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100001011101000010000101110100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(4),
	datab => \CPU|struct_reg|ALT_INV_out\(3),
	datac => \CPU|DP|rB|ALT_INV_out\(0),
	datad => \CPU|DP|rB|ALT_INV_out\(1),
	dataf => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	combout => \CPU|DP|Bin[0]~27_combout\);

-- Location: LABCELL_X85_Y6_N45
\CPU|DP|Bin[0]~28\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[0]~28_combout\ = ( \CPU|FSM|WideNor15~0_combout\ & ( \CPU|struct_reg|out\(0) ) ) # ( !\CPU|FSM|WideNor15~0_combout\ & ( \CPU|DP|Bin[0]~27_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out\(0),
	datad => \CPU|DP|ALT_INV_Bin[0]~27_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|DP|Bin[0]~28_combout\);

-- Location: MLABCELL_X84_Y6_N57
\CPU|DP|Bin[0]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[0]~17_combout\ = ( \CPU|DP|Bin[0]~27_combout\ & ( \CPU|DP|Bin[0]~28_combout\ ) ) # ( !\CPU|DP|Bin[0]~27_combout\ & ( \CPU|DP|Bin[0]~28_combout\ & ( (!\CPU|FSM|WideNor5~1_combout\ & (!\CPU|FSM|WideNor13~combout\ & (\CPU|FSM|always0~2_combout\ & 
-- !\CPU|FSM|WideNor12~combout\))) ) ) ) # ( \CPU|DP|Bin[0]~27_combout\ & ( !\CPU|DP|Bin[0]~28_combout\ & ( (((!\CPU|FSM|always0~2_combout\) # (\CPU|FSM|WideNor12~combout\)) # (\CPU|FSM|WideNor13~combout\)) # (\CPU|FSM|WideNor5~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101111111111100001000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datac => \CPU|FSM|ALT_INV_always0~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datae => \CPU|DP|ALT_INV_Bin[0]~27_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[0]~28_combout\,
	combout => \CPU|DP|Bin[0]~17_combout\);

-- Location: MLABCELL_X84_Y6_N36
\CPU|DP|U2|ALU_AddSub|ai|c[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(2) = ( \CPU|DP|Bin[0]~17_combout\ & ( \CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ & ( (\CPU|DP|Ain[1]~3_combout\ & (!\CPU|DP|Bin[1]~16_combout\ $ (!\CPU|struct_reg|out\(11)))) ) ) ) # ( !\CPU|DP|Bin[0]~17_combout\ & ( 
-- \CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ & ( (!\CPU|DP|Bin[1]~16_combout\ & ((\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[1]~16_combout\ & (\CPU|DP|Ain[1]~3_combout\)) ) ) ) # ( \CPU|DP|Bin[0]~17_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ & ( 
-- (!\CPU|DP|rA|out\(0) & (\CPU|DP|Ain[1]~3_combout\ & (!\CPU|DP|Bin[1]~16_combout\ $ (!\CPU|struct_reg|out\(11))))) # (\CPU|DP|rA|out\(0) & ((!\CPU|DP|Bin[1]~16_combout\ $ (!\CPU|struct_reg|out\(11))) # (\CPU|DP|Ain[1]~3_combout\))) ) ) ) # ( 
-- !\CPU|DP|Bin[0]~17_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ & ( (!\CPU|DP|Bin[1]~16_combout\ & ((\CPU|struct_reg|out\(11)))) # (\CPU|DP|Bin[1]~16_combout\ & (\CPU|DP|Ain[1]~3_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111000101110010101100000101101011110000010100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Bin[1]~16_combout\,
	datab => \CPU|DP|rA|ALT_INV_out\(0),
	datac => \CPU|DP|ALT_INV_Ain[1]~3_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Bin[0]~17_combout\,
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(2));

-- Location: MLABCELL_X78_Y8_N48
\CPU|DP|U3|b[3]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[3]~13_combout\ = ( \CPU|FSM|WideOr11~0_combout\ & ( \CPU|FSM|WideOr26~combout\ & ( (!\CPU|program_counter|out\(3) & ((!\CPU|struct_reg|out\(3)) # (!\CPU|FSM|WideNor6~combout\))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( 
-- \CPU|FSM|WideOr26~combout\ & ( (!\CPU|struct_reg|out\(3)) # (!\CPU|FSM|WideNor6~combout\) ) ) ) # ( \CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(3) & (!\CPU|program_counter|out\(3) & ((!\CPU|struct_reg|out\(3)) # 
-- (!\CPU|FSM|WideNor6~combout\)))) ) ) ) # ( !\CPU|FSM|WideOr11~0_combout\ & ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|rC|out\(3) & ((!\CPU|struct_reg|out\(3)) # (!\CPU|FSM|WideNor6~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010100000100010001000000011111111111100001100110011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(3),
	datab => \CPU|program_counter|ALT_INV_out\(3),
	datac => \CPU|struct_reg|ALT_INV_out\(3),
	datad => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datae => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[3]~13_combout\);

-- Location: LABCELL_X79_Y8_N33
\CPU|DP|U3|b[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(3) = ( \SW[3]~input_o\ & ( (!\CPU|DP|U3|b[3]~13_combout\) # ((\CPU|FSM|vsel\(3) & ((\MEM|mem_rtl_0|auto_generated|ram_block1a3\) # (\Equal5~1_combout\)))) ) ) # ( !\SW[3]~input_o\ & ( (!\CPU|DP|U3|b[3]~13_combout\) # ((\CPU|FSM|vsel\(3) & 
-- (!\Equal5~1_combout\ & \MEM|mem_rtl_0|auto_generated|ram_block1a3\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000100111111110000010011111111000101011111111100010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_Equal5~1_combout\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\,
	datad => \CPU|DP|U3|ALT_INV_b[3]~13_combout\,
	dataf => \ALT_INV_SW[3]~input_o\,
	combout => \CPU|DP|U3|b\(3));

-- Location: LABCELL_X79_Y7_N48
\CPU|DP|REGFILE|regR7|out[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR7|out[3]~feeder_combout\ = ( \CPU|DP|U3|b\(3) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(3),
	combout => \CPU|DP|REGFILE|regR7|out[3]~feeder_combout\);

-- Location: FF_X79_Y7_N49
\CPU|DP|REGFILE|regR7|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR7|out[3]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(3));

-- Location: FF_X79_Y8_N35
\CPU|DP|REGFILE|regR5|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(3),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(3));

-- Location: FF_X80_Y8_N10
\CPU|DP|REGFILE|regR6|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(3));

-- Location: FF_X80_Y6_N8
\CPU|DP|REGFILE|regR4|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(3));

-- Location: LABCELL_X80_Y6_N6
\CPU|DP|REGFILE|mux_out|m|Selector12~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector12~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(3) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|struct_decoder|nsel_mux|out\(0)) # (\CPU|DP|REGFILE|regR5|out\(3)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(3) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR5|out\(3) & (!\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(3) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(3) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(3) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR6|out\(3) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000000001000100000000000111011100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR5|ALT_INV_out\(3),
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datac => \CPU|DP|REGFILE|regR6|ALT_INV_out\(3),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(3),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector12~1_combout\);

-- Location: FF_X80_Y6_N4
\CPU|DP|REGFILE|regR3|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(3));

-- Location: FF_X79_Y6_N23
\CPU|DP|REGFILE|regR0|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(3));

-- Location: MLABCELL_X78_Y6_N30
\CPU|DP|REGFILE|regR1|out[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[3]~feeder_combout\ = ( \CPU|DP|U3|b\(3) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(3),
	combout => \CPU|DP|REGFILE|regR1|out[3]~feeder_combout\);

-- Location: FF_X78_Y6_N32
\CPU|DP|REGFILE|regR1|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[3]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(3));

-- Location: FF_X79_Y6_N44
\CPU|DP|REGFILE|regR2|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(3));

-- Location: LABCELL_X79_Y6_N42
\CPU|DP|REGFILE|mux_out|m|Selector12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(3) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(3)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(3) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(3) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(3) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(3)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR3|out\(3))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(3) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(3)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- (\CPU|DP|REGFILE|regR3|out\(3))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001101010101001100110101010100001111000000000000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR3|ALT_INV_out\(3),
	datab => \CPU|DP|REGFILE|regR0|ALT_INV_out\(3),
	datac => \CPU|DP|REGFILE|regR1|ALT_INV_out\(3),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(3),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\);

-- Location: LABCELL_X79_Y6_N27
\CPU|DP|REGFILE|mux_out|m|Selector12~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector12~2_combout\ = ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(3) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # (\CPU|DP|REGFILE|mux_out|m|Selector12~1_combout\) ) ) ) # ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(3) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector12~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector12~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100110011001111110000111111110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|regR7|ALT_INV_out\(3),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector12~1_combout\,
	datae => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector12~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector12~2_combout\);

-- Location: FF_X79_Y6_N29
\CPU|DP|rA|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector12~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(3));

-- Location: FF_X85_Y6_N20
\CPU|DP|rB|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector12~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out\(3));

-- Location: FF_X82_Y7_N43
\CPU|DP|rB|out[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector11~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out[4]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y6_N27
\CPU|DP|Bin[3]~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~21_combout\ = ( \CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|struct_reg|out\(3) ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out\(3) ) ) ) # ( 
-- \CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[4]~DUPLICATE_q\ ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[2]~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000011110000111101010101010101010011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out\(3),
	datab => \CPU|struct_reg|ALT_INV_out\(3),
	datac => \CPU|DP|rB|ALT_INV_out[4]~DUPLICATE_q\,
	datad => \CPU|DP|rB|ALT_INV_out[2]~DUPLICATE_q\,
	datae => \CPU|struct_decoder|ALT_INV_shift[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~0_combout\,
	combout => \CPU|DP|Bin[3]~21_combout\);

-- Location: LABCELL_X85_Y6_N57
\CPU|DP|Bin[3]~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~22_combout\ = ( \CPU|FSM|WideNor15~0_combout\ & ( \CPU|struct_reg|out\(3) ) ) # ( !\CPU|FSM|WideNor15~0_combout\ & ( \CPU|DP|Bin[3]~21_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out\(3),
	datad => \CPU|DP|ALT_INV_Bin[3]~21_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|DP|Bin[3]~22_combout\);

-- Location: LABCELL_X85_Y6_N15
\CPU|DP|Bin[3]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~14_combout\ = ( \CPU|DP|Bin[3]~22_combout\ & ( \CPU|FSM|WideNor12~combout\ & ( \CPU|DP|Bin[3]~21_combout\ ) ) ) # ( !\CPU|DP|Bin[3]~22_combout\ & ( \CPU|FSM|WideNor12~combout\ & ( \CPU|DP|Bin[3]~21_combout\ ) ) ) # ( 
-- \CPU|DP|Bin[3]~22_combout\ & ( !\CPU|FSM|WideNor12~combout\ & ( ((!\CPU|FSM|WideNor5~1_combout\ & (\CPU|FSM|always0~2_combout\ & !\CPU|FSM|WideNor13~combout\))) # (\CPU|DP|Bin[3]~21_combout\) ) ) ) # ( !\CPU|DP|Bin[3]~22_combout\ & ( 
-- !\CPU|FSM|WideNor12~combout\ & ( (\CPU|DP|Bin[3]~21_combout\ & (((!\CPU|FSM|always0~2_combout\) # (\CPU|FSM|WideNor13~combout\)) # (\CPU|FSM|WideNor5~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000101010101010111010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Bin[3]~21_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datac => \CPU|FSM|ALT_INV_always0~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datae => \CPU|DP|ALT_INV_Bin[3]~22_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor12~combout\,
	combout => \CPU|DP|Bin[3]~14_combout\);

-- Location: MLABCELL_X84_Y6_N48
\CPU|DP|U2|ALU_AddSub|ai|g[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(3) = ( !\CPU|DP|Ain[4]~1_combout\ & ( (\CPU|DP|rA|out\(3) & (!\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[3]~14_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100001010000001010000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(3),
	datad => \CPU|DP|ALT_INV_Bin[3]~14_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(3));

-- Location: LABCELL_X85_Y8_N54
\CPU|DP|U2|ALU_AddSub|ai|p[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(4) = ( \CPU|DP|Bin[4]~13_combout\ & ( !\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(4) & !\CPU|DP|Ain[4]~1_combout\))) ) ) # ( !\CPU|DP|Bin[4]~13_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(4)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001010101010110100101010110100101101010101010010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(4),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[4]~13_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(4));

-- Location: MLABCELL_X84_Y6_N21
\CPU|DP|U2|ALU_AddSub|ai|p[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(3) = ( \CPU|DP|Ain[4]~1_combout\ & ( !\CPU|struct_reg|out\(11) $ (!\CPU|DP|Bin[3]~14_combout\) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( !\CPU|struct_reg|out\(11) $ (!\CPU|DP|rA|out\(3) $ (\CPU|DP|Bin[3]~14_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101010100101010110101010010101010101101010100101010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(3),
	datad => \CPU|DP|ALT_INV_Bin[3]~14_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(3));

-- Location: LABCELL_X85_Y9_N45
\CPU|DP|U2|ALU_AddSub|ai|g[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|g\(2) = ( \CPU|DP|Bin[2]~15_combout\ & ( !\CPU|DP|Ain[4]~1_combout\ & ( (\CPU|DP|rA|out\(2) & !\CPU|struct_reg|out\(11)) ) ) ) # ( !\CPU|DP|Bin[2]~15_combout\ & ( !\CPU|DP|Ain[4]~1_combout\ & ( (\CPU|DP|rA|out\(2) & 
-- \CPU|struct_reg|out\(11)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000011110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|rA|ALT_INV_out\(2),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Bin[2]~15_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|g\(2));

-- Location: LABCELL_X85_Y8_N6
\CPU|DP|U2|ALU_AddSub|ai|comb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|p\(2) & ( \CPU|DP|U2|ALU_AddSub|ai|g\(2) & ( (\CPU|DP|U2|ALU_AddSub|ai|p\(4) & ((\CPU|DP|U2|ALU_AddSub|ai|p\(3)) # (\CPU|DP|U2|ALU_AddSub|ai|g\(3)))) ) ) ) # ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|p\(2) & ( \CPU|DP|U2|ALU_AddSub|ai|g\(2) & ( (\CPU|DP|U2|ALU_AddSub|ai|p\(4) & ((\CPU|DP|U2|ALU_AddSub|ai|p\(3)) # (\CPU|DP|U2|ALU_AddSub|ai|g\(3)))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|p\(2) & ( !\CPU|DP|U2|ALU_AddSub|ai|g\(2) & 
-- ( (\CPU|DP|U2|ALU_AddSub|ai|p\(4) & (((\CPU|DP|U2|ALU_AddSub|ai|c\(2) & \CPU|DP|U2|ALU_AddSub|ai|p\(3))) # (\CPU|DP|U2|ALU_AddSub|ai|g\(3)))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|p\(2) & ( !\CPU|DP|U2|ALU_AddSub|ai|g\(2) & ( (\CPU|DP|U2|ALU_AddSub|ai|g\(3) 
-- & \CPU|DP|U2|ALU_AddSub|ai|p\(4)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000011100000011000011110000001100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(2),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(3),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(4),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(3),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(2),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(2),
	combout => \CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\);

-- Location: LABCELL_X83_Y9_N54
\CPU|DP|U2|main|Mux10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux10~0_combout\ = ( \CPU|DP|Bin[5]~12_combout\ & ( (!\CPU|DP|Ain[4]~1_combout\ & (\CPU|DP|rA|out\(5) & !\CPU|struct_reg|out\(11))) ) ) # ( !\CPU|DP|Bin[5]~12_combout\ & ( \CPU|struct_reg|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|DP|rA|ALT_INV_out\(5),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[5]~12_combout\,
	combout => \CPU|DP|U2|main|Mux10~0_combout\);

-- Location: MLABCELL_X84_Y9_N18
\CPU|DP|U2|main|Mux10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux10~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|g\(4) & ( \CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( (\CPU|DP|U2|main|Mux10~0_combout\ & \CPU|struct_reg|out\(12)) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(4) & ( \CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( 
-- (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\)) # (\CPU|struct_reg|out\(12) & ((\CPU|DP|U2|main|Mux10~0_combout\))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|g\(4) & ( !\CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( (!\CPU|struct_reg|out\(12)) # 
-- (\CPU|DP|U2|main|Mux10~0_combout\) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|g\(4) & ( !\CPU|DP|U2|ALU_AddSub|ai|p\(5) & ( (!\CPU|struct_reg|out\(12) & (\CPU|DP|U2|ALU_AddSub|ai|comb~0_combout\)) # (\CPU|struct_reg|out\(12) & 
-- ((\CPU|DP|U2|main|Mux10~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100001111111111110000111111001100000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_comb~0_combout\,
	datac => \CPU|DP|U2|main|ALT_INV_Mux10~0_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(12),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(4),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(5),
	combout => \CPU|DP|U2|main|Mux10~1_combout\);

-- Location: FF_X84_Y9_N20
\CPU|DP|rC|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux10~1_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(5));

-- Location: FF_X77_Y8_N52
\CPU|Data_Address|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(5),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(5));

-- Location: LABCELL_X77_Y8_N51
\CPU|mem_addr[5]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[5]~6_combout\ = ( \CPU|Data_Address|out\(5) & ( ((!\CPU|FSM|WideNor2~combout\ & (!\CPU|FSM|always0~12_combout\ & \CPU|FSM|WideOr41~1_combout\))) # (\CPU|program_counter|out\(5)) ) ) # ( !\CPU|Data_Address|out\(5) & ( 
-- (\CPU|program_counter|out\(5) & (((!\CPU|FSM|WideOr41~1_combout\) # (\CPU|FSM|always0~12_combout\)) # (\CPU|FSM|WideNor2~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100010101010101011101010101010101000101010101010111010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out\(5),
	datab => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datac => \CPU|FSM|ALT_INV_always0~12_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datae => \CPU|Data_Address|ALT_INV_out\(5),
	combout => \CPU|mem_addr[5]~6_combout\);

-- Location: LABCELL_X85_Y6_N48
\Selector11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector11~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (((\MEM|mem_rtl_0|auto_generated|ram_block1a4\)))) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a4\))) # (\Equal5~0_combout\ & 
-- (\SW[4]~input_o\)))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a4\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111010000000111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[4]~input_o\,
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_Equal5~0_combout\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector11~0_combout\);

-- Location: FF_X85_Y6_N50
\CPU|struct_reg|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector11~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(4));

-- Location: FF_X82_Y7_N49
\CPU|DP|rB|out[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector10~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out[5]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y6_N21
\CPU|DP|Bin[4]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[4]~19_combout\ = ( \CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|struct_reg|out\(4) ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[4]~DUPLICATE_q\ ) ) ) # ( 
-- \CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[5]~DUPLICATE_q\ ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out\(3) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100110011001100001111000011110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(4),
	datab => \CPU|DP|rB|ALT_INV_out[5]~DUPLICATE_q\,
	datac => \CPU|DP|rB|ALT_INV_out[4]~DUPLICATE_q\,
	datad => \CPU|DP|rB|ALT_INV_out\(3),
	datae => \CPU|struct_decoder|ALT_INV_shift[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~0_combout\,
	combout => \CPU|DP|Bin[4]~19_combout\);

-- Location: LABCELL_X85_Y6_N51
\CPU|DP|Bin[4]~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[4]~20_combout\ = ( \CPU|FSM|WideNor15~0_combout\ & ( \CPU|struct_reg|out\(4) ) ) # ( !\CPU|FSM|WideNor15~0_combout\ & ( \CPU|DP|Bin[4]~19_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out\(4),
	datad => \CPU|DP|ALT_INV_Bin[4]~19_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|DP|Bin[4]~20_combout\);

-- Location: MLABCELL_X84_Y6_N30
\CPU|DP|Bin[4]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[4]~13_combout\ = ( \CPU|DP|Bin[4]~19_combout\ & ( \CPU|DP|Bin[4]~20_combout\ ) ) # ( !\CPU|DP|Bin[4]~19_combout\ & ( \CPU|DP|Bin[4]~20_combout\ & ( (!\CPU|FSM|WideNor5~1_combout\ & (\CPU|FSM|always0~2_combout\ & (!\CPU|FSM|WideNor12~combout\ & 
-- !\CPU|FSM|WideNor13~combout\))) ) ) ) # ( \CPU|DP|Bin[4]~19_combout\ & ( !\CPU|DP|Bin[4]~20_combout\ & ( ((!\CPU|FSM|always0~2_combout\) # ((\CPU|FSM|WideNor13~combout\) # (\CPU|FSM|WideNor12~combout\))) # (\CPU|FSM|WideNor5~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111111111111100100000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datab => \CPU|FSM|ALT_INV_always0~2_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datae => \CPU|DP|ALT_INV_Bin[4]~19_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[4]~20_combout\,
	combout => \CPU|DP|Bin[4]~13_combout\);

-- Location: MLABCELL_X84_Y9_N24
\CPU|DP|U2|ALU_AddSub|ai|c[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c\(4) = ( \CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( \CPU|DP|U2|ALU_AddSub|ai|p\(3) & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(2) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(2) & !\CPU|DP|U2|ALU_AddSub|ai|g\(3))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( 
-- \CPU|DP|U2|ALU_AddSub|ai|p\(3) & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(2) & !\CPU|DP|U2|ALU_AddSub|ai|g\(3)) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( !\CPU|DP|U2|ALU_AddSub|ai|p\(3) & ( !\CPU|DP|U2|ALU_AddSub|ai|g\(3) ) ) ) # ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( !\CPU|DP|U2|ALU_AddSub|ai|p\(3) & ( !\CPU|DP|U2|ALU_AddSub|ai|g\(3) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000010101010000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(2),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(2),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(3),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(2),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(3),
	combout => \CPU|DP|U2|ALU_AddSub|ai|c\(4));

-- Location: MLABCELL_X84_Y9_N12
\CPU|DP|U2|main|Mux11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux11~0_combout\ = ( \CPU|struct_reg|out\(12) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(4) & ( (!\CPU|DP|Bin[4]~13_combout\ & (((\CPU|struct_reg|out\(11))))) # (\CPU|DP|Bin[4]~13_combout\ & (!\CPU|DP|Ain[4]~1_combout\ & (!\CPU|struct_reg|out\(11) & 
-- \CPU|DP|rA|out\(4)))) ) ) ) # ( !\CPU|struct_reg|out\(12) & ( \CPU|DP|U2|ALU_AddSub|ai|c\(4) & ( !\CPU|DP|Bin[4]~13_combout\ $ (!\CPU|struct_reg|out\(11) $ (((!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(4))))) ) ) ) # ( \CPU|struct_reg|out\(12) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(4) & ( (!\CPU|DP|Bin[4]~13_combout\ & (((\CPU|struct_reg|out\(11))))) # (\CPU|DP|Bin[4]~13_combout\ & (!\CPU|DP|Ain[4]~1_combout\ & (!\CPU|struct_reg|out\(11) & \CPU|DP|rA|out\(4)))) ) ) ) # ( !\CPU|struct_reg|out\(12) & ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(4) & ( !\CPU|DP|Bin[4]~13_combout\ $ (!\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(4)) # (\CPU|DP|Ain[4]~1_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010010101101001000010100100101001011010100101100000101001001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Bin[4]~13_combout\,
	datab => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|rA|ALT_INV_out\(4),
	datae => \CPU|struct_reg|ALT_INV_out\(12),
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(4),
	combout => \CPU|DP|U2|main|Mux11~0_combout\);

-- Location: FF_X84_Y9_N14
\CPU|DP|rC|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux11~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(4));

-- Location: FF_X81_Y7_N47
\CPU|Data_Address|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(4),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(4));

-- Location: LABCELL_X81_Y7_N45
\CPU|mem_addr[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[4]~5_combout\ = ( \CPU|Data_Address|out\(4) & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out[4]~DUPLICATE_q\ ) ) ) # ( !\CPU|Data_Address|out\(4) & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out[4]~DUPLICATE_q\ ) ) 
-- ) # ( \CPU|Data_Address|out\(4) & ( !\CPU|FSM|always0~12_combout\ & ( ((\CPU|FSM|WideOr41~1_combout\ & !\CPU|FSM|WideNor2~combout\)) # (\CPU|program_counter|out[4]~DUPLICATE_q\) ) ) ) # ( !\CPU|Data_Address|out\(4) & ( !\CPU|FSM|always0~12_combout\ & ( 
-- (\CPU|program_counter|out[4]~DUPLICATE_q\ & ((!\CPU|FSM|WideOr41~1_combout\) # (\CPU|FSM|WideNor2~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010101010111110101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out[4]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datae => \CPU|Data_Address|ALT_INV_out\(4),
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[4]~5_combout\);

-- Location: MLABCELL_X82_Y8_N45
\Selector10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector10~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (((\MEM|mem_rtl_0|auto_generated|ram_block1a5\)))) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a5\))) # (\Equal5~0_combout\ & 
-- (\SW[5]~input_o\)))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a5\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111011110000000111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_Equal5~0_combout\,
	datac => \ALT_INV_SW[5]~input_o\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector10~0_combout\);

-- Location: FF_X82_Y8_N47
\CPU|struct_reg|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector10~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(5));

-- Location: LABCELL_X81_Y6_N18
\CPU|struct_decoder|nsel_mux|out[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out\(0) = ( \CPU|FSM|always0~25_combout\ & ( !\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ( (!\CPU|struct_reg|out\(5)) # ((!\CPU|FSM|WideNor43~combout\ & (!\CPU|FSM|WideOr29~1_combout\ & \CPU|FSM|WideOr29~0_combout\))) ) 
-- ) ) # ( !\CPU|FSM|always0~25_combout\ & ( !\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ( (!\CPU|struct_reg|out\(5)) # ((!\CPU|FSM|WideOr29~1_combout\ & \CPU|FSM|WideOr29~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011111100111100001111100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datab => \CPU|FSM|ALT_INV_WideOr29~1_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(5),
	datad => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	datae => \CPU|FSM|ALT_INV_always0~25_combout\,
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out[0]~3_combout\,
	combout => \CPU|struct_decoder|nsel_mux|out\(0));

-- Location: LABCELL_X80_Y7_N54
\CPU|DP|REGFILE|writenum_OH[7]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|writenum_OH[7]~0_combout\ = (!\CPU|FSM|WideOr11~combout\ & (!\CPU|struct_decoder|nsel_mux|out\(0) & (!\CPU|struct_decoder|nsel_mux|out\(2) & !\CPU|struct_decoder|nsel_mux|out\(1))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000010000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~combout\,
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\);

-- Location: FF_X79_Y8_N52
\CPU|DP|REGFILE|regR7|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(1));

-- Location: FF_X79_Y8_N25
\CPU|DP|REGFILE|regR5|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(1),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(1));

-- Location: FF_X80_Y6_N17
\CPU|DP|REGFILE|regR6|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(1));

-- Location: FF_X80_Y6_N56
\CPU|DP|REGFILE|regR4|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(1));

-- Location: LABCELL_X80_Y6_N54
\CPU|DP|REGFILE|mux_out|m|Selector14~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector14~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(1) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(1) & (((\CPU|DP|REGFILE|regR6|out\(1) & \CPU|struct_decoder|nsel_mux|out\(0))))) # 
-- (\CPU|struct_decoder|nsel_mux|out\(1) & (((\CPU|struct_decoder|nsel_mux|out\(0))) # (\CPU|DP|REGFILE|regR5|out\(1)))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(1) & ( !\CPU|struct_decoder|nsel_mux|out\(2) & ( (!\CPU|struct_decoder|nsel_mux|out\(1) & 
-- (((\CPU|DP|REGFILE|regR6|out\(1) & \CPU|struct_decoder|nsel_mux|out\(0))))) # (\CPU|struct_decoder|nsel_mux|out\(1) & (\CPU|DP|REGFILE|regR5|out\(1) & ((!\CPU|struct_decoder|nsel_mux|out\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100110000000001010011111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR5|ALT_INV_out\(1),
	datab => \CPU|DP|REGFILE|regR6|ALT_INV_out\(1),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(1),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector14~1_combout\);

-- Location: FF_X80_Y6_N31
\CPU|DP|REGFILE|regR3|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(1));

-- Location: MLABCELL_X78_Y7_N57
\CPU|DP|REGFILE|regR1|out[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[1]~feeder_combout\ = ( \CPU|DP|U3|b\(1) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|regR1|out[1]~feeder_combout\);

-- Location: FF_X78_Y7_N58
\CPU|DP|REGFILE|regR1|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[1]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(1));

-- Location: FF_X79_Y6_N5
\CPU|DP|REGFILE|regR0|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(1));

-- Location: FF_X79_Y6_N56
\CPU|DP|REGFILE|regR2|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(1));

-- Location: LABCELL_X79_Y6_N54
\CPU|DP|REGFILE|mux_out|m|Selector14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(1) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(1)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(1) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(1) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(1) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(1)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR3|out\(1))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(1) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(1)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- (\CPU|DP|REGFILE|regR3|out\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101000011110101010100110011000000000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR3|ALT_INV_out\(1),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(1),
	datac => \CPU|DP|REGFILE|regR0|ALT_INV_out\(1),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(1),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\);

-- Location: LABCELL_X79_Y6_N15
\CPU|DP|REGFILE|mux_out|m|Selector14~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector14~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\ & ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(1) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(1) ) ) ) # ( \CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # 
-- (\CPU|DP|REGFILE|mux_out|m|Selector14~1_combout\) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector14~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector14~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111111111110000111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|regR7|ALT_INV_out\(1),
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector14~1_combout\,
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector14~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector14~2_combout\);

-- Location: FF_X85_Y6_N8
\CPU|DP|rB|out[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector14~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rB|out[1]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y6_N9
\CPU|DP|Bin[2]~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[2]~23_combout\ = ( \CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|struct_reg|out\(2) ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( \CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[2]~DUPLICATE_q\ ) ) ) # ( 
-- \CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out\(3) ) ) ) # ( !\CPU|struct_decoder|shift[1]~0_combout\ & ( !\CPU|DP|Bin[3]~0_combout\ & ( \CPU|DP|rB|out[1]~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000001111111101010101010101010011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rB|ALT_INV_out[2]~DUPLICATE_q\,
	datab => \CPU|struct_reg|ALT_INV_out\(2),
	datac => \CPU|DP|rB|ALT_INV_out[1]~DUPLICATE_q\,
	datad => \CPU|DP|rB|ALT_INV_out\(3),
	datae => \CPU|struct_decoder|ALT_INV_shift[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~0_combout\,
	combout => \CPU|DP|Bin[2]~23_combout\);

-- Location: LABCELL_X85_Y6_N33
\CPU|DP|Bin[2]~24\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[2]~24_combout\ = ( \CPU|FSM|WideNor15~0_combout\ & ( \CPU|struct_reg|out\(2) ) ) # ( !\CPU|FSM|WideNor15~0_combout\ & ( \CPU|DP|Bin[2]~23_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out\(2),
	datad => \CPU|DP|ALT_INV_Bin[2]~23_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	combout => \CPU|DP|Bin[2]~24_combout\);

-- Location: MLABCELL_X84_Y6_N33
\CPU|DP|Bin[2]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[2]~15_combout\ = ( \CPU|DP|Bin[2]~23_combout\ & ( \CPU|DP|Bin[2]~24_combout\ ) ) # ( !\CPU|DP|Bin[2]~23_combout\ & ( \CPU|DP|Bin[2]~24_combout\ & ( (!\CPU|FSM|WideNor5~1_combout\ & (\CPU|FSM|always0~2_combout\ & (!\CPU|FSM|WideNor13~combout\ & 
-- !\CPU|FSM|WideNor12~combout\))) ) ) ) # ( \CPU|DP|Bin[2]~23_combout\ & ( !\CPU|DP|Bin[2]~24_combout\ & ( ((!\CPU|FSM|always0~2_combout\) # ((\CPU|FSM|WideNor12~combout\) # (\CPU|FSM|WideNor13~combout\))) # (\CPU|FSM|WideNor5~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111111111111100100000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor5~1_combout\,
	datab => \CPU|FSM|ALT_INV_always0~2_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datae => \CPU|DP|ALT_INV_Bin[2]~23_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[2]~24_combout\,
	combout => \CPU|DP|Bin[2]~15_combout\);

-- Location: MLABCELL_X84_Y9_N51
\CPU|DP|U2|ALU_AddSub|ai|p[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|p\(2) = ( \CPU|DP|Bin[2]~15_combout\ & ( !\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(2) & !\CPU|DP|Ain[4]~1_combout\))) ) ) # ( !\CPU|DP|Bin[2]~15_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(2)) # 
-- (\CPU|DP|Ain[4]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101000001111010110100000111110100101111100001010010111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(2),
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[2]~15_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|p\(2));

-- Location: MLABCELL_X84_Y6_N27
\CPU|DP|U2|main|Mux12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux12~0_combout\ = ( \CPU|DP|Ain[4]~1_combout\ & ( (\CPU|struct_reg|out\(11) & !\CPU|DP|Bin[3]~14_combout\) ) ) # ( !\CPU|DP|Ain[4]~1_combout\ & ( (!\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(3) & \CPU|DP|Bin[3]~14_combout\)) # 
-- (\CPU|struct_reg|out\(11) & ((!\CPU|DP|Bin[3]~14_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100001010010101010000101001010101000000000101010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|DP|rA|ALT_INV_out\(3),
	datad => \CPU|DP|ALT_INV_Bin[3]~14_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	combout => \CPU|DP|U2|main|Mux12~0_combout\);

-- Location: LABCELL_X85_Y9_N18
\CPU|DP|U2|main|Mux12~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux12~1_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( \CPU|DP|U2|main|Mux12~0_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|p\(3) $ (((!\CPU|DP|U2|ALU_AddSub|ai|p\(2) & !\CPU|DP|U2|ALU_AddSub|ai|g\(2))))) # (\CPU|struct_reg|out\(12)) ) ) ) # ( 
-- !\CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( \CPU|DP|U2|main|Mux12~0_combout\ & ( (!\CPU|DP|U2|ALU_AddSub|ai|g\(2) $ (!\CPU|DP|U2|ALU_AddSub|ai|p\(3))) # (\CPU|struct_reg|out\(12)) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( !\CPU|DP|U2|main|Mux12~0_combout\ & ( 
-- (!\CPU|struct_reg|out\(12) & (!\CPU|DP|U2|ALU_AddSub|ai|p\(3) $ (((!\CPU|DP|U2|ALU_AddSub|ai|p\(2) & !\CPU|DP|U2|ALU_AddSub|ai|g\(2)))))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( !\CPU|DP|U2|main|Mux12~0_combout\ & ( (!\CPU|struct_reg|out\(12) & 
-- (!\CPU|DP|U2|ALU_AddSub|ai|g\(2) $ (!\CPU|DP|U2|ALU_AddSub|ai|p\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000011000000011100001000000000111111110011110111111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(2),
	datab => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_g\(2),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_p\(3),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(2),
	dataf => \CPU|DP|U2|main|ALT_INV_Mux12~0_combout\,
	combout => \CPU|DP|U2|main|Mux12~1_combout\);

-- Location: FF_X85_Y9_N20
\CPU|DP|rC|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux12~1_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(3));

-- Location: FF_X81_Y7_N44
\CPU|Data_Address|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(3),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(3));

-- Location: LABCELL_X81_Y7_N0
\CPU|mem_addr[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[3]~4_combout\ = ( \CPU|FSM|WideOr41~1_combout\ & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(3) ) ) ) # ( !\CPU|FSM|WideOr41~1_combout\ & ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(3) ) ) ) # ( 
-- \CPU|FSM|WideOr41~1_combout\ & ( !\CPU|FSM|always0~12_combout\ & ( (!\CPU|FSM|WideNor2~combout\ & (\CPU|Data_Address|out\(3))) # (\CPU|FSM|WideNor2~combout\ & ((\CPU|program_counter|out\(3)))) ) ) ) # ( !\CPU|FSM|WideOr41~1_combout\ & ( 
-- !\CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(3) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111001001110010011100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datab => \CPU|Data_Address|ALT_INV_out\(3),
	datac => \CPU|program_counter|ALT_INV_out\(3),
	datae => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[3]~4_combout\);

-- Location: LABCELL_X85_Y6_N30
\Selector13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~0_combout\ = ( \LED_load~4_combout\ & ( (!\Equal5~0_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a2\)) # (\Equal5~0_combout\ & ((!\LED_load~0_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a2\)) # (\LED_load~0_combout\ & 
-- ((\SW[2]~input_o\))))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a2\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001001110011001100100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal5~0_combout\,
	datab => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\,
	datac => \ALT_INV_SW[2]~input_o\,
	datad => \ALT_INV_LED_load~0_combout\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector13~0_combout\);

-- Location: FF_X85_Y6_N32
\CPU|struct_reg|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector13~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(2));

-- Location: FF_X87_Y8_N25
\CPU|program_counter|out[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(2),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out[2]~DUPLICATE_q\);

-- Location: LABCELL_X79_Y8_N6
\CPU|DP|U3|b[2]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[2]~14_combout\ = ( \CPU|program_counter|out[2]~DUPLICATE_q\ & ( \CPU|DP|rC|out\(2) & ( (!\CPU|FSM|WideOr11~0_combout\ & (\CPU|FSM|WideOr26~combout\ & ((!\CPU|struct_reg|out\(2)) # (!\CPU|FSM|WideNor6~combout\)))) ) ) ) # ( 
-- !\CPU|program_counter|out[2]~DUPLICATE_q\ & ( \CPU|DP|rC|out\(2) & ( (\CPU|FSM|WideOr26~combout\ & ((!\CPU|struct_reg|out\(2)) # (!\CPU|FSM|WideNor6~combout\))) ) ) ) # ( \CPU|program_counter|out[2]~DUPLICATE_q\ & ( !\CPU|DP|rC|out\(2) & ( 
-- (!\CPU|FSM|WideOr11~0_combout\ & ((!\CPU|struct_reg|out\(2)) # (!\CPU|FSM|WideNor6~combout\))) ) ) ) # ( !\CPU|program_counter|out[2]~DUPLICATE_q\ & ( !\CPU|DP|rC|out\(2) & ( (!\CPU|struct_reg|out\(2)) # (!\CPU|FSM|WideNor6~combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111100101010001010100000000000111111000000000010101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	datab => \CPU|struct_reg|ALT_INV_out\(2),
	datac => \CPU|FSM|ALT_INV_WideNor6~combout\,
	datad => \CPU|FSM|ALT_INV_WideOr26~combout\,
	datae => \CPU|program_counter|ALT_INV_out[2]~DUPLICATE_q\,
	dataf => \CPU|DP|rC|ALT_INV_out\(2),
	combout => \CPU|DP|U3|b[2]~14_combout\);

-- Location: LABCELL_X79_Y8_N0
\CPU|DP|U3|b[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(2) = ( \CPU|FSM|vsel\(3) & ( \Equal5~1_combout\ & ( (!\CPU|DP|U3|b[2]~14_combout\) # (\SW[2]~input_o\) ) ) ) # ( !\CPU|FSM|vsel\(3) & ( \Equal5~1_combout\ & ( !\CPU|DP|U3|b[2]~14_combout\ ) ) ) # ( \CPU|FSM|vsel\(3) & ( !\Equal5~1_combout\ & 
-- ( (!\CPU|DP|U3|b[2]~14_combout\) # (\MEM|mem_rtl_0|auto_generated|ram_block1a2\) ) ) ) # ( !\CPU|FSM|vsel\(3) & ( !\Equal5~1_combout\ & ( !\CPU|DP|U3|b[2]~14_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001111111111001100110011001101110111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[2]~input_o\,
	datab => \CPU|DP|U3|ALT_INV_b[2]~14_combout\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\,
	datae => \CPU|FSM|ALT_INV_vsel\(3),
	dataf => \ALT_INV_Equal5~1_combout\,
	combout => \CPU|DP|U3|b\(2));

-- Location: FF_X79_Y8_N10
\CPU|DP|REGFILE|regR7|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(2));

-- Location: FF_X80_Y6_N13
\CPU|DP|REGFILE|regR6|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(2));

-- Location: FF_X79_Y8_N43
\CPU|DP|REGFILE|regR5|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(2));

-- Location: FF_X80_Y6_N38
\CPU|DP|REGFILE|regR4|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(2));

-- Location: LABCELL_X80_Y6_N36
\CPU|DP|REGFILE|mux_out|m|Selector13~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector13~1_combout\ = ( \CPU|DP|REGFILE|regR4|out\(2) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((\CPU|DP|REGFILE|regR5|out\(2)) # (\CPU|struct_decoder|nsel_mux|out\(0)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR4|out\(2) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR5|out\(2) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( \CPU|DP|REGFILE|regR4|out\(2) & ( 
-- !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|DP|REGFILE|regR6|out\(2) & (\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) ) # ( !\CPU|DP|REGFILE|regR4|out\(2) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( 
-- (\CPU|DP|REGFILE|regR6|out\(2) & (\CPU|struct_decoder|nsel_mux|out\(0) & !\CPU|struct_decoder|nsel_mux|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100000000000100010000000000001100000000000011111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR6|ALT_INV_out\(2),
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datac => \CPU|DP|REGFILE|regR5|ALT_INV_out\(2),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR4|ALT_INV_out\(2),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector13~1_combout\);

-- Location: FF_X80_Y6_N49
\CPU|DP|REGFILE|regR3|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(2));

-- Location: LABCELL_X77_Y8_N57
\CPU|DP|REGFILE|regR1|out[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[2]~feeder_combout\ = ( \CPU|DP|U3|b\(2) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|regR1|out[2]~feeder_combout\);

-- Location: FF_X77_Y8_N58
\CPU|DP|REGFILE|regR1|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[2]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(2));

-- Location: FF_X79_Y6_N2
\CPU|DP|REGFILE|regR0|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(2));

-- Location: FF_X79_Y6_N8
\CPU|DP|REGFILE|regR2|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(2));

-- Location: LABCELL_X79_Y6_N6
\CPU|DP|REGFILE|mux_out|m|Selector13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(2) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(2)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(2) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(2) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(2) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(2)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR3|out\(2))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(2) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR0|out\(2)))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- (\CPU|DP|REGFILE|regR3|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101000011110101010100110011000000000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR3|ALT_INV_out\(2),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(2),
	datac => \CPU|DP|REGFILE|regR0|ALT_INV_out\(2),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(2),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\);

-- Location: LABCELL_X79_Y6_N0
\CPU|DP|REGFILE|mux_out|m|Selector13~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector13~2_combout\ = ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(2) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- \CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\) # (\CPU|DP|REGFILE|mux_out|m|Selector13~1_combout\) ) ) ) # ( \CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\ & ( \CPU|DP|REGFILE|regR7|out\(2) ) ) ) # ( !\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|mux_out|m|Selector13~0_combout\ & ( \CPU|DP|REGFILE|mux_out|m|Selector13~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011010101010101010111110011111100110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR7|ALT_INV_out\(2),
	datab => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector13~1_combout\,
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datae => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector13~0_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector13~2_combout\);

-- Location: FF_X79_Y6_N14
\CPU|DP|rA|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|mux_out|m|Selector13~2_combout\,
	sload => VCC,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(2));

-- Location: LABCELL_X83_Y9_N30
\CPU|DP|U2|main|Mux13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux13~0_combout\ = ( \CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( \CPU|DP|Bin[2]~15_combout\ & ( (!\CPU|struct_reg|out\(11) & (!\CPU|DP|Ain[4]~1_combout\ & (\CPU|DP|rA|out\(2)))) # (\CPU|struct_reg|out\(11) & (!\CPU|struct_reg|out\(12) & 
-- ((!\CPU|DP|rA|out\(2)) # (\CPU|DP|Ain[4]~1_combout\)))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( \CPU|DP|Bin[2]~15_combout\ & ( (!\CPU|struct_reg|out\(12) & (!\CPU|struct_reg|out\(11) $ (((!\CPU|DP|Ain[4]~1_combout\ & \CPU|DP|rA|out\(2)))))) # 
-- (\CPU|struct_reg|out\(12) & (!\CPU|DP|Ain[4]~1_combout\ & (\CPU|DP|rA|out\(2) & !\CPU|struct_reg|out\(11)))) ) ) ) # ( \CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( !\CPU|DP|Bin[2]~15_combout\ & ( !\CPU|struct_reg|out\(11) $ ((((!\CPU|DP|Ain[4]~1_combout\ & 
-- \CPU|DP|rA|out\(2))) # (\CPU|struct_reg|out\(12)))) ) ) ) # ( !\CPU|DP|U2|ALU_AddSub|ai|c\(2) & ( !\CPU|DP|Bin[2]~15_combout\ & ( !\CPU|struct_reg|out\(11) $ ((((!\CPU|DP|rA|out\(2)) # (\CPU|struct_reg|out\(12))) # (\CPU|DP|Ain[4]~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000011011111110100000010111111010010001000000010001011010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datab => \CPU|DP|rA|ALT_INV_out\(2),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(2),
	dataf => \CPU|DP|ALT_INV_Bin[2]~15_combout\,
	combout => \CPU|DP|U2|main|Mux13~0_combout\);

-- Location: FF_X83_Y9_N32
\CPU|DP|rC|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux13~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(2));

-- Location: LABCELL_X88_Y8_N6
\CPU|nextPCmux|out~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~8_combout\ = ( \CPU|DP|rC|out\(2) & ( \CPU|FSM|psel[2]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \CPU|DP|rC|ALT_INV_out\(2),
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|nextPCmux|out~8_combout\);

-- Location: MLABCELL_X87_Y8_N24
\CPU|nextPCmux|out[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(2) = ( \CPU|Add0~33_sumout\ & ( \CPU|FSM|WideOr39~2_combout\ & ( ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr9~3_combout\) # (!\CPU|FSM|WideOr41~2_combout\))) # (\CPU|nextPCmux|out~8_combout\) ) ) ) # ( !\CPU|Add0~33_sumout\ & 
-- ( \CPU|FSM|WideOr39~2_combout\ & ( \CPU|nextPCmux|out~8_combout\ ) ) ) # ( \CPU|Add0~33_sumout\ & ( !\CPU|FSM|WideOr39~2_combout\ ) ) # ( !\CPU|Add0~33_sumout\ & ( !\CPU|FSM|WideOr39~2_combout\ & ( \CPU|nextPCmux|out~8_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101111111111111111101010101010101011111111111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|nextPCmux|ALT_INV_out~8_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datae => \CPU|ALT_INV_Add0~33_sumout\,
	dataf => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	combout => \CPU|nextPCmux|out\(2));

-- Location: FF_X87_Y8_N26
\CPU|program_counter|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(2),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(2));

-- Location: FF_X80_Y7_N2
\CPU|Data_Address|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(2),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(2));

-- Location: LABCELL_X80_Y7_N0
\CPU|mem_addr[2]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[2]~3_combout\ = ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(2) ) ) # ( !\CPU|FSM|always0~12_combout\ & ( (!\CPU|FSM|WideOr41~1_combout\ & (((\CPU|program_counter|out\(2))))) # (\CPU|FSM|WideOr41~1_combout\ & 
-- ((!\CPU|FSM|WideNor2~combout\ & ((\CPU|Data_Address|out\(2)))) # (\CPU|FSM|WideNor2~combout\ & (\CPU|program_counter|out\(2))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101101001111000010110100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datac => \CPU|program_counter|ALT_INV_out\(2),
	datad => \CPU|Data_Address|ALT_INV_out\(2),
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[2]~3_combout\);

-- Location: MLABCELL_X82_Y8_N15
\Selector15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector15~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (((\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\)))) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\))) # 
-- (\Equal5~0_combout\ & (\SW[0]~input_o\)))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111011110000000111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_Equal5~0_combout\,
	datac => \ALT_INV_SW[0]~input_o\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector15~0_combout\);

-- Location: FF_X82_Y8_N17
\CPU|struct_reg|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector15~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(0));

-- Location: MLABCELL_X87_Y8_N30
\CPU|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~25_sumout\ = SUM(( (!\CPU|FSM|WideOr37~combout\) # (\CPU|struct_reg|out\(0)) ) + ( \CPU|program_counter|out\(0) ) + ( !VCC ))
-- \CPU|Add0~26\ = CARRY(( (!\CPU|FSM|WideOr37~combout\) # (\CPU|struct_reg|out\(0)) ) + ( \CPU|program_counter|out\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000001100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideOr37~combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(0),
	dataf => \CPU|program_counter|ALT_INV_out\(0),
	cin => GND,
	sumout => \CPU|Add0~25_sumout\,
	cout => \CPU|Add0~26\);

-- Location: LABCELL_X88_Y8_N24
\CPU|nextPCmux|out~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~7_combout\ = ( \CPU|DP|rC|out\(1) & ( \CPU|FSM|psel[2]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	dataf => \CPU|DP|rC|ALT_INV_out\(1),
	combout => \CPU|nextPCmux|out~7_combout\);

-- Location: MLABCELL_X87_Y8_N9
\CPU|nextPCmux|out[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(1) = ( \CPU|Add0~29_sumout\ & ( \CPU|nextPCmux|out~7_combout\ ) ) # ( !\CPU|Add0~29_sumout\ & ( \CPU|nextPCmux|out~7_combout\ ) ) # ( \CPU|Add0~29_sumout\ & ( !\CPU|nextPCmux|out~7_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr41~2_combout\) # (!\CPU|FSM|WideOr9~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datae => \CPU|ALT_INV_Add0~29_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~7_combout\,
	combout => \CPU|nextPCmux|out\(1));

-- Location: FF_X87_Y8_N10
\CPU|program_counter|out[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(1),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out[1]~DUPLICATE_q\);

-- Location: LABCELL_X80_Y7_N3
\CPU|mem_addr[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[1]~2_combout\ = ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out[1]~DUPLICATE_q\ ) ) # ( !\CPU|FSM|always0~12_combout\ & ( (!\CPU|FSM|WideOr41~1_combout\ & (((\CPU|program_counter|out[1]~DUPLICATE_q\)))) # 
-- (\CPU|FSM|WideOr41~1_combout\ & ((!\CPU|FSM|WideNor2~combout\ & ((\CPU|Data_Address|out\(1)))) # (\CPU|FSM|WideNor2~combout\ & (\CPU|program_counter|out[1]~DUPLICATE_q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101101001111000010110100111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datac => \CPU|program_counter|ALT_INV_out[1]~DUPLICATE_q\,
	datad => \CPU|Data_Address|ALT_INV_out\(1),
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[1]~2_combout\);

-- Location: LABCELL_X81_Y7_N30
\Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = ( \Equal5~0_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( (!\LED_load~0_combout\) # ((!\LED_load~2_combout\) # ((!\LED_load~1_combout\) # (!\LED_load~3_combout\))) ) ) ) # ( !\Equal5~0_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a10\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_LED_load~2_combout\,
	datac => \ALT_INV_LED_load~1_combout\,
	datad => \ALT_INV_LED_load~3_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\,
	combout => \Selector5~0_combout\);

-- Location: FF_X81_Y7_N31
\CPU|struct_reg|out[10]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector5~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[10]~DUPLICATE_q\);

-- Location: LABCELL_X83_Y8_N6
\CPU|FSM|WideNor39\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor39~combout\ = ( \CPU|FSM|WideNor27~0_combout\ & ( \CPU|FSM|WideNor42~0_combout\ & ( (\CPU|DP|status|out\(0) & (\CPU|struct_reg|out[10]~DUPLICATE_q\ & (\CPU|FSM|WideNor33~1_combout\ & \CPU|FSM|WideNor5~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|status|ALT_INV_out\(0),
	datab => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideNor33~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor42~0_combout\,
	combout => \CPU|FSM|WideNor39~combout\);

-- Location: LABCELL_X80_Y8_N24
\CPU|FSM|always0~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~23_combout\ = ( !\CPU|FSM|WideNor41~combout\ & ( \CPU|FSM|always0~22_combout\ & ( (!\CPU|FSM|WideNor39~combout\ & (!\CPU|FSM|WideNor43~combout\ & (!\CPU|FSM|WideNor40~combout\ & !\CPU|FSM|WideNor42~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor39~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor40~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor41~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~22_combout\,
	combout => \CPU|FSM|always0~23_combout\);

-- Location: MLABCELL_X82_Y8_N6
\CPU|FSM|always0~27\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~27_combout\ = ( \CPU|FSM|WideNor33~0_combout\ & ( \CPU|FSM|always0~7_combout\ & ( (!\CPU|struct_reg|out[10]~DUPLICATE_q\ & (!\CPU|DP|status|out\(0) & (!\CPU|struct_reg|out[8]~DUPLICATE_q\ & \CPU|struct_reg|out\(9)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datab => \CPU|DP|status|ALT_INV_out\(0),
	datac => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out\(9),
	datae => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~7_combout\,
	combout => \CPU|FSM|always0~27_combout\);

-- Location: LABCELL_X83_Y8_N30
\CPU|FSM|WideOr37~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr37~2_combout\ = ( \CPU|FSM|WideNor34~combout\ & ( \CPU|FSM|always0~7_combout\ ) ) # ( !\CPU|FSM|WideNor34~combout\ & ( \CPU|FSM|always0~7_combout\ & ( ((!\CPU|FSM|WideOr1~0_combout\ & ((\CPU|FSM|WideNor37~combout\) # 
-- (\CPU|FSM|WideNor38~combout\)))) # (\CPU|FSM|WideNor33~combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001111111000011111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor38~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor37~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor33~combout\,
	datad => \CPU|FSM|ALT_INV_WideOr1~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor34~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~7_combout\,
	combout => \CPU|FSM|WideOr37~2_combout\);

-- Location: LABCELL_X81_Y8_N6
\CPU|FSM|WideOr37~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr37~3_combout\ = ( !\CPU|FSM|WideOr37~2_combout\ & ( (!\CPU|FSM|always0~22_combout\) # ((!\CPU|FSM|WideNor41~combout\ & (!\CPU|FSM|WideNor40~combout\ & !\CPU|FSM|WideNor39~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111110000000111111111000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor41~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor40~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor39~combout\,
	datad => \CPU|FSM|ALT_INV_always0~22_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr37~2_combout\,
	combout => \CPU|FSM|WideOr37~3_combout\);

-- Location: LABCELL_X81_Y8_N0
\CPU|FSM|WideOr37\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr37~combout\ = ( \CPU|FSM|WideOr37~3_combout\ & ( ((\CPU|FSM|WideNor46~combout\ & (!\CPU|FSM|WideNor44~0_combout\ & \CPU|FSM|always0~23_combout\))) # (\CPU|FSM|always0~27_combout\) ) ) # ( !\CPU|FSM|WideOr37~3_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000100111111110000010011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor46~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor44~0_combout\,
	datac => \CPU|FSM|ALT_INV_always0~23_combout\,
	datad => \CPU|FSM|ALT_INV_always0~27_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr37~3_combout\,
	combout => \CPU|FSM|WideOr37~combout\);

-- Location: LABCELL_X88_Y8_N39
\CPU|nextPCmux|out~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~6_combout\ = ( \CPU|DP|rC|out\(0) & ( \CPU|FSM|psel[2]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \CPU|DP|rC|ALT_INV_out\(0),
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|nextPCmux|out~6_combout\);

-- Location: MLABCELL_X87_Y8_N15
\CPU|nextPCmux|out[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(0) = ( \CPU|Add0~25_sumout\ & ( \CPU|nextPCmux|out~6_combout\ ) ) # ( !\CPU|Add0~25_sumout\ & ( \CPU|nextPCmux|out~6_combout\ ) ) # ( \CPU|Add0~25_sumout\ & ( !\CPU|nextPCmux|out~6_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr41~2_combout\) # (!\CPU|FSM|WideOr9~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datae => \CPU|ALT_INV_Add0~25_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~6_combout\,
	combout => \CPU|nextPCmux|out\(0));

-- Location: FF_X87_Y8_N16
\CPU|program_counter|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(0),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(0));

-- Location: FF_X80_Y7_N32
\CPU|Data_Address|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(0),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(0));

-- Location: LABCELL_X80_Y7_N30
\CPU|mem_addr[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[0]~1_combout\ = ( \CPU|FSM|always0~12_combout\ & ( \CPU|program_counter|out\(0) ) ) # ( !\CPU|FSM|always0~12_combout\ & ( (!\CPU|FSM|WideNor2~combout\ & ((!\CPU|FSM|WideOr41~1_combout\ & (\CPU|program_counter|out\(0))) # 
-- (\CPU|FSM|WideOr41~1_combout\ & ((\CPU|Data_Address|out\(0)))))) # (\CPU|FSM|WideNor2~combout\ & (\CPU|program_counter|out\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000101011101010100010101110101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out\(0),
	datab => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datac => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	datad => \CPU|Data_Address|ALT_INV_out\(0),
	dataf => \CPU|FSM|ALT_INV_always0~12_combout\,
	combout => \CPU|mem_addr[0]~1_combout\);

-- Location: MLABCELL_X82_Y8_N42
\Selector14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector14~0_combout\ = ( \LED_load~4_combout\ & ( (!\LED_load~0_combout\ & (((\MEM|mem_rtl_0|auto_generated|ram_block1a1\)))) # (\LED_load~0_combout\ & ((!\Equal5~0_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a1\)) # (\Equal5~0_combout\ & 
-- ((\SW[1]~input_o\))))) ) ) # ( !\LED_load~4_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a1\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001110000111110000111000011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_Equal5~0_combout\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\,
	datad => \ALT_INV_SW[1]~input_o\,
	dataf => \ALT_INV_LED_load~4_combout\,
	combout => \Selector14~0_combout\);

-- Location: FF_X82_Y8_N44
\CPU|struct_reg|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector14~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(1));

-- Location: LABCELL_X80_Y9_N3
\CPU|struct_decoder|nsel_mux|out[1]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|struct_decoder|nsel_mux|out[1]~4_combout\ = ( !\CPU|FSM|nsel[2]~0_combout\ & ( \CPU|FSM|WideOr31~combout\ & ( \CPU|struct_reg|out\(1) ) ) ) # ( \CPU|FSM|nsel[2]~0_combout\ & ( !\CPU|FSM|WideOr31~combout\ & ( \CPU|struct_reg|out\(9) ) ) ) # ( 
-- !\CPU|FSM|nsel[2]~0_combout\ & ( !\CPU|FSM|WideOr31~combout\ & ( (\CPU|struct_reg|out\(9)) # (\CPU|struct_reg|out\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010111111111000000001111111101010101010101010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(1),
	datad => \CPU|struct_reg|ALT_INV_out\(9),
	datae => \CPU|FSM|ALT_INV_nsel[2]~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr31~combout\,
	combout => \CPU|struct_decoder|nsel_mux|out[1]~4_combout\);

-- Location: LABCELL_X80_Y9_N30
\CPU|DP|REGFILE|mux_out|m|Selector9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\ = ( !\CPU|struct_decoder|nsel_mux|out~1_combout\ & ( \CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (!\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ((\CPU|struct_decoder|nsel_mux|out~0_combout\) # 
-- (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\))) ) ) ) # ( \CPU|struct_decoder|nsel_mux|out~1_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (!\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & ((\CPU|struct_decoder|nsel_mux|out~0_combout\) 
-- # (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\))) ) ) ) # ( !\CPU|struct_decoder|nsel_mux|out~1_combout\ & ( !\CPU|struct_decoder|nsel_mux|out~2_combout\ & ( (!\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & 
-- (((\CPU|struct_decoder|nsel_mux|out~0_combout\) # (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\)))) # (\CPU|struct_decoder|nsel_mux|out[1]~4_combout\ & (!\CPU|struct_decoder|nsel_mux|out[0]~3_combout\ & ((\CPU|struct_decoder|nsel_mux|out~0_combout\) # 
-- (\CPU|struct_decoder|nsel_mux|out[2]~5_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111011101110000010101010101000001100110011000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_decoder|nsel_mux|ALT_INV_out[1]~4_combout\,
	datab => \CPU|struct_decoder|nsel_mux|ALT_INV_out[0]~3_combout\,
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out[2]~5_combout\,
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out~0_combout\,
	datae => \CPU|struct_decoder|nsel_mux|ALT_INV_out~1_combout\,
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out~2_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\);

-- Location: LABCELL_X79_Y7_N39
\CPU|DP|U3|b[12]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b[12]~4_combout\ = ( \CPU|FSM|WideOr26~combout\ & ( !\CPU|DP|U3|b~0_combout\ ) ) # ( !\CPU|FSM|WideOr26~combout\ & ( (!\CPU|DP|U3|b~0_combout\ & !\CPU|DP|rC|out\(12)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110011001100110011000000110000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U3|ALT_INV_b~0_combout\,
	datac => \CPU|DP|rC|ALT_INV_out\(12),
	datae => \CPU|FSM|ALT_INV_WideOr26~combout\,
	combout => \CPU|DP|U3|b[12]~4_combout\);

-- Location: LABCELL_X80_Y7_N6
\CPU|DP|U3|b[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U3|b\(12) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( \CPU|DP|U3|b[12]~4_combout\ & ( (\CPU|FSM|vsel\(3) & ((!\LED_load~0_combout\) # ((!\LED_load~4_combout\) # (!\Equal5~0_combout\)))) ) ) ) # ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( !\CPU|DP|U3|b[12]~4_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( !\CPU|DP|U3|b[12]~4_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000101010101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_vsel\(3),
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~4_combout\,
	datad => \ALT_INV_Equal5~0_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\,
	dataf => \CPU|DP|U3|ALT_INV_b[12]~4_combout\,
	combout => \CPU|DP|U3|b\(12));

-- Location: FF_X83_Y7_N22
\CPU|DP|REGFILE|regR0|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR0|out\(12));

-- Location: LABCELL_X77_Y7_N15
\CPU|DP|REGFILE|regR1|out[12]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regR1|out[12]~feeder_combout\ = ( \CPU|DP|U3|b\(12) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|U3|ALT_INV_b\(12),
	combout => \CPU|DP|REGFILE|regR1|out[12]~feeder_combout\);

-- Location: FF_X77_Y7_N16
\CPU|DP|REGFILE|regR1|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|regR1|out[12]~feeder_combout\,
	ena => \CPU|DP|REGFILE|writenum_OH[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR1|out\(12));

-- Location: FF_X81_Y6_N16
\CPU|DP|REGFILE|regR3|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR3|out\(12));

-- Location: FF_X84_Y7_N44
\CPU|DP|REGFILE|regR2|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR2|out\(12));

-- Location: MLABCELL_X84_Y7_N42
\CPU|DP|REGFILE|mux_out|m|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector3~0_combout\ = ( \CPU|DP|REGFILE|regR2|out\(12) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) # (\CPU|DP|REGFILE|regR1|out\(12)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(12) & ( \CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (\CPU|DP|REGFILE|regR1|out\(12) & !\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\) ) ) ) # ( \CPU|DP|REGFILE|regR2|out\(12) & ( 
-- !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR0|out\(12))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & ((\CPU|DP|REGFILE|regR3|out\(12)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|regR2|out\(12) & ( !\CPU|DP|REGFILE|mux_out|m|Selector9~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & (\CPU|DP|REGFILE|regR0|out\(12))) # (\CPU|DP|REGFILE|mux_out|m|Selector9~2_combout\ & 
-- ((\CPU|DP|REGFILE|regR3|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100001111010101010000111100110011000000000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR0|ALT_INV_out\(12),
	datab => \CPU|DP|REGFILE|regR1|ALT_INV_out\(12),
	datac => \CPU|DP|REGFILE|regR3|ALT_INV_out\(12),
	datad => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~2_combout\,
	datae => \CPU|DP|REGFILE|regR2|ALT_INV_out\(12),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector3~0_combout\);

-- Location: FF_X78_Y7_N22
\CPU|DP|REGFILE|regR7|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR7|out\(12));

-- Location: FF_X80_Y7_N29
\CPU|DP|REGFILE|regR4|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR4|out\(12));

-- Location: FF_X80_Y7_N8
\CPU|DP|REGFILE|regR5|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U3|b\(12),
	ena => \CPU|DP|REGFILE|writenum_OH[5]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR5|out\(12));

-- Location: FF_X78_Y7_N2
\CPU|DP|REGFILE|regR6|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|U3|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|writenum_OH[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|regR6|out\(12));

-- Location: LABCELL_X80_Y7_N45
\CPU|DP|REGFILE|mux_out|m|Selector3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector3~1_combout\ = ( \CPU|DP|REGFILE|regR6|out\(12) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((!\CPU|struct_decoder|nsel_mux|out\(0) & ((\CPU|DP|REGFILE|regR5|out\(12)))) # 
-- (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR4|out\(12))))) ) ) ) # ( !\CPU|DP|REGFILE|regR6|out\(12) & ( \CPU|struct_decoder|nsel_mux|out\(1) & ( (!\CPU|struct_decoder|nsel_mux|out\(2) & ((!\CPU|struct_decoder|nsel_mux|out\(0) & 
-- ((\CPU|DP|REGFILE|regR5|out\(12)))) # (\CPU|struct_decoder|nsel_mux|out\(0) & (\CPU|DP|REGFILE|regR4|out\(12))))) ) ) ) # ( \CPU|DP|REGFILE|regR6|out\(12) & ( !\CPU|struct_decoder|nsel_mux|out\(1) & ( (\CPU|struct_decoder|nsel_mux|out\(0) & 
-- !\CPU|struct_decoder|nsel_mux|out\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000000000110101000000000011010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|regR4|ALT_INV_out\(12),
	datab => \CPU|DP|REGFILE|regR5|ALT_INV_out\(12),
	datac => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(0),
	datad => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(2),
	datae => \CPU|DP|REGFILE|regR6|ALT_INV_out\(12),
	dataf => \CPU|struct_decoder|nsel_mux|ALT_INV_out\(1),
	combout => \CPU|DP|REGFILE|mux_out|m|Selector3~1_combout\);

-- Location: MLABCELL_X84_Y7_N3
\CPU|DP|REGFILE|mux_out|m|Selector3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|mux_out|m|Selector3~2_combout\ = ( \CPU|DP|REGFILE|mux_out|m|Selector3~1_combout\ & ( (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\) # (\CPU|DP|REGFILE|regR7|out\(12)) ) ) # ( !\CPU|DP|REGFILE|mux_out|m|Selector3~1_combout\ & ( 
-- (!\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (!\CPU|DP|REGFILE|mux_out|m|Selector9~0_combout\ & (\CPU|DP|REGFILE|mux_out|m|Selector3~0_combout\))) # (\CPU|DP|REGFILE|mux_out|d|ShiftLeft0~0_combout\ & (((\CPU|DP|REGFILE|regR7|out\(12))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000111011000010000011101111001100111111111100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector9~0_combout\,
	datab => \CPU|DP|REGFILE|mux_out|d|ALT_INV_ShiftLeft0~0_combout\,
	datac => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector3~0_combout\,
	datad => \CPU|DP|REGFILE|regR7|ALT_INV_out\(12),
	dataf => \CPU|DP|REGFILE|mux_out|m|ALT_INV_Selector3~1_combout\,
	combout => \CPU|DP|REGFILE|mux_out|m|Selector3~2_combout\);

-- Location: FF_X84_Y7_N5
\CPU|DP|rA|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|mux_out|m|Selector3~2_combout\,
	ena => \CPU|FSM|WideOr16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rA|out\(12));

-- Location: MLABCELL_X84_Y8_N18
\CPU|DP|U2|main|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux3~0_combout\ = ( \CPU|DP|Bin[12]~5_combout\ & ( \CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|struct_reg|out\(12) & (!\CPU|struct_reg|out\(11) $ (((\CPU|DP|rA|out\(12) & !\CPU|DP|Ain[4]~1_combout\))))) # (\CPU|struct_reg|out\(12) & 
-- (\CPU|DP|rA|out\(12) & (!\CPU|DP|Ain[4]~1_combout\ & !\CPU|struct_reg|out\(11)))) ) ) ) # ( !\CPU|DP|Bin[12]~5_combout\ & ( \CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|rA|out\(12)) # ((\CPU|DP|Ain[4]~1_combout\) # 
-- (\CPU|struct_reg|out\(12))))) ) ) ) # ( \CPU|DP|Bin[12]~5_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( (!\CPU|struct_reg|out\(11) & (\CPU|DP|rA|out\(12) & ((!\CPU|DP|Ain[4]~1_combout\)))) # (\CPU|struct_reg|out\(11) & (!\CPU|struct_reg|out\(12) & 
-- ((!\CPU|DP|rA|out\(12)) # (\CPU|DP|Ain[4]~1_combout\)))) ) ) ) # ( !\CPU|DP|Bin[12]~5_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c\(12) & ( !\CPU|struct_reg|out\(11) $ ((((\CPU|DP|rA|out\(12) & !\CPU|DP|Ain[4]~1_combout\)) # (\CPU|struct_reg|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110001110011010100001000110001000000101111111001110001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rA|ALT_INV_out\(12),
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Ain[4]~1_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Bin[12]~5_combout\,
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c\(12),
	combout => \CPU|DP|U2|main|Mux3~0_combout\);

-- Location: MLABCELL_X84_Y9_N6
\CPU|DP|U2|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|Equal0~0_combout\ = ( !\CPU|DP|U2|main|Mux12~1_combout\ & ( !\CPU|DP|U2|main|Mux10~1_combout\ & ( (!\CPU|DP|U2|main|Mux15~0_combout\ & (!\CPU|DP|U2|main|Mux14~0_combout\ & (!\CPU|DP|U2|main|Mux13~0_combout\ & 
-- !\CPU|DP|U2|main|Mux11~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|main|ALT_INV_Mux15~0_combout\,
	datab => \CPU|DP|U2|main|ALT_INV_Mux14~0_combout\,
	datac => \CPU|DP|U2|main|ALT_INV_Mux13~0_combout\,
	datad => \CPU|DP|U2|main|ALT_INV_Mux11~0_combout\,
	datae => \CPU|DP|U2|main|ALT_INV_Mux12~1_combout\,
	dataf => \CPU|DP|U2|main|ALT_INV_Mux10~1_combout\,
	combout => \CPU|DP|U2|Equal0~0_combout\);

-- Location: MLABCELL_X84_Y9_N36
\CPU|DP|U2|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|Equal0~1_combout\ = ( !\CPU|DP|U2|main|Mux9~0_combout\ & ( (\CPU|DP|U2|Equal0~0_combout\ & (!\CPU|DP|U2|main|Mux8~0_combout\ & !\CPU|DP|U2|main|Mux7~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|U2|ALT_INV_Equal0~0_combout\,
	datac => \CPU|DP|U2|main|ALT_INV_Mux8~0_combout\,
	datad => \CPU|DP|U2|main|ALT_INV_Mux7~1_combout\,
	dataf => \CPU|DP|U2|main|ALT_INV_Mux9~0_combout\,
	combout => \CPU|DP|U2|Equal0~1_combout\);

-- Location: MLABCELL_X84_Y9_N42
\CPU|DP|U2|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|Equal0~2_combout\ = ( !\CPU|DP|U2|main|Mux4~0_combout\ & ( (!\CPU|DP|U2|main|Mux6~0_combout\ & (!\CPU|DP|U2|main|Mux5~1_combout\ & (\CPU|DP|U2|Equal0~1_combout\ & !\CPU|DP|U2|main|Mux2~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000000000010000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|main|ALT_INV_Mux6~0_combout\,
	datab => \CPU|DP|U2|main|ALT_INV_Mux5~1_combout\,
	datac => \CPU|DP|U2|ALT_INV_Equal0~1_combout\,
	datad => \CPU|DP|U2|main|ALT_INV_Mux2~1_combout\,
	dataf => \CPU|DP|U2|main|ALT_INV_Mux4~0_combout\,
	combout => \CPU|DP|U2|Equal0~2_combout\);

-- Location: MLABCELL_X84_Y9_N39
\CPU|DP|U2|Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|Equal0~3_combout\ = ( \CPU|DP|U2|Equal0~2_combout\ & ( (!\CPU|DP|U2|main|Mux3~0_combout\ & (!\CPU|DP|U2|main|Mux1~0_combout\ & !\CPU|DP|U2|main|Mux0~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010100000000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|main|ALT_INV_Mux3~0_combout\,
	datac => \CPU|DP|U2|main|ALT_INV_Mux1~0_combout\,
	datad => \CPU|DP|U2|main|ALT_INV_Mux0~1_combout\,
	dataf => \CPU|DP|U2|ALT_INV_Equal0~2_combout\,
	combout => \CPU|DP|U2|Equal0~3_combout\);

-- Location: FF_X84_Y9_N41
\CPU|DP|status|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|Equal0~3_combout\,
	ena => \CPU|FSM|loads~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|out\(0));

-- Location: MLABCELL_X82_Y8_N30
\CPU|FSM|WideNor37~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor37~0_combout\ = ( \CPU|FSM|WideNor33~0_combout\ & ( (\CPU|struct_reg|out[8]~DUPLICATE_q\ & (!\CPU|struct_reg|out[10]~DUPLICATE_q\ & \CPU|struct_reg|out\(9))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010100000000000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	datac => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out\(9),
	dataf => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	combout => \CPU|FSM|WideNor37~0_combout\);

-- Location: FF_X83_Y8_N46
\CPU|DP|status|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|ALU_AddSub|ovf~1_combout\,
	sclr => \CPU|struct_reg|out\(12),
	ena => \CPU|FSM|loads~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|out\(2));

-- Location: FF_X81_Y7_N32
\CPU|struct_reg|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector5~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(10));

-- Location: LABCELL_X81_Y7_N12
\CPU|FSM|WideNor39~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor39~0_combout\ = ( \CPU|FSM|WideNor33~0_combout\ & ( (\CPU|struct_reg|out\(10) & \CPU|FSM|WideNor33~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_reg|ALT_INV_out\(10),
	datac => \CPU|FSM|ALT_INV_WideNor33~1_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	combout => \CPU|FSM|WideNor39~0_combout\);

-- Location: LABCELL_X81_Y8_N24
\CPU|FSM|always0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~8_combout\ = ( \CPU|FSM|always0~7_combout\ & ( (!\CPU|FSM|WideNor33~combout\ & (!\CPU|FSM|WideOr1~0_combout\ & !\CPU|FSM|WideNor34~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_WideNor33~combout\,
	datac => \CPU|FSM|ALT_INV_WideOr1~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor34~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~7_combout\,
	combout => \CPU|FSM|always0~8_combout\);

-- Location: LABCELL_X81_Y8_N48
\CPU|FSM|always0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~9_combout\ = ( \CPU|FSM|WideNor39~0_combout\ & ( \CPU|FSM|always0~8_combout\ & ( (!\CPU|DP|status|out\(0) & (!\CPU|DP|status|out[1]~DUPLICATE_q\ $ (\CPU|DP|status|out\(2)))) ) ) ) # ( !\CPU|FSM|WideNor39~0_combout\ & ( 
-- \CPU|FSM|always0~8_combout\ & ( (!\CPU|FSM|WideNor37~0_combout\) # (!\CPU|DP|status|out[1]~DUPLICATE_q\ $ (\CPU|DP|status|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111100111100111000100000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|status|ALT_INV_out\(0),
	datab => \CPU|DP|status|ALT_INV_out[1]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideNor37~0_combout\,
	datad => \CPU|DP|status|ALT_INV_out\(2),
	datae => \CPU|FSM|ALT_INV_WideNor39~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~8_combout\,
	combout => \CPU|FSM|always0~9_combout\);

-- Location: LABCELL_X83_Y8_N12
\CPU|FSM|WideOr18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr18~0_combout\ = ( \CPU|FSM|nsel[2]~0_combout\ & ( (!\CPU|FSM|WideOr29~0_combout\) # ((!\CPU|FSM|WideNor42~combout\ & (\CPU|FSM|WideNor43~combout\ & \CPU|FSM|always0~9_combout\))) ) ) # ( !\CPU|FSM|nsel[2]~0_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111000000101111111100000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor42~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datac => \CPU|FSM|ALT_INV_always0~9_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	dataf => \CPU|FSM|ALT_INV_nsel[2]~0_combout\,
	combout => \CPU|FSM|WideOr18~0_combout\);

-- Location: LABCELL_X85_Y7_N12
\CPU|FSM|WideOr7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr7~0_combout\ = ( !\CPU|FSM|psel[2]~1_combout\ & ( (!\CPU|FSM|WideNor2~combout\ & \CPU|FSM|WideOr20~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|FSM|ALT_INV_WideOr20~0_combout\,
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|FSM|WideOr7~0_combout\);

-- Location: MLABCELL_X82_Y6_N0
\CPU|FSM|state_next_reset[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|state_next_reset[1]~1_combout\ = ( \CPU|FSM|WideOr35~0_combout\ & ( \CPU|FSM|WideOr7~0_combout\ & ( (!\CPU|FSM|WideOr3~0_combout\) # ((!\KEY[1]~input_o\) # ((!\CPU|FSM|WideOr3~1_combout\) # (\CPU|FSM|WideOr18~0_combout\))) ) ) ) # ( 
-- !\CPU|FSM|WideOr35~0_combout\ & ( \CPU|FSM|WideOr7~0_combout\ ) ) # ( \CPU|FSM|WideOr35~0_combout\ & ( !\CPU|FSM|WideOr7~0_combout\ ) ) # ( !\CPU|FSM|WideOr35~0_combout\ & ( !\CPU|FSM|WideOr7~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	datab => \ALT_INV_KEY[1]~input_o\,
	datac => \CPU|FSM|ALT_INV_WideOr3~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr18~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr7~0_combout\,
	combout => \CPU|FSM|state_next_reset[1]~1_combout\);

-- Location: FF_X82_Y6_N2
\CPU|FSM|STATE|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|state_next_reset[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|STATE|out\(1));

-- Location: MLABCELL_X82_Y8_N0
\CPU|FSM|WideNor27\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor27~combout\ = ( \CPU|FSM|WideNor27~0_combout\ & ( (\CPU|FSM|STATE|out\(0) & (\CPU|FSM|STATE|out\(1) & (\CPU|struct_decoder|Equal0~0_combout\ & \CPU|FSM|WideNor31~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(0),
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|struct_decoder|ALT_INV_Equal0~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor31~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	combout => \CPU|FSM|WideNor27~combout\);

-- Location: LABCELL_X83_Y8_N57
\CPU|FSM|m_cmd[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|m_cmd\(1) = ( \CPU|FSM|always0~5_combout\ & ( (\CPU|FSM|WideNor27~combout\ & !\CPU|FSM|WideNor26~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor27~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor26~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|m_cmd\(1));

-- Location: LABCELL_X77_Y7_N27
write : cyclonev_lcell_comb
-- Equation(s):
-- \write~combout\ = ( \CPU|FSM|always0~12_combout\ & ( \CPU|FSM|WideOr41~1_combout\ & ( (\CPU|FSM|m_cmd\(1) & !\CPU|program_counter|out[8]~DUPLICATE_q\) ) ) ) # ( !\CPU|FSM|always0~12_combout\ & ( \CPU|FSM|WideOr41~1_combout\ & ( (\CPU|FSM|m_cmd\(1) & 
-- ((!\CPU|FSM|WideNor2~combout\ & (!\CPU|Data_Address|out\(8))) # (\CPU|FSM|WideNor2~combout\ & ((!\CPU|program_counter|out[8]~DUPLICATE_q\))))) ) ) ) # ( \CPU|FSM|always0~12_combout\ & ( !\CPU|FSM|WideOr41~1_combout\ & ( (\CPU|FSM|m_cmd\(1) & 
-- !\CPU|program_counter|out[8]~DUPLICATE_q\) ) ) ) # ( !\CPU|FSM|always0~12_combout\ & ( !\CPU|FSM|WideOr41~1_combout\ & ( (\CPU|FSM|m_cmd\(1) & !\CPU|program_counter|out[8]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000010101010000000001010001010000000101010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_m_cmd\(1),
	datab => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datac => \CPU|Data_Address|ALT_INV_out\(8),
	datad => \CPU|program_counter|ALT_INV_out[8]~DUPLICATE_q\,
	datae => \CPU|FSM|ALT_INV_always0~12_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	combout => \write~combout\);

-- Location: LABCELL_X83_Y6_N30
\Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = ( \Equal5~0_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( (!\LED_load~1_combout\) # ((!\LED_load~0_combout\) # ((!\LED_load~2_combout\) # (!\LED_load~3_combout\))) ) ) ) # ( !\Equal5~0_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a13\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~1_combout\,
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~2_combout\,
	datad => \ALT_INV_LED_load~3_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\,
	combout => \Selector2~0_combout\);

-- Location: FF_X83_Y6_N31
\CPU|struct_reg|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector2~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(13));

-- Location: MLABCELL_X82_Y5_N24
\CPU|FSM|WideNor48~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor48~0_combout\ = ( \CPU|FSM|STATE|out\(1) & ( !\CPU|FSM|STATE|out\(0) & ( (!\CPU|struct_reg|out[14]~DUPLICATE_q\ & (!\CPU|FSM|STATE|out\(2) & \CPU|struct_reg|out\(13))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|FSM|STATE|ALT_INV_out\(2),
	datad => \CPU|struct_reg|ALT_INV_out\(13),
	datae => \CPU|FSM|STATE|ALT_INV_out\(1),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(0),
	combout => \CPU|FSM|WideNor48~0_combout\);

-- Location: MLABCELL_X82_Y5_N48
\CPU|FSM|WideNor48\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor48~combout\ = ( !\CPU|struct_reg|out\(15) & ( !\CPU|FSM|STATE|out\(3) & ( (\CPU|FSM|WideNor48~0_combout\ & \CPU|FSM|STATE|out\(4)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor48~0_combout\,
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datae => \CPU|struct_reg|ALT_INV_out\(15),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(3),
	combout => \CPU|FSM|WideNor48~combout\);

-- Location: LABCELL_X81_Y8_N54
\CPU|FSM|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr3~0_combout\ = ( \CPU|FSM|WideNor47~combout\ & ( \CPU|FSM|always0~10_combout\ & ( !\CPU|FSM|always0~1_combout\ ) ) ) # ( !\CPU|FSM|WideNor47~combout\ & ( \CPU|FSM|always0~10_combout\ & ( (!\CPU|FSM|always0~1_combout\ & 
-- (((!\CPU|FSM|always0~11_combout\) # (!\CPU|FSM|STATE|out\(0))) # (\CPU|FSM|WideNor48~combout\))) ) ) ) # ( \CPU|FSM|WideNor47~combout\ & ( !\CPU|FSM|always0~10_combout\ & ( !\CPU|FSM|always0~1_combout\ ) ) ) # ( !\CPU|FSM|WideNor47~combout\ & ( 
-- !\CPU|FSM|always0~10_combout\ & ( !\CPU|FSM|always0~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111101000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor48~combout\,
	datab => \CPU|FSM|ALT_INV_always0~11_combout\,
	datac => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|ALT_INV_always0~1_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor47~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~10_combout\,
	combout => \CPU|FSM|WideOr3~0_combout\);

-- Location: LABCELL_X85_Y7_N39
\CPU|FSM|WideOr39~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr39~0_combout\ = ( !\CPU|FSM|WideOr3~2_combout\ & ( !\CPU|FSM|m_cmd\(1) & ( (!\CPU|FSM|STATE|out\(0)) # ((!\CPU|FSM|WideNor1~0_combout\) # (!\CPU|FSM|STATE|out\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111100000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|FSM|ALT_INV_WideNor1~0_combout\,
	datad => \CPU|FSM|STATE|ALT_INV_out\(1),
	datae => \CPU|FSM|ALT_INV_WideOr3~2_combout\,
	dataf => \CPU|FSM|ALT_INV_m_cmd\(1),
	combout => \CPU|FSM|WideOr39~0_combout\);

-- Location: MLABCELL_X82_Y6_N42
\CPU|FSM|state_next_reset[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|state_next_reset[2]~2_combout\ = ( \CPU|FSM|WideOr18~0_combout\ ) # ( !\CPU|FSM|WideOr18~0_combout\ & ( (!\CPU|FSM|WideOr3~0_combout\) # ((!\KEY[1]~input_o\) # ((!\CPU|FSM|WideOr39~0_combout\) # (!\CPU|FSM|WideOr9~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111110111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	datab => \ALT_INV_KEY[1]~input_o\,
	datac => \CPU|FSM|ALT_INV_WideOr39~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr18~0_combout\,
	combout => \CPU|FSM|state_next_reset[2]~2_combout\);

-- Location: FF_X82_Y6_N44
\CPU|FSM|STATE|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|state_next_reset[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|STATE|out\(2));

-- Location: LABCELL_X83_Y6_N18
\CPU|FSM|always0~28\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~28_combout\ = ( \CPU|struct_reg|out\(12) & ( (\CPU|struct_reg|out[15]~DUPLICATE_q\ & ((!\CPU|struct_reg|out[14]~DUPLICATE_q\ & (\CPU|struct_reg|out[13]~DUPLICATE_q\)) # (\CPU|struct_reg|out[14]~DUPLICATE_q\ & 
-- (!\CPU|struct_reg|out[13]~DUPLICATE_q\ & !\CPU|struct_reg|out\(11))))) ) ) # ( !\CPU|struct_reg|out\(12) & ( (!\CPU|struct_reg|out[14]~DUPLICATE_q\ & (\CPU|struct_reg|out[13]~DUPLICATE_q\ & \CPU|struct_reg|out[15]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001000000110000000100000011000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	datac => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|struct_reg|ALT_INV_out\(12),
	combout => \CPU|FSM|always0~28_combout\);

-- Location: MLABCELL_X82_Y6_N18
\CPU|FSM|always0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~0_combout\ = ( \CPU|FSM|STATE|out\(1) & ( \CPU|FSM|always0~28_combout\ & ( (!\CPU|FSM|STATE|out\(2) & (((!\CPU|FSM|STATE|out\(4) & !\CPU|FSM|STATE|out\(3))))) # (\CPU|FSM|STATE|out\(2) & (\CPU|FSM|STATE|out\(0) & (\CPU|FSM|STATE|out\(4) & 
-- \CPU|FSM|STATE|out\(3)))) ) ) ) # ( !\CPU|FSM|STATE|out\(1) & ( \CPU|FSM|always0~28_combout\ & ( (!\CPU|FSM|STATE|out\(4) & (!\CPU|FSM|STATE|out\(3) & ((!\CPU|FSM|STATE|out\(0)) # (!\CPU|FSM|STATE|out\(2))))) ) ) ) # ( \CPU|FSM|STATE|out\(1) & ( 
-- !\CPU|FSM|always0~28_combout\ & ( (!\CPU|FSM|STATE|out\(2) & (((!\CPU|FSM|STATE|out\(4) & !\CPU|FSM|STATE|out\(3))))) # (\CPU|FSM|STATE|out\(2) & (\CPU|FSM|STATE|out\(0) & (\CPU|FSM|STATE|out\(4) & \CPU|FSM|STATE|out\(3)))) ) ) ) # ( 
-- !\CPU|FSM|STATE|out\(1) & ( !\CPU|FSM|always0~28_combout\ & ( (!\CPU|FSM|STATE|out\(2) & (!\CPU|FSM|STATE|out\(4) & !\CPU|FSM|STATE|out\(3))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000111100000000000001100000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(0),
	datab => \CPU|FSM|STATE|ALT_INV_out\(2),
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	datae => \CPU|FSM|STATE|ALT_INV_out\(1),
	dataf => \CPU|FSM|ALT_INV_always0~28_combout\,
	combout => \CPU|FSM|always0~0_combout\);

-- Location: MLABCELL_X82_Y5_N9
\CPU|FSM|always0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~1_combout\ = ( \CPU|struct_reg|out\(13) & ( \CPU|FSM|WideNor7~0_combout\ & ( (\CPU|struct_reg|out\(15) & (!\CPU|FSM|always0~0_combout\ & \CPU|struct_reg|out[14]~DUPLICATE_q\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(15),
	datab => \CPU|FSM|ALT_INV_always0~0_combout\,
	datad => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	datae => \CPU|struct_reg|ALT_INV_out\(13),
	dataf => \CPU|FSM|ALT_INV_WideNor7~0_combout\,
	combout => \CPU|FSM|always0~1_combout\);

-- Location: LABCELL_X85_Y5_N51
\CPU|FSM|always0~24\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~24_combout\ = ( !\CPU|FSM|WideNor48~combout\ & ( \CPU|FSM|always0~23_combout\ & ( (!\CPU|FSM|WideNor46~combout\ & ((!\CPU|FSM|WideNor47~0_combout\) # ((\CPU|struct_reg|out\(11) & !\CPU|struct_reg|out\(12))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011011100000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datab => \CPU|FSM|ALT_INV_WideNor47~0_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|FSM|ALT_INV_WideNor46~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor48~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~23_combout\,
	combout => \CPU|FSM|always0~24_combout\);

-- Location: LABCELL_X85_Y5_N42
\CPU|FSM|WideOr9~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr9~3_combout\ = ( \CPU|FSM|WideOr9~1_combout\ & ( \CPU|FSM|always0~24_combout\ & ( (!\CPU|FSM|always0~1_combout\ & (\CPU|FSM|WideOr9~0_combout\ & ((!\CPU|FSM|STATE|out\(0)) # (!\CPU|FSM|always0~11_combout\)))) ) ) ) # ( 
-- \CPU|FSM|WideOr9~1_combout\ & ( !\CPU|FSM|always0~24_combout\ & ( (!\CPU|FSM|always0~1_combout\ & \CPU|FSM|WideOr9~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001010101000000000000000000000000010101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_always0~1_combout\,
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datac => \CPU|FSM|ALT_INV_always0~11_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr9~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~24_combout\,
	combout => \CPU|FSM|WideOr9~3_combout\);

-- Location: FF_X87_Y8_N20
\CPU|program_counter|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(8),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out\(8));

-- Location: MLABCELL_X87_Y8_N54
\CPU|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~21_sumout\ = SUM(( (\CPU|struct_reg|out\(7) & \CPU|FSM|WideOr37~combout\) ) + ( \CPU|program_counter|out\(8) ) + ( \CPU|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000001000100010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(7),
	datab => \CPU|FSM|ALT_INV_WideOr37~combout\,
	dataf => \CPU|program_counter|ALT_INV_out\(8),
	cin => \CPU|Add0~10\,
	sumout => \CPU|Add0~21_sumout\);

-- Location: LABCELL_X88_Y8_N18
\CPU|nextPCmux|out~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~5_combout\ = ( \CPU|FSM|psel[2]~1_combout\ & ( \CPU|DP|rC|out\(8) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|rC|ALT_INV_out\(8),
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|nextPCmux|out~5_combout\);

-- Location: MLABCELL_X87_Y8_N18
\CPU|nextPCmux|out[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(8) = ( \CPU|Add0~21_sumout\ & ( \CPU|nextPCmux|out~5_combout\ ) ) # ( !\CPU|Add0~21_sumout\ & ( \CPU|nextPCmux|out~5_combout\ ) ) # ( \CPU|Add0~21_sumout\ & ( !\CPU|nextPCmux|out~5_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr9~3_combout\) # (!\CPU|FSM|WideOr41~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datae => \CPU|ALT_INV_Add0~21_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~5_combout\,
	combout => \CPU|nextPCmux|out\(8));

-- Location: FF_X87_Y8_N19
\CPU|program_counter|out[8]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(8),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out[8]~DUPLICATE_q\);

-- Location: LABCELL_X80_Y8_N48
\LED_load~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED_load~2_combout\ = ( \CPU|program_counter|out\(0) & ( \CPU|FSM|WideOr41~2_combout\ & ( (\CPU|Data_Address|out\(8) & (!\CPU|FSM|WideNor2~combout\ & !\CPU|Data_Address|out\(0))) ) ) ) # ( !\CPU|program_counter|out\(0) & ( \CPU|FSM|WideOr41~2_combout\ & 
-- ( (!\CPU|FSM|WideNor2~combout\ & (((\CPU|Data_Address|out\(8) & !\CPU|Data_Address|out\(0))))) # (\CPU|FSM|WideNor2~combout\ & (\CPU|program_counter|out[8]~DUPLICATE_q\)) ) ) ) # ( !\CPU|program_counter|out\(0) & ( !\CPU|FSM|WideOr41~2_combout\ & ( 
-- \CPU|program_counter|out[8]~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101000000000000000000110101000001010011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out[8]~DUPLICATE_q\,
	datab => \CPU|Data_Address|ALT_INV_out\(8),
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|Data_Address|ALT_INV_out\(0),
	datae => \CPU|program_counter|ALT_INV_out\(0),
	dataf => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	combout => \LED_load~2_combout\);

-- Location: LABCELL_X83_Y6_N3
\Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = ( \Equal5~0_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( (!\LED_load~1_combout\) # ((!\LED_load~3_combout\) # ((!\LED_load~0_combout\) # (!\LED_load~2_combout\))) ) ) ) # ( !\Equal5~0_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a14\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~1_combout\,
	datab => \ALT_INV_LED_load~3_combout\,
	datac => \ALT_INV_LED_load~0_combout\,
	datad => \ALT_INV_LED_load~2_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\,
	combout => \Selector1~0_combout\);

-- Location: FF_X83_Y6_N4
\CPU|struct_reg|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector1~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(14));

-- Location: MLABCELL_X82_Y6_N36
\CPU|FSM|always0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~2_combout\ = ( \CPU|FSM|WideNor6~1_combout\ & ( \CPU|FSM|WideNor7~0_combout\ & ( (\CPU|struct_reg|out[15]~DUPLICATE_q\ & (!\CPU|struct_reg|out\(14) & (!\CPU|FSM|always0~0_combout\ & \CPU|struct_reg|out\(13)))) ) ) ) # ( 
-- !\CPU|FSM|WideNor6~1_combout\ & ( \CPU|FSM|WideNor7~0_combout\ & ( (!\CPU|FSM|always0~0_combout\ & ((!\CPU|struct_reg|out\(13) & (!\CPU|struct_reg|out[15]~DUPLICATE_q\)) # (\CPU|struct_reg|out\(13) & ((!\CPU|struct_reg|out\(14)))))) ) ) ) # ( 
-- \CPU|FSM|WideNor6~1_combout\ & ( !\CPU|FSM|WideNor7~0_combout\ & ( (\CPU|struct_reg|out[15]~DUPLICATE_q\ & !\CPU|FSM|always0~0_combout\) ) ) ) # ( !\CPU|FSM|WideNor6~1_combout\ & ( !\CPU|FSM|WideNor7~0_combout\ & ( !\CPU|FSM|always0~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000010100000101000010100000110000000000000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	datab => \CPU|struct_reg|ALT_INV_out\(14),
	datac => \CPU|FSM|ALT_INV_always0~0_combout\,
	datad => \CPU|struct_reg|ALT_INV_out\(13),
	datae => \CPU|FSM|ALT_INV_WideNor6~1_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor7~0_combout\,
	combout => \CPU|FSM|always0~2_combout\);

-- Location: LABCELL_X83_Y7_N48
\CPU|DP|U2|ALU_AddSub|ai|c[1]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c[1]~3_combout\ = ( \CPU|FSM|WideNor19~combout\ & ( (\CPU|FSM|WideNor16~0_combout\ & !\CPU|FSM|WideNor17~combout\) ) ) # ( !\CPU|FSM|WideNor19~combout\ & ( (!\CPU|FSM|WideNor17~combout\ & (((!\CPU|FSM|WideNor20~combout\ & 
-- !\CPU|FSM|WideNor21~combout\)) # (\CPU|FSM|WideNor16~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101000001010000110100000101000001010000010100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor16~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor20~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor17~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor21~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor19~combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c[1]~3_combout\);

-- Location: MLABCELL_X84_Y6_N6
\CPU|DP|U2|ALU_AddSub|ai|c[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ = ( !\CPU|FSM|WideNor15~0_combout\ & ( !\CPU|DP|U2|ALU_AddSub|ai|c[1]~3_combout\ & ( (\CPU|FSM|always0~2_combout\ & (!\CPU|FSM|WideNor16~combout\ & (!\CPU|FSM|WideNor12~combout\ & !\CPU|FSM|WideNor13~combout\))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_always0~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor16~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor12~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datae => \CPU|FSM|ALT_INV_WideNor15~0_combout\,
	dataf => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~3_combout\,
	combout => \CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\);

-- Location: LABCELL_X83_Y9_N27
\CPU|DP|U2|ALU_AddSub|ai|c[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\ = ( \CPU|DP|Bin[0]~17_combout\ & ( \CPU|DP|rA|out\(0) & ( !\CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ ) ) ) # ( !\CPU|DP|Bin[0]~17_combout\ & ( \CPU|DP|rA|out\(0) & ( \CPU|struct_reg|out\(11) ) ) ) # ( 
-- !\CPU|DP|Bin[0]~17_combout\ & ( !\CPU|DP|rA|out\(0) & ( \CPU|struct_reg|out\(11) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000000000000000001111000011111010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~1_combout\,
	datac => \CPU|struct_reg|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Bin[0]~17_combout\,
	dataf => \CPU|DP|rA|ALT_INV_out\(0),
	combout => \CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\);

-- Location: LABCELL_X83_Y9_N3
\CPU|DP|U2|main|Mux14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux14~0_combout\ = ( \CPU|DP|Ain[1]~3_combout\ & ( \CPU|DP|Bin[1]~16_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|struct_reg|out\(12) & !\CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\))) ) ) ) # ( !\CPU|DP|Ain[1]~3_combout\ & ( 
-- \CPU|DP|Bin[1]~16_combout\ & ( (!\CPU|struct_reg|out\(12) & (!\CPU|struct_reg|out\(11) $ (\CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\))) ) ) ) # ( \CPU|DP|Ain[1]~3_combout\ & ( !\CPU|DP|Bin[1]~16_combout\ & ( !\CPU|struct_reg|out\(11) $ 
-- (((\CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\) # (\CPU|struct_reg|out\(12)))) ) ) ) # ( !\CPU|DP|Ain[1]~3_combout\ & ( !\CPU|DP|Bin[1]~16_combout\ & ( !\CPU|struct_reg|out\(11) $ (((!\CPU|DP|U2|ALU_AddSub|ai|c[1]~2_combout\) # (\CPU|struct_reg|out\(12)))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111000011110000110011001111000000001100000011110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|struct_reg|ALT_INV_out\(11),
	datac => \CPU|struct_reg|ALT_INV_out\(12),
	datad => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~2_combout\,
	datae => \CPU|DP|ALT_INV_Ain[1]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~16_combout\,
	combout => \CPU|DP|U2|main|Mux14~0_combout\);

-- Location: FF_X83_Y9_N5
\CPU|DP|rC|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux14~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(1));

-- Location: FF_X80_Y7_N4
\CPU|Data_Address|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(1),
	sload => VCC,
	ena => \CPU|FSM|WideOr3~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|Data_Address|out\(1));

-- Location: LABCELL_X79_Y7_N24
\LED_load~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED_load~3_combout\ = ( \CPU|Data_Address|out\(2) & ( \CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|program_counter|out[2]~DUPLICATE_q\ & (!\CPU|program_counter|out[1]~DUPLICATE_q\ & \CPU|FSM|WideNor2~combout\)) ) ) ) # ( !\CPU|Data_Address|out\(2) & ( 
-- \CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|FSM|WideNor2~combout\ & (!\CPU|Data_Address|out\(1))) # (\CPU|FSM|WideNor2~combout\ & (((!\CPU|program_counter|out[2]~DUPLICATE_q\ & !\CPU|program_counter|out[1]~DUPLICATE_q\)))) ) ) ) # ( \CPU|Data_Address|out\(2) 
-- & ( !\CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|program_counter|out[2]~DUPLICATE_q\ & !\CPU|program_counter|out[1]~DUPLICATE_q\) ) ) ) # ( !\CPU|Data_Address|out\(2) & ( !\CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|program_counter|out[2]~DUPLICATE_q\ & 
-- !\CPU|program_counter|out[1]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110000001100000010101010110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|Data_Address|ALT_INV_out\(1),
	datab => \CPU|program_counter|ALT_INV_out[2]~DUPLICATE_q\,
	datac => \CPU|program_counter|ALT_INV_out[1]~DUPLICATE_q\,
	datad => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datae => \CPU|Data_Address|ALT_INV_out\(2),
	dataf => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	combout => \LED_load~3_combout\);

-- Location: LABCELL_X83_Y6_N33
\Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = ( \Equal5~0_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( (!\LED_load~1_combout\) # ((!\LED_load~0_combout\) # ((!\LED_load~3_combout\) # (!\LED_load~2_combout\))) ) ) ) # ( !\Equal5~0_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a12\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~1_combout\,
	datab => \ALT_INV_LED_load~0_combout\,
	datac => \ALT_INV_LED_load~3_combout\,
	datad => \ALT_INV_LED_load~2_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\,
	combout => \Selector3~0_combout\);

-- Location: FF_X83_Y6_N35
\CPU|struct_reg|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector3~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(12));

-- Location: LABCELL_X83_Y6_N12
\CPU|FSM|WideNor27~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor27~0_combout\ = ( !\CPU|struct_reg|out\(12) & ( !\CPU|struct_reg|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|struct_reg|ALT_INV_out\(11),
	dataf => \CPU|struct_reg|ALT_INV_out\(12),
	combout => \CPU|FSM|WideNor27~0_combout\);

-- Location: LABCELL_X83_Y6_N27
\CPU|FSM|WideNor33~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor33~0_combout\ = ( \CPU|FSM|WideNor5~0_combout\ & ( (\CPU|FSM|WideNor27~0_combout\ & \CPU|FSM|WideNor42~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor27~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor42~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor5~0_combout\,
	combout => \CPU|FSM|WideNor33~0_combout\);

-- Location: LABCELL_X80_Y8_N0
\CPU|FSM|WideNor41\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor41~combout\ = ( \CPU|DP|status|out\(2) & ( (\CPU|FSM|WideNor33~0_combout\ & (\CPU|struct_reg|out[10]~DUPLICATE_q\ & (\CPU|FSM|WideNor33~1_combout\ & !\CPU|DP|status|out\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000001000000000000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	datab => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideNor33~1_combout\,
	datad => \CPU|DP|status|ALT_INV_out\(1),
	dataf => \CPU|DP|status|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor41~combout\);

-- Location: LABCELL_X83_Y7_N30
\CPU|FSM|WideOr41~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~4_combout\ = ( \CPU|FSM|WideNor47~0_combout\ & ( \CPU|FSM|WideNor46~combout\ & ( !\CPU|struct_reg|out[11]~DUPLICATE_q\ ) ) ) # ( \CPU|FSM|WideNor47~0_combout\ & ( !\CPU|FSM|WideNor46~combout\ & ( (!\CPU|struct_reg|out[11]~DUPLICATE_q\) # 
-- (((\CPU|FSM|always0~11_combout\ & \CPU|FSM|WideNor19~0_combout\)) # (\CPU|struct_reg|out\(12))) ) ) ) # ( !\CPU|FSM|WideNor47~0_combout\ & ( !\CPU|FSM|WideNor46~combout\ & ( (\CPU|FSM|always0~11_combout\ & \CPU|FSM|WideNor19~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111101110111011111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out[11]~DUPLICATE_q\,
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|FSM|ALT_INV_always0~11_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor19~0_combout\,
	datae => \CPU|FSM|ALT_INV_WideNor47~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor46~combout\,
	combout => \CPU|FSM|WideOr41~4_combout\);

-- Location: LABCELL_X83_Y7_N57
\CPU|FSM|WideOr41~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~5_combout\ = ( !\CPU|FSM|WideOr41~4_combout\ & ( ((!\CPU|FSM|always0~13_combout\ & !\CPU|FSM|WideNor48~combout\)) # (\CPU|FSM|WideNor46~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000011111111101000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_always0~13_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor48~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor46~combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr41~4_combout\,
	combout => \CPU|FSM|WideOr41~5_combout\);

-- Location: LABCELL_X81_Y8_N9
\CPU|FSM|WideOr41~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~6_combout\ = ( \CPU|FSM|WideNor42~combout\ & ( (!\CPU|FSM|WideNor40~combout\ & !\CPU|FSM|WideNor39~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor40~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor39~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor42~combout\,
	combout => \CPU|FSM|WideOr41~6_combout\);

-- Location: LABCELL_X80_Y8_N18
\CPU|FSM|WideOr41~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr41~2_combout\ = ( \CPU|FSM|always0~22_combout\ & ( \CPU|FSM|always0~23_combout\ & ( (\CPU|FSM|WideOr41~5_combout\ & (!\CPU|FSM|WideOr41~0_combout\ & ((!\CPU|FSM|WideOr41~6_combout\) # (\CPU|FSM|WideNor41~combout\)))) ) ) ) # ( 
-- !\CPU|FSM|always0~22_combout\ & ( \CPU|FSM|always0~23_combout\ & ( (\CPU|FSM|WideOr41~5_combout\ & !\CPU|FSM|WideOr41~0_combout\) ) ) ) # ( \CPU|FSM|always0~22_combout\ & ( !\CPU|FSM|always0~23_combout\ & ( (!\CPU|FSM|WideOr41~0_combout\ & 
-- ((!\CPU|FSM|WideOr41~6_combout\) # (\CPU|FSM|WideNor41~combout\))) ) ) ) # ( !\CPU|FSM|always0~22_combout\ & ( !\CPU|FSM|always0~23_combout\ & ( !\CPU|FSM|WideOr41~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111101010000000000110011000000000011000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor41~combout\,
	datab => \CPU|FSM|ALT_INV_WideOr41~5_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr41~6_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~0_combout\,
	datae => \CPU|FSM|ALT_INV_always0~22_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~23_combout\,
	combout => \CPU|FSM|WideOr41~2_combout\);

-- Location: MLABCELL_X82_Y8_N24
\CPU|FSM|WideNor3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor3~combout\ = ( !\CPU|FSM|STATE|out\(2) & ( (!\CPU|FSM|STATE|out\(0) & (\CPU|FSM|STATE|out\(1) & (!\CPU|FSM|STATE|out\(4) & !\CPU|FSM|STATE|out\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(0),
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|STATE|ALT_INV_out\(4),
	datad => \CPU|FSM|STATE|ALT_INV_out\(3),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor3~combout\);

-- Location: MLABCELL_X82_Y6_N48
\CPU|FSM|state_next_reset[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|state_next_reset[0]~0_combout\ = ( \CPU|FSM|WideOr9~3_combout\ & ( (!\KEY[1]~input_o\) # ((!\CPU|FSM|WideOr41~2_combout\) # (\CPU|FSM|WideNor3~combout\)) ) ) # ( !\CPU|FSM|WideOr9~3_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111010111111111111101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[1]~input_o\,
	datac => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor3~combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	combout => \CPU|FSM|state_next_reset[0]~0_combout\);

-- Location: FF_X82_Y6_N50
\CPU|FSM|STATE|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|state_next_reset[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|STATE|out\(0));

-- Location: LABCELL_X81_Y7_N24
\CPU|FSM|WideNor46\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor46~combout\ = ( \CPU|FSM|WideNor43~0_combout\ & ( (\CPU|FSM|STATE|out\(0) & \CPU|FSM|WideNor12~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|STATE|ALT_INV_out\(0),
	datad => \CPU|FSM|ALT_INV_WideNor12~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor43~0_combout\,
	combout => \CPU|FSM|WideNor46~combout\);

-- Location: LABCELL_X81_Y8_N3
\CPU|FSM|always0~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~20_combout\ = ( !\CPU|FSM|WideNor42~combout\ & ( (\CPU|FSM|WideNor46~combout\ & (!\CPU|FSM|WideNor44~0_combout\ & (!\CPU|FSM|WideNor43~combout\ & \CPU|FSM|always0~9_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001000000000000000100000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor46~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor44~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datad => \CPU|FSM|ALT_INV_always0~9_combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor42~combout\,
	combout => \CPU|FSM|always0~20_combout\);

-- Location: LABCELL_X85_Y7_N57
\CPU|FSM|WideOr1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr1~2_combout\ = ( !\CPU|FSM|psel[2]~1_combout\ & ( !\CPU|FSM|WideOr11~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideOr11~0_combout\,
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|FSM|WideOr1~2_combout\);

-- Location: LABCELL_X83_Y8_N21
\CPU|FSM|WideOr1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr1~1_combout\ = ( !\CPU|FSM|WideNor33~combout\ & ( (\CPU|FSM|always0~7_combout\ & (\CPU|FSM|WideOr1~0_combout\ & !\CPU|FSM|WideNor34~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|ALT_INV_always0~7_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr1~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor34~combout\,
	dataf => \CPU|FSM|ALT_INV_WideNor33~combout\,
	combout => \CPU|FSM|WideOr1~1_combout\);

-- Location: MLABCELL_X82_Y8_N36
\CPU|FSM|always0~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~19_combout\ = ( \CPU|DP|status|out\(2) & ( \CPU|FSM|always0~8_combout\ & ( (!\CPU|DP|status|out[1]~DUPLICATE_q\ & (!\CPU|DP|status|out\(0) & (\CPU|FSM|WideNor39~0_combout\ & !\CPU|FSM|WideNor37~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|status|ALT_INV_out[1]~DUPLICATE_q\,
	datab => \CPU|DP|status|ALT_INV_out\(0),
	datac => \CPU|FSM|ALT_INV_WideNor39~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor37~0_combout\,
	datae => \CPU|DP|status|ALT_INV_out\(2),
	dataf => \CPU|FSM|ALT_INV_always0~8_combout\,
	combout => \CPU|FSM|always0~19_combout\);

-- Location: MLABCELL_X82_Y6_N24
\CPU|FSM|state_next_reset[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|state_next_reset[4]~4_combout\ = ( \CPU|FSM|WideOr1~1_combout\ & ( \CPU|FSM|always0~19_combout\ ) ) # ( !\CPU|FSM|WideOr1~1_combout\ & ( \CPU|FSM|always0~19_combout\ ) ) # ( \CPU|FSM|WideOr1~1_combout\ & ( !\CPU|FSM|always0~19_combout\ ) ) # ( 
-- !\CPU|FSM|WideOr1~1_combout\ & ( !\CPU|FSM|always0~19_combout\ & ( ((!\KEY[1]~input_o\) # ((!\CPU|FSM|WideOr1~2_combout\) # (!\CPU|FSM|WideOr37~1_combout\))) # (\CPU|FSM|always0~20_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111101111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_always0~20_combout\,
	datab => \ALT_INV_KEY[1]~input_o\,
	datac => \CPU|FSM|ALT_INV_WideOr1~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr37~1_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr1~1_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~19_combout\,
	combout => \CPU|FSM|state_next_reset[4]~4_combout\);

-- Location: FF_X82_Y6_N26
\CPU|FSM|STATE|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|state_next_reset[4]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|STATE|out\(4));

-- Location: MLABCELL_X82_Y8_N48
\CPU|FSM|WideNor2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor2~combout\ = ( !\CPU|FSM|STATE|out\(3) & ( (!\CPU|FSM|STATE|out\(4) & (!\CPU|FSM|STATE|out\(1) & (!\CPU|FSM|STATE|out\(2) & \CPU|FSM|STATE|out\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|STATE|ALT_INV_out\(4),
	datab => \CPU|FSM|STATE|ALT_INV_out\(1),
	datac => \CPU|FSM|STATE|ALT_INV_out\(2),
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	dataf => \CPU|FSM|STATE|ALT_INV_out\(3),
	combout => \CPU|FSM|WideNor2~combout\);

-- Location: LABCELL_X80_Y7_N33
\CPU|FSM|WideOr46~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr46~0_combout\ = ( !\CPU|FSM|vsel\(3) & ( !\CPU|FSM|always0~14_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|ALT_INV_always0~14_combout\,
	dataf => \CPU|FSM|ALT_INV_vsel\(3),
	combout => \CPU|FSM|WideOr46~0_combout\);

-- Location: LABCELL_X80_Y8_N36
\Equal5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal5~0_combout\ = ( \CPU|FSM|WideOr46~0_combout\ & ( \CPU|FSM|WideOr41~2_combout\ & ( (\CPU|FSM|WideNor2~combout\ & (\CPU|program_counter|out[6]~DUPLICATE_q\ & !\CPU|FSM|m_cmd\(1))) ) ) ) # ( !\CPU|FSM|WideOr46~0_combout\ & ( 
-- \CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|FSM|m_cmd\(1) & ((!\CPU|FSM|WideNor2~combout\ & ((\CPU|Data_Address|out\(6)))) # (\CPU|FSM|WideNor2~combout\ & (\CPU|program_counter|out[6]~DUPLICATE_q\)))) ) ) ) # ( \CPU|FSM|WideOr46~0_combout\ & ( 
-- !\CPU|FSM|WideOr41~2_combout\ & ( (\CPU|program_counter|out[6]~DUPLICATE_q\ & !\CPU|FSM|m_cmd\(1)) ) ) ) # ( !\CPU|FSM|WideOr46~0_combout\ & ( !\CPU|FSM|WideOr41~2_combout\ & ( (\CPU|program_counter|out[6]~DUPLICATE_q\ & !\CPU|FSM|m_cmd\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000000011011000000000001000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datab => \CPU|program_counter|ALT_INV_out[6]~DUPLICATE_q\,
	datac => \CPU|Data_Address|ALT_INV_out\(6),
	datad => \CPU|FSM|ALT_INV_m_cmd\(1),
	datae => \CPU|FSM|ALT_INV_WideOr46~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	combout => \Equal5~0_combout\);

-- Location: LABCELL_X81_Y7_N18
\Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = ( \LED_load~2_combout\ & ( \LED_load~1_combout\ & ( (\MEM|mem_rtl_0|auto_generated|ram_block1a9\ & ((!\Equal5~0_combout\) # ((!\LED_load~3_combout\) # (!\LED_load~0_combout\)))) ) ) ) # ( !\LED_load~2_combout\ & ( 
-- \LED_load~1_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ ) ) ) # ( \LED_load~2_combout\ & ( !\LED_load~1_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ ) ) ) # ( !\LED_load~2_combout\ & ( !\LED_load~1_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a9\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000111111110000000011111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal5~0_combout\,
	datab => \ALT_INV_LED_load~3_combout\,
	datac => \ALT_INV_LED_load~0_combout\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\,
	datae => \ALT_INV_LED_load~2_combout\,
	dataf => \ALT_INV_LED_load~1_combout\,
	combout => \Selector6~0_combout\);

-- Location: FF_X81_Y7_N19
\CPU|struct_reg|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector6~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(9));

-- Location: LABCELL_X83_Y8_N36
\CPU|FSM|WideNor38\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor38~combout\ = ( !\CPU|DP|status|out[1]~DUPLICATE_q\ & ( \CPU|DP|status|out\(2) & ( (\CPU|struct_reg|out\(9) & (\CPU|struct_reg|out[8]~DUPLICATE_q\ & (\CPU|FSM|WideNor33~0_combout\ & !\CPU|struct_reg|out[10]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000001000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(9),
	datab => \CPU|struct_reg|ALT_INV_out[8]~DUPLICATE_q\,
	datac => \CPU|FSM|ALT_INV_WideNor33~0_combout\,
	datad => \CPU|struct_reg|ALT_INV_out[10]~DUPLICATE_q\,
	datae => \CPU|DP|status|ALT_INV_out[1]~DUPLICATE_q\,
	dataf => \CPU|DP|status|ALT_INV_out\(2),
	combout => \CPU|FSM|WideNor38~combout\);

-- Location: LABCELL_X83_Y8_N24
\CPU|FSM|WideOr37~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr37~0_combout\ = ( \CPU|FSM|always0~7_combout\ & ( (\CPU|FSM|WideNor34~combout\) # (\CPU|FSM|WideNor33~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor33~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor34~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~7_combout\,
	combout => \CPU|FSM|WideOr37~0_combout\);

-- Location: LABCELL_X83_Y8_N48
\CPU|FSM|WideOr37~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr37~1_combout\ = ( \CPU|FSM|always0~8_combout\ & ( !\CPU|FSM|WideOr37~0_combout\ & ( (!\CPU|FSM|WideNor38~combout\ & (!\CPU|FSM|WideNor37~combout\ & (!\CPU|FSM|WideNor40~combout\ & !\CPU|FSM|WideNor39~combout\))) ) ) ) # ( 
-- !\CPU|FSM|always0~8_combout\ & ( !\CPU|FSM|WideOr37~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor38~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor37~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor40~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor39~combout\,
	datae => \CPU|FSM|ALT_INV_always0~8_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr37~0_combout\,
	combout => \CPU|FSM|WideOr37~1_combout\);

-- Location: LABCELL_X83_Y8_N3
\CPU|FSM|WideOr35~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr35~0_combout\ = ( !\CPU|FSM|always0~20_combout\ & ( (\CPU|FSM|WideOr37~1_combout\ & (!\CPU|FSM|WideOr1~1_combout\ & (!\CPU|FSM|WideNor3~combout\ & !\CPU|FSM|always0~19_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000000000000000000001000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr37~1_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr1~1_combout\,
	datac => \CPU|FSM|ALT_INV_WideNor3~combout\,
	datad => \CPU|FSM|ALT_INV_always0~19_combout\,
	datae => \CPU|FSM|ALT_INV_always0~20_combout\,
	combout => \CPU|FSM|WideOr35~0_combout\);

-- Location: MLABCELL_X84_Y8_N42
\CPU|nextPCmux|out~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~4_combout\ = ( \CPU|FSM|psel[2]~1_combout\ & ( \CPU|DP|rC|out\(4) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|rC|ALT_INV_out\(4),
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|nextPCmux|out~4_combout\);

-- Location: MLABCELL_X87_Y8_N6
\CPU|nextPCmux|out[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(4) = ( \CPU|Add0~17_sumout\ & ( \CPU|nextPCmux|out~4_combout\ ) ) # ( !\CPU|Add0~17_sumout\ & ( \CPU|nextPCmux|out~4_combout\ ) ) # ( \CPU|Add0~17_sumout\ & ( !\CPU|nextPCmux|out~4_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr9~3_combout\) # (!\CPU|FSM|WideOr41~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datae => \CPU|ALT_INV_Add0~17_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~4_combout\,
	combout => \CPU|nextPCmux|out\(4));

-- Location: FF_X87_Y8_N7
\CPU|program_counter|out[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(4),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out[4]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y7_N48
\LED_load~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED_load~1_combout\ = ( \CPU|FSM|WideNor2~combout\ & ( \CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|program_counter|out[4]~DUPLICATE_q\ & !\CPU|program_counter|out\(3)) ) ) ) # ( !\CPU|FSM|WideNor2~combout\ & ( \CPU|FSM|WideOr41~2_combout\ & ( 
-- (!\CPU|Data_Address|out\(3) & !\CPU|Data_Address|out\(4)) ) ) ) # ( \CPU|FSM|WideNor2~combout\ & ( !\CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|program_counter|out[4]~DUPLICATE_q\ & !\CPU|program_counter|out\(3)) ) ) ) # ( !\CPU|FSM|WideNor2~combout\ & ( 
-- !\CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|program_counter|out[4]~DUPLICATE_q\ & !\CPU|program_counter|out\(3)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000101000001010000011001100000000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out[4]~DUPLICATE_q\,
	datab => \CPU|Data_Address|ALT_INV_out\(3),
	datac => \CPU|program_counter|ALT_INV_out\(3),
	datad => \CPU|Data_Address|ALT_INV_out\(4),
	datae => \CPU|FSM|ALT_INV_WideNor2~combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	combout => \LED_load~1_combout\);

-- Location: LABCELL_X83_Y6_N0
\Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = ( \Equal5~0_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( (!\LED_load~1_combout\) # ((!\LED_load~3_combout\) # ((!\LED_load~2_combout\) # (!\LED_load~0_combout\))) ) ) ) # ( !\Equal5~0_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a15\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~1_combout\,
	datab => \ALT_INV_LED_load~3_combout\,
	datac => \ALT_INV_LED_load~2_combout\,
	datad => \ALT_INV_LED_load~0_combout\,
	datae => \ALT_INV_Equal5~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\,
	combout => \Selector0~0_combout\);

-- Location: FF_X83_Y6_N2
\CPU|struct_reg|out[15]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector0~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out[15]~DUPLICATE_q\);

-- Location: LABCELL_X83_Y6_N6
\CPU|FSM|WideNor28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor28~0_combout\ = ( \CPU|struct_reg|out[13]~DUPLICATE_q\ & ( (!\CPU|struct_reg|out[15]~DUPLICATE_q\ & \CPU|struct_reg|out[14]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|struct_reg|ALT_INV_out[15]~DUPLICATE_q\,
	datad => \CPU|struct_reg|ALT_INV_out[14]~DUPLICATE_q\,
	dataf => \CPU|struct_reg|ALT_INV_out[13]~DUPLICATE_q\,
	combout => \CPU|FSM|WideNor28~0_combout\);

-- Location: LABCELL_X81_Y8_N45
\CPU|FSM|WideNor28\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideNor28~combout\ = ( \CPU|FSM|WideNor28~1_combout\ & ( (\CPU|FSM|WideNor28~0_combout\ & !\CPU|FSM|STATE|out\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor28~0_combout\,
	datad => \CPU|FSM|STATE|ALT_INV_out\(0),
	dataf => \CPU|FSM|ALT_INV_WideNor28~1_combout\,
	combout => \CPU|FSM|WideNor28~combout\);

-- Location: MLABCELL_X82_Y7_N3
\CPU|FSM|always0~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|always0~18_combout\ = ( \CPU|FSM|always0~2_combout\ & ( (\CPU|FSM|WideNor13~combout\ & !\CPU|FSM|WideNor12~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor13~combout\,
	datad => \CPU|FSM|ALT_INV_WideNor12~combout\,
	dataf => \CPU|FSM|ALT_INV_always0~2_combout\,
	combout => \CPU|FSM|always0~18_combout\);

-- Location: LABCELL_X81_Y6_N42
\CPU|FSM|WideOr29~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr29~0_combout\ = ( !\CPU|FSM|always0~18_combout\ & ( \CPU|FSM|always0~5_combout\ & ( ((!\CPU|FSM|WideNor29~combout\) # ((\CPU|FSM|WideNor27~combout\) # (\CPU|FSM|WideNor26~1_combout\))) # (\CPU|FSM|WideNor28~combout\) ) ) ) # ( 
-- !\CPU|FSM|always0~18_combout\ & ( !\CPU|FSM|always0~5_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011011111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideNor28~combout\,
	datab => \CPU|FSM|ALT_INV_WideNor29~combout\,
	datac => \CPU|FSM|ALT_INV_WideNor26~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideNor27~combout\,
	datae => \CPU|FSM|ALT_INV_always0~18_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~5_combout\,
	combout => \CPU|FSM|WideOr29~0_combout\);

-- Location: LABCELL_X85_Y7_N27
\CPU|FSM|WideOr39~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr39~1_combout\ = ( !\CPU|FSM|loads~combout\ & ( (!\CPU|FSM|WideNor2~combout\ & !\CPU|FSM|always0~26_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|FSM|ALT_INV_always0~26_combout\,
	dataf => \CPU|FSM|ALT_INV_loads~combout\,
	combout => \CPU|FSM|WideOr39~1_combout\);

-- Location: LABCELL_X85_Y7_N18
\CPU|FSM|WideOr39~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr39~2_combout\ = ( \CPU|FSM|nsel[2]~0_combout\ & ( \CPU|FSM|always0~25_combout\ & ( (\CPU|FSM|WideOr29~0_combout\ & (!\CPU|FSM|WideNor43~combout\ & (\CPU|FSM|WideOr39~1_combout\ & \CPU|FSM|WideOr39~0_combout\))) ) ) ) # ( 
-- \CPU|FSM|nsel[2]~0_combout\ & ( !\CPU|FSM|always0~25_combout\ & ( (\CPU|FSM|WideOr29~0_combout\ & (\CPU|FSM|WideOr39~1_combout\ & \CPU|FSM|WideOr39~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010100000000000000000000000000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr29~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor43~combout\,
	datac => \CPU|FSM|ALT_INV_WideOr39~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr39~0_combout\,
	datae => \CPU|FSM|ALT_INV_nsel[2]~0_combout\,
	dataf => \CPU|FSM|ALT_INV_always0~25_combout\,
	combout => \CPU|FSM|WideOr39~2_combout\);

-- Location: LABCELL_X88_Y8_N33
\CPU|nextPCmux|out~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out~1_combout\ = ( \CPU|FSM|psel[2]~1_combout\ & ( \CPU|DP|rC|out\(5) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|rC|ALT_INV_out\(5),
	dataf => \CPU|FSM|ALT_INV_psel[2]~1_combout\,
	combout => \CPU|nextPCmux|out~1_combout\);

-- Location: MLABCELL_X87_Y8_N21
\CPU|nextPCmux|out[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|nextPCmux|out\(5) = ( \CPU|Add0~5_sumout\ & ( \CPU|nextPCmux|out~1_combout\ ) ) # ( !\CPU|Add0~5_sumout\ & ( \CPU|nextPCmux|out~1_combout\ ) ) # ( \CPU|Add0~5_sumout\ & ( !\CPU|nextPCmux|out~1_combout\ & ( (!\CPU|FSM|WideOr39~2_combout\) # 
-- ((!\CPU|FSM|WideOr35~0_combout\) # ((!\CPU|FSM|WideOr41~2_combout\) # (!\CPU|FSM|WideOr9~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~3_combout\,
	datae => \CPU|ALT_INV_Add0~5_sumout\,
	dataf => \CPU|nextPCmux|ALT_INV_out~1_combout\,
	combout => \CPU|nextPCmux|out\(5));

-- Location: FF_X87_Y8_N23
\CPU|program_counter|out[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|nextPCmux|out\(5),
	ena => \CPU|FSM|WideOr39~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|out[5]~DUPLICATE_q\);

-- Location: LABCELL_X80_Y8_N42
\LED_load~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED_load~0_combout\ = ( \CPU|program_counter|out\(7) & ( \CPU|FSM|WideOr41~2_combout\ & ( (!\CPU|Data_Address|out\(5) & (!\CPU|FSM|WideNor2~combout\ & !\CPU|Data_Address|out\(7))) ) ) ) # ( !\CPU|program_counter|out\(7) & ( \CPU|FSM|WideOr41~2_combout\ & 
-- ( (!\CPU|FSM|WideNor2~combout\ & (((!\CPU|Data_Address|out\(5) & !\CPU|Data_Address|out\(7))))) # (\CPU|FSM|WideNor2~combout\ & (!\CPU|program_counter|out[5]~DUPLICATE_q\)) ) ) ) # ( !\CPU|program_counter|out\(7) & ( !\CPU|FSM|WideOr41~2_combout\ & ( 
-- !\CPU|program_counter|out[5]~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010000000000000000011001010000010101100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|ALT_INV_out[5]~DUPLICATE_q\,
	datab => \CPU|Data_Address|ALT_INV_out\(5),
	datac => \CPU|FSM|ALT_INV_WideNor2~combout\,
	datad => \CPU|Data_Address|ALT_INV_out\(7),
	datae => \CPU|program_counter|ALT_INV_out\(7),
	dataf => \CPU|FSM|ALT_INV_WideOr41~2_combout\,
	combout => \LED_load~0_combout\);

-- Location: LABCELL_X83_Y6_N54
\Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~0_combout\ = ( \LED_load~1_combout\ & ( \LED_load~3_combout\ & ( (\MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ((!\LED_load~0_combout\) # ((!\Equal5~0_combout\) # (!\LED_load~2_combout\)))) ) ) ) # ( !\LED_load~1_combout\ & ( 
-- \LED_load~3_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a11\ ) ) ) # ( \LED_load~1_combout\ & ( !\LED_load~3_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a11\ ) ) ) # ( !\LED_load~1_combout\ & ( !\LED_load~3_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a11\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000111111110000000011111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~0_combout\,
	datab => \ALT_INV_Equal5~0_combout\,
	datac => \ALT_INV_LED_load~2_combout\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\,
	datae => \ALT_INV_LED_load~1_combout\,
	dataf => \ALT_INV_LED_load~3_combout\,
	combout => \Selector4~0_combout\);

-- Location: FF_X83_Y6_N56
\CPU|struct_reg|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector4~0_combout\,
	ena => \CPU|FSM|WideNor2~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|struct_reg|out\(11));

-- Location: MLABCELL_X84_Y9_N54
\CPU|DP|U2|main|Mux15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|U2|main|Mux15~0_combout\ = ( \CPU|DP|Bin[0]~17_combout\ & ( (!\CPU|struct_reg|out\(12) & (((!\CPU|DP|rA|out\(0)) # (\CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\)))) # (\CPU|struct_reg|out\(12) & (!\CPU|struct_reg|out\(11) & 
-- (!\CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ & \CPU|DP|rA|out\(0)))) ) ) # ( !\CPU|DP|Bin[0]~17_combout\ & ( (!\CPU|struct_reg|out\(12) & (((!\CPU|DP|U2|ALU_AddSub|ai|c[1]~1_combout\ & \CPU|DP|rA|out\(0))))) # (\CPU|struct_reg|out\(12) & 
-- (\CPU|struct_reg|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000111010001000100011101000111001100001011001100110000101100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|struct_reg|ALT_INV_out\(11),
	datab => \CPU|struct_reg|ALT_INV_out\(12),
	datac => \CPU|DP|U2|ALU_AddSub|ai|ALT_INV_c[1]~1_combout\,
	datad => \CPU|DP|rA|ALT_INV_out\(0),
	dataf => \CPU|DP|ALT_INV_Bin[0]~17_combout\,
	combout => \CPU|DP|U2|main|Mux15~0_combout\);

-- Location: FF_X84_Y9_N56
\CPU|DP|rC|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|main|Mux15~0_combout\,
	ena => \CPU|FSM|WideOr20~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|rC|out\(0));

-- Location: LABCELL_X81_Y7_N6
LED_load : cyclonev_lcell_comb
-- Equation(s):
-- \LED_load~combout\ = ( \LED_load~2_combout\ & ( !\CPU|mem_addr[6]~0_combout\ & ( (\LED_load~1_combout\ & (\CPU|FSM|m_cmd\(1) & (\LED_load~0_combout\ & \LED_load~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LED_load~1_combout\,
	datab => \CPU|FSM|ALT_INV_m_cmd\(1),
	datac => \ALT_INV_LED_load~0_combout\,
	datad => \ALT_INV_LED_load~3_combout\,
	datae => \ALT_INV_LED_load~2_combout\,
	dataf => \CPU|ALT_INV_mem_addr[6]~0_combout\,
	combout => \LED_load~combout\);

-- Location: FF_X81_Y7_N56
\LED_register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(0),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(0));

-- Location: FF_X81_Y7_N1
\LED_register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(1),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(1));

-- Location: FF_X81_Y7_N5
\LED_register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(2),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(2));

-- Location: FF_X82_Y5_N22
\LED_register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(3),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(3));

-- Location: FF_X82_Y5_N28
\LED_register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(4),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(4));

-- Location: FF_X81_Y7_N8
\LED_register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(5),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(5));

-- Location: FF_X81_Y7_N14
\LED_register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(6),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(6));

-- Location: FF_X81_Y7_N16
\LED_register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|rC|out\(7),
	sload => VCC,
	ena => \LED_load~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LED_register|out\(7));

-- Location: LABCELL_X85_Y7_N24
\CPU|FSM|WideOr9~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr9~2_combout\ = ( \CPU|FSM|WideOr41~1_combout\ & ( (\CPU|FSM|WideOr9~0_combout\ & (\CPU|FSM|WideOr3~0_combout\ & (!\CPU|FSM|always0~12_combout\ & \CPU|FSM|WideOr9~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr9~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	datac => \CPU|FSM|ALT_INV_always0~12_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr9~1_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr41~1_combout\,
	combout => \CPU|FSM|WideOr9~2_combout\);

-- Location: LABCELL_X85_Y7_N6
\CPU|FSM|WideOr7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr7~1_combout\ = ( \CPU|FSM|WideOr7~0_combout\ & ( (!\CPU|FSM|WideOr18~0_combout\ & (\CPU|FSM|WideOr3~1_combout\ & (\CPU|FSM|WideOr35~0_combout\ & \CPU|FSM|WideOr3~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000100000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr18~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr3~1_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr35~0_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr7~0_combout\,
	combout => \CPU|FSM|WideOr7~1_combout\);

-- Location: LABCELL_X85_Y7_N54
\CPU|FSM|WideOr1~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|WideOr1~3_combout\ = ( !\CPU|FSM|WideOr1~1_combout\ & ( (!\CPU|FSM|always0~20_combout\ & (\CPU|FSM|WideOr37~1_combout\ & (!\CPU|FSM|always0~19_combout\ & \CPU|FSM|WideOr1~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_always0~20_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr37~1_combout\,
	datac => \CPU|FSM|ALT_INV_always0~19_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr1~2_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr1~1_combout\,
	combout => \CPU|FSM|WideOr1~3_combout\);

-- Location: LABCELL_X88_Y6_N0
\CPU|FSM|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|Equal0~0_combout\ = ( \CPU|FSM|WideOr18~0_combout\ & ( \CPU|FSM|WideOr9~0_combout\ & ( (\CPU|FSM|WideOr3~0_combout\ & (\CPU|FSM|WideOr3~3_combout\ & \CPU|FSM|WideOr3~1_combout\)) ) ) ) # ( !\CPU|FSM|WideOr18~0_combout\ & ( 
-- \CPU|FSM|WideOr9~0_combout\ & ( (\CPU|FSM|WideOr3~0_combout\ & (((\CPU|FSM|WideOr3~3_combout\ & \CPU|FSM|WideOr3~1_combout\)) # (\CPU|FSM|WideOr39~0_combout\))) ) ) ) # ( \CPU|FSM|WideOr18~0_combout\ & ( !\CPU|FSM|WideOr9~0_combout\ & ( 
-- (\CPU|FSM|WideOr3~0_combout\ & (\CPU|FSM|WideOr3~3_combout\ & \CPU|FSM|WideOr3~1_combout\)) ) ) ) # ( !\CPU|FSM|WideOr18~0_combout\ & ( !\CPU|FSM|WideOr9~0_combout\ & ( (\CPU|FSM|WideOr3~0_combout\ & (\CPU|FSM|WideOr3~3_combout\ & 
-- \CPU|FSM|WideOr3~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001100010001000100110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr39~0_combout\,
	datab => \CPU|FSM|ALT_INV_WideOr3~0_combout\,
	datac => \CPU|FSM|ALT_INV_WideOr3~3_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr3~1_combout\,
	datae => \CPU|FSM|ALT_INV_WideOr18~0_combout\,
	dataf => \CPU|FSM|ALT_INV_WideOr9~0_combout\,
	combout => \CPU|FSM|Equal0~0_combout\);

-- Location: LABCELL_X85_Y7_N15
\CPU|FSM|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|Equal0~1_combout\ = ( !\CPU|FSM|Equal0~0_combout\ & ( (!\CPU|FSM|WideOr7~1_combout\ & (\CPU|FSM|WideOr1~3_combout\ & ((!\CPU|FSM|WideOr9~2_combout\) # (\CPU|FSM|WideNor3~combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010110000000000001011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|ALT_INV_WideOr9~2_combout\,
	datab => \CPU|FSM|ALT_INV_WideNor3~combout\,
	datac => \CPU|FSM|ALT_INV_WideOr7~1_combout\,
	datad => \CPU|FSM|ALT_INV_WideOr1~3_combout\,
	dataf => \CPU|FSM|ALT_INV_Equal0~0_combout\,
	combout => \CPU|FSM|Equal0~1_combout\);

-- Location: LABCELL_X88_Y8_N3
\H0|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr6~0_combout\ = (!\CPU|DP|rC|out\(3) & (!\CPU|DP|rC|out\(1) & (!\CPU|DP|rC|out\(0) $ (!\CPU|DP|rC|out\(2))))) # (\CPU|DP|rC|out\(3) & (\CPU|DP|rC|out\(0) & (!\CPU|DP|rC|out\(2) $ (!\CPU|DP|rC|out\(1)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100100100010000010010010001000001001001000100000100100100010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(0),
	datab => \CPU|DP|rC|ALT_INV_out\(3),
	datac => \CPU|DP|rC|ALT_INV_out\(2),
	datad => \CPU|DP|rC|ALT_INV_out\(1),
	combout => \H0|WideOr6~0_combout\);

-- Location: LABCELL_X88_Y8_N42
\H0|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr5~0_combout\ = ( \CPU|DP|rC|out\(1) & ( (!\CPU|DP|rC|out\(0) & (\CPU|DP|rC|out\(2))) # (\CPU|DP|rC|out\(0) & ((\CPU|DP|rC|out\(3)))) ) ) # ( !\CPU|DP|rC|out\(1) & ( (\CPU|DP|rC|out\(2) & (!\CPU|DP|rC|out\(0) $ (!\CPU|DP|rC|out\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100110000000000110011000000110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(2),
	datac => \CPU|DP|rC|ALT_INV_out\(0),
	datad => \CPU|DP|rC|ALT_INV_out\(3),
	dataf => \CPU|DP|rC|ALT_INV_out\(1),
	combout => \H0|WideOr5~0_combout\);

-- Location: LABCELL_X88_Y8_N15
\H0|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr4~0_combout\ = (!\CPU|DP|rC|out\(3) & (\CPU|DP|rC|out\(1) & (!\CPU|DP|rC|out\(2) & !\CPU|DP|rC|out\(0)))) # (\CPU|DP|rC|out\(3) & (\CPU|DP|rC|out\(2) & ((!\CPU|DP|rC|out\(0)) # (\CPU|DP|rC|out\(1)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100001100000001010000110000000101000011000000010100001100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(1),
	datab => \CPU|DP|rC|ALT_INV_out\(3),
	datac => \CPU|DP|rC|ALT_INV_out\(2),
	datad => \CPU|DP|rC|ALT_INV_out\(0),
	combout => \H0|WideOr4~0_combout\);

-- Location: LABCELL_X88_Y8_N54
\H0|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr3~0_combout\ = ( \CPU|DP|rC|out\(1) & ( (!\CPU|DP|rC|out\(2) & (\CPU|DP|rC|out\(3) & !\CPU|DP|rC|out\(0))) # (\CPU|DP|rC|out\(2) & ((\CPU|DP|rC|out\(0)))) ) ) # ( !\CPU|DP|rC|out\(1) & ( (!\CPU|DP|rC|out\(3) & (!\CPU|DP|rC|out\(2) $ 
-- (!\CPU|DP|rC|out\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010100000101000001010000010100001000011010000110100001101000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(3),
	datab => \CPU|DP|rC|ALT_INV_out\(2),
	datac => \CPU|DP|rC|ALT_INV_out\(0),
	dataf => \CPU|DP|rC|ALT_INV_out\(1),
	combout => \H0|WideOr3~0_combout\);

-- Location: LABCELL_X83_Y9_N39
\H0|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr2~0_combout\ = ( \CPU|DP|rC|out\(2) & ( (!\CPU|DP|rC|out\(3) & ((!\CPU|DP|rC|out\(1)) # (\CPU|DP|rC|out\(0)))) ) ) # ( !\CPU|DP|rC|out\(2) & ( (\CPU|DP|rC|out\(0) & ((!\CPU|DP|rC|out\(3)) # (!\CPU|DP|rC|out\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001010000011110000101010101010000010101010101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(3),
	datac => \CPU|DP|rC|ALT_INV_out\(0),
	datad => \CPU|DP|rC|ALT_INV_out\(1),
	dataf => \CPU|DP|rC|ALT_INV_out\(2),
	combout => \H0|WideOr2~0_combout\);

-- Location: LABCELL_X88_Y8_N48
\H0|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr1~0_combout\ = ( \CPU|DP|rC|out\(1) & ( (!\CPU|DP|rC|out\(3) & ((!\CPU|DP|rC|out\(2)) # (\CPU|DP|rC|out\(0)))) ) ) # ( !\CPU|DP|rC|out\(1) & ( (\CPU|DP|rC|out\(0) & (!\CPU|DP|rC|out\(3) $ (\CPU|DP|rC|out\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100100001001000010010000100110001010100010101000101010001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(3),
	datab => \CPU|DP|rC|ALT_INV_out\(2),
	datac => \CPU|DP|rC|ALT_INV_out\(0),
	dataf => \CPU|DP|rC|ALT_INV_out\(1),
	combout => \H0|WideOr1~0_combout\);

-- Location: LABCELL_X88_Y8_N51
\H0|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr0~0_combout\ = ( \CPU|DP|rC|out\(1) & ( ((!\CPU|DP|rC|out\(2)) # (!\CPU|DP|rC|out\(0))) # (\CPU|DP|rC|out\(3)) ) ) # ( !\CPU|DP|rC|out\(1) & ( (!\CPU|DP|rC|out\(3) & (\CPU|DP|rC|out\(2))) # (\CPU|DP|rC|out\(3) & ((!\CPU|DP|rC|out\(2)) # 
-- (\CPU|DP|rC|out\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001011111010110100101111111111111111101011111111111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(3),
	datac => \CPU|DP|rC|ALT_INV_out\(2),
	datad => \CPU|DP|rC|ALT_INV_out\(0),
	dataf => \CPU|DP|rC|ALT_INV_out\(1),
	combout => \H0|WideOr0~0_combout\);

-- Location: MLABCELL_X78_Y7_N48
\H1|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr6~0_combout\ = ( \CPU|DP|rC|out\(7) & ( \CPU|DP|rC|out\(6) & ( (\CPU|DP|rC|out\(4) & !\CPU|DP|rC|out\(5)) ) ) ) # ( !\CPU|DP|rC|out\(7) & ( \CPU|DP|rC|out\(6) & ( (!\CPU|DP|rC|out\(4) & !\CPU|DP|rC|out\(5)) ) ) ) # ( \CPU|DP|rC|out\(7) & ( 
-- !\CPU|DP|rC|out\(6) & ( (\CPU|DP|rC|out\(4) & \CPU|DP|rC|out\(5)) ) ) ) # ( !\CPU|DP|rC|out\(7) & ( !\CPU|DP|rC|out\(6) & ( (\CPU|DP|rC|out\(4) & !\CPU|DP|rC|out\(5)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100000100010001000110001000100010000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(4),
	datab => \CPU|DP|rC|ALT_INV_out\(5),
	datae => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|DP|rC|ALT_INV_out\(6),
	combout => \H1|WideOr6~0_combout\);

-- Location: LABCELL_X83_Y9_N18
\H1|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr5~0_combout\ = ( \CPU|DP|rC|out\(6) & ( (!\CPU|DP|rC|out\(4) & ((\CPU|DP|rC|out\(7)) # (\CPU|DP|rC|out\(5)))) # (\CPU|DP|rC|out\(4) & (!\CPU|DP|rC|out\(5) $ (\CPU|DP|rC|out\(7)))) ) ) # ( !\CPU|DP|rC|out\(6) & ( (\CPU|DP|rC|out\(4) & 
-- (\CPU|DP|rC|out\(5) & \CPU|DP|rC|out\(7))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010101011010101011110101101010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(4),
	datac => \CPU|DP|rC|ALT_INV_out\(5),
	datad => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|DP|rC|ALT_INV_out\(6),
	combout => \H1|WideOr5~0_combout\);

-- Location: LABCELL_X83_Y9_N36
\H1|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr4~0_combout\ = ( \CPU|DP|rC|out\(5) & ( (!\CPU|DP|rC|out\(6) & (!\CPU|DP|rC|out\(4) & !\CPU|DP|rC|out\(7))) # (\CPU|DP|rC|out\(6) & ((\CPU|DP|rC|out\(7)))) ) ) # ( !\CPU|DP|rC|out\(5) & ( (\CPU|DP|rC|out\(6) & (!\CPU|DP|rC|out\(4) & 
-- \CPU|DP|rC|out\(7))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000011000000001100111100000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(6),
	datac => \CPU|DP|rC|ALT_INV_out\(4),
	datad => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|DP|rC|ALT_INV_out\(5),
	combout => \H1|WideOr4~0_combout\);

-- Location: LABCELL_X83_Y9_N15
\H1|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr3~0_combout\ = ( \CPU|DP|rC|out\(7) & ( \CPU|DP|rC|out\(5) & ( !\CPU|DP|rC|out\(4) $ (\CPU|DP|rC|out\(6)) ) ) ) # ( !\CPU|DP|rC|out\(7) & ( \CPU|DP|rC|out\(5) & ( (\CPU|DP|rC|out\(4) & \CPU|DP|rC|out\(6)) ) ) ) # ( !\CPU|DP|rC|out\(7) & ( 
-- !\CPU|DP|rC|out\(5) & ( !\CPU|DP|rC|out\(4) $ (!\CPU|DP|rC|out\(6)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110101010000000000000000000000000010101011010101001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(4),
	datad => \CPU|DP|rC|ALT_INV_out\(6),
	datae => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|DP|rC|ALT_INV_out\(5),
	combout => \H1|WideOr3~0_combout\);

-- Location: LABCELL_X83_Y9_N6
\H1|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr2~0_combout\ = ( \CPU|DP|rC|out\(5) & ( (\CPU|DP|rC|out\(4) & !\CPU|DP|rC|out\(7)) ) ) # ( !\CPU|DP|rC|out\(5) & ( (!\CPU|DP|rC|out\(6) & (\CPU|DP|rC|out\(4))) # (\CPU|DP|rC|out\(6) & ((!\CPU|DP|rC|out\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111010001110100011101000111010001010000010100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(4),
	datab => \CPU|DP|rC|ALT_INV_out\(6),
	datac => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|DP|rC|ALT_INV_out\(5),
	combout => \H1|WideOr2~0_combout\);

-- Location: LABCELL_X83_Y9_N9
\H1|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr1~0_combout\ = ( \CPU|DP|rC|out\(5) & ( (!\CPU|DP|rC|out\(7) & ((!\CPU|DP|rC|out\(6)) # (\CPU|DP|rC|out\(4)))) ) ) # ( !\CPU|DP|rC|out\(5) & ( (\CPU|DP|rC|out\(4) & (!\CPU|DP|rC|out\(6) $ (\CPU|DP|rC|out\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000101010100000000010111110101000000001111010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(4),
	datac => \CPU|DP|rC|ALT_INV_out\(6),
	datad => \CPU|DP|rC|ALT_INV_out\(7),
	dataf => \CPU|DP|rC|ALT_INV_out\(5),
	combout => \H1|WideOr1~0_combout\);

-- Location: LABCELL_X77_Y8_N42
\H1|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr0~0_combout\ = ( \CPU|DP|rC|out\(6) & ( \CPU|DP|rC|out\(7) & ( (\CPU|DP|rC|out\(5)) # (\CPU|DP|rC|out\(4)) ) ) ) # ( !\CPU|DP|rC|out\(6) & ( \CPU|DP|rC|out\(7) ) ) # ( \CPU|DP|rC|out\(6) & ( !\CPU|DP|rC|out\(7) & ( (!\CPU|DP|rC|out\(4)) # 
-- (!\CPU|DP|rC|out\(5)) ) ) ) # ( !\CPU|DP|rC|out\(6) & ( !\CPU|DP|rC|out\(7) & ( \CPU|DP|rC|out\(5) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111111111001111110011111111111111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(4),
	datac => \CPU|DP|rC|ALT_INV_out\(5),
	datae => \CPU|DP|rC|ALT_INV_out\(6),
	dataf => \CPU|DP|rC|ALT_INV_out\(7),
	combout => \H1|WideOr0~0_combout\);

-- Location: MLABCELL_X82_Y10_N36
\H2|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr6~0_combout\ = ( \CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(10) & ( (\CPU|DP|rC|out\(8) & !\CPU|DP|rC|out\(9)) ) ) ) # ( !\CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(10) & ( (!\CPU|DP|rC|out\(8) & !\CPU|DP|rC|out\(9)) ) ) ) # ( \CPU|DP|rC|out\(11) & ( 
-- !\CPU|DP|rC|out\(10) & ( (\CPU|DP|rC|out\(8) & \CPU|DP|rC|out\(9)) ) ) ) # ( !\CPU|DP|rC|out\(11) & ( !\CPU|DP|rC|out\(10) & ( (\CPU|DP|rC|out\(8) & !\CPU|DP|rC|out\(9)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000000000000011001111001100000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(8),
	datad => \CPU|DP|rC|ALT_INV_out\(9),
	datae => \CPU|DP|rC|ALT_INV_out\(11),
	dataf => \CPU|DP|rC|ALT_INV_out\(10),
	combout => \H2|WideOr6~0_combout\);

-- Location: MLABCELL_X82_Y10_N9
\H2|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr5~0_combout\ = ( \CPU|DP|rC|out\(9) & ( \CPU|DP|rC|out\(10) & ( (!\CPU|DP|rC|out\(8)) # (\CPU|DP|rC|out\(11)) ) ) ) # ( !\CPU|DP|rC|out\(9) & ( \CPU|DP|rC|out\(10) & ( !\CPU|DP|rC|out\(11) $ (!\CPU|DP|rC|out\(8)) ) ) ) # ( \CPU|DP|rC|out\(9) & ( 
-- !\CPU|DP|rC|out\(10) & ( (\CPU|DP|rC|out\(11) & \CPU|DP|rC|out\(8)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010101011010010110101111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(11),
	datac => \CPU|DP|rC|ALT_INV_out\(8),
	datae => \CPU|DP|rC|ALT_INV_out\(9),
	dataf => \CPU|DP|rC|ALT_INV_out\(10),
	combout => \H2|WideOr5~0_combout\);

-- Location: MLABCELL_X82_Y10_N0
\H2|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr4~0_combout\ = ( \CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(10) & ( (!\CPU|DP|rC|out\(8)) # (\CPU|DP|rC|out\(9)) ) ) ) # ( !\CPU|DP|rC|out\(11) & ( !\CPU|DP|rC|out\(10) & ( (!\CPU|DP|rC|out\(8) & \CPU|DP|rC|out\(9)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000000000000000000000000000001100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(8),
	datad => \CPU|DP|rC|ALT_INV_out\(9),
	datae => \CPU|DP|rC|ALT_INV_out\(11),
	dataf => \CPU|DP|rC|ALT_INV_out\(10),
	combout => \H2|WideOr4~0_combout\);

-- Location: MLABCELL_X82_Y10_N57
\H2|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr3~0_combout\ = ( \CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(9) & ( !\CPU|DP|rC|out\(10) $ (\CPU|DP|rC|out\(8)) ) ) ) # ( !\CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(9) & ( (\CPU|DP|rC|out\(10) & \CPU|DP|rC|out\(8)) ) ) ) # ( !\CPU|DP|rC|out\(11) & ( 
-- !\CPU|DP|rC|out\(9) & ( !\CPU|DP|rC|out\(10) $ (!\CPU|DP|rC|out\(8)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001011010000000000000000000000101000001011010010110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(10),
	datac => \CPU|DP|rC|ALT_INV_out\(8),
	datae => \CPU|DP|rC|ALT_INV_out\(11),
	dataf => \CPU|DP|rC|ALT_INV_out\(9),
	combout => \H2|WideOr3~0_combout\);

-- Location: MLABCELL_X82_Y10_N12
\H2|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr2~0_combout\ = (!\CPU|DP|rC|out\(9) & ((!\CPU|DP|rC|out\(10) & (\CPU|DP|rC|out\(8))) # (\CPU|DP|rC|out\(10) & ((!\CPU|DP|rC|out\(11)))))) # (\CPU|DP|rC|out\(9) & (((\CPU|DP|rC|out\(8) & !\CPU|DP|rC|out\(11)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111001100100000011100110010000001110011001000000111001100100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(10),
	datab => \CPU|DP|rC|ALT_INV_out\(8),
	datac => \CPU|DP|rC|ALT_INV_out\(9),
	datad => \CPU|DP|rC|ALT_INV_out\(11),
	combout => \H2|WideOr2~0_combout\);

-- Location: MLABCELL_X82_Y10_N15
\H2|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr1~0_combout\ = ( \CPU|DP|rC|out\(9) & ( (!\CPU|DP|rC|out\(11) & ((!\CPU|DP|rC|out\(10)) # (\CPU|DP|rC|out\(8)))) ) ) # ( !\CPU|DP|rC|out\(9) & ( (\CPU|DP|rC|out\(8) & (!\CPU|DP|rC|out\(10) $ (\CPU|DP|rC|out\(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000010001001000100001000110111011000000001011101100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(10),
	datab => \CPU|DP|rC|ALT_INV_out\(8),
	datad => \CPU|DP|rC|ALT_INV_out\(11),
	dataf => \CPU|DP|rC|ALT_INV_out\(9),
	combout => \H2|WideOr1~0_combout\);

-- Location: MLABCELL_X82_Y10_N30
\H2|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr0~0_combout\ = ( \CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(10) & ( (\CPU|DP|rC|out\(9)) # (\CPU|DP|rC|out\(8)) ) ) ) # ( !\CPU|DP|rC|out\(11) & ( \CPU|DP|rC|out\(10) & ( (!\CPU|DP|rC|out\(8)) # (!\CPU|DP|rC|out\(9)) ) ) ) # ( \CPU|DP|rC|out\(11) & 
-- ( !\CPU|DP|rC|out\(10) ) ) # ( !\CPU|DP|rC|out\(11) & ( !\CPU|DP|rC|out\(10) & ( \CPU|DP|rC|out\(9) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111111111111111111111111111110011000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(8),
	datad => \CPU|DP|rC|ALT_INV_out\(9),
	datae => \CPU|DP|rC|ALT_INV_out\(11),
	dataf => \CPU|DP|rC|ALT_INV_out\(10),
	combout => \H2|WideOr0~0_combout\);

-- Location: MLABCELL_X82_Y10_N48
\H3|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr6~0_combout\ = ( \CPU|DP|rC|out\(14) & ( (!\CPU|DP|rC|out\(13) & (!\CPU|DP|rC|out\(15) $ (\CPU|DP|rC|out\(12)))) ) ) # ( !\CPU|DP|rC|out\(14) & ( (\CPU|DP|rC|out\(12) & (!\CPU|DP|rC|out\(15) $ (\CPU|DP|rC|out\(13)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100100001001000010010000100110000100100001001000010010000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(15),
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(12),
	dataf => \CPU|DP|rC|ALT_INV_out\(14),
	combout => \H3|WideOr6~0_combout\);

-- Location: MLABCELL_X82_Y10_N51
\H3|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr5~0_combout\ = (!\CPU|DP|rC|out\(15) & (\CPU|DP|rC|out\(14) & (!\CPU|DP|rC|out\(13) $ (!\CPU|DP|rC|out\(12))))) # (\CPU|DP|rC|out\(15) & ((!\CPU|DP|rC|out\(12) & ((\CPU|DP|rC|out\(14)))) # (\CPU|DP|rC|out\(12) & (\CPU|DP|rC|out\(13)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011100011001000001110001100100000111000110010000011100011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(15),
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(14),
	datad => \CPU|DP|rC|ALT_INV_out\(12),
	combout => \H3|WideOr5~0_combout\);

-- Location: MLABCELL_X82_Y10_N21
\H3|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr4~0_combout\ = ( \CPU|DP|rC|out\(12) & ( \CPU|DP|rC|out\(15) & ( (\CPU|DP|rC|out\(13) & \CPU|DP|rC|out\(14)) ) ) ) # ( !\CPU|DP|rC|out\(12) & ( \CPU|DP|rC|out\(15) & ( \CPU|DP|rC|out\(14) ) ) ) # ( !\CPU|DP|rC|out\(12) & ( !\CPU|DP|rC|out\(15) & 
-- ( (\CPU|DP|rC|out\(13) & !\CPU|DP|rC|out\(14)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000000000000000000000001111000011110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(14),
	datae => \CPU|DP|rC|ALT_INV_out\(12),
	dataf => \CPU|DP|rC|ALT_INV_out\(15),
	combout => \H3|WideOr4~0_combout\);

-- Location: MLABCELL_X82_Y10_N24
\H3|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr3~0_combout\ = ( \CPU|DP|rC|out\(14) & ( (!\CPU|DP|rC|out\(13) & (!\CPU|DP|rC|out\(15) & !\CPU|DP|rC|out\(12))) # (\CPU|DP|rC|out\(13) & ((\CPU|DP|rC|out\(12)))) ) ) # ( !\CPU|DP|rC|out\(14) & ( (!\CPU|DP|rC|out\(15) & (!\CPU|DP|rC|out\(13) & 
-- \CPU|DP|rC|out\(12))) # (\CPU|DP|rC|out\(15) & (\CPU|DP|rC|out\(13) & !\CPU|DP|rC|out\(12))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001100000011000000110000001100010000011100000111000001110000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(15),
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(12),
	dataf => \CPU|DP|rC|ALT_INV_out\(14),
	combout => \H3|WideOr3~0_combout\);

-- Location: MLABCELL_X82_Y10_N27
\H3|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr2~0_combout\ = (!\CPU|DP|rC|out\(13) & ((!\CPU|DP|rC|out\(14) & ((\CPU|DP|rC|out\(12)))) # (\CPU|DP|rC|out\(14) & (!\CPU|DP|rC|out\(15))))) # (\CPU|DP|rC|out\(13) & (!\CPU|DP|rC|out\(15) & ((\CPU|DP|rC|out\(12)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100011101010000010001110101000001000111010100000100011101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(15),
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(14),
	datad => \CPU|DP|rC|ALT_INV_out\(12),
	combout => \H3|WideOr2~0_combout\);

-- Location: MLABCELL_X82_Y10_N42
\H3|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr1~0_combout\ = ( \CPU|DP|rC|out\(14) & ( (\CPU|DP|rC|out\(12) & (!\CPU|DP|rC|out\(13) $ (!\CPU|DP|rC|out\(15)))) ) ) # ( !\CPU|DP|rC|out\(14) & ( (!\CPU|DP|rC|out\(15) & ((\CPU|DP|rC|out\(12)) # (\CPU|DP|rC|out\(13)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000011110000001100001111000000000000001111000000000000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(15),
	datad => \CPU|DP|rC|ALT_INV_out\(12),
	dataf => \CPU|DP|rC|ALT_INV_out\(14),
	combout => \H3|WideOr1~0_combout\);

-- Location: MLABCELL_X82_Y10_N45
\H3|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr0~0_combout\ = (!\CPU|DP|rC|out\(12) & ((!\CPU|DP|rC|out\(15) $ (!\CPU|DP|rC|out\(14))) # (\CPU|DP|rC|out\(13)))) # (\CPU|DP|rC|out\(12) & ((!\CPU|DP|rC|out\(13) $ (!\CPU|DP|rC|out\(14))) # (\CPU|DP|rC|out\(15))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111101101111101011110110111110101111011011111010111101101111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|rC|ALT_INV_out\(15),
	datab => \CPU|DP|rC|ALT_INV_out\(13),
	datac => \CPU|DP|rC|ALT_INV_out\(14),
	datad => \CPU|DP|rC|ALT_INV_out\(12),
	combout => \H3|WideOr0~0_combout\);

-- Location: FF_X84_Y9_N40
\CPU|DP|status|out[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|U2|Equal0~3_combout\,
	ena => \CPU|FSM|loads~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|out[0]~DUPLICATE_q\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: IOIBUF_X4_Y0_N18
\SW[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X2_Y0_N58
\SW[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: LABCELL_X19_Y25_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


