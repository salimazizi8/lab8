module shifter (in, shift, sout);
    input [15:0] in;
    input [1:0] shift;
    output reg [15:0] sout;
    wire copy_signal;
    reg [15:0] temp_shift;
    assign copy_signal = in[15];

    always @(*) begin
	temp_shift = in>>1;
        case(shift)
            // Not shifting:
            2'b00: sout = in;


            // Shifting left 1-bit(placing 0 in LSB):
            2'b01: sout = in<<1;   


            // Shifting right 1-bit(placing 0 in MSB)
            2'b10: sout = in>>1;


            // shifting right 1-bit, MSB is copy of in[15] :
            2'b11: sout = {copy_signal,temp_shift[14:0]};
           
            
            default: sout = {16{1'bx}};
        endcase

    end
endmodule