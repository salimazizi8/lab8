module cpu(clk, reset, mdata, Z, N, V, m_cmd, mem_addr, halt, out);
  input clk, reset;
  input [15:0] mdata;
  output [15:0] out;
  output N, V, Z;
  wire [15:0] struct_out;
  wire [2:0] opcode;
  wire [1:0] op;
  wire [2:0] decode_out, nsel; 
  //Wires to be used as inputs for datapath.
  wire write, bsel, asel, loada, loadb, loadc, loads;
  wire [1:0] shift, ALUop;
  wire [2:0] readnum, writenum;
  wire [3:0] vsel;
  wire [15:0] sximm8, sximm5;

  //Lab7 signals:
  wire [8:0] next_pc;
  wire [8:0] PC;
  output [8:0] mem_addr;

  wire [8:0] data_address_in;
  wire [8:0] data_address_out;
  //FSM output wires:
  wire [2:0] psel;
  wire load_pc; 
  wire addr_sel;
  wire load_ir;
  wire load_addr;
  output [1:0] m_cmd;

  //Lab8 Additional wires
  wire [8:0] out_pc, PC_added, PC_in;
  wire add_pc;
  wire [2:0] condition;
  output halt;

  `define M_NONE  2'b00
  `define M_READ  2'b01
  `define M_WRITE 2'b10

  
  //Instruction Register:
  register #(16) struct_reg(.clk(clk), .data_in(mdata), .write(load_ir), .out(struct_out)); //Instruction Register:Instantiate the D Flip-flop that works when load is 1.
  //Instruction Decoder:
  structDecoder struct_decoder(struct_out, nsel, opcode, op, readnum, writenum, ALUop, sximm5, sximm8, shift, condition); //The decoder
  
  //Program Counter:
  register #(9) program_counter (next_pc,load_pc,clk,PC);//Program Counter
  //assign next_pc = reset_pc ? 9'b0 : PC + 1; // Program Counter MULTIPLEXER (Lab7)
  
  assign PC_in = add_pc ? (PC + sximm8[8:0]): PC + 1; //Lab8 PC modifications

  assign out_pc = out[8:0]; //out is datapath output (used for function call instructions in Stage 2 of Lab8)

  //Data Register (used for LDR and STR):
  assign data_address_in = out[8:0];
  register #(9) Data_Address (data_address_in,load_addr,clk,data_address_out);

  assign mem_addr= addr_sel ? PC : data_address_out;  //  Memory address fetching MUX

  mux3_1hot #(9) nextPCmux (PC_in,9'b0,out_pc,psel,next_pc); // PC_in, 9'b0 , out_pc are 3 inputs of the MUX fetching into PC


  stateMachine FSM( //Instantiation of the state machine.
  .reset(reset),
  .clk(clk),
  .Z(Z),
  .V(V),
  .N(N),
  .condition(condition),
  .opcode(opcode), 
  .op(op),
  .nsel(nsel),
  .write(write),
  .bsel(bsel),
  .asel(asel),
  .loada(loada),
  .loadb(loadb),
  .loadc(loadc),
  .loads(loads),
  .vsel(vsel),

  //lab7 & 8 signals:
  .load_ir(load_ir),  //The "load instruction" load input of the Instruction Register
  .load_pc(load_pc),
  .psel(psel),
  .add_pc(add_pc),
  .m_cmd(m_cmd),
  .addr_sel(addr_sel),
  .load_addr(load_addr),
  .halt(halt)
  );
  
  
  datapath DP ( //The module that builds upon the different components.
    //.datapath_in(sximm8), //Inputs
    .clk(clk),
    .write(write),
    .bsel(bsel),
    .asel(asel),
    .loada(loada),
    .loadb(loadb),
    .loadc(loadc),
    .loads(loads),
    .shift(shift),
    .ALUop(ALUop),
    .readnum(readnum),
    .writenum(writenum),
    .vsel(vsel),
    .sximm5(sximm5),
    .sximm8(sximm8),
    .mdata(mdata),
    .PC(PC[7:0]),
    .datapath_out(out), //Outputs
    .N(N), 
    .V(V), 
    ._Z(Z)
    );
  

 
  

endmodule

module vDFF1(clk, in, out); //A verilog D Flip-Flop. - Already implemented in lab6_top.
  parameter n = 1;
  input clk;
  input [n-1:0] in;
  output reg [n-1:0] out;

  always @(posedge clk)
    out <= in;
endmodule

module stateMachine(reset, clk, opcode, op, nsel, write, bsel, asel, loada, loadb, loadc, loads, vsel, psel, add_pc, addr_sel, m_cmd, load_ir, load_pc, load_addr, condition, Z, V, N, halt); //Controls values of Datapath based on the current position.
  input  reset, clk, Z, V, N;
  input [2:0] opcode, condition;
  input [1:0] op;
  output [1:0] m_cmd;
  output [2:0] nsel;
  //datapath controls.
  output write, bsel, asel, loada, loadb, loadc, loads, addr_sel, load_ir, load_pc, load_addr;
  output [3:0] vsel; 
  output [2:0] psel;
  output add_pc, halt;

  wire [19:0] p; //So it works with autograder.

  `define mrst 5'b11111 //Move to reset state
  `define rset 5'b00000 //Reset
  `define if01 5'b00001 //IF1
  `define if02 5'b00010 //IF2
  `define upPC 5'b00011 //Update PC.
  `define deco 5'b00100 //decode
  `define getA 5'b00101 //Step 1 of ALU case.
  `define getB 5'b00110 //Step 2 of ALU case
  `define cmbn 5'b00111 //Step 3 of ALU case
  `define wReg 5'b01000 //Write to a register (Step 4 of ALU case)
  `define iReg 5'b01001 //store a register from immediate.
  `define outZ 5'b01010 //Output of comparison function.
  `define mAdr 5'b01011 //Load a register from memory.
  `define lAdr 5'b01100 //Store a register in memory.
  `define lMem 5'b01101 //Loads the output into the load addres register.
  `define wMem 5'b01110
  `define halt 5'b01111 //Halts the program counter and FSM from iterating.
  `define SW 5 //Bit size of switch values.
  //Lab8 states:
  `define gtPC 5'b10101     //gets the value of the counter and stores it in R7.
  `define ldPC 5'b10010     //Loads the counter with the value out of the datapath.

  //FSM controls
  wire [`SW-1:0] present_state, state_next_reset, state_next;
  reg [(`SW+24)-1:0] next;

  vDFF1 #(`SW) STATE(clk, state_next_reset, present_state); //Register stores the next state and outputs the current state whenever when the clock is updated.

  assign halt = (state_next == `halt); //Halt is 1 when the FSM is in the halt state.

  assign state_next_reset = reset ? `mrst : state_next; //If reset then go to the wait state other go to the next state.

  //nsel: 001 = Rn = a, 100 = Rm = b, 010 = Rd = c

  always @(*) begin //Switching through the different cases.
    casex ({present_state, opcode, op, condition, Z, N, V})
      //<present_state,opcode,op,condition,Z,N,V> <state_next, write, bsel, asel, loada, loadb, loadc, loads, vsel, nsel, psel, add_pc, load_pc, addr_sel, load_ir, load_addr, m_cmd>
      {`mrst, 11'bxxx_xx_xxx_xxx}: next = {`rset, 22'b0000000_0000_000_010_01000, `M_NONE};
      {`rset, 11'bxxx_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ}; //reset -> IF1
      {`if01, 11'bxxx_xx_xxx_xxx}: next = {`if02, 22'b0000000_0000_000_001_00110, `M_READ}; //IF1 -> IF2.
      {`if02, 11'bxxx_xx_xxx_xxx}: next = {`upPC, 22'b0000000_0000_000_001_01000, `M_NONE}; //IF2 -> updatePC
      {`upPC, 11'bxxx_xx_xxx_xxx}: next = {`deco, 22'b0000000_0000_000_001_00000, `M_NONE}; //updatePC -> decoder 
      {`deco, 11'b101_xx_xxx_xxx}: next = {`getA, 22'b0001000_0000_001_001_00000, `M_NONE}; //Decoder -> get value for A DFF.
      {`deco, 11'b110_10_xxx_xxx}: next = {`iReg, 22'b1000000_0100_001_001_00000, `M_NONE}; //Decoder -> store immediate value in regiser.
      {`deco, 11'b110_00_xxx_xxx}: next = {`getB, 22'b0000100_0000_100_001_00000, `M_NONE}; //***(MOV Rd,Rm) Decoder -> store value stored in B. (To move to another register.)
      {`deco, 11'b011_00_xxx_xxx}: next = {`getA, 22'b0001000_0000_001_001_00000, `M_NONE}; //Decoder -> stores address of value to be loaded in A.
      {`deco, 11'b111_00_xxx_xxx}: next = {`halt, 22'b0000000_0000_000_001_00000, `M_NONE};
      {`deco, 11'b100_00_xxx_xxx}: next = {`getA, 22'b0001000_0000_001_001_00000, `M_NONE};
      {`deco, 11'b010_10_xxx_xxx}: next = {`gtPC, 22'b1000000_0010_001_001_00000, `M_NONE};
      {`deco, 11'b010_11_xxx_xxx}: next = {`gtPC, 22'b1000000_0010_001_001_00000, `M_NONE};
      {`deco, 11'b010_00_xxx_xxx}: next = {`getB, 22'b0000100_0000_010_001_00000, `M_NONE};
      {`getA, 11'b101_xx_xxx_xxx}: next = {`getB, 22'b0000100_0000_100_001_00000, `M_NONE}; //get value for A -> get value for B DFF.
      {`getA, 11'bxxx_xx_xxx_xxx}: next = {`cmbn, 22'b0100010_0000_000_001_00000, `M_NONE};
      {`getB, 11'b101_01_xxx_xxx}: next = {`outZ, 22'b0000001_0000_000_001_00000, `M_NONE}; //get B -> CMP and wait.
      {`getB, 11'b110_00_xxx_xxx}: next = {`cmbn, 22'b0010010_0001_000_001_00000, `M_NONE}; //getB -> combine with 0 to move value from one register to another. (And possibly shift it.)
      {`getB, 11'b101_xx_xxx_xxx}: next = {`cmbn, 22'b0000010_0001_000_001_00000, `M_NONE}; //get b -> combine with ALU and store in C
      {`getB, 11'b100_xx_xxx_xxx}: next = {`mAdr, 22'b0010010_0000_000_001_00000, `M_NONE}; 
      {`getB, 11'b010_00_xxx_xxx}: next = {`cmbn, 22'b0010010_0000_000_001_00000, `M_NONE};
      {`getB, 11'b010_10_xxx_xxx}: next = {`cmbn, 22'b0010010_0000_000_001_00000, `M_NONE};
      {`cmbn, 11'b011_xx_xxx_xxx}: next = {`lAdr, 22'b0000000_0000_000_001_00001, `M_NONE}; //combine -> load address register with output.
      {`cmbn, 11'b100_xx_xxx_xxx}: next = {`lAdr, 22'b0000000_0000_000_001_00001, `M_NONE}; //combine -> load address register with output.
      {`cmbn, 11'b010_00_xxx_xxx}: next = {`ldPC, 22'b0000000_0000_000_100_01000, `M_NONE};
      {`cmbn, 11'b010_10_xxx_xxx}: next = {`ldPC, 22'b0000000_0000_000_100_01000, `M_NONE};
      {`cmbn, 11'bxxx_xx_xxx_xxx}: next = {`wReg, 22'b1000000_0001_010_001_00000, `M_NONE}; //**come back to this; combine -> write to register
      {`mAdr, 11'b100_00_xxx_xxx}: next = {`wMem, 22'b0000000_0000_000_001_00000, `M_WRITE};// move address to output -> write to address the value of output.
      {`lAdr, 11'b011_xx_xxx_xxx}: next = {`lMem, 22'b0000000_0000_000_001_00000, `M_READ}; //Load address in register for RAM -> read register corresponding to address. 
      {`lAdr, 11'b100_xx_xxx_xxx}: next = {`getB, 22'b0000100_0000_010_001_00000, `M_NONE}; //Write address loaded in register -> get value for address.
      {`lMem, 11'b011_xx_xxx_xxx}: next = {`iReg, 22'b1000000_1000_010_001_00000, `M_READ}; //Take value from register -> load cpu register from RAM address.
      {`wReg, 11'bxxx_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ}; //Write reg -> next instruction
      {`iReg, 11'bxxx_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ}; //Read reg -> next instruction
      //Lab8:
      {`deco, 11'b001_00_000_xxx}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; //Unconditional branch (B)
      {`deco, 11'b001_00_001_1xx}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // BEQ (Z=1)  
      //{`deco, 11'b001_00_001_0xx}: next = {`ldPC, 22'b0000000_0000_000_001_01000, `M_NONE}; // BEQ (Z=0) ; PC = PC+1
      {`deco, 11'b001_00_010_0xx}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // BNE (Z=0)
      {`deco, 11'b001_00_010_1xx}: next = {`ldPC, 22'b0000000_0000_000_001_01000, `M_NONE}; // BNE (Z=1)
      {`deco, 11'b001_00_011_x10}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // BLT (N=1 V=0)
      {`deco, 11'b001_00_011_x01}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // ldPC (N=0 V=1)
      //{`deco, 11'b001_00_011_x11}: next = {`ldPC, 22'b0000000_0000_000_001_01000, `M_NONE}; // ldPC (N=1 V=1) : just pc = pc + 1
      //{`deco, 11'b001_00_011_x00}: next = {`ldPC, 22'b0000000_0000_000_001_01000, `M_NONE}; // BLT (N=0 V=0) : pc = pc + 1
      {`deco, 11'b001_00_100_1xx}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // ldPC (Z=1): pc = pc + 1 + sx(im8)
      {`deco, 11'b001_00_100_x10}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // ldPC (N=1 V=0): pc = pc + 1 + sx(im8)
      {`deco, 11'b001_00_100_x01}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE}; // ldPC (N=0 V=1): pc = pc + 1 + sx(im8)
      //{`deco, 11'b001_00_100_x00}: next = {`ldPC,  22'b0000000_0000_000_001_01000, `M_NONE}; // ldPC (N=0 V=0): pc = pc + 1
      //{`deco, 11'b001_00_100_x11}: next = {`ldPC,  22'b0000000_0000_000_001_01000, `M_NONE}; // ldPC (N=1 V=1): pc = pc + 1
      {`deco, 11'b001_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ};
      {`gtPC, 11'b010_10_xxx_xxx}: next = {`getB, 22'b0000100_0000_010_001_00000, `M_NONE};
      {`ldPC, 11'b010_00_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ};
      {`ldPC, 11'b010_10_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ};
      {`gtPC, 11'b010_11_xxx_xxx}: next = {`ldPC, 22'b0000000_0000_000_001_11000, `M_NONE};
      {`ldPC, 11'b010_11_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ};
      {`ldPC, 11'b001_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ};
      {`outZ, 11'bxxx_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ}; //outZ -> next instruction.
      {`wMem, 11'b100_xx_xxx_xxx}: next = {`if01, 22'b0000000_0000_000_001_00100, `M_READ}; //wReg -> next instruction.
      {`halt, 11'bxxx_xx_xxx_xxx}: next = {`halt, 22'b0000000_0000_000_001_00000, `M_NONE}; //Halt -> Halt until reset.
      default: next = {{`SW{1'bx}}, {24{1'bx}}}; //Base case
    endcase

  end

  assign {state_next, write, bsel, asel, loada, loadb, loadc, loads, vsel, nsel, psel, add_pc, load_pc, addr_sel, load_ir, load_addr, m_cmd} = next; //Assign all outputs based on the value given by the state machine.

endmodule

module structDecoder(instruction, nsel, opcode, op, readnum, writenum, ALUop, sximm5, sximm8, shift, condition); //The decoder based on the numbers inputted that are not reliant on a previous state.
  input [15:0] instruction;
  input [2:0] nsel; 
  output [2:0] opcode, condition, readnum, writenum;

  output [1:0] ALUop, op;
  output [15:0] sximm5;
  output [15:0] sximm8;
  output [1:0] shift;
  wire   [2:0] MUX_output, condition;

  assign ALUop = instruction[12:11]; //ALUop values are bits 11 & 12 of the instruction input.

  // sign extension: checking if MSB is 0 (positive number) or 1 (negative integer), and extends the integer accordingly.
  // sign extending from 5 to 16 bits:
  assign sximm5 = (instruction[4] == 1 ) ? { {11{1'b1}},instruction[4:0] } : { {11{1'b0}}, instruction[4:0] } ;

  // sign extending from 8 to 16 bits:
  assign sximm8 = (instruction[7] == 1 ) ? { {8{1'b1}}, instruction[7:0] } : { {8{1'b0}}, instruction[7:0] } ;


  // this shift will be inputted into our Datapath
  assign shift = (opcode == 3'b100) ? 2'b00 : instruction [4:3]; //If store instruction do NOT shift.

  assign condition = instruction[10:8];

  mux3_1hot nsel_mux(instruction[10:8], instruction[7:5], instruction[2:0], nsel, MUX_output);

  // The output of MUX is assigned to both 'readnum' and 'writenum':
  assign readnum = MUX_output;
  assign writenum= MUX_output;

  assign op = instruction [12:11]; //The op values are the same as ALUop.
  assign opcode = instruction [15:13]; //The opcode are bits 13 - 15 of the instruction input.

endmodule



// 3-input MUX inputted selecting among Rn, Rd, and Rm register specifers:
module mux3_1hot(Rn,Rd,Rm,nsel,out);
        parameter k = 3;
        input [k-1:0] Rn,Rd,Rm; // 3 inputs: Rn, Rd, Rm : each of 3 bits
        input [2:0] nsel; // one-hot selecting signal specifying which register's value to be read
        output [k-1:0] out;
    
        // If and only if both a0 AND'ed with s[0] is TRUE, it will be copied to output(b) .
       assign out=( {k{nsel[0]}} & Rn ) |
                  ( {k{nsel[1]}} & Rd ) |
                  ( {k{nsel[2]}} & Rm ) ;
                 
            // Think of the above ANDing and ORing as logic gate ANDing and ORing; Don't think of them as "TRUE" and "FALSE" manner.
        // We replicate 'k' copies of each bit of the select signal, because we can only bitwise-AND same bit-wide busses (collection of wires).
        // ONLY 1 of the 8 bits of 'select' signal (s) will be ON because it is ONE_HOT code,
        // and it will be replicated 8 times; then bitwise ANDing will happen.
        // Meanwhile, ONLY 1 of the above 8 input combinations will be TRUE.
endmodule