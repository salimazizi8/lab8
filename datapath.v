module datapath(
    //input [15:0] datapath_in
    input [3:0] vsel,
    input [2:0] writenum,
    input write,
    input [2:0] readnum,
    input clk,
    input [1:0] shift,
    input bsel,
    input asel,
    input loada, loadb, loadc, loads,
    input [1:0] ALUop,
    output N,
    output _Z,
    output V,

    output [15:0] datapath_out, 
    input [15:0] sximm8,  // (now 16-bit) sign extended immediate value
    input [15:0] sximm5,
    input [15:0] mdata,
    input [7:0] PC
    //
);

    wire [15:0] data_in, data_out, sout, Ain, Bin, out;
    wire [15:0] rA_out; // pipeline "Register A" output
    wire [15:0] in;     // pipeline "Register B" output
    wire [2:0] status_Z;

    wire [2:0] Z_out;

    assign _Z= Z_out[0];  // Zero flag
    assign  V= Z_out[2];  // overflow flag
    assign  N= Z_out[1];  // negative flag

// ***NOTE: OUTPUT by default is WIRE, if not explicitly mentioned.
    
    

    // The MUX inputted into the REGFILE:
    //module mux_1hot(a3,a2,a1,a0,select,b);
    mux_1hot U3(mdata, sximm8, {8'b0,PC}, datapath_out, vsel, data_in );

    
    // *bubble 1*
    // instantiating Register File (8 registers of 16-bits each):
    regfile REGFILE (data_in, writenum, write, readnum, clk, data_out);

// module register1(data_in, load, clk, out);

    // *bubble 3*
    // This register holds datapath signal A.
    // This pipeline register is used while executing an individual instruction.(temporary storage to do calculation later with their output values)
    // instantiating pipeline Register A:
    register rA (data_out, loada, clk, rA_out );
    

    // *bubble 4*
    // instantiating Register B
    register rB (data_out, loadb, clk, in );

    // *bubble 8*
    // instantiating shifter unit (U1):
    //module shifter (in, shift, sout);
    shifter U1 (in, shift, sout);

// The following 2 Multiplexers are Source Operand Multiplexers.
// They are used in order to change the inputs to the ALU. 
// For some instructions added in Lab 6 through
//  8 these multiplexers are used to set the 16-bit Ain input to the ALU to zero. For other instructions we use
//  them to input an ?immediate operand? (described in Lab 6).
    
    // *bubble 7*
    assign Bin = bsel ? sximm5 : sout ;
    //sximm5 is "sign extended 5-bit immediate".

    // *bubble 6*
    assign Ain = asel ? 16'b0 : rA_out ;


    // *bubble 2*
    // Arithmetic Logic Unit(the piece of hardware that actually "computes" things inside a computer) (ALU) instantiation:
    //module ALU (Ain, Bin, ALUop, out, Z);
    ALU U2 (Ain, Bin, ALUop, out, status_Z);

    
    // *bubble 5*
    // pipeline Register C:
    register rC (out, loadc, clk, datapath_out);
    //The value on output "out" of datapath should be the contents o register C.



    // "An important piece of information we will later need in Lab8 is
    //  knowing if the main 16-bit result of the ALU was exactly zero.
    //  If the result is zero, the status register will be set to 1, otherwise it will be set to 0.
    
    // 3-bit status Register:
    register #(3) status (status_Z, loads, clk, Z_out);    // The register module is parametrized, and we want 3-bit to be stored in this register.
    // The input and output of "status" register is 3-bit in Lab6, as it stores 3-bits--> OVERFLOW, ZERO, NEGATIVE.
endmodule
    



// 4-input MUX inputted into the REGFILE:
module mux_1hot(a3,a2,a1,a0,select,b);
        parameter k = 16;
        input [k-1:0] a3,a2,a1,a0; // 4 inputs into the MUX.
        input [3:0] select; // one-hot 'select' signal specifying which register's value to be read
        output [k-1:0] b; // This will correspond to data_out: the final value that will be outputted through the REGISTER_FILE.
       
        //input and output are both k-bit wide.
        
        // If and only if both a0 AND'ed with s[0] is TRUE, it will be copied to output(b) .
       assign b=( {k{select[0]}} & a0 ) |
                ( {k{select[1]}} & a1 ) |
                ( {k{select[2]}} & a2 ) |
                ( {k{select[3]}} & a3 ) ;
                 
            // Think of the above ANDing and ORing as logic gate ANDing and ORing; Don't think of them as "TRUE" and "FALSE" manner.
        // We replicate 'k' copies of each bit of the select signal, because we can only bitwise-AND same bit-wide busses (collection of wires).
        // ONLY 1 of the 8 bits of 'select' signal (s) will be ON because it is ONE_HOT code,
        // and it will be replicated 8 times; then bitwise ANDing will happen.
        // Meanwhile, ONLY 1 of the above 8 input combinations will be TRUE.
endmodule




