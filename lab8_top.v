module lab8_top(KEY,SW,LEDR,HEX0,HEX1,HEX2,HEX3,HEX4,HEX5,CLOCK_50);
    input [3:0] KEY;
    input [9:0] SW;
    input CLOCK_50;
    output [9:0] LEDR;
    output [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;

    wire [15:0] ir, out;
    wire Z, N, V;
    wire load_addr, addr_sel, load_ir;
    wire [8:0] addr_out;
    wire clk, halt;
    wire tri_enable1;
    wire tri_enable2; // used for 3rd stage for the combination circuit
    wire msel;
    wire [15:0] mdata;
    wire [15:0] read_data; //If the buffer's enable signal is ON, we allow the data read from the memory to be driven to the data-bus that fetches into Instruction Register.
    wire [15:0] write_data;
    reg LED_load;
    reg [15:0] sw_out;

    wire [8:0] mem_addr; //The memory address is 9-bits, but we ONLY use the left 8-bits for READ/WRITE into RAM.
    wire [1:0] m_cmd;
    `define M_NONE  2'b00
    `define M_READ  2'b01
    `define M_WRITE 2'b10

    //RAM wires:
    wire [7:0] read_address,write_address;
    wire write;
    wire [15:0] din,dout;

    assign LEDR[8] = halt; //When the FSM is done LED[8] should turn on.


    assign clk = CLOCK_50; //Assign clk to the on board clock in the DE1-SoC.
  //input clk, reset, s, load;
  //input [15:0] in;
  //output [15:0] out;
  //output N, V, Z, w;
    cpu CPU( //Instantiate the CPU module
        .clk    (clk),
        .reset  (~KEY[1]),
        .mdata   (read_data), //input to the Instruction Register inside the CPU, as well as 1 of the inputs into the Datapath
        .Z      (Z),
        .N      (N),
        .V      (V),
        .m_cmd  (m_cmd),
        .mem_addr(mem_addr),
        .halt   (halt),
        .out    (write_data)
    );

    //D flip flop with load input to store the value of the data address. (Last 9 bits of datapath_out.)
    register #(9) data_address(.clk(clk),.write(load_addr),.data_in(write_data[8:0]), .out(addr_out));

    //RAM instantiation
    RAM #(16,8) MEM (
            .clk(clk),
            .read_address(mem_addr[7:0]),
            .write_address(mem_addr[7:0]),
            .write(write),
            .din(write_data),
            .dout(dout)
    );


    //If RISC Machine tries to read or write to an address from 
  // 256 to 511, we do not want the memory (RAM) to respond, 
  // because memory locations 256 to 511 are allocated for input/output
  // addresses.
  // "tri_enable1" input to the tri-state driver is designed to enable
  // the driver ONLY if the CPU is both trying to read from memory,
  // and the address it is trying to read from is a location inside of the memory block:


  assign msel = mem_addr[8] == 1'b0;
  // 2 comparators to ensure that CPU gets access to memory ONLY if it is trying to read from memory AND 8th bit is 0(specifying we access memory locations 0-255).
  assign tri_enable1 = (m_cmd == `M_READ) & (msel==1);
  // The buffer is disconnected (z) if its enable signal is OFF:

  //Write input of the RAM, which determines if a Write operation should be executed or not:
  assign write     = (m_cmd == `M_WRITE) & (msel==1); 

  // Stage 3 :
  assign tri_enable2 = (m_cmd == `M_READ) & (mem_addr==9'h140);
  
  //Combinational circuit for the input SWITCHES:
  always @(*) begin
    case({tri_enable1,tri_enable2})
      {2'b10} : sw_out = dout;
      {2'b01} : sw_out = SW[7:0];
      default : sw_out = {16{1'bx}};
    endcase
  end

  assign read_data = sw_out;

  //Combinational circuit for the output LED's:
  register #(8) LED_register (write_data[7:0],LED_load,clk,LEDR[7:0]);  //Register to hold the "write_data" and output on LED's at the rising edge of clock.
  always @(*) begin
    if(mem_addr == 9'h100 & m_cmd == `M_WRITE)
      LED_load = 1'b1;
    else LED_load = 1'b0;
  end
  
  assign HEX5[0] = ~Z; //Assign part of HEX5 to Z (zero.)
  assign HEX5[6] = ~N; //Assign part of HEX5 to N (negative.)
  assign HEX5[3] = ~V; //Assign part of HEX5 to V (overflow.)
  
  sseg H0(write_data[3:0],   HEX0);
  sseg H1(write_data[7:4],   HEX1);
  sseg H2(write_data[11:8],  HEX2);
  sseg H3(write_data[15:12], HEX3);
  assign HEX4 = 7'b1111111; //Hex 4 disabled.
  assign {HEX5[2:1],HEX5[5:4]} = 4'b1111; // disabled
  assign LEDR[9] = 1'b0; //LED 9 & 8 disabled.

endmodule

module vDFF(clk,D,Q); //Verilog D Flip-Flop.
  parameter n=1;
  input clk;
  input [n-1:0] D;
  output [n-1:0] Q;
  reg [n-1:0] Q;
  always @(posedge clk)
    Q <= D;
endmodule

//Read and Write memory : (Random Access Memory(RAM)) module:
//Instruction + Data Memory
module RAM (clk,read_address,write_address,write,din,dout);
    parameter data_width = 32; //data width
    parameter addr_width = 4;  //address width
    parameter filename = "data.txt" ; 

    input clk;
    input [addr_width-1:0] read_address,write_address;
    input write;
    input [data_width-1:0] din; //data_in
    output[data_width-1:0] dout;//data_out
    reg   [data_width-1:0] dout;
    
    //NOTE: ( 2^(addr_width) ) gives us the number of memory locations we will have access to inside the Altera's special Memory Blocks.
    reg [data_width-1:0] mem[2**addr_width-1:0];

    //INITIALIZES memory to the contents of the file specified:
    initial $readmemb(filename, mem);

    // Edge-Sensitive always block:
    always @(posedge clk) begin
      if (write)
        //if "write" is ON, data_in will be written onto the memory on next clock cycle.
        mem[write_address] <= din;  //data entering the memory should be written to the write_address(if "write" is ON). read_data delayed 1 clock cycle due to non-blocking assignment
    
      dout <= mem[read_address];
    end
endmodule

`define SS_0 7'b1000000 //Values for all seven segment options.
`define SS_1 7'b1111001
`define SS_2 7'b0100100
`define SS_3 7'b0110000
`define SS_4 7'b0011001
`define SS_5 7'b0010010
`define SS_6 7'b0000010
`define SS_7 7'b1111000
`define SS_8 7'b0000000
`define SS_9 7'b0010000
`define SS_A 7'b0001000
`define SS_B 7'b0000011
`define SS_C 7'b1000110
`define SS_D 7'b0100001
`define SS_E 7'b0000110
`define SS_F 7'b0001110

module sseg(in,segs); //Choose the seven segment value based on 3-bit input.
  input [3:0] in;
  output [6:0] segs;
  reg [6:0] segs;

  always @(*) begin
    case(in)
      0: segs = `SS_0;
      1: segs = `SS_1;
      2: segs = `SS_2;
      3: segs = `SS_3;
      4: segs = `SS_4;
      5: segs = `SS_5;
      6: segs = `SS_6;
      7: segs = `SS_7;
      8: segs = `SS_8;
      9: segs = `SS_9;
      10: segs = `SS_A;
      11: segs = `SS_B;
      12: segs = `SS_C;
      13: segs = `SS_D;
      14: segs = `SS_E;
      15: segs = `SS_F;
      default: segs = 7'b1111111;
    endcase
  end
endmodule

