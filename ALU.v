module ALU (Ain, Bin, ALUop, out, _Z);
    input [15:0] Ain, Bin;
    input [1:0] ALUop;
    wire [15:0] out_ADD_CMP;
    wire sub;
    output [2:0] _Z;
    wire ovf;
    output [15:0] out;
	
	//
	//
	assign sub = (ALUop[0] == 1) ? 1'b1 : 1'b0;  //assigning 1 to sub if ALUop[0] is 1.

    AddSub ALU_AddSub(Ain, Bin, sub, out_ADD_CMP, ovf);

    four_instruc main (Ain,Bin, ALUop, sub, out_ADD_CMP, out );

    assign _Z[0] = (out==16'b0) ? 1'b1 : 1'b0;  // Z status flag: checking if the output is 0.
    assign _Z[1] = (out[15]==1) ? 1'b1 : 1'b0;  // checking if MSB of the 16-bit ALU result is 1 (NEGATIVE flag) 
    assign _Z[2] = (ovf==1 & (ALUop == 2'b00 | ALUop == 2'b01)) ? 1'b1 : 1'b0;  // checking if there is an overflow

endmodule


// Determining if 'sub' signal should be ON or OFF:
module four_instruc(Ain,Bin,ALUop,sub,out_ADD_CMP,out);
	input [15:0] Ain, Bin;
	input [1:0] ALUop;
	input [15:0] out_ADD_CMP;
	input sub;
	//wire sub;
	output reg [15:0] out ;

	// WHY assigning into 'sub' here works but in top module does not work???

	always @(*) begin
		casex ({ALUop,sub}) 
			{2'b00,1'b0}: out = out_ADD_CMP;
		       //ADD instruction
			

			{2'b01,1'b1}: out = out_ADD_CMP;
			// CMP instruction: The only instruction that should update the 3 status bits:
			
			// AND instruction
			{3'b10x}: out = Ain & Bin;

			
			{3'b11x}: out  = ~Bin; 
			
				// MVN instruction

			default: out = {16{1'bx}};
			
		endcase
  	 end
endmodule
		

	// add a+b or subtract a-b (based on whether) 'sub' signal is ON or OFF,
	// also CHECK for Overflow:
module AddSub (a, b, sub, s, ovf);
	parameter n=16;
	input [n-1:0] a;
	input [n-1:0] b;
	input sub;     // subtract if sub=1, otherwise add
	output [n-1:0] s;
	output ovf;
	wire c1, c2;  // carry in and carry out of last 2 bits
		
		// Overflow happens if 'carry in' and 'carry out' are different:
		wire ovf = c1 ^ c2;

		// 2's complement addition :

		// add non-sign bits: add from bit position 0 to bit position n-1
		Adder2 #(n-1) ai( a[n-2:0], b[n-2:0]^{n-1{sub}}, sub, c1, s[n-2:0] );

		// add sign bits:  The sign-bit is ONLY 1-bit, and we want the result to also be 1-bit
		Adder2 #(1)   as( a[n-1], b[n-1]^sub , c1, c2, s[n-1] );
endmodule


	// Multi-bit Adder(behavioural):
/*module Adder1(a, b, cin, cout, s);
	parameter n =8;
	input [n-1:0] a;
	input [n-1:0] b;
	input cin;
	output cout;
	wire cout;
	output [n-1:0] s;
	wire [n-1:0]  s;
		
	assign {cout, s} = a + b + cin ;

endmodule */

module Adder2(a,b,cin,cout,s);
	parameter n = 16;
	input [n-1:0] a,b;
	input cin;
	output [n-1:0] s;
	output cout;

	wire [n-1:0] p = a^b;
	wire [n-1:0] g = a&b;
	wire [n:0] c = {g | ( p & c[n-1:0]), cin };
	wire [n-1:0] s = p ^ c[n-1:0];
	wire cout = c[n];
endmodule





	














